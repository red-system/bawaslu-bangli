<section class="section-sub-banner bg-9 page-header" style="background: url(<?php echo base_url();?>assets/images/banner/<?php echo $imageheader['picture_name'];?>) " title="<?php echo $header['general_data'];?>" alt="<?php echo $header['general_data'];?>">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2><?php echo $header['general_data'];?></h2>
                <p><?php echo $header['general_desc'];?></p>
            </div>
        </div>
    </div>
</section>
<section class="section_page-gallery">
    <div class="container">
        <div class="gallery">
            <div class="gallery-cat text-center">
                <ul class="list-inline">
                    <li><a data-filter=".villa"><?php if( $lang == 'de') { echo $villa['general_sub_data']; } else  if ( $lang == 'en' ) { echo $villa['general_data']; } ?></a></li>
                    <li><a data-filter=".facets"><?php if( $lang == 'de') { echo $facets['general_sub_data']; } else  if ( $lang == 'en' ) { echo $facets['general_data']; } ?></a></li>
                    <li><a data-filter=".additional"><?php if( $lang == 'de') { echo $additional['general_sub_data']; } else  if ( $lang == 'en' ) { echo $additional['general_data']; } ?></a></li>
                </ul>
            </div>
            <div class="gallery-content">
                <div class="row">
                    <div class="gallery-isotope col-4">
                        <div class="item-size "></div>
                        <?php foreach ($imagethevilla as $picvilla):?>
                            <div class="item-isotope  villa">
                                <div class="gallery_item">
                                    <a href="<?php echo base_url();?>assets/images/gallery/<?php echo $picvilla['picture_name'];?>" class="mfp-image">
                                        <img src="<?php echo base_url();?>assets/images/gallery/thumb/<?php echo $picvilla['second_picture'];?>" width="280" height="auto" alt="<?php if( $lang == 'de') { echo $villa['general_sub_data']; } else  if ( $lang == 'en' ) { echo $villa['general_data']; } ?>" title="<?php if( $lang == 'de') { echo $villa['general_sub_data']; } else  if ( $lang == 'en' ) { echo $villa['general_data']; } ?>">
                                    </a>
                                </div>
                            </div>
                        <?php endforeach;?>
                        <?php foreach ($imagefacets as $picfacets):?>
                            <div class="item-isotope  facets">
                                <div class="gallery_item">
                                    <a href="<?php echo base_url();?>assets/images/gallery/<?php echo $picfacets['picture_name'];?>" class="mfp-image">
                                        <img src="<?php echo base_url();?>assets/images/gallery/thumb/<?php echo $picfacets['second_picture'];?>" width="280" height="auto" alt="<?php if( $lang == 'de') { echo $facets['general_sub_data']; } else  if ( $lang == 'en' ) { echo $facets['general_data']; } ?>" title="<?php if( $lang == 'de') { echo $facets['general_sub_data']; } else  if ( $lang == 'en' ) { echo $facets['general_data']; } ?>">
                                    </a>
                                </div>
                            </div>
                        <?php endforeach;?>
                        <?php foreach ($imageadditional as $picadditional):?>
                            <div class="item-isotope  additional">
                                <div class="gallery_item">
                                    <a href="<?php echo base_url();?>assets/images/gallery/<?php echo $picadditional['picture_name'];?>" class="mfp-image">
                                        <img src="<?php echo base_url();?>assets/images/gallery/thumb/<?php echo $picadditional['second_picture'];?>" width="280" height="auto" alt="<?php if( $lang == 'de') { echo $additional['general_sub_data']; } else  if ( $lang == 'en' ) { echo $additional['general_data']; } ?>" title="<?php if( $lang == 'de') { echo $additional['general_sub_data']; } else  if ( $lang == 'en' ) { echo $additional['general_data']; } ?>">
                                    </a>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>       
</section>