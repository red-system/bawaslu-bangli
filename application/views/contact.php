<section class="section-sub-banner bg-9 page-header" style="background: url(<?php echo base_url();?>assets/images/banner/<?php echo $imageheader['picture_name'];?>) " title="<?php echo $header['general_data'];?>" alt="<?php echo $header['general_data'];?>">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2><?php echo $header['general_data'];?></h2>
                <p><?php echo $header['general_desc'];?></p>
            </div>
        </div>
    </div>
</section>

<section class="section-contact section-reservasi" >
    <div class="container">
        <div class="contact">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <h2><?php echo $content['general_data'];?></h2>
                    </div>
                    <div class="text margin-top15" >
                        <?php echo $content['general_desc'];?>
                        <ul class="margin-top0" ">
                            <li><i class="icon lotus-icon-location"></i> <?php echo $address['general_data'];?></li>
                            <li><i class="icon lotus-icon-phone"></i> <a class="color-black" href="tel:<?php echo $phone1['general_data'];?>"><?php echo $phone1['general_data'];?></a></li>
                            <li><i class="icon lotus-icon-phone"></i> <a class="color-black" href="tel:<?php echo $phone2['general_data'];?>"><?php echo $phone2['general_data'];?></a></li>
                            <li><i class="icon fa fa-envelope-o"></i> <a class="color-black" href="mailto:<?php echo $email['general_data'];?>" ><?php echo $email['general_data'];?></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-12 margin-top15" >
                    <div class="text">
                        <h3><strong> <?php echo $content['general_sub_data'];?></strong></h3>
                        <?php echo $content['general_sub_desc'];?>
                    </div>
                </div>
            </div> 
        </div>  
    </div>
</section>
<section class="section-map">
    <h1 class="element-invisible">Map</h1>
    <div class="contact-map" >
        <?php echo $content['general_link'];?>
    </div>
</section>