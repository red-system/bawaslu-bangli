<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li><a href="<?php echo base_url();?>backend_availability/<?php echo $general_name;?>"> <?php echo $title; ?></a></li>
					<li class="active"> Add <?php echo $title; ?></li>
				</ul>
				<!--breadcrumbs end -->
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
                      	Form Add <?php echo $title; ?>
                      	<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
                  	</header>
					<div class="panel-body">
						<?php echo form_open_multipart('backend_availability/reservation_addprocess','class="form-horizontal tasi-form"','method="post"'); ?>
                    		
                    		<div class="form-group">
								<label class="col-sm-2 ">Name <font color=red>*</font></label>
								<div class="col-sm-10">
									<input type="text" class="form-control" placeholder="Enter Name" name="name" required="" >
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 ">Email <font color=red>*</font></label>
								<div class="col-sm-10">
									<input type="email" class="form-control" placeholder="Enter Email" name="email" required="" >
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 ">Phone Number</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" placeholder="Enter Phone Number" name="phone" >
								</div>
							</div>

							<div class="form-group">
	                            <label class="col-sm-2 ">Arrival <font color=red>*</font></label>
	                            <div class="col-sm-4">
	                                <input class="form-control form-control-inline input-medium default-date-picker"  size="16" type="text" value="" name="arrival" required=""/>
                                      <span class="help-block">Select date</span>
	                            </div>
	                            <label class="col-sm-2 ">Departure <font color=red>*</font></label>
	                            <div class="col-sm-4">
	                                <input class="form-control form-control-inline input-medium default-date-picker"  size="16" type="text" value="" name="departure" required=""/>
                                      <span class="help-block">Select date</span>
	                            </div>
	                        </div>

	                        <div class="form-group">
								<label class="col-sm-2 ">Guest <font color=red>*</font></label>
								<div class="col-sm-10">
									<select class="form-control m-bot15" name="guest" required="">
                                      	<option value="1-2">1-2</option>
                                      	<option value="3-4">3-4</option>
                                      	<option value="5-10">5-10</option>
                                  	</select>
								</div>
							</div>

							<div class="form-group">
				                <label class="col-sm-2">Message </label>
				                <div class="col-lg-10">
				                  <textarea class="form-control" name="message" rows="7"></textarea><br>
				                </div>
				            </div>

				            <div class="form-group">
				                <label class="col-sm-2">Status </label>
				                <div class="radios">
				                	<div class="col-sm-4">
		                              	<label class="label_radio" for="radio-01">
		                                  	<input name="status" id="radio-01" value="reserve" type="radio" checked /> Reserve
		                              	</label>
		                              	<label class="label_radio" for="radio-02">
		                                  	<input name="status" id="radio-02" value="booked" type="radio" />Booked
		                             	</label>
		                            </div>
		                            
	                            </div>
							</div>	
                          	
							
							<div class="form-group">
								<div class="col-lg-12">
									<a class="btn btn-shadow btn-white" title="back" href="<?php echo base_url();?>backend_availability/<?php echo $general_name;?>" type="button"><i class="icon-reply"></i> Back</a>
									<button class="btn btn-shadow btn-success pull-right" type="submit" >Save  <i class=" icon-ok"></i></button>
								</div>
							</div>
						</form>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<!--main content start-->

 <script src="<?php echo base_url();?>assets_b/js/form-component.js"></script>



             