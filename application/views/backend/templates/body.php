<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <?php 
                        if($this->session->flashdata('true')){
                    ?>
                    <div class="alert alert-success"> 
                        <?php  echo $this->session->flashdata('true'); ?>
                    </div>
                    <?php    
                        }else if($this->session->flashdata('err')){
                    ?>
                    <div class = "alert alert-success">
                        <?php echo $this->session->flashdata('err'); ?>
                    </div>
                    <?php } ?>
                    
            <aside class="profile-nav col-lg-3">
                <section class="panel">
                    <div class="user-heading round">
                        <a href="#">
                            <img style="width: 112px;height: 112px" src="<?php echo base_url();?>assets/images/<?php echo $profile['image'];?>" alt="">
                        </a>
                        <h1><?php echo $profile['name'];?></h1>
                        <p><?php echo $profile['email'];?></p>
                    </div>
                </section>
            </aside>
            <aside class="profile-info col-lg-9">
                <section>
                    <div class="panel panel-primary">
                        <div class="panel-heading">Sets Profile</div>
                        <div class="panel-body">
                            <?php echo form_open_multipart('backend/profile_updateprocess','class="cmxform form-horizontal tasi-form"','method="get"', 'id="signupForm"'); ?>
                                <div class="form-group">
                                    <label  class="col-lg-2 control-label">Name</label>
                                    <div class="col-lg-10">
                                        <input type="hidden" class="form-control" value="<?php echo $profile['id'];?>" name="id">
                                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $profile['name'];?>" required>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="email" class="control-label col-lg-2">Email</label>
                                    <div class="col-lg-10">
                                        <input class="form-control " id="email2" name="email" type="email" value="<?php echo $profile['email'];?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label col-lg-2" >Change Avatar</label>
                                    <div class="col-md-10">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 112px;height: 112px">
                                                <img src="<?php echo base_url();?>assets/images/<?php echo $profile['image'];?>" alt="" />
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <span class="btn btn-white btn-file">
                                                    <span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
                                                    <span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
                                                    <input name="thumb_image" type="file" class="default"/>
                                                </span>
                                                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label  class="col-lg-2 control-label">Current Password</label>
                                    <div class="col-lg-10">
                                        <input type="password" class="form-control" name="currentpass" id="c-pwd" required>
                                        <p class="help-block">Input your current password before save!!!</p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button type="submit" name="action" class="btn btn-info">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="panel panel-primary">
                        <div class="panel-heading"> Sets New Password</div>
                        <div class="panel-body">
                            <?php echo form_open_multipart('backend/profilepass_updateprocess','class="cmxform form-horizontal tasi-form"','method="get"', 'id="signupForm"'); ?>
                                <div class="form-group ">
                                    <label for="password" class="control-label col-lg-2">New Password</label>
                                    <div class="col-lg-10">
                                        <input type="hidden" class="form-control" value="<?php echo $profile['id'];?>" name="id">
                                        <input class="form-control " id="password" name="password" type="password" value="" required>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="confirm_password" class="control-label col-lg-2">Confirm Password</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" id="confirm_password" name="repass" type="password" required/>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label  class="col-lg-2 control-label">Current Password</label>
                                    <div class="col-lg-10">
                                        <input type="password" class="form-control" name="currentpass" id="c-pwd" required>
                                        <p class="help-block">Input your current password before save!!!</p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button type="submit" name="action" class="btn btn-info">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </aside>
        </div>

        <!-- page end-->
    </section>
</section>
<!--main content end