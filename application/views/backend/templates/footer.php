 <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              <p>Copyright &copy; <?php echo date ('Y');?> <a href="http://samarihillvillas.com" target="_blank" style="color: white;">Samari Hill Villas</a> |  Template by <a href="http://cortechstudio.com" target="_blank" style="color: white;">Cortech Studio</a> | All rights reserved.</p>
              <a href="#" class="go-top">
                  <i class="icon-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets_b/js/jquery.js"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url();?>assets_b/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo base_url();?>assets_b/js/jquery-1.8.3.min.js"></script>
    <script src="<?php echo base_url();?>assets_b/js/bootstrap.min.js"></script>
   
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets_b/assets/advanced-datatable/media/js/jquery.js"></script>
    <script src="<?php echo base_url();?>assets_b/js/bootstrap-switch.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets_b/js/ga.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets_b/assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets_b/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets_b/assets/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url();?>assets_b/assets/dropzone/dropzone.js"></script>
    <script src="<?php echo base_url();?>assets_b/assets/fancybox/source/jquery.fancybox.js"></script>
    <script src="<?php echo base_url();?>assets_b/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url();?>assets_b/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets_b/js/jquery.customSelect.min.js" ></script>
    <script src="<?php echo base_url();?>assets_b/js/respond.min.js" ></script>

    <script class="include" type="text/javascript" src="<?php echo base_url();?>assets_b/js/jquery.dcjqaccordion.2.7.js"></script>

    <script src="<?php echo base_url();?>assets_b/js/modernizr.custom.js"></script>
    <script src="<?php echo base_url();?>assets_b/js/toucheffects.js"></script>
     <script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets_b/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>

    <!--common script for all pages-->
    <script src="<?php echo base_url();?>assets_b/js/common-scripts.js"></script>

    <script src="<?php echo base_url();?>assets_b/js/advanced-form-components.js"></script>
   

    <!--script for this page-->
    

    <script>
        //custom select box

        $(function(){
            $('select.styled').customSelect();
        });

    </script>

    <script type="text/javascript">
      $(function() {
        //    fancybox
          jQuery(".fancybox").fancybox();
      });

    </script>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            $('#example').dataTable( {
                "aaSorting": [[ 8, "asc" ]]
            } );
        } );
    </script>

  </body>
</html>