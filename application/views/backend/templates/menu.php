<?php
$menu = $this->session->menu;

// Informasi Publik
$informasi_publik = array(
    'backend_pengumuman',
    'backend_pengumuman_meta',
    'backend_pengumuman_blog',
    'backend_agenda',
    'backend_agenda_meta',
    'backend_agenda_blog',
    'backend_artikel',
    'backend_artikel_meta',
    'backend_artikel_blog',
    'backend_berita',
    'backend_berita_meta',
    'backend_berita_blog',
);

$informasi_publik_pengumuman = array(
    'backend_pengumuman',
    'backend_pengumuman_meta',
    'backend_pengumuman_blog'
);

$informasi_publik_agenda = array(
    'backend_agenda',
    'backend_agenda_meta',
    'backend_agenda_blog'
);

$informasi_publik_artikel = array(
    'backend_artikel',
    'backend_artikel_meta',
    'backend_artikel_blog'
);

$informasi_publik_berita = array(
    'backend_berita',
    'backend_berita_meta',
    'backend_berita_blog'
);

$profil = array(
    'backend_sejarah',
    'backend_sejarah_meta',
    'backend_sejarah_deskripsi',
    'backend_sekretariat',
    'backend_sekretariat_meta',
    'backend_sekretariat_deskripsi',
    'backend_struktur',
    'backend_struktur_meta',
    'backend_struktur_deskripsi',
    'backend_tentang',
    'backend_tentang_meta',
    'backend_tentang_deskripsi'
);

?>

<!--sidebar start-->
<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <li>
                <a class="<?php if ($this->session->menu == 'backend') {
                    echo('active');
                } ?>" href="<?php echo base_url(); ?>backend">
                    <i class="icon-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <!--                  <li>-->
            <!--                      <a class="-->
            <?php //if($this->session->menu=='backend_menutitle'){echo('active');} ?><!--" href="-->
            <?php //echo base_url();?><!--backend_menu_title">-->
            <!--                          <i class="icon-reorder"></i>-->
            <!--                          <span>Menu & Title Template</span>-->
            <!--                      </a>-->
            <!--                  </li>-->
            <li>
                <a class="<?php if ($this->session->menu == 'backend_headerfooter') {
                    echo('active');
                } ?>" href="<?php echo base_url(); ?>backend_headerfooter">
                    <i class="icon-laptop"></i>
                    <span>Header & Footer</span>
                </a>
            </li>
            <li class="sub-menu">
                <a class="<?php if ($this->session->menu == 'backend_homepicture' || $this->session->menu == 'backend_youtube_link' || $this->session->menu == 'backend_paradise_on_earth' || $this->session->menu == 'backend_service' || $this->session->menu == 'backend_homemeta' || $this->session->menu == 'backend_aboutvilla' || $this->session->menu == 'backend_location' || $this->session->menu == 'backend_generous_surrounding' || $this->session->menu == 'backend_luxurious_furnishing' || $this->session->menu == 'backend_bathrooms' || $this->session->menu == 'backend_perfect_equipped_kitchen' || $this->session->menu == 'backend_heavenly_bedrooms' || $this->session->menu == 'backend_gallery' || $this->session->menu == 'backend_rooftop' || $this->session->menu == 'backend_villa_by_night') {
                    echo('active');
                } ?>" href="javascript:;">
                    <i class="icon-home"></i>
                    <span>Home</span>
                </a>
                <ul class="sub">
                    <li class="<?php if ($this->session->menu == 'backend_homemeta') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_home/metapage">Set Meta Page</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_homepicture') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_home/homepicture">Home Picture</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_youtube_link') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_home/youtube_link">Youtube Link</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_paradise_on_earth') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_home/paradise_on_earth">Paradise On Earth</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_service') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_home/service">Service</a></li>
                    <li class="sub-menu">
                        <a class="<?php if ($this->session->menu == 'backend_aboutvilla' || $this->session->menu == 'backend_location' || $this->session->menu == 'backend_generous_surrounding' || $this->session->menu == 'backend_luxurious_furnishing' || $this->session->menu == 'backend_bathrooms' || $this->session->menu == 'backend_perfect_equipped_kitchen' || $this->session->menu == 'backend_heavenly_bedrooms' || $this->session->menu == 'backend_gallery' || $this->session->menu == 'backend_rooftop' || $this->session->menu == 'backend_villa_by_night') {
                            echo('active');
                        } ?>" href="javascript:;">
                            <span>About</span>
                        </a>
                        <ul class="sub">
                            <li class="<?php if ($this->session->menu == 'backend_aboutvilla') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_thevilla/about_villa">The Villa</a></li>
                            <li class="<?php if ($this->session->menu == 'backend_location') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_thevilla/location">Location</a></li>
                            <li class="<?php if ($this->session->menu == 'backend_generous_surrounding') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_thevilla/generous_surrounding">Generous
                                    Surrounding</a></li>
                            <li class="<?php if ($this->session->menu == 'backend_luxurious_furnishing') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_thevilla/luxurious_furnishing">Luxurious
                                    Furnishing</a></li>
                            <li class="<?php if ($this->session->menu == 'backend_bathrooms') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_thevilla/bathrooms">Bathrooms</a></li>
                            <li class="<?php if ($this->session->menu == 'backend_perfect_equipped_kitchen') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_thevilla/perfect_equipped_kitchen">Perfect
                                    Equipped Kitchen</a></li>
                            <li class="<?php if ($this->session->menu == 'backend_heavenly_bedrooms') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_thevilla/heavenly_bedrooms">Heavenly
                                    Bedrooms</a></li>
                            <li class="<?php if ($this->session->menu == 'backend_gallery') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_thevilla/gallery">Gallery</a></li>
                            <li class="<?php if ($this->session->menu == 'backend_rooftop') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_thevilla/rooftop">Rooftop</a></li>
                            <li class="<?php if ($this->session->menu == 'backend_villa_by_night') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_thevilla/villa_by_night">Villa by Night</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="sub-menu">
                <a class="<?php if ($this->session->menu == 'backend_pageheaderoffers' || $this->session->menu == 'backend_offers1' || $this->session->menu == 'backend_offers2' || $this->session->menu == 'backend_offers3' || $this->session->menu == 'backend_offers4' || $this->session->menu == 'backend_offers5' || $this->session->menu == 'backend_offers6' || $this->session->menu == 'backend_offers7' || $this->session->menu == 'backend_offers8' || $this->session->menu == 'backend_offersmeta') {
                    echo('active');
                } ?>" href="javascript:;">
                    <i class="icon-certificate"></i>
                    <span>Offers</span>
                </a>
                <ul class="sub">
                    <li class="<?php if ($this->session->menu == 'backend_offersmeta') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_offers/metapage">Set Meta Page</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_pageheaderoffers') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_offers/header_offers">Page Header</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_offers1') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_offers/offers_first">Offers First</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_offers2') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_offers/offers_second">Offers Second</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_offers3') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_offers/offers_third">Offers Third</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_offers4') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_offers/offers_fourth">Offers Fourth</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_offers5') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_offers/offers_fifth">Offers Fifth</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_offers6') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_offers/offers_sixth">Offers Sixth</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_offers7') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_offers/offers_seventh">Offers Seventh</a></li>
                </ul>
            </li>

            <li class="sub-menu">
                <a class="<?php if ($this->session->menu == 'backend_pageheadergallery' || $this->session->menu == 'backend_gallery1' || $this->session->menu == 'backend_gallery2' || $this->session->menu == 'backend_gallerythevilla' || $this->session->menu == 'backend_galleryfacets' || $this->session->menu == 'backend_galleryadditional' || $this->session->menu == 'backend_gallerymeta') {
                    echo('active');
                } ?>" href="javascript:;">
                    <i class="icon-folder-open"></i>
                    <span>Gallery</span>
                </a>
                <ul class="sub">
                    <li class="<?php if ($this->session->menu == 'backend_gallerymeta') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_gallery/metapage">Set Meta Page</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_pageheadergallery') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_gallery/header_gallery">Page Header</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_gallerythevilla') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_gallery/the_villa">The Villa</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_galleryfacets') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_gallery/facets">Facets</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_galleryadditional') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_gallery/additional">Additional</a></li>
                </ul>
            </li>

            <li class="sub-menu">
                <a class="<?php if ($this->session->menu == 'backend_pageheaderavailability' || $this->session->menu == 'backend_availabilitymeta' || $this->session->menu == 'backend_pricelist' || $this->session->menu == 'backend_menulist' || $this->session->menu == 'backend_reservation' || $this->session->menu == 'backend_formreservation' || $this->session->menu == 'backend_pageheaderguestbook' || $this->session->menu == 'backend_guestbookabout' || $this->session->menu == 'backend_guestbookform' || $this->session->menu == 'backend_guestbooklist' || $this->session->menu == 'backend_guestbookmeta' || $this->session->menu == 'backend_pageheadercontact' || $this->session->menu == 'backend_content' || $this->session->menu == 'backend_contactmeta') {
                    echo('active');
                } ?>" href="javascript:;">
                    <i class="icon-info"></i>
                    <span>Info</span>
                </a>
                <ul class="sub">
                    <li class="sub-menu">
                        <a class="<?php if ($this->session->menu == 'backend_availabilitymeta' || $this->session->menu == 'backend_pageheaderavailability') {
                            echo('active');
                        } ?>" href="javascript:;">
                            <span>Availability</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php if ($this->session->menu == 'backend_availabilitymeta') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_availability/metapage">Set Meta Page</a>
                            </li>
                            <li class="<?php if ($this->session->menu == 'backend_pageheaderavailability') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_availability/header_availability">Page
                                    Header</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a class="<?php if ($this->session->menu == 'backend_pricelist' || $this->session->menu == 'backend_menulist') {
                            echo('active');
                        } ?>" href="javascript:;">
                            <span>Pricing</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php if ($this->session->menu == 'backend_pricelist') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_pricing/pricelist">Pricelist</a></li>
                            <li class="<?php if ($this->session->menu == 'backend_menulist') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_pricing/menulist">Menu List</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a class="<?php if ($this->session->menu == 'backend_reservation' || $this->session->menu == 'backend_formreservation') {
                            echo('active');
                        } ?>" href="javascript:;">
                            <span>Reservation</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php if ($this->session->menu == 'backend_reservation') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_availability/reservation">Reservation</a>
                            </li>
                            <li class="<?php if ($this->session->menu == 'backend_formreservation') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_availability/reservation_form">Reservation
                                    Form</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a class="<?php if ($this->session->menu == 'backend_pageheaderguestbook' || $this->session->menu == 'backend_guestbookabout' || $this->session->menu == 'backend_guestbookform' || $this->session->menu == 'backend_guestbooklist' || $this->session->menu == 'backend_guestbookmeta') {
                            echo('active');
                        } ?>" href="javascript:;">
                            <span>Guestbook</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php if ($this->session->menu == 'backend_guestbookmeta') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_guestbook/metapage">Set Meta Page</a></li>
                            <li class="<?php if ($this->session->menu == 'backend_pageheaderguestbook') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_guestbook/header_guestbook">Page Header</a>
                            </li>
                            <li class="<?php if ($this->session->menu == 'backend_guestbookabout') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_guestbook/about_guestbook">About
                                    Guestbook</a></li>
                            <li class="<?php if ($this->session->menu == 'backend_guestbookform') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_guestbook/guestbook_form">Guestbook Form</a>
                            </li>
                            <li class="<?php if ($this->session->menu == 'backend_guestbooklist') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_guestbook/guestbook_list">Guestbook List</a>
                            </li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a class="<?php if ($this->session->menu == 'backend_pageheadercontact' || $this->session->menu == 'backend_content' || $this->session->menu == 'backend_contactmeta') {
                            echo('active');
                        } ?>" href="javascript:;">
                            <span>Contact</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php if ($this->session->menu == 'backend_contactmeta') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_contact/metapage">Set Meta Page</a></li>
                            <li class="<?php if ($this->session->menu == 'backend_pageheadercontact') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_contact/header_contact">Page Header</a></li>
                            <li class="<?php if ($this->session->menu == 'backend_content') {
                                echo('active');
                            } ?>"><a href="<?php echo base_url(); ?>backend_contact/content">Content</a></li>

                        </ul>
                    </li>
                </ul>
            </li>

            <li class="sub-menu">
                <a class="<?php if ($this->session->menu == 'backend_pageheadergetting_there' || $this->session->menu == 'backend_getting_theremeta' || $this->session->menu == 'backend_getting_there') {
                    echo('active');
                } ?>" href="javascript:;">
                    <i class="icon-road"></i>
                    <span>Getting There</span>
                </a>
                <ul class="sub">
                    <li class="<?php if ($this->session->menu == 'backend_getting_theremeta') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_getting_there/metapage">Set Meta Page</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_pageheadergetting_there') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_getting_there/header_getting_there">Page Header</a>
                    </li>
                    <li class="<?php if ($this->session->menu == 'backend_getting_there') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_getting_there/getting_there">Getting There</a></li>
                </ul>
            </li>

            <li class="sub-menu">
                <a class="<?php if ($this->session->menu == 'backend_pageheaderdream' || $this->session->menu == 'backend_dreammeta' || $this->session->menu == 'backend_dream' || $this->session->menu == 'backend_dream_gallery') {
                    echo('active');
                } ?>" href="javascript:;">
                    <i class="icon-magic"></i>
                    <span>A Dream Comes True</span>
                </a>
                <ul class="sub">
                    <li class="<?php if ($this->session->menu == 'backend_dreammeta') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_dream/metapage">Set Meta Page</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_pageheaderdream') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_dream/header_dream">Page Header</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_dream') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_dream/dream">A Dream Comes True</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_dream_gallery') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_dream/dream_gallery">Dream Gallery</a></li>
                </ul>
            </li>
            <li class="sub-menu">
                <a class="<?php if ($this->session->menu == 'backend_pageheaderprivacy_policy' || $this->session->menu == 'backend_privacy_policymeta' || $this->session->menu == 'backend_privacy_policy') {
                    echo('active');
                } ?>" href="javascript:;">
                    <i class="icon-warning-sign"></i>
                    <span>Privacy Policy</span>
                </a>
                <ul class="sub">
                    <li class="<?php if ($this->session->menu == 'backend_privacy_policymeta') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_privacy_policy/metapage">Set Meta Page</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_pageheaderprivacy_policy') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_privacy_policy/header_privacy_policy">Page
                            Header</a></li>
                    <li class="<?php if ($this->session->menu == 'backend_privacy_policy') {
                        echo('active');
                    } ?>"><a href="<?php echo base_url(); ?>backend_privacy_policy/privacy_policy">Privacy Policy</a>
                    </li>
                </ul>
            </li>


            <li class="sub-menu">
                <a class="<?php echo in_array($menu, $profil) ? 'active' : '' ?>" href="javascript:;">
                    <i class="icon-info"></i>
                    <span>Profil</span>
                </a>
                <ul class="sub">
                    <li class="sub-menu">
                        <a class="<?php echo in_array($menu, array('backend_sejarah_meta','backend_sejarah_deskripsi')) ? 'active':'' ?>" href="javascript:;">
                            <span>Sejarah</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php echo $menu == 'backend_sejarah_meta' ? 'active':'' ?>">
                                <a href="<?php echo base_url(); ?>backend_sejarah/metapage">
                                    Set Meta Page
                                </a>
                            </li>
                            <li class="<?php echo $menu == 'backend_sejarah_deskripsi' ? 'active':'' ?>">
                                <a href="<?php echo base_url(); ?>backend_sejarah/deskripsi">
                                    Edit Deskripsi
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a class="<?php echo in_array($menu, array('backend_sekretariat_meta','backend_sekretariat_deskripsi')) ? 'active':'' ?>" href="javascript:;">
                            <span>Sekretariat</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php echo $menu == 'backend_sekretariat_meta' ? 'active':'' ?>">
                                <a href="<?php echo base_url(); ?>backend_sekretariat/metapage">
                                    Set Meta Page
                                </a>
                            </li>
                            <li class="<?php echo $menu == 'backend_sekretariat_deskripsi' ? 'active':'' ?>">
                                <a href="<?php echo base_url(); ?>backend_sekretariat/deskripsi">
                                    Edit Deskripsi
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a class="<?php echo in_array($menu, array('backend_struktur_meta','backend_struktur_deskripsi')) ? 'active':'' ?>" href="javascript:;">
                            <span>Struktur</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php echo $menu == 'backend_struktur_meta' ? 'active':'' ?>">
                                <a href="<?php echo base_url(); ?>backend_struktur/metapage">
                                    Set Meta Page
                                </a>
                            </li>
                            <li class="<?php echo $menu == 'backend_struktur_deskripsi' ? 'active':'' ?>">
                                <a href="<?php echo base_url(); ?>backend_struktur/deskripsi">
                                    Edit Deskripsi
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a class="<?php echo in_array($menu, array('backend_tentang_meta','backend_tentang_deskripsi')) ? 'active':'' ?>" href="javascript:;">
                            <span>Tentang</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php echo $menu == 'backend_tentang_meta' ? 'active':'' ?>">
                                <a href="<?php echo base_url(); ?>backend_tentang/metapage">
                                    Set Meta Page
                                </a>
                            </li>
                            <li class="<?php echo $menu == 'backend_tentang_deskripsi' ? 'active':'' ?>">
                                <a href="<?php echo base_url(); ?>backend_tentang/deskripsi">
                                    Edit Deskripsi
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>

            <li class="sub-menu">
                <a class="<?php echo in_array($menu, array('backend_publikasi_meta','backend_publikasi')) ? 'active' : '' ?>" href="javascript:;">
                    <i class="icon-warning-sign"></i>
                    <span>Publikasi</span>
                </a>
                <ul class="sub">
                    <li class="<?php $menu == 'backend_publikasi_meta' ? 'active' : ''; ?>">
                        <a href="<?php echo base_url(); ?>backend_publikasi/metapage">Set Meta Page</a>
                    </li>
                    <li class="<?php $menu == 'backend_publikasi' ? 'active' : ''; ?>">
                        <a href="<?php echo base_url(); ?>backend_blog/daftar/publikasi/0">Daftar Publikasi</a>
                    </li>
                </ul>
            </li>

            <li class="sub-menu">
                <a class="<?php echo in_array($menu, $informasi_publik) ? 'active' : '' ?>" href="javascript:;">
                    <i class="icon-info"></i>
                    <span>Informasi Publik</span>
                </a>
                <ul class="sub">
                    <li class="sub-menu">
                        <a class="<?php echo in_array($menu, $informasi_publik_pengumuman) ? 'active':'' ?>" href="javascript:;">
                            <span>Pengumuman</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php echo $menu == 'informasi_publik_pengumuman_meta' ?>">
                                <a href="<?php echo base_url(); ?>backend_pengumuman/metapage">
                                    Set Meta Page
                                </a>
                            </li>
                            <li class="<?php echo $menu == 'informasi_publik_pengumuman_blog' ?>">
                                <a href="<?php echo base_url(); ?>backend_blog/daftar/pengumuman/0">
                                    Daftar Pengumuman
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a class="<?php echo in_array($menu, $informasi_publik_agenda) ? 'active':'' ?>" href="javascript:;">
                            <span>Agenda</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php echo $menu == 'informasi_publik_agenda_meta' ?>">
                                <a href="<?php echo base_url(); ?>backend_agenda/metapage">
                                    Set Meta Page
                                </a>
                            </li>
                            <li class="<?php echo $menu == 'informasi_publik_agenda_blog' ?>">
                                <a href="<?php echo base_url(); ?>backend_blog/daftar/agenda/0">
                                    Daftar Agenda
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a class="<?php echo in_array($menu, $informasi_publik_artikel) ? 'active':'' ?>" href="javascript:;">
                            <span>Artikel</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php echo $menu == 'informasi_publik_artikel_meta' ?>">
                                <a href="<?php echo base_url(); ?>backend_artikel/metapage">
                                    Set Meta Page
                                </a>
                            </li>
                            <li class="<?php echo $menu == 'informasi_publik_artikel_blog' ?>">
                                <a href="<?php echo base_url(); ?>backend_blog/daftar/artikel/0">
                                    Daftar Artikel
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a class="<?php echo in_array($menu, $informasi_publik_berita) ? 'active':'' ?>" href="javascript:;">
                            <span>Berita</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php echo $menu == 'informasi_publik_berita_meta' ?>">
                                <a href="<?php echo base_url(); ?>backend_berita/metapage">
                                    Set Meta Page
                                </a>
                            </li>
                            <li class="<?php echo $menu == 'informasi_publik_berita_blog' ?>">
                                <a href="<?php echo base_url(); ?>backend_blog/daftar/berita/0">
                                    Daftar Berita
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>


            <li class="sub-menu">
                <a class="<?php echo in_array($menu, array('backend_kecamatan_meta','backend_kecamatan_daftar','backend_kecamatan')) ? 'active': ''; ?>" href="javascript:;">
                    <i class="icon-anchor"></i>
                    <span>Bawaslu Kecamatan</span>
                </a>
                <ul class="sub">
                    <li class="<?php echo $menu == 'backend_kecamatan_meta' ? 'active' : '';  ?>">
                        <a href="<?php echo base_url(); ?>backend_kecamatan/metapage">Set Meta Page</a>
                    </li>
                    <li class="<?php echo in_array($menu, array('backend_kecamatan_daftar')) ? 'active' : '';?>">
                        <a href="<?php echo base_url(); ?>backend_kecamatan/daftar">Daftar Kecamatan</a>
                    </li>
                </ul>
            </li>

        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->
>>>>>>> application/views/backend/templates/menu.php
