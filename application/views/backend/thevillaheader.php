<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
          <div class="col-lg-12">
              <!--breadcrumbs start -->
              <ul class="breadcrumb">
                  <li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li class="active">Backend <?php echo $title;?></li>
              </ul>
              <!--breadcrumbs end -->
          </div>
        </div>
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Form Backend <?php echo $title;?>
                        <span class="tools pull-right">
                          <a href="javascript:;" class="icon-chevron-down"></a> 
                        </span>
                    </header>
                    <div class="panel-body">
                      <?php 
                         if($this->session->flashdata('true')){
                       ?>
                         <div class="alert alert-success"> 
                           <?php  echo $this->session->flashdata('true'); ?>
                          </div>
                      <?php    
                      }else if($this->session->flashdata('err')){
                      ?>
                       <div class = "alert alert-success">
                         <?php echo $this->session->flashdata('err'); ?>
                       </div>
                      <?php } ?>
                      <div class="adv-table">
                        <div class="adv-table">
                        <div class="space15"></div> <br>
                        <table  class="display table table-bordered table-striped" id="example">
                          <thead>
                          <tr>
                            <th width="25%">Name</th>
                            <th width="25%">Discription</th>
                            <th width="20%">Language</th>
                            <th width="10%">Edit</th>
                          </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($thevillas as $thevilla) :  ?>
                              <tr class="gradeX">
                                  <td><?php echo $thevilla['general_data'];?></td>
                                  <td><?php echo word_limiter($thevilla['general_desc'],20); ?></td>
                                  <td><?php if( $thevilla['general_lang'] == 'de') { echo 'German'; } else  if ( $thevilla['general_lang'] == 'en' ) { echo 'English'; } ?></td>
                                  <td class="text-center"><a class="btn btn-round btn-primary" title="view & edit" href="<?php echo site_url('backend_thevilla/'.$link.'/'.$thevilla['general_id']); ?>" type="button"><i class="icon-pencil"></i></a></td>
                              </tr>
                            <?php endforeach; ?>
                          </tbody>
                          <tfoot>
                          <tr>
                            <th width="25%">Name</th>
                            <th width="25%">Discription</th>
                            <th width="20%">Language</th>
                            <th width="10%">Edit</th>
                          </tr>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
        
    </section>
</section>
<!--main content end-->