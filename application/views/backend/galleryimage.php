<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
          <div class="col-lg-12">
              <!--breadcrumbs start -->
              <ul class="breadcrumb">
                  <li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li class="active">Backend <?php echo $title;?></li>
              </ul>
              <!--breadcrumbs end -->
          </div>
        </div>
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Form Backend <?php echo $title;?>
                        <span class="tools pull-right">
                          <a href="javascript:;" class="icon-chevron-down"></a> 
                        </span>
                    </header>
                    <div class="panel-body">
                    <?php 
                       if($this->session->flashdata('true')){
                     ?>
                       <div class="alert alert-success"> 
                         <?php  echo $this->session->flashdata('true'); ?>
                        </div>
                    <?php    
                    }else if($this->session->flashdata('err')){
                    ?>
                     <div class = "alert alert-success">
                       <?php echo $this->session->flashdata('err'); ?>
                     </div>
                    <?php } ?>
                    <?php echo form_open_multipart('backend_gallery/gallery_editprocess','class="form-horizontal tasi-form"','method="post"'); ?>
                        <div class="form-group">
                          <label class="col-sm-2 ">Title (English)</label>
                          <div class="col-sm-4">
                              <input type="hidden" class="form-control" placeholder="Enter Postion" name="general_id" value="<?php echo $gallery['general_id'];?>" />
                              <input type="text" class="form-control" placeholder="Enter Postion" name="titleen" value="<?php echo $gallery['general_data'];?>" />
                          </div>
                          <label class="col-sm-2 ">Title (German)</label>
                          <div class="col-sm-4">
                              <input type="text" class="form-control" placeholder="Enter Postion" name="titlede" value="<?php echo $gallery['general_sub_data'];?>" />
                          </div>
                      </div>

                      
                      <div class="form-group">
                        <div class="col-lg-12">
                          <button class="btn btn-shadow btn-primary pull-right" type="submit" name="action">Update
                          <i class=" icon-repeat"></i></button>
                        </div>
                      </div>
                    </form>
                  </div>
                      
                </section>
            </div>
        </div>
        <!-- page end-->
        <div class="row">
      <div class="col-lg-12">
        <section class="panel">
                <header class="panel-heading">
                    <strong>Gallery</strong>
                  <span class="tools pull-right">
                      <a href="javascript:;" class="icon-chevron-down"></a> 
                  </span>
                </header>
                <div class="panel-body">
                    <ul class="grid cs-style-3">
                        <?php foreach ($imagegallery as $pic):?>
                          <li>
                              <figure >
                                  <img  src="<?php echo base_url();?>assets/images/gallery/<?php echo $pic['picture_name'];?>" alt="" style="width: 351px; height: auto;">
                                  
                                  <figcaption style="width: 351px; height: auto;">
                                      <a class="btn btn-danger btn-block" href="<?php echo base_url();?>backend_gallery/image_delete/<?php echo $pic['general_id'];?>" onclick="return confirm('Are you sure to delete this picture?')" type="button"><i class="icon-trash"></i> Delete image</a> 
                                  </figcaption>
                              </figure>
                          </li>
                        <?php endforeach;?>
                    </ul>
                    <a type="button" class="btn btn-round btn-default btn-block" href="<?php echo base_url();?>backend_gallery/<?php echo $link;?>"><i class="icon-plus-sign"> </i> Add Picture </a>
                </div>
            </section>
            </div>
        </div>
    </section>
</section>
<!--main content end-->