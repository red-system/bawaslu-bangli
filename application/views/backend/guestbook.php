<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
          <div class="col-lg-12">
              <!--breadcrumbs start -->
              <ul class="breadcrumb">
                  <li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li class="active">Backend <?php echo $title;?></li>
              </ul>
              <!--breadcrumbs end -->
          </div>
        </div>
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Form Backend <?php echo $title;?>
                        <span class="tools pull-right">
                          <a href="javascript:;" class="icon-chevron-down"></a> 
                        </span>
                    </header>
                    <div class="panel-body">
                      <?php 
                         if($this->session->flashdata('true')){
                       ?>
                         <div class="alert alert-success"> 
                           <?php  echo $this->session->flashdata('true'); ?>
                          </div>
                      <?php    
                      }else if($this->session->flashdata('err')){
                      ?>
                       <div class = "alert alert-success">
                         <?php echo $this->session->flashdata('err'); ?>
                       </div>
                      <?php } ?>
                      <div class="adv-table">
                        <div class="adv-table">
                        <div class="space15"></div> <br>
                        <table  class="display table table-bordered table-striped" id="example">
                          <thead>
                          <tr>
                            <th width="10%">Name</th>
                            <th width="10%">Email</th>
                            <th width="10%">Locations</th>
                            <th width="15%">Testimonial Title</th>
                            <th width="20%">Testimonial</th>
                            <th width="10%">Photo</th>
                            <th width="10%">Publish?</th>
                            <th width="5%">Edit</th>
                            <th width="5%">Delete</th>
                          </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($guestbooks as $guestbook) :  ?>
                              <tr class="gradeX">
                                  <td><?php echo $guestbook['general_data'];?></td>
                                  <td><?php echo $guestbook['general_sub_data'];?></td>
                                  <td><?php echo $guestbook['general_link'];?></td>
                                  <td><?php echo $guestbook['general_desc'];?></td>
                                  <td><?php echo word_limiter($guestbook['general_sub_desc'],20); ?></td>
                                  <td class="text-center"><img src="<?php echo base_url();?>assets/images/guestbook/<?php echo $guestbook['main_image'];?>" style="width:50px;height: 50px" /></td>
                                  <td><div class="switch switch-square"
                                          data-on-label="<i class=' icon-ok'></i>"
                                          data-off-label="<i class='icon-remove'></i>">
                                          <input type="checkbox"  <?php if ($guestbook['general_additional_info']== 'checked') echo ('checked') ?> />
                                      </div></td>
                                  <td class="text-center"><a class="btn btn-round btn-primary" title="view & edit" href="<?php echo site_url('backend_guestbook/'.$link.'/'.$guestbook['general_id']); ?>" type="button"><i class="icon-pencil"></i></a></td>
                                  <td class="text-center"><a class="btn btn-round btn-danger" title="delete" href="<?php echo site_url('backend_guestbook/'.$link1.'/'.$guestbook['general_id']); ?>" onclick="return confirm('Are you sure to delete?')" type="button"><i class="icon-trash"></i></a></td>
                              </tr>
                            <?php endforeach; ?>
                          </tbody>
                          <tfoot>
                          <tr>
                            <th width="17%">Name</th>
                            <th width="13%">Email</th>
                            <th width="10%">Locations</th>
                            <th width="15%">Testimonial Title</th>
                            <th width="20%">Testimonial</th>
                            <th width="10%">Photo</th>
                            <th width="10%">Publish?</th>
                            <th width="5%">Edit</th>
                            <th width="5%">Delete</th>
                          </tr>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
        
    </section>
</section>
<!--main content end-->