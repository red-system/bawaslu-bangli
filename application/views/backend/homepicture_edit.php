<section id="main-content">
	<section class="wrapper">
		<div class="row">
	        <div class="col-lg-12">
	            <!--breadcrumbs start -->
	            <ul class="breadcrumb">
	                <li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li><a href="<?php echo base_url();?>backend_home/<?php echo $link;?>"> <?php echo $title; ?></a></li>
	                <li class="active">Image Slider</li>
	            </ul>
	            <!--breadcrumbs end -->
	        </div>
	      </div>
	    <div class="row">
	        <div class="col-lg-12">
	            <section class="panel">
					<header class="panel-heading">
						Form Backend Home Picture
						<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
					</header>
					<div class="panel-body">
						<?php 
						   if($this->session->flashdata('true')){
						 ?>
						   <div class="alert alert-success"> 
						     <?php  echo $this->session->flashdata('true'); ?>
						    </div>
						<?php    
						}else if($this->session->flashdata('err')){
						?>
						 <div class = "alert alert-success">
						   <?php echo $this->session->flashdata('err'); ?>
						 </div>
						<?php } ?>
						<?php echo form_open_multipart('backend_home/homepicture_update/'.$slider['general_id'],'class="form-horizontal tasi-form"'); ?>
							
	                        <div class="form-group">
								<label class="col-sm-2" >Image</label>
								<div class="col-md-10">
									<div class="fileupload fileupload-new" data-provides="fileupload">
										<div class="fileupload-new thumbnail" style="width: 210px; height: auto;">
											<img src="<?php echo base_url() ?>assets/images/slider/<?php echo $slider['main_image'] ?>" style="width: 210px; height: auto;" alt="" />
										</div>
										<div class="fileupload-preview fileupload-exists thumbnail" style="width: 210px; height: auto;"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
												<span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
												<input name="picture" type="file" class="default" />
											</span>
											<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
										</div>
									</div>
								</div>
							</div>
	                        <div class="form-group">
								<label class="col-sm-2">Line 1  </label>
								<div class="col-sm-10">
									<input type="text" name="line1" class="form-control" placeholder="Enter Line 1" value="<?php echo $slider['general_data'] ?>">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2">Line 2 </label>
								<div class="col-sm-10">
									<input type="text" name="line2" class="form-control" placeholder="Enter Line 2" value="<?php echo $slider['general_sub_data'] ?>">
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-12">
									<a class="btn btn-shadow btn-white" title="back" href="<?php echo base_url();?>backend_home/<?php echo $link;?>" type="button"><i class="icon-reply"></i> Back</a>
									<button class="btn btn-shadow btn-primary pull-right" type="submit" name="action">Update
									<i class=" icon-repeat"></i></button>
								</div>
							</div>
	                      
						</form>
					</div>
				</section>

	        </div>
	    </div>
	</section>
</section>