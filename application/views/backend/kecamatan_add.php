<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li class="active">Backend <?php echo $title; ?> Meta Tag</li>
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Form Backend <?php echo $title; ?> Meta Tag
                        <span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
                    </header>
                    <div class="panel-body">
                        <?php
                        if ($this->session->flashdata('true')) {
                            ?>
                            <div class="alert alert-success">
                                <?php echo $this->session->flashdata('true'); ?>
                            </div>
                            <?php
                        } else if ($this->session->flashdata('err')) {
                            ?>
                            <div class="alert alert-success">
                                <?php echo $this->session->flashdata('err'); ?>
                            </div>
                        <?php } ?>
                        <?php echo form_open_multipart('backend_kecamatan/insert', 'class="form-horizontal tasi-form"'); ?>
                        <div class="form-group">
                            <label class="col-sm-2">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="district_name" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2">Description</label>
                            <div class="col-sm-10">
                                <textarea class="form-control ckeditor" id="editor1" name="district_description"
                                          required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2">Meta Title</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="meta_title" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2">Meta Description</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="meta_description" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2">Meta Keywords</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="meta_keywords" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-12">
                                <button class="btn btn-shadow btn-primary pull-right" type="submit">Insert Data
                                    <i class=" icon-repeat"></i></button>
                            </div>
                        </div>

                        </form>
                    </div>
                </section>

            </div>
        </div>
    </section>
</section>