<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/<?php echo $logofooter['main_image'];?>">

    <title>Administrator - Samari Hill Villas</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets_b/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets_b/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url();?>assets_b/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets_b/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets_b/css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

  <body class="login-body">

    <div class="container">
        <?php echo form_open(site_url().'user_auth/reset_password/'.$remember_token); ?>
            <div class="modal-dialog">
                <?php 
                   if($this->session->flashdata('true')){
                 ?>
                   <div class="alert alert-success"> 
                     <?php  echo $this->session->flashdata('true'); ?>
                    </div>
                <?php    
                }else if($this->session->flashdata('err')){
                ?>
                 <div class = "alert alert-success">
                   <?php echo $this->session->flashdata('err'); ?>
                 </div>
                <?php } ?>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Reset Password ?</h4>
                    </div>
                    <div class="modal-body">
                        <input type="password" name="password" placeholder="New Password" class="form-control placeholder-no-fix" value="<?php echo set_value('password'); ?>"/>   
                        <p> <?php echo form_error('password','<p class="alert alert-danger center">','</p>'); ?> </p>   
                        <input type="password" name="passconf" placeholder="Confirm Password" class="form-control placeholder-no-fix" value="<?php echo set_value('passconf'); ?>"/>   
                        <p> <?php echo form_error('passconf','<p class="alert alert-danger center">','</p>'); ?> </p>     

                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-default" title="cancel" href="<?php echo base_url();?>user_auth" type="button">Cancel</a>
                        <button class="btn btn-success" type="submit">Submit</button>
                    </div>

                </div>
            </div>
        <?php echo form_close(); ?>  
    </div>



    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets_b/js/jquery.js"></script>
    <script src="<?php echo base_url();?>assets_b/js/bootstrap.min.js"></script>


  </body>
</html>
