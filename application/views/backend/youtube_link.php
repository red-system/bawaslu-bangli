<section id="main-content">
	<section class="wrapper">
		<div class="row">
	        <div class="col-lg-12">
	            <!--breadcrumbs start -->
	            <ul class="breadcrumb">
	                <li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li class="active">Youtube Link</li>
	            </ul>
	            <!--breadcrumbs end -->
	        </div>
	      </div>
	    <div class="row">
	        <div class="col-lg-12">
	            <section class="panel">
					<header class="panel-heading">
						Form Backend Youtube Link
						<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
					</header>
					<div class="panel-body">
						<?php 
						   if($this->session->flashdata('true')){
						 ?>
						   <div class="alert alert-success"> 
						     <?php  echo $this->session->flashdata('true'); ?>
						    </div>
						<?php    
						}else if($this->session->flashdata('err')){
						?>
						 <div class = "alert alert-success">
						   <?php echo $this->session->flashdata('err'); ?>
						 </div>
						<?php } ?>
						<?php echo form_open_multipart('backend_home/update_home','class="form-horizontal tasi-form"'); ?>
							
	                        <div class="form-group">
								<label class="col-sm-2">Link</label>
								<div class="col-lg-10">
									<input type="hidden" class="form-control" placeholder="" name="general_id" value="<?php echo $youtube['general_id'];?>" />
									<textarea class="form-control" name="description" rows="4"><?php echo $youtube['general_desc'];?></textarea>
									<span class="help-block">Change the width to 100% ,height to 450 , and src to data-src for maximum results</span>
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-12">
									<button class="btn btn-shadow btn-primary pull-right" type="submit" name="action">Update
									<i class=" icon-repeat"></i></button>
								</div>
							</div>
	                      
						</form>
					</div>
				</section>

	        </div>
	    </div>
	    
	</section>
</section>