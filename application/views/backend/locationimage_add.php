<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="row">
			<div class="col-lg-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li><a href="<?php echo base_url();?>backend_location/<?php echo $general_name;?>"> <?php echo $title; ?></a></li>
	                <li class="active">Add Image</li>
				</ul>
				<!--breadcrumbs end -->
			</div>
		</div>
      <!-- page start-->
      	<section class="panel">
      		<div class="row">
				<div class="col-lg-12">
		         	<header class="panel-heading">
		              	Dropzone File Image Upload
		          	</header>
		          	
			          	<div class="panel-body">
			          		<form action="<?php echo base_url();?>backend_location/image_addprocess" class="dropzone" id="my-awesome-dropzone" enctype="multipart/form-data">
			          			<input type="hidden" class="form-control" name="general_name" value="<?php echo $general_name;?>" >
			          		</form>
			          		
			          	</div>
			          	<div class="form-group">
							<div class="col-lg-12">
								<a class="btn btn-shadow btn-white" title="back" href="<?php echo base_url();?>backend_location/<?php echo $general_name;?>" type="button"><i class="icon-reply"></i> Back</a>
								<a class="btn btn-info btn-default pull-right" title="view" href="<?php echo base_url();?>backend_location/<?php echo $general_name;?>_image_add" type="button"><i class="icon-repeat"></i> Refresh</a>
							</div>
						</div>
			          	
				</div>
			</div>
      	</section>
      <!-- page end-->
  	</section>
</section>

