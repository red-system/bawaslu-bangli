<section id="main-content">
	<section class="wrapper">
		<div class="row">
	        <div class="col-lg-12">
	            <!--breadcrumbs start -->
	            <ul class="breadcrumb">
	                <li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li class="active">Backend Menu &amp; Title</li>
	            </ul>
	            <!--breadcrumbs end -->
	        </div>
	      </div>
	    <div class="row">
	        <div class="col-lg-12">
	            <section class="panel">
					<header class="panel-heading">
						Form Backend Menu &amp; Title
						<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
					</header>
					<div class="panel-body">
						<?php 
						   if($this->session->flashdata('true')){
						 ?>
						   <div class="alert alert-success"> 
						     <?php  echo $this->session->flashdata('true'); ?>
						    </div>
						<?php    
						}else if($this->session->flashdata('err')){
						?>
						 <div class = "alert alert-success">
						   <?php echo $this->session->flashdata('err'); ?>
						 </div>
						<?php } ?>
						<?php echo form_open_multipart('backend_menu_title/update_menutitle','class="form-horizontal tasi-form"'); ?>
							<div class="form-group">
								<label class="col-sm-2">Template Name</label>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="groundplanen" value="<?php echo $groundplanen['title'];?>" />
								</div>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="groundplande" value="<?php echo $groundplande['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="homeen" value="<?php echo $homeen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="homede" value="<?php echo $homede['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="paradiseen" value="<?php echo $paradiseen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="paradisede" value="<?php echo $paradisede['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="serviceen" value="<?php echo $serviceen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="servicede" value="<?php echo $servicede['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="abouten" value="<?php echo $abouten['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="aboutde" value="<?php echo $aboutde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="thevillaen" value="<?php echo $thevillaen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="thevillade" value="<?php echo $thevillade['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="locationen" value="<?php echo $locationen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="locationde" value="<?php echo $locationde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="generousen" value="<?php echo $generousen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="generousde" value="<?php echo $generousde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="luxuriousen" value="<?php echo $luxuriousen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="luxuriousde" value="<?php echo $luxuriousde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="bathroomsen" value="<?php echo $bathroomsen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="bathroomsde" value="<?php echo $bathroomsde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="kitchenen" value="<?php echo $kitchenen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="kitchende" value="<?php echo $kitchende['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="bedroomsen" value="<?php echo $bedroomsen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="bedroomsde" value="<?php echo $bedroomsde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="rooftopen" value="<?php echo $rooftopen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="rooftopde" value="<?php echo $rooftopde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="offersen" value="<?php echo $offersen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="offersde" value="<?php echo $offersde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="galleryen" value="<?php echo $galleryen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="galleryde" value="<?php echo $galleryde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="infoen" value="<?php echo $infoen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="infode" value="<?php echo $infode['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="availabilityen" value="<?php echo $availabilityen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="availabilityde" value="<?php echo $availabilityde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="guestbooken" value="<?php echo $guestbooken['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="guestbookde" value="<?php echo $guestbookde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="pricingen" value="<?php echo $pricingen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="pricingde" value="<?php echo $pricingde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="reservationen" value="<?php echo $reservationen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="reservationde" value="<?php echo $reservationde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="contacten" value="<?php echo $contacten['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="contactde" value="<?php echo $contactde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="gettingen" value="<?php echo $gettingen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="gettingde" value="<?php echo $gettingde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="dreamen" value="<?php echo $dreamen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="dreamde" value="<?php echo $dreamde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="privacyen" value="<?php echo $privacyen['title'];?>" />
								</div>
								<div class="col-sm-6">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="privacyde" value="<?php echo $privacyde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-12">
									<button class="btn btn-shadow btn-primary pull-right" type="submit" name="action">Update
									<i class=" icon-repeat"></i></button>
								</div>
							</div>
	                      
						</form>
					</div>
				</section>

	        </div>
	    </div>
	</section>
</section>