<section id="main-content">
	<section class="wrapper">
		<div class="row">
	        <div class="col-lg-12">
	            <!--breadcrumbs start -->
	            <ul class="breadcrumb">
	                <li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li><a href="<?php echo base_url();?>backend_home/our_best"> Our Best</a></li>
	                <li class="active"><?php echo $ourbest['general_data'];?> <?php if( $ourbest['general_lang'] == 'de') { echo 'German'; } else  if ( $ourbest['general_lang'] == 'en' ) { echo 'English'; } ?></li>
	            </ul>
	            <!--breadcrumbs end -->
	        </div>
	      </div>
	    <div class="row">
	        <div class="col-lg-12">
	            <section class="panel">
					<header class="panel-heading">
						Form Backend <?php echo $ourbest['general_data'];?> <?php if( $ourbest['general_lang'] == 'de') { echo 'German'; } else  if ( $ourbest['general_lang'] == 'en' ) { echo 'English'; } ?>
						<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
					</header>
					<div class="panel-body">
						<?php 
						   if($this->session->flashdata('true')){
						 ?>
						   <div class="alert alert-success"> 
						     <?php  echo $this->session->flashdata('true'); ?>
						    </div>
						<?php    
						}else if($this->session->flashdata('err')){
						?>
						 <div class = "alert alert-success">
						   <?php echo $this->session->flashdata('err'); ?>
						 </div>
						<?php } ?>
						<?php echo form_open_multipart('backend_home/update_ourbest','class="form-horizontal tasi-form"'); ?>
							
	                        <div class="form-group"> 
	                            <label class="col-sm-2 ">Name</label>
	                            <div class="col-sm-9"> 
	                            	<input type="hidden" class="form-control" placeholder="" name="general_id" value="<?php echo $ourbest['general_id'];?>" />
	                                <input type="text" class="form-control" placeholder="Enter Name" name="name" value="<?php echo $ourbest['general_data'];?>" />
	                            </div>
	                        </div>

	                        <div class="form-group">
								<label class="col-sm-2">Description</label>
								<div class="col-lg-10">
									<textarea class="form-control ckeditor" id="editor1" name="description"><?php echo $ourbest['general_desc'];?></textarea><br>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2" >Picture</label>
								<div class="col-md-10">
									<div class="fileupload fileupload-new" data-provides="fileupload">
										<div class="fileupload-new thumbnail" style="width: 210px; height: 112px;">
											<img src="<?php echo base_url();?>assets/images/home/<?php echo $image['picture_name'];?>" style="width: 210px; height: 112px;" alt="" />

										</div>
										<input type="hidden" class="form-control" placeholder="" name="id" value="<?php echo $image['general_id'];?>" />
										<div class="fileupload-preview fileupload-exists thumbnail" style="width: 210px; height: 112px;"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
												<span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
												<input name="image" type="file" class="default" />

											</span>
											<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
										</div>
									</div>
								</div>
							</div>

	                        <div class="form-group"> 
	                            <label class="col-sm-2 ">Feature 1</label>
	                            <div class="col-sm-5"> 
	                            	<input type="hidden" class="form-control" placeholder="" name="id1" value="<?php echo $feature1['general_id'];?>" />
	                                <input type="text" class="form-control" placeholder="Enter Name" name="feature1" value="<?php echo $feature1['general_data'];?>" />
	                            </div>

	                            <label class="col-sm-1" >Icon</label>
								<div class="col-md-4">
									<div class="fileupload fileupload-new" data-provides="fileupload">
										<div class="fileupload-new thumbnail" style="width: 48px; height: 48px;">
											<img src="<?php echo base_url();?>assets/images/home/<?php echo $featureimg1['picture_name'];?>" style="width: 48px; height: 48px;" alt="" />
										</div>
										<div class="fileupload-preview fileupload-exists thumbnail" style="width: 48px; height: 48px;"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
												<span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
												<input name="icon1" type="file" class="default" />
											</span>
											<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
										</div>
									</div>
								</div>
	                        </div>

	                        <div class="form-group"> 
	                            <label class="col-sm-2 ">Feature 2</label>
	                            <div class="col-sm-5"> 
	                            	<input type="hidden" class="form-control" placeholder="" name="id2" value="<?php echo $feature2['general_id'];?>" />
	                                <input type="text" class="form-control" placeholder="Enter Name" name="feature2" value="<?php echo $feature2['general_data'];?>" />
	                            </div>

	                            <label class="col-sm-1" >Icon</label>
								<div class="col-md-4">
									<div class="fileupload fileupload-new" data-provides="fileupload">
										<div class="fileupload-new thumbnail" style="width: 48px; height: 48px;">
											<img src="<?php echo base_url();?>assets/images/home/<?php echo $featureimg2['picture_name'];?>" style="width: 48px; height: 48px;" alt="" />
										</div>
										<div class="fileupload-preview fileupload-exists thumbnail" style="width: 48px; height: 48px;"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
												<span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
												<input name="icon2" type="file" class="default" />
											</span>
											<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
										</div>
									</div>
								</div>
	                        </div>

	                        <div class="form-group"> 
	                            <label class="col-sm-2 ">Feature 3</label>
	                            <div class="col-sm-5"> 
	                            	<input type="hidden" class="form-control" placeholder="" name="id3" value="<?php echo $feature3['general_id'];?>" />
	                                <input type="text" class="form-control" placeholder="Enter Name" name="feature3" value="<?php echo $feature3['general_data'];?>" />
	                            </div>

	                            <label class="col-sm-1" >Icon</label>
								<div class="col-md-4">
									<div class="fileupload fileupload-new" data-provides="fileupload">
										<div class="fileupload-new thumbnail" style="width: 48px; height: 48px;">
											<img src="<?php echo base_url();?>assets/images/home/<?php echo $featureimg3['picture_name'];?>" style="width: 48px; height: 48px;" alt="" />
										</div>
										<div class="fileupload-preview fileupload-exists thumbnail" style="width: 48px; height: 48px;"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
												<span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
												<input name="icon3" type="file" class="default" />
											</span>
											<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
										</div>
									</div>
								</div>
	                        </div>

	                        <div class="form-group"> 
	                            <label class="col-sm-2 ">Feature 4</label>
	                            <div class="col-sm-5"> 
	                            	<input type="hidden" class="form-control" placeholder="" name="id4" value="<?php echo $feature4['general_id'];?>" />
	                                <input type="text" class="form-control" placeholder="Enter Name" name="feature4" value="<?php echo $feature4['general_data'];?>" />
	                            </div>

	                            <label class="col-sm-1" >Icon</label>
								<div class="col-md-4">
									<div class="fileupload fileupload-new" data-provides="fileupload">
										<div class="fileupload-new thumbnail" style="width: 48px; height: 48px;">
											<img src="<?php echo base_url();?>assets/images/home/<?php echo $featureimg4['picture_name'];?>" style="width: 48px; height: 48px;" alt="" />
										</div>
										<div class="fileupload-preview fileupload-exists thumbnail" style="width: 48px; height: 48px;"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
												<span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
												<input name="icon4" type="file" class="default" />
											</span>
											<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
										</div>
									</div>
								</div>
	                        </div>

	                        <div class="form-group"> 
	                            <label class="col-sm-2 ">Feature 5</label>
	                            <div class="col-sm-5"> 
	                            	<input type="hidden" class="form-control" placeholder="" name="id5" value="<?php echo $feature5['general_id'];?>" />
	                                <input type="text" class="form-control" placeholder="Enter Name" name="feature5" value="<?php echo $feature5['general_data'];?>" />
	                            </div>

	                            <label class="col-sm-1" >Icon</label>
								<div class="col-md-4">
									<div class="fileupload fileupload-new" data-provides="fileupload">
										<div class="fileupload-new thumbnail" style="width: 48px; height: 48px;">
											<img src="<?php echo base_url();?>assets/images/home/<?php echo $featureimg5['picture_name'];?>" style="width: 48px; height: 48px;" alt="" />
										</div>
										<div class="fileupload-preview fileupload-exists thumbnail" style="width: 48px; height: 48px;"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
												<span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
												<input name="icon5" type="file" class="default" />
											</span>
											<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
										</div>
									</div>
								</div>
	                        </div>

	                        <div class="form-group"> 
	                            <label class="col-sm-2 ">Feature 6</label>
	                            <div class="col-sm-5"> 
	                            	<input type="hidden" class="form-control" placeholder="" name="id6" value="<?php echo $feature6['general_id'];?>" />
	                                <input type="text" class="form-control" placeholder="Enter Name" name="feature6" value="<?php echo $feature6['general_data'];?>" />
	                            </div>

	                            <label class="col-sm-1" >Icon</label>
								<div class="col-md-4">
									<div class="fileupload fileupload-new" data-provides="fileupload">
										<div class="fileupload-new thumbnail" style="width: 48px; height: 48px;">
											<img src="<?php echo base_url();?>assets/images/home/<?php echo $featureimg6['picture_name'];?>" style="width: 48px; height: 48px;" alt="" />
										</div>
										<div class="fileupload-preview fileupload-exists thumbnail" style="width: 48px; height: 48px;"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
												<span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
												<input name="icon6" type="file" class="default" />
											</span>
											<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
										</div>
									</div>
								</div>
	                        </div>
		                    
							<div class="form-group">
								<div class="col-lg-12">
									<a class="btn btn-shadow btn-white" title="back" href="<?php echo base_url();?>backend_home/our_best" type="button"><i class="icon-reply"></i> Back</a>
									<button class="btn btn-shadow btn-primary pull-right" type="submit" name="action">Update
									<i class=" icon-repeat"></i></button>
								</div>
							</div>
	                      
						</form>
					</div>
				</section>

	        </div>
	    </div>
	</section>
</section>