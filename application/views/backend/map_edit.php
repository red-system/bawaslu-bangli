<section id="main-content">
	<section class="wrapper">
		<div class="row">
	        <div class="col-lg-12">
	            <!--breadcrumbs start -->
	            <ul class="breadcrumb">
	                <li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li><a href="<?php echo base_url();?>backend_location/<?php echo $general_name;?>"> <?php echo $title;?></a></li>
	                <li class="active"><?php echo $location['general_data'];?> <?php if( $location['general_lang'] == 'de') { echo 'German'; } else  if ( $location['general_lang'] == 'en' ) { echo 'English'; } ?></li>
	            </ul>
	            <!--breadcrumbs end -->
	        </div>
	      </div>
	    <div class="row">
	        <div class="col-lg-12">
	            <section class="panel">
					<header class="panel-heading">
						Form Backend <?php echo $location['general_data'];?> <?php if( $location['general_lang'] == 'de') { echo 'German'; } else  if ( $location['general_lang'] == 'en' ) { echo 'English'; } ?>
						<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
					</header>
					<div class="panel-body">
						<?php 
						   if($this->session->flashdata('true')){
						 ?>
						   <div class="alert alert-success"> 
						     <?php  echo $this->session->flashdata('true'); ?>
						    </div>
						<?php    
						}else if($this->session->flashdata('err')){
						?>
						 <div class = "alert alert-success">
						   <?php echo $this->session->flashdata('err'); ?>
						 </div>
						<?php } ?>
						<?php echo form_open_multipart('backend_location/update_map','class="form-horizontal tasi-form"'); ?>
							
	                        <div class="form-group"> 
	                            <label class="col-sm-2 ">Title</label>
	                            <div class="col-sm-9"> 
	                            	<input type="hidden" class="form-control" placeholder="" name="general_id" value="<?php echo $location['general_id'];?>" />
	                                <input type="text" class="form-control" placeholder="Enter Name" name="name" value="<?php echo $location['general_data'];?>" />
	                            </div>
	                        </div>

	                        <div class="form-group">
								<label class="col-sm-2">Description</label>
								<div class="col-lg-10">
									<textarea class="form-control ckeditor" id="editor1" name="description"><?php echo $location['general_desc'];?></textarea><br>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2" >Map Picture</label>
								<div class="col-md-10">
									<div class="fileupload fileupload-new" data-provides="fileupload">
										<div class="fileupload-new thumbnail" style="width: 210px; height: 112px;">
											<img src="<?php echo base_url();?>assets/images/<?php echo $image['picture_name'];?>" style="width: 210px; height: 112px;" alt="" />

										</div>
										<input type="hidden" class="form-control" placeholder="" name="id" value="<?php echo $image['general_id'];?>" />
										<div class="fileupload-preview fileupload-exists thumbnail" style="width: 210px; height: 112px;"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
												<span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
												<input name="image" type="file" class="default" />

											</span>
											<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
										</div>
									</div>
								</div>
							</div>



	                        <div class="form-group">
								<div class="col-lg-12">
									<a class="btn btn-shadow btn-white" title="back" href="<?php echo base_url();?>backend_location/<?php echo $general_name;?>" type="button"><i class="icon-reply"></i> Back</a>
									<button class="btn btn-shadow btn-primary pull-right" type="submit" name="action">Update
									<i class=" icon-repeat"></i></button>
								</div>
							</div>
	                      
						</form>
					</div>
				</section>

	        </div>
	    </div>
	    
	</section>
</section>

<script type="text/javascript">
    var map;
    var markers = [];

    function initialize() {
        var mylatlong = {lat: <?php echo $map['latitude'];?>, lng: <?php echo $map['longitude'];?>};

        map = new google.maps.Map(document.getElementById('map_canvas'), {
          zoom: 15,                        // Set the zoom level manually
          center: mylatlong,
          mapTypeId:  google.maps.MapTypeId.ROADMAP
        });

        var marker = new google.maps.Marker({
          position: mylatlong,
          map: map,
        });

        // This event listener will call addMarker() when the map is clicked.
        map.addListener('click', function(event) {
          if (markers.length >= 1) {
              deleteMarkers();
          }

          addMarker(event.latLng);
          document.getElementById('lat').value = event.latLng.lat();
          document.getElementById('long').value =  event.latLng.lng();
        });
    }

    // Adds a marker to the map and push to the array.
    function addMarker(location) {
        var marker = new google.maps.Marker({
          position: location,
          map: map
        });
        markers.push(marker);
    }

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }
</script>
