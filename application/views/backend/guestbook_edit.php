<section id="main-content">
	<section class="wrapper">
		<div class="row">
	        <div class="col-lg-12">
	            <!--breadcrumbs start -->
	            <ul class="breadcrumb">
	                <li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li><a href="<?php echo base_url();?>backend_guestbook/<?php echo $general_name;?>"> <?php echo $title;?></a></li>
	                <li class="active"><?php echo $guestbook['general_data'];?> </li>
	            </ul>
	            <!--breadcrumbs end -->
	        </div>
	      </div>
	    <div class="row">
	        <div class="col-lg-12">
	            <section class="panel">
					<header class="panel-heading">
						Form Backend <?php echo $guestbook['general_data'];?>
						<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
					</header>
					<div class="panel-body">
						<?php 
						   if($this->session->flashdata('true')){
						 ?>
						   <div class="alert alert-success"> 
						     <?php  echo $this->session->flashdata('true'); ?>
						    </div>
						<?php    
						}else if($this->session->flashdata('err')){
						?>
						 <div class = "alert alert-success">
						   <?php echo $this->session->flashdata('err'); ?>
						 </div>
						<?php } ?>
						<?php echo form_open_multipart('backend_guestbook/update_guestbooklist','class="form-horizontal tasi-form"'); ?>
							
	                        <div class="form-group"> 
	                            <label class="col-sm-2 ">Name</label>
	                            <div class="col-sm-9"> 
	                            	<input type="hidden" class="form-control" placeholder="" name="general_id" value="<?php echo $guestbook['general_id'];?>" />
	                                <input type="text" class="form-control" placeholder="Enter Name" name="name" value="<?php echo $guestbook['general_data'];?>" readonly/>
	                            </div>
	                        </div>

	                        <div class="form-group"> 
	                            <label class="col-sm-2 ">Email</label>
	                            <div class="col-sm-9"> 
	                            	<input type="email" class="form-control" placeholder="Enter Email" name="email" value="<?php echo $guestbook['general_sub_data'];?>" readonly/>
	                            </div>
	                        </div>

	                        <div class="form-group"> 
	                            <label class="col-sm-2 ">Location</label>
	                            <div class="col-sm-9"> 
	                            	<input type="text" class="form-control" placeholder="Enter Location" name="location" value="<?php echo $guestbook['general_link'];?>" readonly/>
	                            </div>
	                        </div>

	                        <div class="form-group"> 
	                            <label class="col-sm-2 ">Testimonial Title</label>
	                            <div class="col-sm-9"> 
	                            	<input type="text" class="form-control" placeholder="Enter Location" name="location" value="<?php echo $guestbook['general_desc'];?>" readonly/>
	                            </div>
	                        </div>

	                        <div class="form-group">
								<label class="col-sm-2">Testimonial</label>
								<div class="col-lg-10">
									<textarea readonly="" rows="5" class="form-control" name="testimonial"><?php echo $guestbook['general_sub_desc'];?> </textarea><br>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2" >Picture</label>
								<div class="col-md-10">
									<div class="fileupload fileupload-new" data-provides="fileupload">
										<div class="fileupload-new thumbnail" style="width: 200px; height: 200px;">
											<img src="<?php echo base_url();?>assets/images/guestbook/<?php echo $guestbook['main_image'];?>" style="width: 200px; height: 200px;" alt="" />
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2" >Publish ?</label>
								<div class="col-md-10">
									<div class="switch switch-square"
                                        data-on-label="<i class=' icon-ok'></i>"
                                        data-off-label="<i class='icon-remove'></i>">
                                        <input type="checkbox" name="published" value="checked"  <?php if ($guestbook['general_additional_info']== 'checked') echo ('checked') ?> />
                                    </div>
								</div>
							</div>

	                        <div class="form-group">
								<div class="col-lg-12">
									<a class="btn btn-shadow btn-white" title="back" href="<?php echo base_url();?>backend_guestbook/<?php echo $general_name;?>" type="button"><i class="icon-reply"></i> Back</a>
									<button class="btn btn-shadow btn-primary pull-right" type="submit" name="action">Update
									<i class=" icon-repeat"></i></button>
								</div>
							</div>
	                      
						</form>
					</div>
				</section>

	        </div>
	    </div>
	    
	</section>
</section>