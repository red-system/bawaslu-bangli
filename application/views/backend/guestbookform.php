<section id="main-content">
	<section class="wrapper">
		<div class="row">
	        <div class="col-lg-12">
	            <!--breadcrumbs start -->
	            <ul class="breadcrumb">
	                <li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li class="active">Backend Guest Book Form</li>
	            </ul>
	            <!--breadcrumbs end -->
	        </div>
	      </div>
	    <div class="row">
	        <div class="col-lg-12">
	            <section class="panel">
					<header class="panel-heading">
						Form Backend Guest Book Form
						<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
					</header>
					<div class="panel-body">
						<?php 
						   if($this->session->flashdata('true')){
						 ?>
						   <div class="alert alert-success"> 
						     <?php  echo $this->session->flashdata('true'); ?>
						    </div>
						<?php    
						}else if($this->session->flashdata('err')){
						?>
						 <div class = "alert alert-success">
						   <?php echo $this->session->flashdata('err'); ?>
						 </div>
						<?php } ?>
						<?php echo form_open_multipart('backend_guestbook/update_headerfooter','class="form-horizontal tasi-form"'); ?>
							<div class="form-group">
								<label class="col-sm-2">Address</label>
								<div class="col-sm-10">
									<textarea class="form-control" name="address" placeholder="Enter Address..." ><?php echo $address['general_data'];?></textarea>
								</div>
							</div>

							<div class="form-group">
	                            <label class="col-sm-2 ">Phone</label>
	                            <div class="col-sm-10"> 
	                                <input type="text" class="form-control" placeholder="Enter Phone Number" name="phone" value="<?php echo $phone['general_data'];?>" />
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label class="col-sm-2 ">Email</label>
	                            <div class="col-sm-10"> 
	                                <input type="email" class="form-control" placeholder="Enter Email" name="email" value="<?php echo $email['general_data'];?>" />
	                            </div>
	                        </div>

	                        <div class="form-group">
								<label class="col-sm-2">Quote</label>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">English</label>
									<textarea class="form-control" name="quoteen" placeholder="Enter Quote..." ><?php echo $quoteen['general_desc'];?></textarea>
								</div>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">German</label>
									<textarea class="form-control" name="quotede" placeholder="Enter Quote..." ><?php echo $quotede['general_desc'];?></textarea>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2">Button</label>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">English</label>
									<textarea class="form-control" name="buttonen" placeholder="Enter Button..." ><?php echo $buttonen['general_data'];?></textarea>
								</div>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">German</label>
									<textarea class="form-control" name="buttonde" placeholder="Enter Button..." ><?php echo $buttonde['general_data'];?></textarea>
								</div>
							</div>

	                        <div class="form-group">
								<label class="col-sm-2" >Logo Header</label>
								<div class="col-md-10">
									<div class="fileupload fileupload-new" data-provides="fileupload">
										<div class="fileupload-new thumbnail" style="width: 190px; height: 92px;">
											<img src="<?php echo base_url();?>assets/images/<?php echo $logoheader['main_image'];?>" style="width: 190px; height: 92px;" alt="" />
										</div>
										<div class="fileupload-preview fileupload-exists thumbnail" style="width: 190px; height: 92px;"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
												<span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
												<input name="logoheader" type="file" class="default" />
											</span>
											<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2" >Logo Footer</label>
								<div class="col-md-10">
									<div class="fileupload fileupload-new" data-provides="fileupload">
										<div class="fileupload-new thumbnail" style="width: 145px; height: 149px;">
											<img src="<?php echo base_url();?>assets/images/<?php echo $logofooter['main_image'];?>" style="width: 145px; height: 149px;" alt="" />
										</div>
										<div class="fileupload-preview fileupload-exists thumbnail" style="width: 145px; height: 149px;"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
												<span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
												<input name="logofooter" type="file" class="default" />
											</span>
											<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
										</div>
									</div>
								</div>
							</div>

	                        <div class="form-group">
								<label class="col-sm-2" ><?php echo $facebook['social_name'];?></label>
								<div class="col-lg-10">
									<input type="text" name="facebook" class="form-control" placeholder="Enter Facebook Link" value="<?php echo $facebook['social_link'];?>"><br>
									<div class="col-lg-10">
										<label class="col-sm-2 pull">Publish ?</label>
										<div class="switch switch-square"
	                                        data-on-label="<i class=' icon-ok'></i>"
	                                        data-off-label="<i class='icon-remove'></i>">
	                                        <input type="checkbox" name="publishedfacebook" value="checked"  <?php if ($facebook['published']== 'checked') echo ('checked') ?> />
	                                    </div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2" ><?php echo $instagram['social_name'];?></label>
								<div class="col-lg-10">
									<input type="text" name="instagram" class="form-control" placeholder="Enter Instagram Link" value="<?php echo $instagram['social_link'];?>"><br>
									<div class="col-lg-10">
										<label class="col-sm-2 pull">Publish ?</label>
										<div class="switch switch-square"
	                                        data-on-label="<i class=' icon-ok'></i>"
	                                        data-off-label="<i class='icon-remove'></i>">
	                                        <input type="checkbox" name="publishedinstagram" value="checked"  <?php if ($instagram['published']== 'checked') echo ('checked') ?> />
	                                    </div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2" ><?php echo $twitter['social_name'];?></label>
								<div class="col-lg-10">
									<input type="text" name="twitter" class="form-control" placeholder="Enter Twitter Link" value="<?php echo $twitter['social_link'];?>"><br>
									<div class="col-lg-10">
										<label class="col-sm-2 pull">Publish ?</label>
										<div class="switch switch-square"
	                                        data-on-label="<i class=' icon-ok'></i>"
	                                        data-off-label="<i class='icon-remove'></i>">
	                                        <input type="checkbox" name="publishedtwitter" value="checked"  <?php if ($twitter['published']== 'checked') echo ('checked') ?> />
	                                    </div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2" ><?php echo $youtube['social_name'];?></label>
								<div class="col-lg-10">
									<input type="text" name="youtube" class="form-control" placeholder="Enter Youtube Link" value="<?php echo $youtube['social_link'];?>"><br>
									<div class="col-lg-10">
										<label class="col-sm-2 pull">Publish ?</label>
										<div class="switch switch-square"
	                                        data-on-label="<i class=' icon-ok'></i>"
	                                        data-off-label="<i class='icon-remove'></i>">
	                                        <input type="checkbox" name="publishedyoutube" value="checked"  <?php if ($youtube['published']== 'checked') echo ('checked') ?> />
	                                    </div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-12">
									<button class="btn btn-shadow btn-primary pull-right" type="submit" name="action">Update
									<i class=" icon-repeat"></i></button>
								</div>
							</div>
	                      
						</form>
					</div>
				</section>

	        </div>
	    </div>
	</section>
</section>