<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li class="active"> Form Edit <?php echo $title; ?></li>
				</ul>
				<!--breadcrumbs end -->
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
                      	Form Edit <?php echo $title; ?>
                      	<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
                  	</header>
					<div class="panel-body">
						<?php 
                         if($this->session->flashdata('true')){
                       ?>
                         <div class="alert alert-success"> 
                           <?php  echo $this->session->flashdata('true'); ?>
                          </div>
                      <?php    
                      }else if($this->session->flashdata('err')){
                      ?>
                       <div class = "alert alert-success">
                         <?php echo $this->session->flashdata('err'); ?>
                       </div>
                      <?php } ?>
						<?php echo form_open_multipart('backend_pricing/update_'.$func,'class="form-horizontal tasi-form"','method="post"'); ?>
                    		
                    		<div class="form-group">
	                            <label class="col-sm-2 ">Title (English)</label>
	                            <div class="col-sm-4">
	                                <input type="text" class="form-control" placeholder="Enter Postion" name="titleen" value="<?php echo $pricing['general_data'];?>" />
	                            </div>
	                            <label class="col-sm-2 ">Title (German)</label>
	                            <div class="col-sm-4">
	                                <input type="text" class="form-control" placeholder="Enter Postion" name="titlede" value="<?php echo $pricing['general_sub_data'];?>" />
	                            </div>
	                        </div>

	                        <div class="form-group">
	                           	<label class="col-sm-2 ">File</label>
	                           	<div class="col-md-2">
		                           	<a href="<?php echo base_url();?>assets/<?php echo $pricing['main_image'];?>" class="mfp-image">
				                         <img src="<?php echo base_url();?>assets/images/icons-pdf.png" alt="" style="width: 50px; height: 50px;">
				                    </a>
				                    <?php echo $pricing['main_image'];?><br>
				                    <div class="fileupload fileupload-new" data-provides="fileupload" style="margin-top: 10px">
		                                <span class="btn btn-white btn-file">
		                                	<span class="fileupload-new"><i class="icon-paper-clip"></i> Select file</span>
		                               		<span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
		                                	<input type="file" name="file" class="default" accept=".doc,.docx, .pdf" />
		                                </span>
		                                <span class="fileupload-preview" style="margin-left:5px;"></span>
		                                <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
		                          	</div>
			                    </div> 
	                            
	                        </div>

	                        <div class="form-group">
								<div class="col-lg-12">
									<button class="btn btn-shadow btn-success pull-right" type="submit" >Save  <i class=" icon-ok"></i></button>
								</div>
							</div>
						</form>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<!--main content start-->



             