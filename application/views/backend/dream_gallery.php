<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
			<div class="col-lg-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
					<li class="active">Backend <?php echo $title;?></li>
				</ul>
				 <!--breadcrumbs end -->
			</div>
        </div>
        
        <div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
						<strong><?php echo $title;?></strong>
						<span class="tools pull-right">
							<a href="javascript:;" class="icon-chevron-down"></a> 
						</span>
					</header>
					<div class="panel-body">
						<ul class="grid cs-style-3">
							<?php foreach ($imagegallery as $pic):?>
							  <li>
								  <figure >
									  <img  src="<?php echo base_url();?>assets/images/dream/<?php echo $pic['picture_name'];?>" alt="" style="width: 280px; height: auto;">
									  
									  <figcaption style="width: 280px; height: auto;">
										  <a class="btn btn-danger btn-block" href="<?php echo base_url();?>backend_dream/image_delete/<?php echo $pic['general_id'];?>" onclick="return confirm('Are you sure to delete this picture?')" type="button"><i class="icon-trash"></i> Delete image</a> 
									  </figcaption>
								  </figure>
							  </li>
							<?php endforeach;?>
						</ul>
						<a type="button" class="btn btn-round btn-default btn-block" href="<?php echo base_url();?>backend_dream/<?php echo $link;?>"><i class="icon-plus-sign"> </i> Add Picture </a>
					</div>
				</section>
            </div>
        </div>
    </section>
</section>
<!--main content end-->