<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li class="active">Backend <?php echo $title; ?></li>
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Form Backend <?php echo $title; ?>
                        <span class="tools pull-right">
                          <a href="javascript:;" class="icon-chevron-down"></a> 
                        </span>
                    </header>
                    <div class="panel-body">
                        <?php
                        if ($this->session->flashdata('true')) {
                            ?>
                            <div class="alert alert-success">
                                <?php echo $this->session->flashdata('true'); ?>
                            </div>
                            <?php
                        } else if ($this->session->flashdata('err')) {
                            ?>
                            <div class="alert alert-success">
                                <?php echo $this->session->flashdata('err'); ?>
                            </div>
                        <?php } ?>
                        <div class="adv-table">
                            <div class="adv-table">
                                <div class="clearfix">
                                    <div class="btn-group pull-right">
                                        <a class="btn btn-default"
                                           href="<?php echo base_url(); ?>backend_kecamatan/add">
                                            <i class="icon-plus-sign"> </i> Add District
                                        </a>
                                    </div>
                                </div>
                                <div class="space15"></div>
                                <br>
                                <table class="display table table-bordered table-striped" id="example">
                                    <thead>
                                    <tr>
                                        <th width="25%">Name</th>
                                        <th width="25%">Description</th>
                                        <th width="10%">Blog</th>
                                        <th width="10%">Edit</th>
                                        <th width="10%">Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($daftar as $row) : ?>
                                        <tr class="gradeX">
                                            <td><?php echo $row['district_name'] ?></td>
                                            <td><?php echo $row['district_description']; ?></td>
                                            <td class="text-center">
                                                <a class="btn btn-round btn-warning" title="view & edit"
                                                   href="<?php echo site_url('backend_blog/daftar/kecamatan/' . $row['district_id']); ?>"
                                                   type="button">
                                                    <i class="icon-anchor"></i>
                                                </a>
                                            </td>
                                            <td class="text-center"><a class="btn btn-round btn-primary"
                                                                       title="view & edit"
                                                                       href="<?php echo site_url('backend_kecamatan/edit/' . $row['district_id']); ?>"
                                                                       type="button"><i class="icon-pencil"></i></a>
                                            </td>
                                            <td class="text-center"><a class="btn btn-round btn-danger" title="delete"
                                                                       href="<?php echo site_url('backend_kecamatan/delete/' . $row['district_id']); ?>"
                                                                       onclick="return confirm('Are you sure to delete?')"
                                                                       type="button"><i class="icon-trash"></i></a></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                    <tfoot>
                                </table>
                            </div>
                        </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->