<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li><a href="<?php echo base_url();?>backend_thevilla/team_member"> <?php echo $title; ?></a></li>
					<li class="active"> Edit <?php echo $title; ?></li>
				</ul>
				<!--breadcrumbs end -->
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
                      	Form Edit <?php echo $title; ?>
                      	<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
                  	</header>
					<div class="panel-body">
						<?php 
                         if($this->session->flashdata('true')){
                       ?>
                         <div class="alert alert-success"> 
                           <?php  echo $this->session->flashdata('true'); ?>
                          </div>
                      <?php    
                      }else if($this->session->flashdata('err')){
                      ?>
                       <div class = "alert alert-success">
                         <?php echo $this->session->flashdata('err'); ?>
                       </div>
                      <?php } ?>
						<?php echo form_open_multipart('backend_thevilla/team_member_editprocess','class="form-horizontal tasi-form"','method="post"'); ?>
                    		
                    		<div class="form-group">
								<label class="col-sm-2 ">Name <font color=red>*</font></label>
								<div class="col-sm-10">
									<input type="hidden" class="form-control" placeholder="Enter Name" name="general_id" value="<?php echo $team['general_id'];?>" required="" >
									<input type="text" class="form-control" placeholder="Enter Name" name="name" value="<?php echo $team['general_data'];?>" required="" >
								</div>
							</div>
							
                          	<div class="form-group">
	                            <label class="col-sm-2 ">Position (English)</label>
	                            <div class="col-sm-4">
	                                <input type="text" class="form-control" placeholder="Enter Postion" name="positionen" value="<?php echo $team['general_desc'];?>" />
	                            </div>
	                            <label class="col-sm-2 ">Position (German)</label>
	                            <div class="col-sm-4">
	                                <input type="text" class="form-control" placeholder="Enter Postion" name="positionde" value="<?php echo $team['general_sub_desc'];?>" />
	                            </div>
	                        </div>

	                        <div class="form-group">
								<label class="col-sm-2" >Picture</label>
								<div class="col-md-10">
									<div class="fileupload fileupload-new" data-provides="fileupload">
										<div class="fileupload-new thumbnail" style="width: 150px; height: 150px;">
											<img src="<?php echo base_url();?>assets/images/team/<?php echo $team['main_image'];?>" style="width: 150px; height: 150px;" alt="" />
										</div>
										<div class="fileupload-preview fileupload-exists thumbnail" style="width: 150px; height: 150px;"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
												<span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
												<input name="picture" type="file" class="default" />
											</span>
											<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
										</div>
									</div>
								</div>
							</div>

	                        <div class="form-group">
								<label class="col-sm-2 ">Email </label>
								<div class="col-sm-10">
									<input type="email" class="form-control" placeholder="Enter Email" name="email" value="<?php echo $team['email_link'];?>" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 ">Facebook </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" placeholder="Enter Facebook" name="facebook" value="<?php echo $team['facebook_link'];?>" >
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 ">Twitter </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" placeholder="Enter Twitter" name="twitter" value="<?php echo $team['twitter_link'];?>" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 ">Instagram </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" placeholder="Enter Instagram" name="instagram" value="<?php echo $team['instagram_link'];?>" />
								</div>
							</div>
	                        
							
							<div class="form-group">
								<div class="col-lg-12">
									<a class="btn btn-shadow btn-white" title="back" href="<?php echo base_url();?>backend_thevilla/team_member" type="button"><i class="icon-reply"></i> Back</a>
									<button class="btn btn-shadow btn-primary pull-right" type="submit" name="action">Update
									<i class=" icon-repeat"></i></button>
								</div>
							</div>
						</form>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<!--main content start-->



             