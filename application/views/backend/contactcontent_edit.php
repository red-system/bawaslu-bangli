<section id="main-content">
	<section class="wrapper">
		<div class="row">
	        <div class="col-lg-12">
	            <!--breadcrumbs start -->
	            <ul class="breadcrumb">
	                <li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li><a href="<?php echo base_url();?>backend_contact/<?php echo $general_name;?>"> <?php echo $title;?></a></li>
	                <li class="active"><?php echo $contact['general_data'];?> <?php if( $contact['general_lang'] == 'de') { echo 'German'; } else  if ( $contact['general_lang'] == 'de' ) { echo 'English'; } ?></li>
	            </ul>
	            <!--breadcrumbs end -->
	        </div>
	      </div>
	    <div class="row">
	        <div class="col-lg-12">
	            <section class="panel">
					<header class="panel-heading">
						Form Backend <?php echo $contact['general_data'];?> <?php if( $contact['general_lang'] == 'de') { echo 'German'; } else  if ( $contact['general_lang'] == 'de' ) { echo 'English'; } ?>
						<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
					</header>
					<div class="panel-body">
						<?php 
						   if($this->session->flashdata('true')){
						 ?>
						   <div class="alert alert-success"> 
						     <?php  echo $this->session->flashdata('true'); ?>
						    </div>
						<?php    
						}else if($this->session->flashdata('err')){
						?>
						 <div class = "alert alert-success">
						   <?php echo $this->session->flashdata('err'); ?>
						 </div>
						<?php } ?>
						<?php echo form_open_multipart('backend_contact/update_contactcontent','class="form-horizontal tasi-form"'); ?>
							
	                        <div class="form-group"> 
	                            <label class="col-sm-2 ">Title1 </label>
	                            <div class="col-sm-9"> 
	                            	<input type="hidden" class="form-control" placeholder="" name="general_id" value="<?php echo $contact['general_id'];?>" />
	                                <input type="text" class="form-control" placeholder="Enter Name" name="name" value="<?php echo $contact['general_data'];?>" />
	                            </div>
	                        </div>

	                        <div class="form-group">
								<label class="col-sm-2">Description 2</label>
								<div class="col-lg-10">
									<textarea class="form-control ckeditor" id="editor1" name="description"><?php echo $contact['general_desc'];?></textarea><br>
								</div>
							</div>

							<div class="form-group"> 
	                            <label class="col-sm-2 ">Title 2</label>
	                            <div class="col-sm-9"> 
	                            	<input type="text" class="form-control" placeholder="Enter Name" name="name1" value="<?php echo $contact['general_sub_data'];?>" />
	                            </div>
	                        </div>

	                        <div class="form-group">
								<label class="col-sm-2">Description 2</label>
								<div class="col-lg-10">
									<textarea class="form-control ckeditor" id="editor1" name="description1"><?php echo $contact['general_sub_desc'];?></textarea><br>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2">Google Map</label>
								<div class="col-lg-10">
									<textarea class="form-control" name="map" rows="4"><?php echo $contact['general_link'];?></textarea>
									<span class="help-block">Change the width to 100% ,height to 450 , and src to data-src for maximum results</span>
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-12">
									<a class="btn btn-shadow btn-white" title="back" href="<?php echo base_url();?>backend_contact/<?php echo $general_name;?>" type="button"><i class="icon-reply"></i> Back</a>
									<button class="btn btn-shadow btn-primary pull-right" type="submit" name="action">Update
									<i class=" icon-repeat"></i></button>
								</div>
							</div>
	                      
						</form>
					</div>
				</section>

	        </div>
	    </div>
	    
	</section>
</section>