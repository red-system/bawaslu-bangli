<section id="main-content">
	<section class="wrapper">
		<div class="row">
	        <div class="col-lg-12">
	            <!--breadcrumbs start -->
	            <ul class="breadcrumb">
	                <li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li class="active">Backend Of Reservation Form</li>
	            </ul>
	            <!--breadcrumbs end -->
	        </div>
	      </div>
	    <div class="row">
	        <div class="col-lg-12">
	            <section class="panel">
					<header class="panel-heading">
						Form Backend Of Reservation Form
						<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
					</header>
					<div class="panel-body">
						<?php 
						   if($this->session->flashdata('true')){
						 ?>
						   <div class="alert alert-success"> 
						     <?php  echo $this->session->flashdata('true'); ?>
						    </div>
						<?php    
						}else if($this->session->flashdata('err')){
						?>
						 <div class = "alert alert-success">
						   <?php echo $this->session->flashdata('err'); ?>
						 </div>
						<?php } ?>
						<?php echo form_open_multipart('backend_availability/reservation_form_edit','class="form-horizontal tasi-form"'); ?>
							<div class="form-group">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-sm-12 text-center">English</label><br>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="hidden" class="form-control" placeholder="Enter Name" name="id_form_en" value="<?php echo $reservationen['general_id'];?>" required="" />
											<input type="text" class="form-control" placeholder="Enter Name" name="general_data_form_en" value="<?php echo $reservationen['general_data'];?>" required="" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" placeholder="Enter Name" name="general_sub_data_form_en" value="<?php echo $reservationen['general_sub_data'];?>" required="" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" placeholder="Enter Name" name="general_desc_form_en" value="<?php echo $reservationen['general_desc'];?>" required="" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" placeholder="Enter Name" name="general_sub_desc_form_en" value="<?php echo $reservationen['general_sub_desc'];?>" required="" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" placeholder="Enter Name" name="general_link_form_en" value="<?php echo $reservationen['general_link'];?>" required="" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" placeholder="Enter Name" name="main_image_form_en" value="<?php echo $reservationen['main_image'];?>" required="" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" placeholder="Enter Name" name="secondary_image_form_en" value="<?php echo $reservationen['secondary_image'];?>" required="" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" placeholder="Enter Name" name="general_password_form_en" value="<?php echo $reservationen['general_password'];?>" required="" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" placeholder="Enter Name" name="facebook_link_form_en" value="<?php echo $reservationen['facebook_link'];?>" required="" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" placeholder="Enter Name" name="general_additional_info_form_en" value="<?php echo $reservationen['general_additional_info'];?>" required="" />
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-sm-12 text-center">German</label><br>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="hidden" class="form-control" placeholder="Enter Name" name="id_form_de" value="<?php echo $reservationde['general_id'];?>" required="" />
											<input type="text" class="form-control" placeholder="Enter Name" name="general_data_form_de" value="<?php echo $reservationde['general_data'];?>" required="" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" placeholder="Enter Name" name="general_sub_data_form_de" value="<?php echo $reservationde['general_sub_data'];?>" required="" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" placeholder="Enter Name" name="general_desc_form_de" value="<?php echo $reservationde['general_desc'];?>" required="" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" placeholder="Enter Name" name="general_sub_desc_form_de" value="<?php echo $reservationde['general_sub_desc'];?>" required="" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" placeholder="Enter Name" name="general_link_form_de" value="<?php echo $reservationde['general_link'];?>" required="" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" placeholder="Enter Name" name="main_image_form_de" value="<?php echo $reservationde['main_image'];?>" required="" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" placeholder="Enter Name" name="secondary_image_form_de" value="<?php echo $reservationde['secondary_image'];?>" required="" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" placeholder="Enter Name" name="general_password_form_de" value="<?php echo $reservationde['general_password'];?>" required="" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" placeholder="Enter Name" name="facebook_link_form_de" value="<?php echo $reservationde['facebook_link'];?>" required="" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" placeholder="Enter Name" name="general_additional_info_form_de" value="<?php echo $reservationde['general_additional_info'];?>" required="" />
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-12">
									<br><button class="btn btn-shadow btn-primary pull-right" type="submit" name="action">Update
									<i class=" icon-repeat"></i></button>
								</div>
							</div>
						</form>
					</div>
				</section>

	        </div>
	    </div>
	</section>
</section>