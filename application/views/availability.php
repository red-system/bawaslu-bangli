<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap-year-calendar-master/css/bootstrap-year-calendar.min.css">
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-year-calendar-master/js/bootstrap-year-calendar.min.js"></script>

<section class="section-sub-banner bg-9 page-header" style="background: url(<?php echo base_url();?>assets/images/banner/<?php echo $imageheader['picture_name'];?>) " title="<?php echo $header['general_data'];?>" alt="<?php echo $header['general_data'];?>">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2><?php echo $header['general_data'];?></h2>
                <p><?php echo $header['general_desc'];?></p>
            </div>
        </div>
    </div>
</section>
<section class="section-about" id="availability">
    <div class="container">
        <div class="row">
            <ul class="menubookres">
                <li class="booked"><span><?php if( $lang == 'de') { echo 'Gebucht'; } else  if ( $lang == 'en' ) { echo 'Booked'; } ?></span></li>
            </ul>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div id="calendar"></div>
            </div>
        </div>
    </div>
</section>
<section class="section-about" id="pricing">
    <div class="container">
        <div class="about">
            <div class="col-md-4">
                <div class="text">
                    <br><h3><strong><?php if( $lang == 'de') { echo  $pricinglist['general_sub_data']; } else  if ( $lang == 'en' ) { echo $pricinglist['general_data']; } ?></strong></h3><br>
                    <div class="desc">
                        <a href="<?php echo base_url();?>assets/<?php echo $pricinglist['main_image'];?>" class="mfp-image">
                            <img src="<?php echo base_url();?>assets/images/logo-price-list-download.png" width="60" height="60" alt="<?php echo $pricinglist['general_data'];?>" title="<?php if( $lang == 'de') { echo  $pricinglist['general_sub_data']; } else  if ( $lang == 'en' ) { echo $pricinglist['general_data']; } ?>">
                        </a> 
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="text">
                    <br><h3><strong><?php if( $lang == 'de') { echo  $menulist['general_sub_data']; } else  if ( $lang == 'en' ) { echo $menulist['general_data']; } ?></strong></h3><br>
                    <div class="desc">
                        <a href="<?php echo base_url();?>assets/<?php echo $menulist['main_image'];?>" class="mfp-image">
                            <img src="<?php echo base_url();?>assets/images/logo-menu-list-download.png" width="60" height="60" alt="<?php echo $menulist['general_data'];?>" title="<?php if( $lang == 'de') { echo  $menulist['general_sub_data']; } else  if ( $lang == 'en' ) { echo $menulist['general_data']; } ?>">
                        </a> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-reservation-page bg-white" id="reservation">
    <div class="container">
        <div class="reservation-page">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="reservation-sidebar">
                        <div class="reservation-sidebar_availability bg-gray">
                            <h2 class="reservation-heading"><?php echo $reservation['general_data'];?></h2>
                            <?php echo form_open_multipart($lang.'/info/send-reservation',' class="formreservation"'); ?>
                                <div class="check_availability-field col-md-4">
                                    <label><?php echo $reservation['general_sub_data'];?> <font class="color-red">*</font></label>
                                    <input type="text" class="awe-input from inputreservasi font-roboto"  placeholder="<?php echo $reservation['general_sub_data'];?>" name="name" >
                                    <span class="form-error"></span>
                                </div>
                                <div class="check_availability-field col-md-4">
                                    <label><?php echo $reservation['general_desc'];?> <font class="color-red">*</font></label>
                                    <input type="email" class="awe-input from inputreservasi font-roboto"  placeholder="<?php echo $reservation['general_desc'];?>" name="email" >
                                    <span class="form-error"></span>
                                </div>
                                <div class="check_availability-field col-md-4">
                                    <label><?php echo $reservation['general_sub_desc'];?> <font class="color-red">*</font></label></label> 
                                    <input type="text" class="awe-input from inputreservasi font-roboto"  placeholder="<?php echo $reservation['general_sub_desc'];?>" name="phone" >
                                    <span class="form-error"></span>
                                </div>
                                <div class="check_availability-field col-md-4">
                                    <label><?php echo $reservation['general_link'];?> <font class="color-red">*</font></label></label>
                                    <input type="text" class="awe-calendar awe-input from inputreservasi font-roboto"  placeholder="<?php echo $reservation['general_link'];?>" name="arrival" >
                                    <span class="form-error"></span>
                                </div>
                                <div class="check_availability-field col-md-4">
                                    <label><?php echo $reservation['main_image'];?> <font class="color-red">*</font></label></label>
                                    <input type="text" class="awe-calendar awe-input to inputreservasi font-roboto" placeholder="<?php echo $reservation['main_image'];?>" name="departure">
                                    <span class="form-error"></span>
                                </div>
                                <div class="check_availability-field col-md-4">
                                    <label><?php echo $reservation['secondary_image'];?></label>
                                    <select class="awe-select inputreservasi font-roboto" name="guest">
                                        <option value="0" class="font-roboto"><?php echo $reservation['secondary_image'];?></option>
                                        <option value="1-2" class="font-roboto">1-2</option>
                                        <option value="3-4" class="font-roboto">3-4</option>
                                        <option value="5-10" class="font-roboto">5-10</option>
                                    </select>
                                </div>
                                <div class="check_availability-field col-md-12">
                                    <label><?php echo $reservation['general_password'];?> <font class="color-red">*</font></label>
                                    <textarea name="message" class="awe-input from textareareservasi" placeholder="<?php echo $reservation['general_password'];?>" ></textarea>
                                    <span class="form-error"></span>
                                </div>
                                <div class="check_availability-field col-md-3">
                                    <?php echo $captcha; ?>
                                </div>
                                <div class="col-md-3">
                                    <br>
                                    <label><?php echo $reservation['facebook_link'];?><font class="color-red">*</font></label>
                                    <input type="text" class="form-control" name="captcha" placeholder="" >
                                    <span class="form-error"></span>
                                </div>
                                <button class="awe-btn awe-btn-13" type="submit" ><img src="<?php echo base_url();?>assets/plugin/loading.gif" id="gif" class="button-reservasi"> &emsp;<?php echo $reservation['general_additional_info'];?></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(function() {
        <?php 
              $array= array();
              $status = '';

        ?>    
        <?php $i=0;
         foreach ($days as $key => $value) { 
            $year =  substr ($value['day'],0,4);
            $month =  substr ($value['day'],5,2);
            $mm = $month-1;
            $date =  substr ($value['day'],8,2);
            $check = $this->db->get_where('tb_day', array('day' => $value['day']) )->result_array();
            if (count($check) > 1 ) { 
                if (($check[0]['status']=='reserve') && ($check[1]['status']=='booked')) { ?>
                    var InBookedOutReverse<?php echo $i; ?> = new Date(<?php echo $year;?>, <?php echo $mm;?>, <?php echo $date;?>).getTime();
                    <?php $status = 'InBookedOutReverse'.$i; ?>
                <?php } else {
                    if (($check[0]['status']=='booked') && ($check[1]['status']=='reserve')) { ?>
                        var InReverseOutBooked<?php echo $i; ?> = new Date(<?php echo $year;?>, <?php echo $mm;?>, <?php echo $date;?>).getTime();
                        <?php $status = 'InReverseOutBooked'.$i; ?>
                    <?php } else {
                        if (($check[0]['status']=='booked') && ($check[1]['status']=='booked')) { ?>
                            var InOut<?php echo $i; ?> = new Date(<?php echo $year;?>, <?php echo $mm;?>, <?php echo $date;?>).getTime();
                            <?php $status = 'InOut'.$i; ?>
                        <?php } else {
                            if (($check[0]['status']=='reserve') && ($check[1]['status']=='reserve')) { ?> 
                                var InOutReserve<?php echo $i; ?> = new Date(<?php echo $year;?>, <?php echo $mm;?>, <?php echo $date;?>).getTime();
                                <?php $status = 'InOutReserve'.$i; ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>   
            <?php } else {
                $check = $this->db->get_where('tb_day', array('day' => $value['day']))->row_array();
                if (($check['info']=='checkIn') && ($check['status']=='booked')) { ?>
                    var checkIn<?php echo $i; ?> = new Date(<?php echo $year;?>, <?php echo $mm;?>, <?php echo $date;?>).getTime();
                    <?php $status = 'checkIn'.$i ?>                        
                <?php } else {
                    if (($check['info']=='checkIn') && ($check['status']=='reserve')) { ?>
                    var checkInReserve<?php echo $i; ?> = new Date(<?php echo $year;?>, <?php echo $mm;?>, <?php echo $date;?>).getTime();
                    <?php $status = 'checkInReserve'.$i ?>                        
                    <?php } else {
                         if (($check['info']=='checkOut') && ($check['status']=='booked')) { ?>
                            var checkOut<?php echo $i; ?> = new Date(<?php echo $year;?>, <?php echo $mm;?>, <?php echo $date;?>).getTime();
                            <?php $status = 'checkOut'.$i ?>
                        <?php } else {
                            if (($check['info']=='checkOut') && ($check['status']=='reserve')) { ?>
                            var checkOutReserve<?php echo $i; ?> = new Date(<?php echo $year;?>, <?php echo $mm;?>, <?php echo $date;?>).getTime();
                            <?php $status = 'checkOutReserve'.$i ?>
                            <?php } else { 
                                if (($check['info']=='booked') && ($check['status']=='reserve')) { ?>
                                var reserve<?php echo $i; ?> = new Date(<?php echo $year;?>, <?php echo $mm;?>, <?php echo $date;?>).getTime();
                                <?php $status = 'reserve'.$i ?>
                                <?php } else {   
                                    if (($check['info']=='booked') && ($check['status']=='booked')) { ?>
                                       var booked<?php echo $i; ?> = new Date(<?php echo $year;?>, <?php echo $mm;?>, <?php echo $date;?>).getTime();
                                       <?php $status = 'booked'.$i ?>
                                    <?php } ?>
                                 <?php } ?>
                             <?php } ?>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        <?php 
            $array[$value['day']] = $status;
            $i++;

            }
        ?>
    $('#calendar').calendar({
        customDayRenderer: function(element, date) {
            <?php foreach($array as $tanggal=>$status) { ?>
            if(date.getTime() ==    <?php echo $status ?>) {
                <?php if(strpos($status, 'checkIn') !== false) { ?>
                    $(element).css('background-image', 'linear-gradient(transparent,transparent),url(<?php echo base_url();?>assets/images/checkIn.svg)');
                    $(element).css('background-repeat', 'no-repeat');
                    $(element).css('background-size', '100% 100%');
                    $(element).css('background-position', '50% 50%');
                <?php } ?>
                <?php if(strpos($status, 'checkInReserve') !== false) { ?>
                    $(element).css('background-image', 'linear-gradient(transparent,transparent),url(<?php echo base_url();?>assets/images/checkInReserve.svg)');
                    $(element).css('background-repeat', 'no-repeat');
                    $(element).css('background-size', '100% 100%');
                    $(element).css('background-position', '50% 50%');
                <?php } ?>
                <?php if(strpos($status, 'checkOut') !== false) { ?>
                    $(element).css('background-image', 'linear-gradient(transparent,transparent),url(<?php echo base_url();?>assets/images/checkOut.svg)');
                    $(element).css('background-repeat', 'no-repeat');
                    $(element).css('background-size', '100% 100%');
                    $(element).css('background-position', '50% 50%');
                <?php } ?>
                <?php if(strpos($status, 'checkOutReserve') !== false) { ?>
                    $(element).css('background-image', 'linear-gradient(transparent,transparent),url(<?php echo base_url();?>assets/images/checkOutReserve.svg)');
                    $(element).css('background-repeat', 'no-repeat');
                    $(element).css('background-size', '100% 100%');
                    $(element).css('background-position', '50% 50%');
                <?php } ?>
                <?php if(strpos($status, 'booked') !== false) { ?>
                    $(element).css('background-image', 'linear-gradient(transparent,transparent),url(<?php echo base_url();?>assets/images/booked.svg)');
                    $(element).css('background-repeat', 'no-repeat');
                    $(element).css('background-size', '100% 100%');
                    $(element).css('background-position', '50% 50%');
                <?php } ?>
                <?php if(strpos($status, 'reserve') !== false) { ?>
                    $(element).css('background-image', 'linear-gradient(transparent,transparent),url(<?php echo base_url();?>assets/images/reserve.svg)');
                    $(element).css('background-repeat', 'no-repeat');
                    $(element).css('background-size', '100% 100%');
                    $(element).css('background-position', '50% 50%');
                <?php } ?>
                <?php if(strpos($status, 'InOut') !== false) { ?>
                    $(element).css('background-image', 'linear-gradient(transparent,transparent),url(<?php echo base_url();?>assets/images/checkInOut.svg)');
                    $(element).css('background-repeat', 'no-repeat');
                    $(element).css('background-size', '100% 100%');
                    $(element).css('background-position', '50% 50%');
                <?php } ?>
                <?php if(strpos($status, 'InOutReserve') !== false) { ?>
                    $(element).css('background-image', 'linear-gradient(transparent,transparent),url(<?php echo base_url();?>assets/images/checkInOutReserve.svg)');
                    $(element).css('background-repeat', 'no-repeat');
                    $(element).css('background-size', '100% 100%');
                    $(element).css('background-position', '50% 50%');
                <?php } ?>
                <?php if(strpos($status, 'InBookedOutReverse') !== false) { ?>
                    $(element).css('background-image', 'linear-gradient(transparent,transparent),url(<?php echo base_url();?>assets/images/InBookedOutReserve.svg)');
                    $(element).css('background-repeat', 'no-repeat');
                    $(element).css('background-size', '100% 100%');
                    $(element).css('background-position', '50% 50%');
                <?php } ?>
                <?php if(strpos($status, 'InReverseOutBooked') !== false) { ?>
                    $(element).css('background-image', 'linear-gradient(transparent,transparent),url(<?php echo base_url();?>assets/images/InReserveOutBooked.svg)');
                    $(element).css('background-repeat', 'no-repeat');
                    $(element).css('background-size', '100% 100%');
                    $(element).css('background-position', '50% 50%');
                <?php } ?>
                }
        <?php } ?>
        }
    });
});
</script>