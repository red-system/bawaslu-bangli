<div class="gdlr-page-title-wrapper">
            <div class="gdlr-page-title-overlay"></div>
            <div class="gdlr-page-title-container container">
                <h1 class="gdlr-page-title">Detail Berita </h1>
            </div>
        </div>
        <!-- is search -->
        <div class="content-wrapper">
            <div class="gdlr-content">

                <!-- Above Sidebar Section-->

                <!-- Sidebar With Content Section-->
                <div class="with-sidebar-wrapper">
                    <div class="with-sidebar-container container">
                        <div class="with-sidebar-left ten columns">
                            <div class="with-sidebar-content twelve columns">
                                <section id="content-section-1">
                                    <div class="section-container container">
                                        <div class="blog-item-wrapper">
                                            <div class="blog-item-holder">
                                                <div class="gdlr-isotope" data-type="blog" data-layout="fitRows">
                                                    <div class="clear"></div>
                                                    <div class="twelve columns">
                                                        <div class="gdlr-item gdlr-blog-grid">
                                                            <div class="gdlr-ux gdlr-blog-grid-ux">
                                                                <article id="post-852" class="post-852 post type-post status-publish format-standard has-post-thumbnail hentry category-fit-row tag-blog tag-life-style">
                                                                    <div class="gdlr-standard-style">
                                                                        <div class="gdlr-blog-thumbnail">
                                                                            <a href="#"> <img src="<?php echo base_url();?>assets/upload/shutterstock_226039468-1024x682.jpg" alt="" width="960" height="639" /></a>
                                                                        </div>

                                                                        <header class="post-header">
                                                                            <h3 class="gdlr-blog-title"><a href="#">Donec luctus imperdiet</a></h3>

                                                                            <div class="gdlr-blog-info gdlr-info"><span class="gdlr-sep"></span>
                                                                                <div class="blog-info blog-date"><span class="gdlr-head">Date</span><a href="#">03 Dec 2013</a></div><span class="gdlr-sep"></span>
                                                                                <div class="clear"></div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </header>
                                                                        <!-- entry-header -->

                                                                        <div class="gdlr-blog-content">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere voluntate liceret: sese habere. Magna pars studiorum, prodita quaerimus. Magna pars studiorum, prodita quaerimus. Fabio vel iudice vincam, sunt in culpa qui officia. Vivamus sagittis lacus vel augue laoreet...
                                                                            
                                                                    </div>
                                                                </article>
                                                                <!-- #post -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </section>
                            </div>

                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>

                <!-- Below Sidebar Section-->

            </div>
            <!-- gdlr-content -->
            <div class="clear"></div>
        </div>
        <!-- content wrapper -->