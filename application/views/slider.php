        <!-- is search -->
        <div class="content-wrapper">
            <div class="gdlr-content">
                <!-- Above Sidebar Section-->
                <!-- Sidebar With Content Section-->
                <div class="with-sidebar-wrapper">
                    <section id="content-section-1">
                        <div class="gdlr-full-size-wrapper gdlr-show-all " style="padding-bottom: 0px;  background-color: #ffffff; ">
                            <div class="gdlr-master-slider-item gdlr-slider-item gdlr-item" style="margin-bottom: 0px;">
                                <!-- MasterSlider -->
                                <div id="P_MS5c0e2bf129c3d" class="master-slider-parent ms-parent-id-1">

                                    <!-- MasterSlider Main -->
                                    <div id="MS5c0e2bf129c3d" class="master-slider ms-skin-default">
                                        <?php foreach ($pictures as $picture) : ?>
                                            <div class="ms-slide" data-delay="8" data-fill-mode="fill">
                                                <img src="<?php echo base_url('assets/images/slider/'.$picture['main_image']);?>" alt="" title="" data-src="<?php echo base_url('assets/images/slider/'.$picture['main_image']);?>" />

                                                <div class="ms-layer  msp-cn-1-1" style="color: white; text-align: center" data-effect="t(true,150,n,n,n,n,n,n,n,n,n,n,n,n,n)" data-duration="450" data-ease="easeOutQuint" data-offset-x="12" data-offset-y="-146" data-origin="ml" data-position="normal">
                                                    <?php echo $picture['general_data'] ?></div>
                                                <div class="ms-layer  msp-cn-1-1" style="color: white; text-align: center" data-effect="t(true,150,n,n,n,n,n,n,n,n,n,n,n,n,n)" data-duration="400" data-delay="462" data-ease="easeOutQuint" data-offset-x="1" data-offset-y="-58" data-origin="ml" data-position="normal">
                                                    <?php echo $picture['general_sub_data'] ?></div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                    <!-- END MasterSlider Main -->

                                </div>
                                <!-- END MasterSlider -->


                            </div>
                            <div class="clear"></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </section>
                </div>
            </div>
            <!-- gdlr-content -->
            <div class="clear"></div>
        </div>
        <!-- content wrapper -->