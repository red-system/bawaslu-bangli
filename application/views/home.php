        <!-- is search -->
        <div class="content-wrapper">
            <div class="gdlr-content">

                <!-- Above Sidebar Section-->

                <!-- Sidebar With Content Section-->
                <div class="with-sidebar-wrapper">
                    <section id="content-section-1">
                        <div class="gdlr-color-wrapper  gdlr-show-all no-skin" style="background-color: #f5f5f5; padding-top: 80px; padding-bottom: 45px; ">
                            <div class="container">
                                <div class="gdlr-banner-item-wrapper">
                                    <div class="gdlr-banner-images gdlr-item">
                                        <div class="flexslider" data-pausetime="7000" data-slidespeed="600" data-effect="fade" data-columns="4" data-type="carousel" data-nav-container="gdlr-banner-images">
                                            
                                        <div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides" style="width: 100%; margin-left: 0px;">
                                                <li style="width: 25%; float: left; display: block;"><a href="<?php echo base_url();?>bawaslu-kecamatan-bangli"><img src="<?php echo base_url();?>assets/images/logo_bawaslu_bali.png" alt="" width="159" height="36" draggable="false"><h5>Kecamatan Bangli</h5></a></li>
                                                <li style="width: 25%; float: left; display: block;"><a href="<?php echo base_url();?>bawaslu-kecamatan-bangli"><img src="<?php echo base_url();?>assets/images/logo_bawaslu_bali.png" alt="" width="159" height="36" draggable="false"><h5>Kecamatan Kintamani</h5></a></li>
                                                <li style="width: 25%; float: left; display: block;"><a href="<?php echo base_url();?>bawaslu-kecamatan-bangli"><img src="<?php echo base_url();?>assets/images/logo_bawaslu_bali.png" alt="" width="159" height="36" draggable="false"><h5>Kecamatan Susut</h5></a></li>
                                                <li style="width: 25%; float: left; display: block;"><a href="<?php echo base_url();?>bawaslu-kecamatan-bangli"><img src="<?php echo base_url();?>assets/images/logo_bawaslu_bali.png" alt="" width="159" height="36" draggable="false"><h5>Kecamatan Tembuku</h5></a></li>

                                            </ul></div><ul class="flex-direction-nav"><li><a class="flex-prev" href="#"><i class="icon-angle-left"></i></a></li><li><a class="flex-next" href="#"><i class="icon-angle-right"></i></a></li></ul></div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </section>

                    <div class="clear"></div>

                    <section id="content-section-2">
                        <div class="gdlr-color-wrapper  gdlr-show-all gdlr-skin-portfolio-dark" style="background-color: #333333; padding-top: 75px; padding-bottom: 30px; ">
                            <div class="container">
                                <div class="gdlr-item-title-wrapper gdlr-item  gdlr-left gdlr-large gdlr-title-classic-portfolio ">
                                    <div class="gdlr-item-title-head">
                                        <h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border">Seputar Bawaslu Bangli</h3></div><a class="gdlr-item-title-link" href="portfolio-grid-3-columns-no-space.html">Selengkapnya →</a></div>
                                <div class="portfolio-item-wrapper type-classic-portfolio" data-ajax="wp-admin/admin-ajax.html">
                                    <div class="portfolio-item-holder  gdlr-portfolio-column-3 gdlr-hide-tag">
                                        <div class="gdlr-isotope" data-type="portfolio" data-layout="fitRows">
                                            <div class="clear"></div>
                                            <div class="four columns">
                                                <div class="gdlr-item gdlr-portfolio-item gdlr-classic-portfolio">
                                                    <div class="gdlr-ux gdlr-classic-portfolio-ux" style="opacity: 1; padding-top: 0px; margin-bottom: 0px;">
                                                        <div class="portfolio-thumbnail gdlr-image"><img src="<?php echo base_url();?>assets/upload/shutterstock_204407506-700x400.jpg" alt="" width="700" height="400"><span class="portfolio-overlay">&nbsp;</span>
                                                            <div class="portfolio-overlay-content"><a class="portfolio-overlay-wrapper" href="<?php echo base_url();?>assets/upload/shutterstock_204407506.jpg" data-rel="fancybox"><span class="portfolio-icon"><i class="fa fa-search"></i></span></a></div>
                                                        </div>
                                                        <h3 class="portfolio-title"><a href="#">Pengumuman Pilpres Segera Terbit</a></h3>
                                                        <div class="gdlr-portfolio-info">
                                                            <div class="portfolio-info portfolio-tag"><span class="info-head gdlr-title">Tags </span><a href="#" rel="tag">Advise</a><span class="sep">/</span><a href="#" rel="tag">Business</a></div>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="portfolio-excerpt">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Vivamus sagittis...
                                                            <div class="clear"></div><a href="#" class="excerpt-read-more">Read More →</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="four columns">
                                                <div class="gdlr-item gdlr-portfolio-item gdlr-classic-portfolio">
                                                    <div class="gdlr-ux gdlr-classic-portfolio-ux" style="opacity: 1; padding-top: 0px; margin-bottom: 0px;">
                                                        <div class="portfolio-thumbnail gdlr-image"><img src="<?php echo base_url();?>assets/upload/shutterstock_204407506-700x400.jpg" alt="" width="700" height="400"><span class="portfolio-overlay">&nbsp;</span>
                                                            <div class="portfolio-overlay-content"><a class="portfolio-overlay-wrapper" href="<?php echo base_url();?>assets/upload/shutterstock_204407506.jpg" data-rel="fancybox"><span class="portfolio-icon"><i class="fa fa-search"></i></span></a></div>
                                                        </div>
                                                        <h3 class="portfolio-title"><a href="#">Pengumuman Pilpres Segera Terbit</a></h3>
                                                        <div class="gdlr-portfolio-info">
                                                            <div class="portfolio-info portfolio-tag"><span class="info-head gdlr-title">Tags </span><a href="#" rel="tag">Advise</a><span class="sep">/</span><a href="#" rel="tag">Business</a></div>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="portfolio-excerpt">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Vivamus sagittis...
                                                            <div class="clear"></div><a href="#" class="excerpt-read-more">Read More →</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="four columns">
                                                <div class="gdlr-item gdlr-portfolio-item gdlr-classic-portfolio">
                                                    <div class="gdlr-ux gdlr-classic-portfolio-ux" style="opacity: 1; padding-top: 0px; margin-bottom: 0px;">
                                                        <div class="portfolio-thumbnail gdlr-image"><img src="<?php echo base_url();?>assets/upload/shutterstock_204407506-700x400.jpg" alt="" width="700" height="400"><span class="portfolio-overlay">&nbsp;</span>
                                                            <div class="portfolio-overlay-content"><a class="portfolio-overlay-wrapper" href="<?php echo base_url();?>assets/upload/shutterstock_204407506.jpg" data-rel="fancybox"><span class="portfolio-icon"><i class="fa fa-search"></i></span></a></div>
                                                        </div>
                                                        <h3 class="portfolio-title"><a href="#">Pengumuman Pilpres Segera Terbit</a></h3>
                                                        <div class="gdlr-portfolio-info">
                                                            <div class="portfolio-info portfolio-tag"><span class="info-head gdlr-title">Tags </span><a href="#" rel="tag">Advise</a><span class="sep">/</span><a href="#" rel="tag">Business</a></div>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="portfolio-excerpt">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Vivamus sagittis...
                                                            <div class="clear"></div><a href="#" class="excerpt-read-more">Read More →</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </section>

                    <section id="content-section-3">
                    <div class="gdlr-page-title-wrapper">
                        <div class="gdlr-page-title-overlay"></div>
                        <div class="gdlr-page-title-container container">
                            <h1 class="gdlr-page-title">Agenda Kegiatan Bawaslu Bangli</h1>
                        </div>
                    </div>
                    <div class="gdlr-color-wrapper  gdlr-show-all no-skin" style="background-color: #f7f7f7; padding-top: 85px; padding-bottom: 30px; ">
                            <div class="container">
                                <div class="four columns">
                                    <div class="gdlr-ux column-service-ux" style="opacity: 1; padding-top: 0px; margin-bottom: 0px;">
                                        <div class="gdlr-item gdlr-column-service-item gdlr-type-1" style="margin-bottom: 30px;">
                                            <div class="column-service-image"><img src="<?php echo base_url();?>assets/upload/icon-about-9.png" alt="" width="40" height="40"></div>
                                            <div class="column-service-content-wrapper">
                                                <h3 class="column-service-title">Sosialisasi Pemilihan Umum</h3>
                                                <span>Jumat, 28 November 2019</span>
                                                <div class="column-service-content gdlr-skin-content">
                                                    <p>Donec ullamcorper nulla non metus auctor fringilla. Cum sociis natoque penatibus etmagnis dis parturie nt montes, nascetur.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="four columns">
                                    <div class="gdlr-ux column-service-ux" style="opacity: 1; padding-top: 0px; margin-bottom: 0px;">
                                        <div class="gdlr-item gdlr-column-service-item gdlr-type-1" style="margin-bottom: 30px;">
                                            <div class="column-service-image"><img src="<?php echo base_url();?>assets/upload/icon-about-9.png" alt="" width="40" height="40"></div>
                                            <div class="column-service-content-wrapper">
                                                <h3 class="column-service-title">Sosialisasi Pemilihan Umum</h3>
                                                <span>Jumat, 28 November 2019</span>
                                                <div class="column-service-content gdlr-skin-content">
                                                    <p>Donec ullamcorper nulla non metus auctor fringilla. Cum sociis natoque penatibus etmagnis dis parturie nt montes, nascetur.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="four columns">
                                    <div class="gdlr-ux column-service-ux" style="opacity: 1; padding-top: 0px; margin-bottom: 0px;">
                                        <div class="gdlr-item gdlr-column-service-item gdlr-type-1" style="margin-bottom: 30px;">
                                            <div class="column-service-image"><img src="<?php echo base_url();?>assets/upload/icon-about-9.png" alt="" width="40" height="40"></div>
                                            <div class="column-service-content-wrapper">
                                                <h3 class="column-service-title">Sosialisasi Pemilihan Umum</h3>
                                                <span>Jumat, 28 November 2019</span>
                                                <div class="column-service-content gdlr-skin-content">
                                                    <p>Donec ullamcorper nulla non metus auctor fringilla. Cum sociis natoque penatibus etmagnis dis parturie nt montes, nascetur.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </section>


                    <section id="content-section-3">
                    <div class="gdlr-page-title-wrapper">
                        <div class="gdlr-page-title-overlay"></div>
                        <div class="gdlr-page-title-container container">
                            <h1 class="gdlr-page-title">Galeri Bawaslu Bangli</h1>
                        </div>
                    </div>
                    <br>
                    <div class="gdlr-shortcode-wrapper">
                            <div class="gdlr-gallery-item gdlr-item gdlr-gallery-thumbnail">
                                <div class="gdlr-gallery-thumbnail-container" style="height: 497.883px;">
                                    <div class="gdlr-gallery-thumbnail" data-id="4577" style="display: none;"><img src="<?php echo base_url();?>assets/upload/shutterstock_172217969.jpg" alt="" width="1280" height="853" style="opacity: 1;">
                                        <div class="gallery-caption-wrapper"><span class="gallery-caption">Clients</span></div>
                                    </div>
                                    <div class="gdlr-gallery-thumbnail" data-id="4574" style="display: none;"><img src="<?php echo base_url();?>assets/upload/shutterstock_146762978.jpg" alt="" width="1280" height="915">
                                        <div class="gallery-caption-wrapper"><span class="gallery-caption">Dollars</span></div>
                                    </div>
                                    <div class="gdlr-gallery-thumbnail" data-id="4568" style="display: none;"><img src="<?php echo base_url();?>assets/upload/shutterstock_123790510.jpg" alt="" width="1280" height="853">
                                        <div class="gallery-caption-wrapper"><span class="gallery-caption">Advise</span></div>
                                    </div>
                                    <div class="gdlr-gallery-thumbnail" data-id="4586" style="display: none;"><img src="<?php echo base_url();?>assets/upload/shutterstock_251068444.jpg" alt="" width="1280" height="855">
                                        <div class="gallery-caption-wrapper"><span class="gallery-caption">Building</span></div>
                                    </div>
                                    <div class="gdlr-gallery-thumbnail" data-id="4585" style="display: block;"><img src="<?php echo base_url();?>assets/upload/shutterstock_247037281.jpg" alt="" width="1280" height="873">
                                        <div class="gallery-caption-wrapper"><span class="gallery-caption">Wealth</span></div>
                                    </div>
                                    <div class="gdlr-gallery-thumbnail" data-id="4581" style="display: none;"><img src="<?php echo base_url();?>assets/upload/shutterstock_207507838.jpg" alt="" width="1280" height="853">
                                        <div class="gallery-caption-wrapper"><span class="gallery-caption">ROI</span></div>
                                    </div>
                                    <div class="gdlr-gallery-thumbnail" data-id="4572" style="display: none;"><img src="<?php echo base_url();?>assets/upload/shutterstock_132181055.jpg" alt="" width="1280" height="853">
                                        <div class="gallery-caption-wrapper"><span class="gallery-caption">Dollars</span></div>
                                    </div>
                                </div>
                                <div class="gallery-column one-seventh column">
                                    <div class="gallery-item" data-id="4577"><img src="<?php echo base_url();?>assets/upload/shutterstock_172217969-150x150.jpg" alt="" width="150" height="150" style="opacity: 1;"></div>
                                </div>
                                <div class="gallery-column one-seventh column">
                                    <div class="gallery-item" data-id="4574"><img src="<?php echo base_url();?>assets/upload/shutterstock_146762978-150x150.jpg" alt="" width="150" height="150" style="opacity: 1;"></div>
                                </div>
                                <div class="gallery-column one-seventh column">
                                    <div class="gallery-item" data-id="4568"><img src="<?php echo base_url();?>assets/upload/shutterstock_123790510-150x150.jpg" alt="" width="150" height="150" style="opacity: 1;"></div>
                                </div>
                                <div class="gallery-column one-seventh column">
                                    <div class="gallery-item" data-id="4586"><img src="<?php echo base_url();?>assets/upload/shutterstock_251068444-150x150.jpg" alt="" width="150" height="150" style="opacity: 1;"></div>
                                </div>
                                <div class="gallery-column one-seventh column">
                                    <div class="gallery-item" data-id="4585"><img src="<?php echo base_url();?>assets/upload/shutterstock_247037281-150x150.jpg" alt="" width="150" height="150" style="opacity: 1;"></div>
                                </div>
                                <div class="gallery-column one-seventh column">
                                    <div class="gallery-item" data-id="4581"><img src="<?php echo base_url();?>assets/upload/shutterstock_207507838-150x150.jpg" alt="" width="150" height="150" style="opacity: 1;"></div>
                                </div>
                                <div class="gallery-column one-seventh column">
                                    <div class="gallery-item" data-id="4572"><img src="<?php echo base_url();?>assets/upload/shutterstock_132181055-150x150.jpg" alt="" width="150" height="150" style="opacity: 1;"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>          
                    </section>
                </div>
                <!-- Below Sidebar Section-->

                

            </div>

            </div>
            <!-- gdlr-content -->
            <div class="clear"></div>
        </div>
        <!-- content wrapper -->

