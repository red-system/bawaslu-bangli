<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7 ltie8 ltie9" lang="en-US"><![endif]-->
<!--[if IE 8]><html class="ie ie8 ltie9" lang="en-US"><![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US">
<!--<![endif]-->

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="initial-scale=1.0" />

    <title>Bawaslu Bangli &#8211; Pusat Informasi Badan Pengawas Pemilu Bangli</title>

    <link rel='stylesheet' href='<?php echo base_url();?>assets/css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url();?>assets/plugins/superfish/css/superfish.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url();?>assets/plugins/dl-menu/component.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url();?>assets/plugins/font-awesome-new/css/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url();?>assets/plugins/fancybox/jquery.fancybox.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url();?>assets/plugins/flexslider/flexslider.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url();?>assets/css/style-responsive.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url();?>assets/css/style-custom.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url();?>assets/plugins/masterslider/public/assets/css/masterslider.main.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url();?>assets/css/mastercustom.css' type='text/css' media='all' />

    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&amp;subset=greek%2Ccyrillic-ext%2Ccyrillic%2Clatin%2Clatin-ext%2Cvietnamese%2Cgreek-ext&amp;ver=6efe5869748aa765b5a2045c3b70667c' type='text/css' media='all' />
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=ABeeZee%3Aregular%2Citalic&amp;subset=latin&amp;ver=6efe5869748aa765b5a2045c3b70667c' type='text/css' media='all' />
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Montserrat:regular,700%2CRoboto:300,100' type='text/css' media='all' />
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/<?php echo $logofavicon['main_image'];?>"/>

</head>

<body class="home page-template-default page page-id-4547 _masterslider _msp_version_3.2.7 gdlr-carousel-no-scroll">
<div class="body-wrapper  float-menu gdlr-header-solid" data-home="#">
    <header class="gdlr-header-wrapper" id="gdlr-header-wrapper">
        <!-- top navigation -->
        <div class="top-navigation-wrapper">
            <div class="top-navigation-container container">
                <div class="top-navigation-left">
                    <div class="top-social-wrapper">
                        <?php foreach ($social_medias as $social_media) :  ?>
                            <div class="social-icon">
                                <a href="<?php echo $social_media['social_link'];?>" target="_blank">
                                    <i class="<?php echo $social_media['social_logo'];?>"></i></a>
                            </div>
                        <?php endforeach; ?>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="top-navigation-right">
                    <div class="top-navigation-right-text">
                        <div class="gdlr-text-block"><i class="fa fa-clock-o"></i><?php echo $working['general_desc'];?></div>
                        <div class="gdlr-text-block"><i class="fa fa-envelope"></i><a href="mailto:<?php echo $email['general_data'];?>" ><?php echo $email['general_data'];?></a></div>
                        <div class="gdlr-text-block"><i class="fa fa-phone"></i><a href="tel:<?php echo $phone1['general_data'];?>"><?php echo $phone1['general_data'];?></a></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>

        <!-- logo -->
        <div id="gdlr-header-substitute"></div>
        <div class="gdlr-header-inner">
            <div class="gdlr-header-container" style="padding-left: 20px; padding-right: 20px">
                <!-- logo -->
                <div class="gdlr-logo">
                    <a class="gdlr-solid-logo" href="<?php echo base_url() ?>">
                        <img src="<?php echo base_url();?>assets/images/<?php echo $logoheader['main_image'];?>" alt="" />
                    </a>
                    <div class="gdlr-responsive-navigation dl-menuwrapper" id="gdlr-responsive-navigation">
                        <button class="dl-trigger">Open Menu</button>
                        <ul id="menu-main-menu" class="dl-menu gdlr-main-mobile-menu">
                            <li class="menu-item menu-item-home current-menu-item "><a href="index.html">Profil</a>
                                <ul class="dl-submenu">
                                    <li class="menu-item "><a href="homepage-2.html">Sejarah</a></li>
                                    <li class="menu-item "><a href="homepage-3.html">Sekretariat</a></li>
                                    <li class="menu-item "><a href="homepage-3.html">Struktur</a></li>
                                    <li class="menu-item "><a href="homepage-3.html">Tentang</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- navigation -->
                <div class="gdlr-navigation-wrapper">
                    <nav class="gdlr-navigation" id="gdlr-main-navigation" role="navigation">
                        <ul id="menu-main-menu-1" class="sf-menu gdlr-main-menu">
                            <li class="menu-item gdlr-normal-menu"><a href="<?php echo base_url();?>" class="sf-with-ul-pre"><i class="fa fa-home"></i>Beranda</a></li>
                            <li class="menu-item menu-item-has-children menu-item menu-item-has-children  gdlr-normal-menu"><a href="#" class="sf-with-ul-pre"><i class="fa fa-bank"></i>Profil</a>
                                <ul class="sub-menu">
                                    <li class="menu-item "><a href="<?php echo base_url();?>sejarah">Sejarah</a></li>
                                    <li class="menu-item "><a href="<?php echo base_url();?>sekretariat">Sekretariat</a></li>
                                    <li class="menu-item "><a href="<?php echo base_url();?>struktur">Struktur</a></li>
                                    <li class="menu-item "><a href="<?php echo base_url();?>tentang">Tentang</a></li>
                                </ul>
                            </li>
                            <li class="menu-item gdlr-normal-menu"><a href="<?php echo base_url();?>publikasi" class="sf-with-ul-pre"><i class="fa fa-instagram"></i>Publikasi</a></li>
                            <li class="menu-item menu-item-has-children menu-item menu-item-has-children  gdlr-normal-menu"><a href="#" class="sf-with-ul-pre"><i class="fa fa-newspaper-o"></i>Informasi Publik</a>
                                <ul class="sub-menu">
                                    <li class="menu-item "><a href="<?php echo base_url();?>pengumuman">Pengumuman</a></li>
                                    <li class="menu-item "><a href="<?php echo base_url();?>agenda">Agenda</a></li>
                                    <li class="menu-item "><a href="<?php echo base_url();?>artikel">Artikel</a></li>
                                    <li class="menu-item "><a href="<?php echo base_url();?>berita">Berita</a></li>
                                </ul>
                            </li>
                            <li class="menu-item menu-item-has-children menu-item menu-item-has-children  gdlr-normal-menu"><a href="#" class="sf-with-ul-pre"><i class="fa fa-photo"></i>Galeri</a>
                                <ul class="sub-menu">
                                    <li class="menu-item "><a href="<?php echo base_url();?>photo">Photo</a></li>
                                    <li class="menu-item "><a href="<?php echo base_url();?>video">Video</a></li>
                                </ul>
                            </li>
                            <li class="menu-item gdlr-normal-menu"><a href="<?php echo base_url();?>kontak-kami" class="sf-with-ul-pre"><i class="fa fa-group"></i>Kontak Kami</a></li>
                            <li class="menu-item menu-item-has-children menu-item menu-item-has-children  gdlr-normal-menu"><a href="#" class="sf-with-ul-pre"><i class="fa fa-bank"></i>Bawaslu Kecamatan</a>
                                <ul class="sub-menu">
                                    <li class="menu-item "><a href="<?php echo base_url();?>kecamatan">Kecamatan Bangli</a></li>
                                    <li class="menu-item "><a href="<?php echo base_url();?>kecamatan">Kecamatan Kintamani</a></li>
                                    <li class="menu-item "><a href="<?php echo base_url();?>kecamatan">Kecamatan Susut</a></li>
                                    <li class="menu-item "><a href="<?php echo base_url();?>kecamatan">Kecamatan Tembuku</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </header>