
    <footer class="footer-wrapper">
        <div class="footer-container container ">
            <div class="footer-column three columns" id="footer-widget-3">
                <div id="text-4" style="margin-left:0px; margin-right:0px" class="widget widget_text gdlr-item gdlr-widget">
                    <h3 class="gdlr-widget-title">Statistik</h3>
                    <div class="clear"></div>
                    <div class="textwidget">
                        <p><i class="gdlr-icon fa fa-users" style="color: #bbbbbb; font-size: 16px; "></i> 1478 Kunjungan </p>
                        <div class="clear"></div>
                        <div class="gdlr-space" style="margin-top: -10px;"></div>
                        <p><i class="gdlr-icon fa fa-check" style="color: #bbbbbb; font-size: 16px; "></i> 5 Orang Aktif </p>
                        <div class="clear"></div>
                        <div class="gdlr-space" style="margin-top: -10px;"></div>


                    </div>
                </div>
            </div>
            <div class="footer-column three columns" id="footer-widget-3">
                <div id="text-4" style="margin-left:0px; margin-right:0px" class="widget widget_text gdlr-item gdlr-widget">
                    <h3 class="gdlr-widget-title">Kontak Kami</h3>
                    <div class="clear"></div>
                    <div class="textwidget">
                        <p><i class="gdlr-icon fa fa-phone" style="color: #bbbbbb; font-size: 16px; "></i><a href="tel:<?php echo $phone1['general_data'];?>"><?php echo $phone1['general_data'];?></a></p>
                        <div class="clear"></div>
                        <div class="gdlr-space" style="margin-top: -10px;"></div>
                        <p><i class="gdlr-icon fa fa-envelope" style="color: #bbbbbb; font-size: 16px; "></i><a href="mailto:<?php echo $email['general_data'];?>" ><?php echo $email['general_data'];?></a></p>
                        <div class="clear"></div>
                        <div class="gdlr-space" style="margin-top: -10px;"></div>
                        <p><i class="gdlr-icon fa fa-clock-o" style="color: #bbbbbb; font-size: 16px; "></i> <?php echo $working['general_desc'];?></p>
                        <div class="clear"></div>
                        <div class="gdlr-space" style="margin-top: 30px;"></div>

                        <p>
                            <?php foreach ($social_medias as $social_media) :  ?>
                                <a href="<?php echo $social_media['social_link'];?>" target="_blank">
                                    <i class="gdlr-icon <?php echo $social_media['social_logo'];?>" style="color: #bbbbbb; font-size: 20px; " ></i>
                                </a>
                            <?php endforeach; ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="footer-column three columns" id="footer-widget-3">
                <div id="text-4" style="margin-left:0px; margin-right:0px" class="widget widget_text gdlr-item gdlr-widget">
                    <h3 class="gdlr-widget-title">Situs Terkait</h3>
                    <div class="clear"></div>
                    <div class="textwidget">
                        <a href="http://www.dkpp.go.id"><i class="gdlr-icon fa fa-globe" style="color: #bbbbbb; font-size: 16px; "></i> DKPP </a>
                        <div class="clear"></div>
                        <br>
                        <a href="http://www.bawaslu.go.id"><i class="gdlr-icon fa fa-globe" style="color: #bbbbbb; font-size: 16px; "></i> Bawaslu RI </a>
                        <div class="clear"></div>
                        <br>
                        <a href="http://www.kpu.go.id"><i class="gdlr-icon fa fa-globe" style="color: #bbbbbb; font-size: 16px; "></i> KPU RI </a>
                        <div class="clear"></div>
                        <br>
                        <a href="http://www.kpud-baliprov.go.id"><i class="gdlr-icon fa fa-globe" style="color: #bbbbbb; font-size: 16px; "></i> KPUD Provinsi Bali </a>
                        <div class="clear"></div>
                        <br>
                    </div>
                </div>
            </div>
            <div class="footer-column three columns" id="footer-widget-3">
                <div id="text-4" style="margin-left:0px; margin-right:0px" class="widget widget_text gdlr-item gdlr-widget">
                    <h3 class="gdlr-widget-title">Media Sosial</h3>
                    <div class="clear"></div>
                    <div class="fb-page" data-href="https://www.facebook.com/Bawaslukabbangli/" data-tabs="timeline" data-width="" data-height="180" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Bawaslukabbangli/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Bawaslukabbangli/">Bawaslu Kabupaten Bangli</a></blockquote></div>
                </div>
            </div>

            <div class="clear"></div>
        </div>

        <div class="copyright-wrapper">
            <div class="copyright-container container">
                <div class="copyright-left">
                    <a href="#" style="margin-right: 8px">Tentang Kami</a> | <a href="#" style="margin-left: 8px;margin-right: 8px">Kontak Kami</a>  </div>
                <div class="copyright-right">
                    Copyright Bawaslu Bangli (Developed by <a href="http://www.redsystem.id/">RedSystem</a>)</div>
                <div class="clear"></div>
            </div>
        </div>

    </footer>
</div>
<!-- body-wrapper -->
<script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery/jquery.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery/jquery-migrate.min.js'></script>

<script>
    var ms_grabbing_curosr = '<?php echo base_url();?>assets/plugins/masterslider/public/assets/css/common/grabbing.html',
        ms_grab_curosr = '<?php echo base_url();?>assets/plugins/masterslider/public/assets/css/common/grab.html';
</script>
<script type="text/javascript">
    (function(url) {
        if (/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)) {
            return;
        }
        var addEvent = function(evt, handler) {
            if (window.addEventListener) {
                document.addEventListener(evt, handler, false);
            } else if (window.attachEvent) {
                document.attachEvent('on' + evt, handler);
            }
        };
        var removeEvent = function(evt, handler) {
            if (window.removeEventListener) {
                document.removeEventListener(evt, handler, false);
            } else if (window.detachEvent) {
                document.detachEvent('on' + evt, handler);
            }
        };
        var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
        var logHuman = function() {
            if (window.wfLogHumanRan) {
                return;
            }
            window.wfLogHumanRan = true;
            var wfscr = document.createElement('script');
            wfscr.type = 'text/javascript';
            wfscr.async = true;
            wfscr.src = url + '&r=' + Math.random();
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(wfscr);
            for (var i = 0; i < evts.length; i++) {
                removeEvent(evts[i], logHuman);
            }
        };
        for (var i = 0; i < evts.length; i++) {
            addEvent(evts[i], logHuman);
        }
    });
</script>

<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/superfish/js/superfish.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>assets/js/hoverIntent.min.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/dl-menu/modernizr.custom.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/dl-menu/jquery.dlmenu.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery.easing.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery.transit.min.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/fancybox/jquery.fancybox.pack.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/fancybox/helpers/jquery.fancybox-media.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/fancybox/helpers/jquery.fancybox-thumbs.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/flexslider/jquery.flexslider.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery.isotope.min.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>assets/js/plugins.js'></script>
<div id="fb-root"></div>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v4.0"></script>

<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/gdlr-portfolio/gdlr-portfolio-script.js'></script>

<script>
    (window.MSReady = window.MSReady || []).push(function($) {

        "use strict";
        var masterslider_9c3d = new MasterSlider();

        // slider controls
        masterslider_9c3d.control('arrows', {
            autohide: true,
            overVideo: true
        });
        masterslider_9c3d.control('bullets', {
            autohide: false,
            overVideo: true,
            dir: 'h',
            align: 'bottom',
            space: 6,
            margin: 20
        });
        // slider setup
        masterslider_9c3d.setup("MS5c0e2bf129c3d", {
            width: 1140,
            height: 630,
            minHeight: 0,
            space: 0,
            start: 1,
            grabCursor: true,
            swipe: true,
            mouse: false,
            keyboard: false,
            layout: "fullwidth",
            wheel: false,
            autoplay: true,
            instantStartLayers: false,
            mobileBGVideo: false,
            loop: true,
            shuffle: false,
            preload: 0,
            heightLimit: true,
            autoHeight: false,
            smoothHeight: true,
            endPause: false,
            overPause: true,
            fillMode: "fill",
            centerControls: true,
            startOnAppear: false,
            layersMode: "center",
            autofillTarget: "",
            hideLayers: false,
            fullscreenMargin: 0,
            speed: 20,
            dir: "h",
            parallaxMode: 'swipe',
            view: "basic"
        });


        window.masterslider_instances = window.masterslider_instances || [];
        window.masterslider_instances.push(masterslider_9c3d);
    });
</script>
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/masterslider/public/assets/js/masterslider.min.js'></script>
</body>
</html>