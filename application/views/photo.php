<div class="gdlr-page-title-wrapper">
    <div class="gdlr-page-title-overlay"></div>
    <div class="gdlr-page-title-container container">
        <h1 class="gdlr-page-title">Photo Bawaslu</h1>
    </div>
</div>

<div class="with-sidebar-container container">
    <div class="with-sidebar-left twelve columns">
        <div class="with-sidebar-content twelve columns">
            <div class="main-content-container container gdlr-item-start-content">
                <div class="gdlr-item gdlr-main-content">
                    <div class="clear"></div>
                    <div class="gdlr-space" style="margin-top: 40px;"></div>
                    <div class="clear"></div>
                    <div class="gdlr-space" style="margin-top: 30px;"></div>
                    <div class="gdlr-shortcode-wrapper">
                        <div class="gdlr-gallery-item gdlr-item">
                            <div style="box-shadow: 0 0 3px black">
                                <div class="gallery-column column">
                                    <div class="gallery-item">
                                        <a href="upload/shutterstock_174966551.jpg" data-fancybox-group="gdlr-gal-1"
                                           data-rel="fancybox"><img
                                                    src="<?php echo base_url(); ?>assets/upload/shutterstock_174966551-150x150.jpg"
                                                    alt="" width="150" height="150" style="opacity: 1;"></a>
                                    </div>
                                </div>
                                <div class="gallery-column one-eight column">
                                    <div class="gallery-item">
                                        <a href="upload/shutterstock_150893516.jpg" data-fancybox-group="gdlr-gal-1"
                                           data-rel="fancybox"><img
                                                    src="<?php echo base_url(); ?>assets/upload/shutterstock_150893516-150x150.jpg"
                                                    alt="" width="150" height="150" style="opacity: 1;"></a>
                                    </div>
                                </div>
                                <div class="gallery-column one-eight column">
                                    <div class="gallery-item">
                                        <a href="upload/shutterstock_174482054.jpg" data-fancybox-group="gdlr-gal-1"
                                           data-rel="fancybox"><img
                                                    src="<?php echo base_url(); ?>assets/upload/shutterstock_174482054-150x150.jpg"
                                                    alt="" width="150" height="150" style="opacity: 1;"></a>
                                    </div>
                                </div>
                                <div class="gallery-column one-eight column">
                                    <div class="gallery-item">
                                        <a href="upload/shutterstock_172217969.jpg" data-fancybox-group="gdlr-gal-1"
                                           data-rel="fancybox"><img
                                                    src="<?php echo base_url(); ?>assets/upload/shutterstock_172217969-150x150.jpg"
                                                    alt="" width="150" height="150" style="opacity: 1;"></a>
                                    </div>
                                </div>
                                <div class="gallery-column one-eight column">
                                    <div class="gallery-item">
                                        <a href="upload/shutterstock_146762978.jpg" data-fancybox-group="gdlr-gal-1"
                                           data-rel="fancybox"><img
                                                    src="<?php echo base_url(); ?>assets/upload/shutterstock_146762978-150x150.jpg"
                                                    alt="" width="150" height="150" style="opacity: 1;"></a>
                                    </div>
                                </div>
                                <div class="gallery-column one-eight column">
                                    <div class="gallery-item">
                                        <a href="upload/shutterstock_123790510.jpg" data-fancybox-group="gdlr-gal-1"
                                           data-rel="fancybox"><img
                                                    src="<?php echo base_url(); ?>assets/upload/shutterstock_123790510-150x150.jpg"
                                                    alt="" width="150" height="150" style="opacity: 1;"></a>
                                    </div>
                                </div>
                                <div class="gallery-column one-eight column">
                                    <div class="gallery-item">
                                        <a href="upload/shutterstock_251068444.jpg" data-fancybox-group="gdlr-gal-1"
                                           data-rel="fancybox"><img
                                                    src="<?php echo base_url(); ?>assets/upload/shutterstock_251068444-150x150.jpg"
                                                    alt="" width="150" height="150" style="opacity: 1;"></a>
                                    </div>
                                </div>
                                <div class="gallery-column one-eight column">
                                    <div class="gallery-item">
                                        <a href="upload/shutterstock_247037281.jpg" data-fancybox-group="gdlr-gal-1"
                                           data-rel="fancybox"><img
                                                    src="<?php echo base_url(); ?>assets/upload/shutterstock_247037281-150x150.jpg"
                                                    alt="" width="150" height="150" style="opacity: 1;"></a>
                                    </div>
                                </div>
                                <div class="gallery-column one-eight column">
                                    <div class="gallery-item">
                                        <a href="upload/shutterstock_207507838.jpg" data-fancybox-group="gdlr-gal-1"
                                           data-rel="fancybox"><img
                                                    src="<?php echo base_url(); ?>assets/upload/shutterstock_207507838-150x150.jpg"
                                                    alt="" width="150" height="150" style="opacity: 1;"></a>
                                    </div>
                                </div>
                                <div class="gallery-column one-eight column">
                                    <div class="gallery-item">
                                        <a href="upload/shutterstock_132181055.jpg" data-fancybox-group="gdlr-gal-1"
                                           data-rel="fancybox"><img
                                                    src="<?php echo base_url(); ?>assets/upload/shutterstock_132181055-150x150.jpg"
                                                    alt="" width="150" height="150" style="opacity: 1;"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>


                </div>
            </div>

        </div>

        <div class="clear"></div>
    </div>

    <div class="clear"></div>
</div>