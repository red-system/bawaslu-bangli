<section class="section-sub-banner bg-9 page-header" style="background: url(<?php echo base_url();?>assets/images/banner/<?php echo $imageheader['picture_name'];?>) " title="<?php echo $header['general_data'];?>" alt="<?php echo $header['general_data'];?>">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2><?php echo $header['general_data'];?></h2>
                <p><?php echo $header['general_desc'];?></p>
            </div>
        </div>
    </div>
</section>
<section class="section-blog bg-white">
    <div class="container">
        <div class="blog">
            <div class="row">

                <div class="col-md-8 col-md-offset-2">
                    <div class="blog-content events-content">
                        <h1 class="element-invisible">Event fullwidth</h1>
                        <article class="post">
                            <?php echo $dreamcont['general_desc'];?>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section_page-gallery">
    <div class="container">
        <div class="gallery">
            <div class="gallery-content">
                <div class="row">
                    <h3 class="text-center"><strong><?php if( $lang == 'de') { echo 'Bauphasen'; } else  if ( $lang == 'en' ) { echo 'Construction Phases'; } ?></strong></h3>
                    <div class="gallery-isotope col-4 margin-top60">
                        <div class="item-size "></div>
                        <?php foreach ($imagegallery as $pic):?>
                            <div class="item-isotope  villa">
                                <div class="gallery_item">
                                    <a href="<?php echo base_url();?>assets/images/dream/<?php echo $pic['picture_name'];?>" class="mfp-image">
                                        <img src="<?php echo base_url();?>assets/images/dream/thumb/<?php echo $pic['second_picture'];?>" width="280" height="auto" alt="<?php echo $header['general_data'];?>" title="<?php echo $header['general_data'];?>">
                                    </a>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>       
</section>
<section class="section-about" >
    <div class="container">
        <h3 class="text-center"><strong><?php if( $lang == 'de') { echo 'Das Endresultat: Ein Traum; DANKE MADE.'; } else  if ( $lang == 'en' ) { echo 'The final result: A dream; Thank you Made'; } ?></strong></h3><br><br>
        <div style="text-align: center;">
            <img src="<?php echo base_url();?>assets/images/Samari-Hill-Villas.jpg" width="900" height="675" alt="Samari Hill Villas" title="Samari Hill Villas">
        </div>
    </div>
</section>