<section class="section-sub-banner bg-9 page-header" style="background: url(<?php echo base_url();?>assets/images/banner/<?php echo $imageheader['picture_name'];?>) " title="<?php echo $header['general_data'];?>" alt="<?php echo $header['general_data'];?>">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2><?php echo $header['general_data'];?></h2>
                <p><?php echo $header['general_desc'];?></p>
            </div>
        </div>
    </div>
</section>
<section class="section-guest-book">
    <div class="container">
        <div class="guest-book">
            <div class="guest-book_head bg-8" style="background: url(<?php echo base_url();?>assets/images/guestbook/form-guestbook.jpg) no-repeat firebrick;" title="<?php echo $header['general_data'];?>" alt="<?php echo $header['general_data'];?>">
                <div class="text">
                    <button id="myBtn" class="awe-btn awe-btn-13"><?php echo $aboutgb['general_sub_desc'];?></button>
                </div>
            </div>
            <div class="guest-book_content">
                <div class="row">
                    <div class="guest-book_mansory">
                        <?php foreach ($data->result() as $guestbooklist) :  ?>
                            <div class="item-masonry col-xs-6 col-md-4">
                                <div class="guest-book_item guest-book_item-2">
                                    <span class="icon lotus-icon-quote-left"></span>
                                    <div class="avatar">
                                        <img src="<?php echo base_url();?>assets/images/guestbook/<?php echo $guestbooklist->main_image;?>" width="60" height="60"  alt="<?php echo $guestbooklist->general_data;?>" title="<?php echo $guestbooklist->general_data;?>">
                                    </div>
                                    <h2><?php echo $guestbooklist->general_desc;?></h2>
                                    <p><?php echo $guestbooklist->general_sub_desc;?></p>
                                    <span><b><?php echo $guestbooklist->general_data;?></b> (<?php echo $guestbooklist->general_sub_data;?>) - <?php echo $guestbooklist->general_link;?></span>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>
</section>
<div id="myModal" class="modal">
    <div class="modal-content">
        <span class="close" >&times;</span>
        <h2><?php echo $modal['general_data'];?></h2>
        <p><?php echo $modal['general_desc'];?></p>
        <?php echo form_open_multipart($lang.'/guest-book/guestbook-addprocess','class="form-horizontal tasi-form" id="guestbook"'); ?>
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="text" class="form-control font-roboto" placeholder="<?php echo $modal['general_sub_data'];?>*" name="name"  />
                    <span class="form-error"></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="text" class="form-control font-roboto" placeholder="E-mail*" name="email"  />
                    <span class="form-error"></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="text" class="form-control font-roboto" placeholder="<?php echo $modal['general_sub_desc'];?>*" name="location"  />
                    <span class="form-error"></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="text" class="form-control font-roboto" placeholder="<?php echo $modal['general_link'];?>*" name="title"  />
                    <span class="form-error"></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <textarea class="form-control font-roboto" placeholder="<?php echo $modal['main_image'];?>*" name="testimonial" rows="5" ></textarea>
                    <span class="form-error"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12 "><?php echo $modal['secondary_image'];?></label> 
                <div class="col-sm-12">
                    <input name="picture" type="file" class="form-control font-roboto" placeholder="Profil Picture">
                </div>
            </div>
           <div class="form-group">
                <label class="col-sm-12 "><?php if( $lang == 'de') { echo 'Sicherheitscode'; } else  if ( $lang == 'en' ) { echo 'Security Code '; } ?><font color=red>*</font></label>
                <div class="col-sm-12">
                    <?php echo $captcha; ?>
                </div>
                <div class="col-sm-12">
                    <br><input type="text" class="form-control font-roboto" name="captcha" placeholder="Input Security Code" >
                    <span class="form-error"></span>
                </div>
            </div>
            <div class="form-field text-center">
                <button class="awe-btn awe-btn-13" type="submit" name="action"><img src="<?php echo base_url();?>assets/plugin/loading.gif" id="gif" style="display: none; margin: 0 auto; width: 25px; z-index: 999 ">&emsp;<?php echo $modal['general_password'];?></button>
            </div>
        </form>
    </div>
</div>

