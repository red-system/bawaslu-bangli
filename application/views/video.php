<div class="gdlr-page-title-wrapper">
    <div class="gdlr-page-title-overlay"></div>
    <div class="gdlr-page-title-container container">
        <h1 class="gdlr-page-title">Video Bawaslu</h1>
    </div>
</div>
<div class="content-wrapper">
    <div class="gdlr-content">

        <!-- Above Sidebar Section-->

        <!-- Sidebar With Content Section-->
        <div class="with-sidebar-wrapper">
            <div class="with-sidebar-container container">
                <div class="with-sidebar-left twelve columns">
                    <div class="with-sidebar-content six columns">
                        <div class="main-content-container container gdlr-item-start-content">
                            <div class="gdlr-item gdlr-main-content">
                                <h3 class="gdlr-heading-shortcode " style="font-size: 20px;font-weight: normal;">Video
                                    Title</h3>
                                <p>
                                    <iframe src="http://www.youtube.com/embed/SZEflIVnhH8?feature=oembed"
                                            frameborder="0" allowfullscreen="" id="gdlr-video-91786"></iframe>
                                </p>
                                <div class="clear"></div>
                                <div class="gdlr-space" style="margin-top: 60px;"></div>

                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>

                    <div class="with-sidebar-content six columns">
                        <div class="main-content-container container gdlr-item-start-content">
                            <div class="gdlr-item gdlr-main-content">
                                <h3 class="gdlr-heading-shortcode " style="font-size: 20px;font-weight: normal;">Video
                                    Title</h3>
                                <p>
                                    <iframe src="http://www.youtube.com/embed/SZEflIVnhH8?feature=oembed"
                                            frameborder="0" allowfullscreen="" id="gdlr-video-91786"></iframe>
                                </p>
                                <div class="clear"></div>
                                <div class="gdlr-space" style="margin-top: 60px;"></div>

                                <div class="clear"></div>
                            </div>
                        </div>

                    </div>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>
        </div>

        <!-- Below Sidebar Section-->

    </div>
    <!-- gdlr-content -->
    <div class="clear"></div>
</div>