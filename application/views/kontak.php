<div class="content-wrapper">
            <div class="gdlr-content">

                <!-- Above Sidebar Section-->

                <div class="gdlr-page-title-wrapper">
                    <div class="gdlr-page-title-overlay"></div>
                    <div class="gdlr-page-title-container container">
                        <h1 class="gdlr-page-title">Kontak Bawaslu Bangli</h1>
                    </div>
                </div>

                <!-- Sidebar With Content Section-->
                <div class="with-sidebar-wrapper">
                    <div class="with-sidebar-container container">
                        <div class="with-sidebar-left twelve columns">
                            <div class="with-sidebar-content eight columns">
                                <section id="content-section-1">
                                    <div class="section-container container">
                                        <div class="gdlr-item gdlr-content-item" style="margin-bottom: 60px;">
                                            <p>
                                                </p><div class="clear"></div>
                                                <div class="gdlr-space" style="margin-top: -22px;"></div>
                                                <h5 class="gdlr-heading-shortcode " style="font-weight: bold;">Selahkan Mengsisi Data Berikut Ini.</h5>
                                                <div class="clear"></div>
                                                <div class="gdlr-space" style="margin-top: 25px;"></div>
                                                <div role="form" class="wpcf7" id="wpcf7-f5-o1" lang="en-US" dir="ltr">
                                                    <div class="screen-reader-response"></div>
                                                    <form action="https://demo.goodlayers.com/finanza/contact-page-2/#wpcf7-f5-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                                        <div style="display: none;">
                                                            <input type="hidden" name="_wpcf7" value="5">
                                                            <input type="hidden" name="_wpcf7_version" value="5.0.5">
                                                            <input type="hidden" name="_wpcf7_locale" value="en_US">
                                                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f5-o1">
                                                            <input type="hidden" name="_wpcf7_container_post" value="0">
                                                        </div>
                                                        <p>
                                                            <span class="wpcf7-form-control-wrap placeholder"><input type="text" name="placeholder" value="Name*" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span> </p>
                                                        <p>
                                                            <span class="wpcf7-form-control-wrap placeholder"><input type="email" name="placeholder" value="Email*" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span> </p>
                                                        <p>
                                                            <span class="wpcf7-form-control-wrap placeholder"><input type="text" name="placeholder" value="Subject*" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span> </p>
                                                        <p>
                                                            <span class="wpcf7-form-control-wrap placeholder"><textarea name="placeholder" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false">Message*</textarea></span> </p>
                                                        <p>
                                                            <input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit">
                                                        </p>
                                                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                                                    </form>
                                                </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </section>
                            </div>

                            <div class="gdlr-sidebar gdlr-left-sidebar four columns">
                                <div class="gdlr-item-start-content sidebar-left-item">
                                    <div id="text-6" class="widget widget_text gdlr-item gdlr-widget">
                                        <h3 class="gdlr-widget-title">Before Contacting Us</h3>
                                        <div class="clear"></div>
                                        <div class="textwidget">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Non equidem invideo, miror magis posuere velit aliquet.</div>
                                    </div>
                                    <div id="text-7" class="widget widget_text gdlr-item gdlr-widget">
                                        <h3 class="gdlr-widget-title">Contact Information</h3>
                                        <div class="clear"></div>
                                        <div class="textwidget">
                                            <p>184 Main Collins Street West Victoria 8007 Australia</p>
                                            <p><i class="gdlr-icon fa fa-phone" style="color: #444444; font-size: 16px; "></i> 1800-222-222</p>
                                            <p><i class="gdlr-icon fa fa-envelope" style="color: #444444; font-size: 16px; "></i> contact@versatilewptheme.com</p>
                                            <p><i class="gdlr-icon fa fa-clock-o" style="color: #444444; font-size: 16px; "></i> Everyday 9:00-17:00</p>
                                        </div>
                                    </div>
                                    <div id="text-8" class="widget widget_text gdlr-item gdlr-widget">
                                        <h3 class="gdlr-widget-title">Social Media</h3>
                                        <div class="clear"></div>
                                        <div class="textwidget"><a href="http://facebook.com/goodlayers"><i class="gdlr-icon fa fa-facebook" style="color: #444444; font-size: 28px; "></i></a> <a href="http://twitter.com/goodlayers"><i class="gdlr-icon fa fa-twitter" style="color: #444444; font-size: 28px; "></i></a> <a href="#"><i class="gdlr-icon fa fa-dribbble" style="color: #444444; font-size: 28px; "></i></a> <a href="#"><i class="gdlr-icon fa fa-pinterest" style="color: #444444; font-size: 28px; "></i></a> <a href="#"><i class="gdlr-icon fa fa-google-plus" style="color: #444444; font-size: 28px; "></i></a> <a href="#"><i class="gdlr-icon fa fa-instagram" style="color: #444444; font-size: 28px; "></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="clear"></div>
                    </div>
                </div>

                <!-- Below Sidebar Section-->
                <div class="below-sidebar-wrapper">
                    <section id="content-section-2">
                        <div class="gdlr-full-size-wrapper gdlr-show-all " style="padding-bottom: 0px;  background-color: #ffffff; ">
                            <div class="gdlr-item gdlr-content-item" style="margin-bottom: 0px;">
                                <div style="overflow:hidden;width: 100%;position: relative;">
                                    <iframe style="width:100%; height:400px; " src="https://maps.google.com/maps?hl=en&amp;q=London%2C%20United%20Kingdom+(Title)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                    <div style="position: absolute;width: 80%;bottom: 20px;left: 0;right: 0;margin-left: auto;margin-right: auto;color: #000;">
                                    </div>
                                    <style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </section>
                </div>

            </div>
            <!-- gdlr-content -->
            <div class="clear"></div>
        </div>