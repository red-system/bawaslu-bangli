<div class="gdlr-page-title-wrapper">
            <div class="gdlr-page-title-overlay"></div>
            <div class="gdlr-page-title-container container">
                <h1 class="gdlr-page-title">Berita Bawaslu Bangli</h1>
            </div>
        </div>
        <!-- is search -->
        <div class="content-wrapper">
            <div class="gdlr-content">

                <!-- Above Sidebar Section-->

                <!-- Sidebar With Content Section-->
                <div class="with-sidebar-wrapper">
                    <div class="with-sidebar-container container">
                        <div class="with-sidebar-left eight columns">
                            <div class="with-sidebar-content twelve columns">
                                <section id="content-section-1">
                                    <div class="section-container container">
                                        <div class="blog-item-wrapper">
                                            <div class="blog-item-holder">
                                                <div class="gdlr-isotope" data-type="blog" data-layout="fitRows">
                                                    <div class="clear"></div>
                                                    <div class="twelve columns">
                                                        <div class="gdlr-item gdlr-blog-grid">
                                                            <div class="gdlr-ux gdlr-blog-grid-ux">
                                                                <article id="post-852" class="post-852 post type-post status-publish format-standard has-post-thumbnail hentry category-fit-row tag-blog tag-life-style">
                                                                    <div class="gdlr-standard-style">
                                                                        <div class="gdlr-blog-thumbnail">
                                                                            <a href="<?php echo base_url(); ?>berita/detail"> <img src="<?php echo base_url();?>assets/upload/shutterstock_226039468-1024x682.jpg" alt="" width="960" height="639" /></a>
                                                                        </div>

                                                                        <header class="post-header">
                                                                            <h3 class="gdlr-blog-title"><a href="<?php echo base_url(); ?>berita/detail">Donec luctus imperdiet</a></h3>

                                                                            <div class="gdlr-blog-info gdlr-info"><span class="gdlr-sep">/</span>
                                                                                <div class="blog-info blog-date"><span class="gdlr-head">Date</span><a href="<?php echo base_url(); ?>berita/detail">03 Dec 2013</a></div><span class="gdlr-sep"></span>
                                                                               
                                                                                <div class="clear"></div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </header>
                                                                        <!-- entry-header -->

                                                                        <div class="gdlr-blog-content">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere voluntate liceret: sese habere. Magna pars studiorum, prodita quaerimus. Magna pars studiorum, prodita quaerimus. Fabio vel iudice vincam, sunt in culpa qui officia. Vivamus sagittis lacus vel augue laoreet...
                                                                            <div class="clear"></div><a href="<?php echo base_url(); ?>berita/detail" class="excerpt-read-more">Read More →</a></div>
                                                                    </div>
                                                                </article>
                                                                <!-- #post -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                    <div class="twelve columns">
                                                        <div class="gdlr-item gdlr-blog-grid">
                                                            <div class="gdlr-ux gdlr-blog-grid-ux">
                                                                <article id="post-862" class="post-862 post type-post status-publish format-standard has-post-thumbnail hentry category-blog category-fit-row tag-blog tag-link tag-news">
                                                                    <div class="gdlr-standard-style">
                                                                        <div class="gdlr-blog-thumbnail">
                                                                            <a href="<?php echo base_url(); ?>berita/detail"> <img src="<?php echo base_url();?>assets/upload/shutterstock_146762978-1024x732.jpg" alt="" width="960" height="686" /></a>
                                                                        </div>

                                                                        <header class="post-header">
                                                                            <h3 class="gdlr-blog-title"><a href="<?php echo base_url(); ?>berita/detail">Magna pars studiorum</a></h3>

                                                                            <div class="gdlr-blog-info gdlr-info"><span class="gdlr-sep">/</span>
                                                                                <div class="blog-info blog-date"><span class="gdlr-head">Date</span><a href="<?php echo base_url(); ?>berita/detail">03 Dec 2013</a></div><span class="gdlr-sep"></span>
                                                                            
                                                                                <div class="clear"></div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </header>
                                                                        <!-- entry-header -->

                                                                        <div class="gdlr-blog-content">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere voluntate liceret: sese habere. Magna pars studiorum, prodita quaerimus. Magna pars studiorum, prodita quaerimus. Fabio vel iudice vincam, sunt in culpa qui officia. Vivamus sagittis lacus vel augue laoreet...
                                                                            <div class="clear"></div><a href="<?php echo base_url(); ?>berita/detail" class="excerpt-read-more">Read More →</a></div>
                                                                    </div>
                                                                </article>
                                                                <!-- #post -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                            <div class="gdlr-pagination"><span aria-current='page' class='page-numbers current'>1</span>
                                                <a class='page-numbers' href='page/2/index.html'>2</a>
                                                <a class="next page-numbers" href="page/2/index.html">Next &rsaquo;</a></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </section>
                            </div>

                            <div class="clear"></div>
                        </div>

                        <div class="gdlr-sidebar gdlr-right-sidebar four columns">
                            <div class="gdlr-item-start-content sidebar-right-item">
                                <div id="search-3" class="widget widget_search gdlr-item gdlr-widget">
                                    <div class="gdl-search-form">
                                        <form method="get"  action="https://demo.goodlayers.com/finanza/">
                                            <div class="search-text" id="search-text">
                                                <input type="text" name="s" id="s" autocomplete="off" data-default="Type keywords..." />
                                            </div>
                                            <input type="submit" id="searchsubmit" value="" />
                                            <div class="clear"></div>
                                        </form>
                                    </div>
                                </div>
                                <div id="text-2" class="widget widget_text gdlr-item gdlr-widget">
                                    <h3 class="gdlr-widget-title">Tentang Bawaslu Bangli</h3>
                                    <div class="clear"></div>
                                    <div class="textwidget">Sed posuere consectetur est at lobortis. Donec id elit non mi porta gravida at eget metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id dolor id nibh ultricies vehicula ut id elit.</div>
                                </div>
                                <div id="gdlr-popular-post-widget-6" class="widget widget_gdlr-popular-post-widget gdlr-item gdlr-widget">
                                    <h3 class="gdlr-widget-title">Berita Terkait</h3>
                                    <div class="clear"></div>
                                    <div class="gdlr-recent-post-widget">
                                        <div class="recent-post-widget">
                                            <div class="recent-post-widget-thumbnail">
                                                <a href="<?php echo base_url(); ?>berita/detail"><img src="<?php echo base_url();?>assets/upload/shutterstock_172217969-150x150.jpg" alt="" width="150" height="150" /></a>
                                            </div>
                                            <div class="recent-post-widget-content">
                                                <div class="recent-post-widget-title"><a href="<?php echo base_url(); ?>berita/detail">Nihilne te nocturnu</a></div>
                                                
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="recent-post-widget">
                                            <div class="recent-post-widget-thumbnail">
                                                <a href="<?php echo base_url(); ?>berita/detail"><img src="<?php echo base_url();?>assets/upload/shutterstock_174482054-150x150.jpg" alt="" width="150" height="150" /></a>
                                            </div>
                                            <div class="recent-post-widget-content">
                                                <div class="recent-post-widget-title"><a href="<?php echo base_url(); ?>berita/detail">Eiusmod tempor incidunt</a></div>
                                               
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="recent-post-widget">
                                            <div class="recent-post-widget-thumbnail">
                                                <a href="<?php echo base_url(); ?>berita/detail"><img src="<?php echo base_url();?>assets/upload/shutterstock_15900814-150x150.jpg" alt="" width="150" height="150" /></a>
                                            </div>
                                            <div class="recent-post-widget-content">
                                                <div class="recent-post-widget-title"><a href="<?php echo base_url(); ?>berita/detail">Gallery Post Format Title</a></div>
                                                
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>

                <!-- Below Sidebar Section-->

            </div>
            <!-- gdlr-content -->
            <div class="clear"></div>
        </div>
        <!-- content wrapper -->