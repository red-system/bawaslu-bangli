<section class="section-sub-banner bg-9 page-header" style="background: url(<?php echo base_url();?>assets/images/banner/<?php echo $imageheader['picture_name'];?>) " title="<?php echo $header['general_data'];?>" alt="<?php echo $header['general_data'];?>">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2><?php echo $header['general_data'];?></h2>
                <p><?php echo $header['general_desc'];?></p>
            </div>
        </div>
    </div>
</section>
<section class="section-blog bg-white">
    <div class="container">
        <div class="blog">
            <div class="row">

                <div class="col-md-8 col-md-offset-2">
                    <div class="blog-content events-content">
                        <h1 class="element-invisible">Event fullwidth</h1>
                        <article class="post">
                            <?php echo $gettingcont['general_desc'];?>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>