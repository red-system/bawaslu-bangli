<section class="section-sub-banner bg-9 page-header" style="background: url(<?php echo base_url();?>assets/images/banner/<?php echo $imageheader['picture_name'];?>) " title="<?php echo $header['general_data'];?>" alt="<?php echo $header['general_data'];?>">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2><?php echo $header['general_data'];?></h2>
                <p><?php echo $header['general_desc'];?></p>
            </div>
        </div>
    </div>
</section>
<section class="section-about">
    <div class="container">
        <div class="about">
            <div class="about-item">
                <div class="img owl-single">
                    <?php foreach ($imagefirst as $picfirst):?>
                        <div class="gallery_item">
                            <a href="<?php echo base_url();?>assets/images/offers/<?php echo $picfirst['picture_name'];?>" class="mfp-image">
                                 <img src="<?php echo base_url();?>assets/images/offers/<?php echo $picfirst['picture_name'];?>" width="585" height="439" alt="<?php echo $first['general_data'];?>" title="<?php echo $first['general_data'];?>">
                            </a>
                        </div>
                    <?php endforeach;?>
                </div>
                <div class="text margin-top30" >
                    <h3><strong> <?php echo $first['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $first['general_desc'];?></p>
                    </div>
                </div>
            </div>
            <div class="about-item about-right">
                <div class="img owl-single">
                    <?php foreach ($imagesecond as $picsecond):?>
                        <div class="gallery_item">
                            <a href="<?php echo base_url();?>assets/images/offers/<?php echo $picsecond['picture_name'];?>" class="mfp-image">
                                 <img src="<?php echo base_url();?>assets/images/offers/<?php echo $picsecond['picture_name'];?>" width="585" height="439" alt="<?php echo $second['general_data'];?>" title="<?php echo $second['general_data'];?>">
                            </a>
                        </div>
                    <?php endforeach;?>
                </div>
                <div class="text">
                    <h3><strong> <?php echo $second['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $second['general_desc'];?></p>
                    </div>
                </div>
            </div>
            <div class="about-item">
                <div class="img owl-single">
                    <?php foreach ($imagethird as $picthird):?>
                        <div class="gallery_item">
                            <a href="<?php echo base_url();?>assets/images/offers/<?php echo $picthird['picture_name'];?>" class="mfp-image">
                                 <img src="<?php echo base_url();?>assets/images/offers/<?php echo $picthird['picture_name'];?>" width="585" height="439" alt="<?php echo $third['general_data'];?>" title="<?php echo $third['general_data'];?>">
                            </a>
                        </div>
                    <?php endforeach;?>
                </div>
                <div class="text">
                    <h3><strong> <?php echo $third['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $third['general_desc'];?></p>
                    </div>
                </div>
            </div>
            <div class="about-item about-right">
                <div class="img owl-single">
                    <?php foreach ($imagefourth as $picfourth):?>
                        <div class="gallery_item">
                            <a href="<?php echo base_url();?>assets/images/offers/<?php echo $picfourth['picture_name'];?>" class="mfp-image">
                                 <img src="<?php echo base_url();?>assets/images/offers/<?php echo $picfourth['picture_name'];?>" width="585" height="439" alt="<?php echo $fourth['general_data'];?>" title="<?php echo $fourth['general_data'];?>">
                            </a>
                        </div>
                    <?php endforeach;?>
                </div>
                <div class="text">
                    <h3><strong> <?php echo $fourth['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $fourth['general_desc'];?></p>
                    </div>
                </div>
            </div>
            <div class="about-item">
                <div class="img owl-single">
                    <?php foreach ($imagefifth as $picfifth):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/offers/<?php echo $picfifth['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/offers/<?php echo $picfifth['picture_name'];?>" width="585" height="439"alt="<?php echo $fifth['general_data'];?>" title="<?php echo $fifth['general_data'];?>">
                        </a>
                    </div>
                    <?php endforeach;?>
                </div>
                <div class="text">
                    <h3><strong> <?php echo $fifth['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $fifth['general_desc'];?></p>
                    </div>
                </div>
            </div>
            <div class="about-item about-right">
                <div class="img owl-single">
                    <?php foreach ($imagesixth as $picsixth):?>
                        <div class="gallery_item">
                            <a href="<?php echo base_url();?>assets/images/offers/<?php echo $picsixth['picture_name'];?>" class="mfp-image">
                                 <img src="<?php echo base_url();?>assets/images/offers/<?php echo $picsixth['picture_name'];?>" width="585" height="439" alt="<?php echo $sixth['general_data'];?>" title="<?php echo $sixth['general_data'];?>">
                            </a>
                        </div>
                    <?php endforeach;?>
                </div>
                <div class="text">
                    <h3><strong> <?php echo $sixth['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $sixth['general_desc'];?></p>
                    </div>
                </div>
            </div>
            <div class="about-item">
                <div class="img owl-single">
                    <?php foreach ($imageseventh as $picseventh):?>
                        <div class="gallery_item">
                            <a href="<?php echo base_url();?>assets/images/offers/<?php echo $picseventh['picture_name'];?>" class="mfp-image">
                                 <img src="<?php echo base_url();?>assets/images/offers/<?php echo $picseventh['picture_name'];?>" width="585" height="439" alt="<?php echo $seventh['general_data'];?>" title="<?php echo $seventh['general_data'];?>">
                            </a>
                        </div>
                    <?php endforeach;?>
                </div>
                <div class="text">
                    <h3><strong> <?php echo $seventh['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $seventh['general_desc'];?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>