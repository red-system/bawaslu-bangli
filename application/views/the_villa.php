<section class="section-sub-banner bg-9 page-header" style="background: url(<?php echo base_url();?>assets/images/banner/<?php echo $imageheader['picture_name'];?>) " title="<?php echo $header['general_data'];?>" alt="<?php echo $header['general_data'];?>">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2><?php echo $header['general_data'];?></h2>
                <p><?php echo $header['general_desc'];?></p>
            </div>
        </div>
    </div>
</section>
<section class="section-about">
    <div class="container">
        <div class="about">
            <div class="about-item">
                <div class="img owl-single">
                    <?php foreach ($imageabout as $picabout):?>
                        <div class="gallery_item">
                            <a href="<?php echo base_url();?>assets/images/about/<?php echo $picabout['picture_name'];?>" class="mfp-image">
                                 <img src="<?php echo base_url();?>assets/images/about/<?php echo $picabout['picture_name'];?>" width="585" height="439" alt="<?php echo $about['general_data'];?>" title="<?php echo $about['general_data'];?>">
                            </a>
                        </div>
                    <?php endforeach;?>
                </div>
                <div class="text margin-top30" >
                    <h2 class="heading"><?php echo $about['general_data'];?></h2>
                    <div class="desc">
                        <?php echo $about['general_desc'];?>
                    </div>
                </div>
            </div>
            <div class="about-item about-right">
                <div class="img owl-single">
                    <?php foreach ($imagearea as $picarea):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/about/<?php echo $picarea['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/about/<?php echo $picarea['picture_name'];?>" width="585" height="439" alt="<?php echo $area['general_data'];?>" title="<?php echo $area['general_data'];?>">
                        </a>
                    </div>
                    <?php endforeach;?>
                </div>
                <div class="text">
                    <h2 class="heading"><?php echo $area['general_data'];?></h2>
                    <div class="desc">
                        <?php echo $area['general_desc'];?>
                    </div>
                </div>
            </div>
            <div class="about-item">
                <div class="img owl-single">
                    <?php foreach ($imagebuilding as $picbuilding):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/about/<?php echo $picbuilding['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/about/<?php echo $picbuilding['picture_name'];?>" width="585" height="439" alt="<?php echo $building['general_data'];?>" title="<?php echo $building['general_data'];?>">
                        </a>
                    </div>
                    <?php endforeach;?>
                </div>
                <div class="text">
                    <h2 class="heading"><?php echo $building['general_data'];?></h2>
                    <div class="desc">
                        <?php echo $building['general_desc'];?>
                    </div>
                </div>
            </div>
            <div class="about-item about-right">
                <div class="img owl-single">
                    <?php foreach ($imagecomfort as $piccomfort):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/about/<?php echo $piccomfort['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/about/<?php echo $piccomfort['picture_name'];?>" width="585" height="439" alt="<?php echo $comfort['general_data'];?>" title="<?php echo $comfort['general_data'];?>">
                        </a>
                    </div>
                    <?php endforeach;?>
                </div>
                <div class="text">
                    <h2 class="heading"><?php echo $comfort['general_data'];?></h2>
                    <div class="desc">
                        <?php echo $comfort['general_desc'];?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-team">
    <div class="container">
        <div class="text">
            <h2 class="heading text-center"><?php echo $groundplan['title'];?></h2><br><br>
            <div class="slideshow-container">
                <div class="mySlides fadeslide">
                    <h3 class="text-center font-groundplan" ><?php if( $lang == 'de') { echo 'Erdgeschoss'; } else  if ( $lang == 'en' ) { echo 'First Floor'; } ?></h3>
                    <div class="map">
                        <div class="map-item map-item1">
                            <a class="marker marker1" href="#marker1"></a>
                            <aside id="marker1" class="map-popup">
                                <h3 class="popup-title font-size20"><?php if( $lang == 'de') { echo 'Lager'; } else  if ( $lang == 'en' ) { echo 'Storage'; } ?></h3>
                            </aside>
                        </div>
                        <div class="map-item map-item2">
                            <a class="marker marker2" href="#marker2"></a>
                            <aside id="marker2" class="map-popup">
                                <h3 class="popup-title font-size20"><?php if( $lang == 'de') { echo 'Bar'; } else  if ( $lang == 'en' ) { echo 'Bar'; } ?></h3>
                            </aside>
                        </div>
                        <div class="map-item map-item3">
                            <a class="marker marker3" href="#marker3"></a>
                            <aside id="marker3" class="map-popup">
                                <h3 class="popup-title font-size20"><?php if( $lang == 'de') { echo 'Gäste-WC'; } else  if ( $lang == 'en' ) { echo 'Guest Toilet'; } ?></h3>
                            </aside>
                        </div>
                        <div class="map-item map-item4">
                            <a class="marker marker4" href="#marker4"></a>
                            <aside id="marker4" class="map-popup">
                                <h3 class="popup-title font-size20"><?php if( $lang == 'de') { echo 'Kühlschrank'; } else  if ( $lang == 'en' ) { echo 'Fridge'; } ?></h3>
                            </aside>
                        </div>
                        <div class="map-item map-item5">
                            <a class="marker marker5" href="#marker5"></a>

                            <aside id="marker5" class="map-popup">
                                <h3 class="popup-title font-size20"><?php if( $lang == 'de') { echo 'Wohnzimmer'; } else  if ( $lang == 'en' ) { echo 'Living Room'; } ?></h3>
                            </aside>
                        </div>
                        <div class="map-item map-item6">
                            <a class="marker marker6" href="#marker6"></a>

                            <aside id="marker6" class="map-popup">
                                <h3 class="popup-title font-size20"><?php if( $lang == 'de') { echo 'Minibar'; } else  if ( $lang == 'en' ) { echo 'Mini Bar'; } ?></h3>
                            </aside>
                        </div>
                        <div class="map-item map-item7">
                            <a class="marker marker7" href="#marker7"></a>
                            <aside id="marker7" class="map-popup">
                                <h3 class="popup-title font-size20"><?php if( $lang == 'de') { echo 'Terrasse'; } else  if ( $lang == 'en' ) { echo 'Terrace'; } ?></h3>
                            </aside>
                        </div>
                        <div class="map-item map-item8">
                            <a class="marker marker8" href="#marker8"></a>
                            <aside id="marker8" class="map-popup">
                                <h3 class="popup-title font-size20"><?php if( $lang == 'de') { echo 'Schlafzimmer'; } else  if ( $lang == 'en' ) { echo 'Bedroom'; } ?></h3>
                            </aside>
                        </div>
                        <div class="map-item map-item9">
                            <a class="marker marker9" href="#marker9"></a>
                            <aside id="marker9" class="map-popup">
                                <h3 class="popup-title font-size20"><?php if( $lang == 'de') { echo 'das großes Schlafzimmer'; } else  if ( $lang == 'en' ) { echo 'Master Bedroom'; } ?></h3>
                            </aside>
                        </div>
                        <div class="map-item map-item10">
                            <a class="marker marker10" href="#marker10"></a>
                            <aside id="marker10" class="map-popup">
                                <h3 class="popup-title font-size20"><?php if( $lang == 'de') { echo 'das Badezimmer'; } else  if ( $lang == 'en' ) { echo 'Bathroom'; } ?></h3>
                            </aside>
                        </div>
                        <div class="map-item map-item11">
                            <a class="marker marker11" href="#marker11"></a>
                            <aside id="marker11" class="map-popup">
                                <h3 class="popup-title font-size20"><?php if( $lang == 'de') { echo 'das Badezimmer'; } else  if ( $lang == 'en' ) { echo 'Bathroom'; } ?></h3>
                            </aside>
                        </div>
                        <div class="map-item map-item12">
                            <a class="marker marker12" href="#marker12"></a>
                            <aside id="marker12" class="map-popup">
                                <h3 class="popup-title font-size20"><?php if( $lang == 'de') { echo 'Pavillon'; } else  if ( $lang == 'en' ) { echo 'Gazebo'; } ?></h3>
                            </aside>
                        </div>
                        <div class="map-item map-item13">
                            <a class="marker marker13" href="#marker13"></a>
                            <aside id="marker13" class="map-popup">
                                <h3 class="popup-title font-size20"><?php if( $lang == 'de') { echo 'Pool-Deck'; } else  if ( $lang == 'en' ) { echo 'Pool Deck'; } ?></h3>
                            </aside>
                        </div>
                        <div class="map-item map-item14">
                            <a class="marker marker14" href="#marker14"></a>
                            <aside id="marker14" class="map-popup">
                                <h3 class="popup-title font-size20"><?php if( $lang == 'de') { echo 'Pool'; } else  if ( $lang == 'en' ) { echo 'Pool'; } ?></h3>
                            </aside>
                        </div>
                        <div class="map-item map-item15">
                            <a class="marker marker15" href="#marker15"></a>
                            <aside id="marker15" class="map-popup">
                                <h3 class="popup-title font-size20"><?php if( $lang == 'de') { echo 'Dusche'; } else  if ( $lang == 'en' ) { echo 'Shower'; } ?></h3>
                            </aside>
                        </div>
                        <img src="<?php echo base_url();?>assets/images/Groundplan-first-floor-samari-hill-villas.png" width="1000" height="872" alt="Samari Hill Villas <?php echo $groundplan['title'];?>" title="Samari Hill Villas <?php echo $groundplan['title'];?>">
                    </div>
                </div>
                <div class="mySlides fadeslide">
                    <h3 class="text-center font-groundplan" ><?php if( $lang == 'de') { echo 'Zweiter Stock'; } else  if ( $lang == 'en' ) { echo 'Second Floor'; } ?></h3>
                    <div class="map2">
                        <div class="map2-item map2-item1">
                            <a class="marker2 marker21" href="#marker21"></a>
                            <aside id="marker21" class="map2-popup">
                                <h3 class="popup-title font-size20"><?php if( $lang == 'de') { echo 'Schlafzimmer'; } else  if ( $lang == 'en' ) { echo 'Bedroom'; } ?></h3>
                            </aside>
                        </div>
                        <img src="<?php echo base_url();?>assets/images/Groundplan-second-floor-samari-hill-villas.png" alt="Samari Hill Villas <?php echo $groundplan['title'];?>" width="1000" height="872" title="Samari Hill Villas <?php echo $groundplan['title'];?>">
                    </div>
                </div>
                <a class="prevslide" onclick="plusSlides(-1)">&#10094;</a>
                <a class="nextslide" onclick="plusSlides(1)">&#10095;</a>
            </div>
            <br>
            <div style="text-align:center">
                <span class="dotslide" onclick="currentSlide(1)"></span> 
                <span class="dotslide" onclick="currentSlide(2)"></span> 
            </div>
        </div>
    </div>
</section>
<section class="section-team">
    <div class="container">
        <div class="team">
            <h2 class="heading text-center"><?php echo $teammember['title'];?></h2>
            <div class="team_content">
                <div class="row">
                    <?php foreach ($teams as $team):?>
                        <div class="col-xs-6 col-md-3">
                            <div class="team_item text-center">
                                <div class="img">
                                    <a href=""><img src="<?php echo base_url();?>assets/images/team/<?php echo $team['main_image'];?>" width="200" height="200" alt="<?php echo $team['general_data'];?>" title="<?php echo $team['general_data'];?>"></a>
                                </div> 
                                <div class="text">
                                    <h2><?php echo $team['general_data'];?></h2>
                                    <span><?php if( $lang == 'de') { echo $team['general_sub_desc']; } else  if ( $lang == 'en' ) { echo $team['general_desc']; } ?></span>
                                    <div class="team-share">
                                        <a href="mailto:<?php echo $team['email_link'];?>" target="_blank"><i class="fa fa-envelope"></i></a>
                                        <a href="<?php echo $team['facebook_link'];?>" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="<?php echo $team['twitter_link'];?>" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="<?php echo $team['instagram_link'];?>" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</section>