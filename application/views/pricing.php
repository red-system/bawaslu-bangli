<section class="section-sub-banner bg-9 page-header" style="background: url(<?php echo base_url();?>assets/images/banner/<?php echo $imageheader['picture_name'];?>) " title="<?php echo $header['general_data'];?>" alt="<?php echo $header['general_data'];?>">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2><?php echo $header['general_data'];?></h2>
                <p><?php echo $header['general_desc'];?></p>
            </div>
        </div>
    </div>
</section>
<section class="section-about">
    <div class="container">
        <div class="about">
            <div class="about-item ">
                <div class="img owl-single">
                    <?php foreach ($imagefirst as $picfirst):?>
                        <div class="gallery_item">
                            <a href="<?php echo base_url();?>assets/images/pricing/<?php echo $picfirst['picture_name'];?>" class="mfp-image">
                                 <img src="<?php echo base_url();?>assets/images/pricing/<?php echo $picfirst['picture_name'];?>" width="585" height="439" alt="<?php echo $first['general_data'];?>" title="<?php echo $first['general_data'];?>">
                            </a>
                        </div>
                    <?php endforeach;?>
                </div>
                <div class="text margin-top30">
                    <h3><strong> <?php echo $first['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $first['general_desc'];?></p>
                    </div>
                </div>
            </div>
            <div class="about-item about-right">
                <div class="img owl-single">
                    <?php foreach ($imagesecond as $picsecond):?>
                        <div class="gallery_item">
                            <a href="<?php echo base_url();?>assets/images/pricing/<?php echo $picsecond['picture_name'];?>" class="mfp-image">
                                 <img src="<?php echo base_url();?>assets/images/pricing/<?php echo $picsecond['picture_name'];?>" width="585" height="439" alt="<?php echo $second['general_data'];?>" title="<?php echo $second['general_data'];?>">
                            </a>
                        </div>
                    <?php endforeach;?>
                </div>
                <div class="text margin-top30">
                    <h3><strong> <?php echo $second['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $second['general_desc'];?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="text">
                <br><h3><strong> <?php echo $pricinglist['general_data'];?></strong></h3><br>
                <div class="desc">
                  	<a href="<?php echo base_url();?>assets/<?php echo $pricinglist['main_image'];?>" class="mfp-image">
                        <img src="<?php echo base_url();?>assets/images/icons-pdf.png" width="96" height="96" alt="<?php echo $pricinglist['general_data'];?>" title="<?php echo $pricinglist['general_data'];?>">
                    </a> 
                </div>
            </div>
        </div>
    </div>
</section>