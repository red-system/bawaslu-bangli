<section class="section-sub-banner bg-9 page-header" style="background: url(<?php echo base_url();?>assets/images/banner/<?php echo $imageheader['picture_name'];?>) " title="<?php echo $header['general_data'];?>" alt="<?php echo $header['general_data'];?>">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2><?php echo $header['general_data'];?></h2>
                <p><?php echo $header['general_desc'];?></p>
            </div>
        </div>
    </div>
</section>
<section class="section-about">
    <div class="container">
        <div class="about">
            <div class="about-item">
                <div class="img owl-single">
                    <?php foreach ($imageabout as $picabout):?>
                        <div class="gallery_item">
                            <a href="<?php echo base_url();?>assets/images/location/<?php echo $picabout['picture_name'];?>" class="mfp-image">
                                 <img src="<?php echo base_url();?>assets/images/location/<?php echo $picabout['picture_name'];?>" width="585" height="439" alt="<?php echo $about['general_data'];?>" title="<?php echo $about['general_data'];?>">
                            </a>
                        </div>
                    <?php endforeach;?>
                </div>
                <div class="text margin-top30">
                    <h3><strong> <?php echo $about['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $about['general_desc'];?></p>
                    </div>
                </div>
            </div>
            <div class="about-item about-right">
                <div class="img owl-single">
                    <?php foreach ($imagecondition as $picabout):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/location/<?php echo $picabout['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/location/<?php echo $picabout['picture_name'];?>" width="585" height="439" alt="<?php echo $condition['general_data'];?>" title="<?php echo $condition['general_data'];?>">
                        </a>
                    </div>
                    <?php endforeach;?>
                </div>
                <div class="text">
                    <h3><strong> <?php echo $condition['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $condition['general_desc'];?></p>
                    </div>
                </div>
            </div>
            <div class="text-center margin-top60">
                    <h3><strong> <?php echo $map['general_data'];?></strong></h3>
                <div class="desc">
                    <p><?php echo $map['general_desc'];?></p>
                </div>
            </div>
            <div class="text-center">
                <img src="<?php echo base_url();?>assets/images/<?php echo $image['picture_name'];?>" width="563" height="389" alt="Bali map Airport to Samari Hill Villas" title="Bali map Airport to Samari Hill Villas">
            </div>
        </div>
    </div>
</section>