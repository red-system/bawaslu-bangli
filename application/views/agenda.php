<div class="gdlr-page-title-wrapper">
    <div class="gdlr-page-title-overlay"></div>
    <div class="gdlr-page-title-container container">
        <h1 class="gdlr-page-title">Agenda Bawaslu Bangli</h1>
    </div>
</div>

<div class="gdlr-content">

                <!-- Above Sidebar Section-->

                <!-- Sidebar With Content Section-->
                <div class="with-sidebar-wrapper">
                    <div class="with-sidebar-container container">
                        <div class="with-sidebar-left">
                            <div class="with-sidebar-content twelve columns">
                                <section id="content-section-1">
                                    <div class="section-container container">
                                        <div class="blog-item-wrapper">
                                            <div class="blog-item-holder">
                                                <div class="gdlr-isotope" data-type="blog" data-layout="fitRows">
                                                    <div class="three columns">
                                                        <div class="gdlr-item gdlr-blog-grid">
                                                            <div class="gdlr-ux gdlr-blog-grid-ux">
                                                                <article id="post-852" class="post-852 post type-post status-publish format-standard has-post-thumbnail hentry category-fit-row tag-blog tag-life-style">
                                                                    <div class="gdlr-standard-style">
                                                                        <div class="gdlr-blog-thumbnail">
                                                                            <a href="<?php echo base_url();?>agenda/detail"> <img src="<?php echo base_url();?>assets/upload/shutterstock_226039468-400x300.jpg" alt="" width="400" height="300"></a>
                                                                        </div>

                                                                        <header class="post-header">
                                                                            <h3 class="gdlr-blog-title"><a href="<?php echo base_url();?>agenda/detail">Donec luctus imperdiet</a></h3>

                                                                            <div class="gdlr-blog-info gdlr-info"><span class="gdlr-sep">/</span>
                                                                                <div class="blog-info blog-date"><span class="gdlr-head">Date</span><a href="<?php echo base_url();?>agenda/detail">03 Dec 2013</a></div><span class="gdlr-sep">/</span>
                                                                                <div class="blog-info blog-comment"><span class="gdlr-head">Comments</span><a href="<?php echo base_url();?>agenda/detail">2</a></div>
                                                                                <div class="clear"></div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </header>
                                                                        <!-- entry-header -->

                                                                        <div class="gdlr-blog-content">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere voluntate liceret: sese habere....
                                                                            <div class="clear"></div><a href="<?php echo base_url();?>agenda/detail" class="excerpt-read-more">Read More →</a></div>
                                                                    </div>
                                                                </article>
                                                                <!-- #post -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="three columns">
                                                        <div class="gdlr-item gdlr-blog-grid">
                                                            <div class="gdlr-ux gdlr-blog-grid-ux">
                                                                <article id="post-862" class="post-862 post type-post status-publish format-standard has-post-thumbnail hentry category-blog category-fit-row tag-blog tag-link tag-news">
                                                                    <div class="gdlr-standard-style">
                                                                        <div class="gdlr-blog-thumbnail">
                                                                            <a href="<?php echo base_url();?>agenda/detail"> <img src="<?php echo base_url();?>assets/upload/shutterstock_146762978-400x300.jpg" alt="" width="400" height="300"></a>
                                                                        </div>

                                                                        <header class="post-header">
                                                                            <h3 class="gdlr-blog-title"><a href="<?php echo base_url();?>agenda/detail">Magna pars studiorum</a></h3>

                                                                            <div class="gdlr-blog-info gdlr-info"><span class="gdlr-sep">/</span>
                                                                                <div class="blog-info blog-date"><span class="gdlr-head">Date</span><a href="<?php echo base_url();?>agenda/detail">03 Dec 2013</a></div><span class="gdlr-sep">/</span>
                                                                                <div class="blog-info blog-comment"><span class="gdlr-head">Comments</span><a href="<?php echo base_url();?>agenda/detail">2</a></div>
                                                                                <div class="clear"></div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </header>
                                                                        <!-- entry-header -->

                                                                        <div class="gdlr-blog-content">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere voluntate liceret: sese habere....
                                                                            <div class="clear"></div><a href="<?php echo base_url();?>agenda/detail" class="excerpt-read-more">Read More →</a></div>
                                                                    </div>
                                                                </article>
                                                                <!-- #post -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="three columns">
                                                        <div class="gdlr-item gdlr-blog-grid">
                                                            <div class="gdlr-ux gdlr-blog-grid-ux">
                                                                <article id="post-859" class="post-859 post type-post status-publish format-standard has-post-thumbnail hentry category-blog category-fit-row">
                                                                    <div class="gdlr-standard-style">
                                                                        <div class="gdlr-blog-thumbnail">
                                                                            <a href="<?php echo base_url();?>agenda/detail"> <img src="<?php echo base_url();?>assets/upload/blog-1-400x300.jpg" alt="" width="400" height="300"></a>
                                                                        </div>

                                                                        <header class="post-header">
                                                                            <h3 class="gdlr-blog-title"><a href="<?php echo base_url();?>agenda/detail">Sedial eiusmod tempor</a></h3>

                                                                            <div class="gdlr-blog-info gdlr-info"><span class="gdlr-sep">/</span>
                                                                                <div class="blog-info blog-date"><span class="gdlr-head">Date</span><a href="<?php echo base_url();?>agenda/detail">03 Dec 2013</a></div><span class="gdlr-sep">/</span>
                                                                                <div class="blog-info blog-comment"><span class="gdlr-head">Comment</span><a href="<?php echo base_url();?>agenda/detail">0</a></div>
                                                                                <div class="clear"></div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </header>
                                                                        <!-- entry-header -->

                                                                        <div class="gdlr-blog-content">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere voluntate liceret: sese habere....
                                                                            <div class="clear"></div><a href="<?php echo base_url();?>agenda/detail" class="excerpt-read-more">Read More →</a></div>
                                                                    </div>
                                                                </article>
                                                                <!-- #post -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="three columns">
                                                        <div class="gdlr-item gdlr-blog-grid">
                                                            <div class="gdlr-ux gdlr-blog-grid-ux">
                                                                <article id="post-858" class="post-858 post type-post status-publish format-standard has-post-thumbnail hentry category-fit-row category-life-style tag-animal tag-safari">
                                                                    <div class="gdlr-standard-style">
                                                                        <div class="gdlr-blog-thumbnail">
                                                                            <a href="<?php echo base_url();?>agenda/detail"> <img src="<?php echo base_url();?>assets/upload/shutterstock_174482054-400x300.jpg" alt="" width="400" height="300"></a>
                                                                        </div>

                                                                        <header class="post-header">
                                                                            <h3 class="gdlr-blog-title"><a href="<?php echo base_url();?>agenda/detail">Eiusmod tempor incidunt</a></h3>

                                                                            <div class="gdlr-blog-info gdlr-info"><span class="gdlr-sep">/</span>
                                                                                <div class="blog-info blog-date"><span class="gdlr-head">Date</span><a href="<?php echo base_url();?>agenda/detail">03 Dec 2013</a></div><span class="gdlr-sep">/</span>
                                                                                <div class="blog-info blog-comment"><span class="gdlr-head">Comments</span><a href="<?php echo base_url();?>agenda/detail">3</a></div>
                                                                                <div class="clear"></div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </header>
                                                                        <!-- entry-header -->

                                                                        <div class="gdlr-blog-content">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere voluntate liceret: sese habere....
                                                                            <div class="clear"></div><a href="<?php echo base_url();?>agenda/detail" class="excerpt-read-more">Read More →</a></div>
                                                                    </div>
                                                                </article>
                                                                <!-- #post -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="three columns">
                                                        <div class="gdlr-item gdlr-blog-grid">
                                                            <div class="gdlr-ux gdlr-blog-grid-ux" style="opacity: 1; padding-top: 0px; margin-bottom: 0px;">
                                                                <article id="post-43" class="post-43 post type-post status-publish format-standard has-post-thumbnail hentry category-blog category-fit-row category-life-style tag-blog tag-life-style tag-news">
                                                                    <div class="gdlr-standard-style">
                                                                        <div class="gdlr-blog-thumbnail">
                                                                            <a href="<?php echo base_url();?>agenda/detail"> <img src="<?php echo base_url();?>assets/upload/shutterstock_251068444-400x300.jpg" alt="" width="400" height="300"></a>
                                                                        </div>

                                                                        <header class="post-header">
                                                                            <h3 class="gdlr-blog-title"><a href="<?php echo base_url();?>agenda/detail">Standard Post Format Title</a></h3>

                                                                            <div class="gdlr-blog-info gdlr-info"><span class="gdlr-sep">/</span>
                                                                                <div class="blog-info blog-date"><span class="gdlr-head">Date</span><a href="<?php echo base_url();?>agenda/detail">10 Nov 2013</a></div><span class="gdlr-sep">/</span>
                                                                                <div class="blog-info blog-comment"><span class="gdlr-head">Comment</span><a href="<?php echo base_url();?>agenda/detail">1</a></div>
                                                                                <div class="clear"></div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </header>
                                                                        <!-- entry-header -->

                                                                        <div class="gdlr-blog-content">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere voluntate liceret: sese habere....
                                                                            <div class="clear"></div><a href="<?php echo base_url();?>agenda/detail" class="excerpt-read-more">Read More →</a></div>
                                                                    </div>
                                                                </article>
                                                                <!-- #post -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="three columns">
                                                        <div class="gdlr-item gdlr-blog-grid">
                                                            <div class="gdlr-ux gdlr-blog-grid-ux" style="opacity: 1; padding-top: 0px; margin-bottom: 0px;">
                                                                <article id="post-862" class="post-862 post type-post status-publish format-standard has-post-thumbnail hentry category-blog category-fit-row tag-blog tag-link tag-news">
                                                                    <div class="gdlr-standard-style">
                                                                        <div class="gdlr-blog-thumbnail">
                                                                            <a href="<?php echo base_url();?>agenda/detail"> <img src="<?php echo base_url();?>assets/upload/shutterstock_146762978-400x300.jpg" alt="" width="400" height="300"></a>
                                                                        </div>

                                                                        <header class="post-header">
                                                                            <h3 class="gdlr-blog-title"><a href="<?php echo base_url();?>agenda/detail">Magna pars studiorum</a></h3>

                                                                            <div class="gdlr-blog-info gdlr-info"><span class="gdlr-sep">/</span>
                                                                                <div class="blog-info blog-date"><span class="gdlr-head">Date</span><a href="<?php echo base_url();?>agenda/detail">03 Dec 2013</a></div><span class="gdlr-sep">/</span>
                                                                                <div class="blog-info blog-comment"><span class="gdlr-head">Comments</span><a href="<?php echo base_url();?>agenda/detail">2</a></div>
                                                                                <div class="clear"></div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </header>
                                                                        <!-- entry-header -->

                                                                        <div class="gdlr-blog-content">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere voluntate liceret: sese habere....
                                                                            <div class="clear"></div><a href="<?php echo base_url();?>agenda/detail" class="excerpt-read-more">Read More →</a></div>
                                                                    </div>
                                                                </article>
                                                                <!-- #post -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="three columns">
                                                        <div class="gdlr-item gdlr-blog-grid">
                                                            <div class="gdlr-ux gdlr-blog-grid-ux" style="opacity: 1; padding-top: 0px; margin-bottom: 0px;">
                                                                <article id="post-852" class="post-852 post type-post status-publish format-standard has-post-thumbnail hentry category-fit-row tag-blog tag-life-style">
                                                                    <div class="gdlr-standard-style">
                                                                        <div class="gdlr-blog-thumbnail">
                                                                            <a href="<?php echo base_url();?>agenda/detail"> <img src="<?php echo base_url();?>assets/upload/shutterstock_226039468-400x300.jpg" alt="" width="400" height="300"></a>
                                                                        </div>

                                                                        <header class="post-header">
                                                                            <h3 class="gdlr-blog-title"><a href="<?php echo base_url();?>agenda/detail">Donec luctus imperdiet</a></h3>

                                                                            <div class="gdlr-blog-info gdlr-info"><span class="gdlr-sep">/</span>
                                                                                <div class="blog-info blog-date"><span class="gdlr-head">Date</span><a href="<?php echo base_url();?>agenda/detail">03 Dec 2013</a></div><span class="gdlr-sep">/</span>
                                                                                <div class="blog-info blog-comment"><span class="gdlr-head">Comments</span><a href="<?php echo base_url();?>agenda/detail">2</a></div>
                                                                                <div class="clear"></div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </header>
                                                                        <!-- entry-header -->

                                                                        <div class="gdlr-blog-content">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere voluntate liceret: sese habere....
                                                                            <div class="clear"></div><a href="<?php echo base_url();?>agenda/detail" class="excerpt-read-more">Read More →</a></div>
                                                                    </div>
                                                                </article>
                                                                <!-- #post -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="three columns">
                                                        <div class="gdlr-item gdlr-blog-grid">
                                                            <div class="gdlr-ux gdlr-blog-grid-ux" style="opacity: 1; padding-top: 0px; margin-bottom: 0px;">
                                                                <article id="post-862" class="post-862 post type-post status-publish format-standard has-post-thumbnail hentry category-blog category-fit-row tag-blog tag-link tag-news">
                                                                    <div class="gdlr-standard-style">
                                                                        <div class="gdlr-blog-thumbnail">
                                                                            <a href="<?php echo base_url();?>agenda/detail"> <img src="<?php echo base_url();?>assets/upload/shutterstock_146762978-400x300.jpg" alt="" width="400" height="300"></a>
                                                                        </div>

                                                                        <header class="post-header">
                                                                            <h3 class="gdlr-blog-title"><a href="<?php echo base_url();?>agenda/detail">Magna pars studiorum</a></h3>

                                                                            <div class="gdlr-blog-info gdlr-info"><span class="gdlr-sep">/</span>
                                                                                <div class="blog-info blog-date"><span class="gdlr-head">Date</span><a href="<?php echo base_url();?>agenda/detail">03 Dec 2013</a></div><span class="gdlr-sep">/</span>
                                                                                <div class="blog-info blog-comment"><span class="gdlr-head">Comments</span><a href="<?php echo base_url();?>agenda/detail">2</a></div>
                                                                                <div class="clear"></div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </header>
                                                                        <!-- entry-header -->

                                                                        <div class="gdlr-blog-content">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere voluntate liceret: sese habere....
                                                                            <div class="clear"></div><a href="<?php echo base_url();?>agenda/detail" class="excerpt-read-more">Read More →</a></div>
                                                                    </div>
                                                                </article>
                                                                <!-- #post -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-pagination"><span aria-current="page" class="page-numbers current">1</span>
                                                <a class="page-numbers" href="page/2/index.html">2</a>
                                                <a class="next page-numbers" href="page/2/index.html">Next ›</a></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </section>
                            </div>

                            <div class="clear"></div>
                        </div>
        
                        <div class="clear"></div>
                    </div>
                </div>

                <!-- Below Sidebar Section-->

            </div>