<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'home';
$route['sejarah'] = 'history';
$route['sekretariat'] = 'sekretariat';
$route['struktur'] = 'struktur';
$route['tentang'] = 'about';
$route['publikasi'] = 'publikasi';
$route['publikasi/detail'] = 'publikasi/detail';
$route['pengumuman'] = 'pengumuman';
$route['pengumuman/detail'] = 'pengumuman/detail';
$route['agenda'] = 'agenda';
$route['agenda/detail'] = 'agenda/detail';
$route['artikel'] = 'artikel';
$route['berita'] = 'berita';
$route['foto'] = 'foto';
$route['video'] = 'video';
$route['kontak-kami'] = 'kontak';
$route['kecamatan'] = 'kecamatan';
$route['kecamatan/detail'] = 'kecamatan/detail';


$langs = array('en/','de/');

foreach($langs as $lang) {
	$route[$lang.'offers'] = "offers";
	$route[$lang.'gallery'] = "gallery";
	$route[$lang.'info'] = "info";
	$route[$lang.'guest-book'] = "guest_book";
	$route[$lang.'guest-book/guestbook-addprocess'] = "guest_book/guestbook_addprocess";
	$route[$lang.'guest-book/index'] = "guest_book/index";
	$route[$lang.'guest-book/index/(:any)'] = "guest_book/index/$1";
	$route[$lang.'pricing'] = "pricing";
	$route[$lang.'contact'] = "contact";
	$route[$lang.'info/send-reservation'] = "info/send_reservation";
	$route[$lang.'getting-there'] = "getting_there";
	$route[$lang.'a-dream-comes-true'] = "dream";
	$route[$lang.'privacy-policy'] = "privacy_policy";
}

$route['^en$'] = $route['default_controller'];
$route['^de$'] = $route['default_controller'];

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
