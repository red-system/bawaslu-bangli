<?php
class Backend_getting_theremodel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_getting_thereall($slug){
    $query = $this->db->get_where('tb_general_data', array('general_name' => $slug));
    return $query->result_array();
  }

  public function getrow_getting_thereall($slug){
    $query = $this->db->get_where('tb_general_data', array('general_sub_name' => $slug));
    return $query->row_array();
  }

  public function getrow_getting_therefront($title,$slug){
    $query = $this->db->get_where('tb_general_data', array('general_name' => $title,'general_lang' => $slug));
    return $query->row_array();
  }

  public function get_image($slug){
    $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
    return $query->result_array();
  }

  public function getrow_image($slug){
    $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
    return $query->row_array();
  }

  public function get_getting_there_by_id($id){
      $query = $this->db->get_where('tb_general_data', array('general_id' => $id));
      return $query->row_array();
  }

  public function get_getting_thereimage_by_id($id){
      $query = $this->db->get_where('tb_picture', array('general_id' => $id));
      return $query->row_array();
  }

  public function get_getting_thereimage_by_refid($slug){
      $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
      return $query->row_array();
  }

  public function update_getting_there(){
    $data = array(
      'general_data' => $this->input->post('name'),
      'general_desc' => $this->input->post('description')
    );
    $this->db->where('general_id', $this->input->post('general_id'));
    return $this->db->update('tb_general_data',$data);
  }

  public function update_headergetting_there($additional_data){
    $data  = array(
       'picture_name'      => $additional_data['file_name']
    );
   
    $this->db->where('general_ref_id','header_getting_there');
    return $this->db->update('tb_picture',$data);
  }

  public function update_reservationgetting_there(){
    $data = array(
      'general_data' => $this->input->post('name'),
      'general_sub_data' => $this->input->post('line1'),
      'general_desc' => $this->input->post('line2'),
      'general_sub_desc' => $this->input->post('line3'),
      'general_link' => $this->input->post('line4'),
      'main_image' => $this->input->post('line5'),
      'secondary_image' => $this->input->post('line6'),
      'general_password' => $this->input->post('line7'),
      'general_additional_info' => $this->input->post('button')
    );
    $this->db->where('general_id', $this->input->post('general_id'));
    return $this->db->update('tb_general_data',$data);
  }

  public function update_getting_therecontent(){
    $data = array(
      'general_data' => $this->input->post('name'),
      'general_sub_data' => $this->input->post('name1'),
      'general_desc' => $this->input->post('description'),
      'general_sub_desc' => $this->input->post('description1')
    );
    $this->db->where('general_id', $this->input->post('general_id'));
    return $this->db->update('tb_general_data',$data);
  }

  public function image_add($data){
    $this->db->insert('tb_picture', $data);
    return;
  }

  public function image_delete($id){
    $this->db->where('general_id', $id);
    $this->db->delete('tb_picture');
    return true;
  }

  
}