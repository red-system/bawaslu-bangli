<?php
class Backend_homemodel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function getrow_homefront($title,$slug){
    $query = $this->db->get_where('tb_general_data', array('general_name' => $title,'general_lang' => $slug));
    return $query->row_array();
  }

  public function get_homerow($slug){
    $query = $this->db->get_where('tb_general_data',array('general_name' => $slug));
    return $query->row_array();
  }

  public function get_home_by_id($id){
    $query = $this->db->get_where('tb_general_data',array('general_id' => $id));
    return $query->row_array();
  }

  public function get_homearray($slug){
    $query = $this->db->get_where('tb_general_data', array('general_name' => $slug));
    return $query->result_array();
  }

  public function homepicture_insert($data)
  {
      $this->db->insert('tb_general_data', $data);
  }

  public function homepicture_update($where, $data) {
      $this->db->where($where)->update('tb_general_data', $data);
  }

  public function homepicture_delete($id){
    $this->db->where('general_id', $id);
    $this->db->delete('tb_general_data');
    return true;
  }

  public function get_metarow($slug){
    $query = $this->db->get_where('tb_general_data',array('general_name' => $slug));
    return $query->row_array();
  }

  public function update_metapage(){
    $data = array(
      'general_sub_name' => $this->input->post('titleenglish'),
      'general_link' => $this->input->post('titlegerman'),
      'general_data' => $this->input->post('descriptionenglish'),
      'general_sub_data' => $this->input->post('descriptiongerman'),
      'general_desc' => $this->input->post('keywordenglish'),
      'general_sub_desc' => $this->input->post('keywordgerman')
    );
    $this->db->where('general_id', $this->input->post('general_id'));
    return $this->db->update('tb_general_data',$data);
  }
  
}