<?php
class Backend_menutitlemodel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_menutitle($slug){
    $query = $this->db->get_where('tb_kamus',array('param' => $slug));
    return $query->row_array();
  }

  public function getrow_menutitlefront($title,$slug){
    $query = $this->db->get_where('tb_kamus', array('kategori' => $title,'id_lang' => $slug));
    return $query->row_array();
  }

  public function update_homeen(){
    $data = array(
      'title' => $this->input->post('homeen')
    );
    $this->db->where('param','homeen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_homede(){
    $data = array(
      'title' => $this->input->post('homede')
    );
    $this->db->where('param','homede');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_paradiseen(){
    $data = array(
      'title' => $this->input->post('paradiseen')
    );
    $this->db->where('param','paradiseen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_paradisede(){
    $data = array(
      'title' => $this->input->post('paradisede')
    );
    $this->db->where('param','paradisede');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_serviceen(){
    $data = array(
      'title' => $this->input->post('serviceen')
    );
    $this->db->where('param','serviceen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_servicede(){
    $data = array(
      'title' => $this->input->post('servicede')
    );
    $this->db->where('param','servicede');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_abouten(){
    $data = array(
      'title' => $this->input->post('abouten')
    );
    $this->db->where('param','abouten');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_aboutde(){
    $data = array(
      'title' => $this->input->post('aboutde')
    );
    $this->db->where('param','aboutde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_thevillaen(){
    $data = array(
      'title' => $this->input->post('thevillaen')
    );
    $this->db->where('param','thevillaen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_thevillade(){
    $data = array(
      'title' => $this->input->post('thevillade')
    );
    $this->db->where('param','thevillade');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_locationen(){
    $data = array(
      'title' => $this->input->post('locationen')
    );
    $this->db->where('param','locationen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_locationde(){
    $data = array(
      'title' => $this->input->post('locationde')
    );
    $this->db->where('param','locationde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_generousen(){
    $data = array(
      'title' => $this->input->post('generousen')
    );
    $this->db->where('param','generousen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_generousde(){
    $data = array(
      'title' => $this->input->post('generousde')
    );
    $this->db->where('param','generousde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_luxuriousen(){
    $data = array(
      'title' => $this->input->post('luxuriousen')
    );
    $this->db->where('param','luxuriousen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_luxuriousde(){
    $data = array(
      'title' => $this->input->post('luxuriousde')
    );
    $this->db->where('param','luxuriousde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_bathroomsen(){
    $data = array(
      'title' => $this->input->post('bathroomsen')
    );
    $this->db->where('param','bathroomsen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_bathroomsde(){
    $data = array(
      'title' => $this->input->post('bathroomsde')
    );
    $this->db->where('param','bathroomsde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_kitchenen(){
    $data = array(
      'title' => $this->input->post('kitchenen')
    );
    $this->db->where('param','kitchenen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_kitchende(){
    $data = array(
      'title' => $this->input->post('kitchende')
    );
    $this->db->where('param','kitchende');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_bedroomsen(){
    $data = array(
      'title' => $this->input->post('bedroomsen')
    );
    $this->db->where('param','bedroomsen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_bedroomsde(){
    $data = array(
      'title' => $this->input->post('bedroomsde')
    );
    $this->db->where('param','bedroomsde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_rooftopen(){
    $data = array(
      'title' => $this->input->post('rooftopen')
    );
    $this->db->where('param','rooftopen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_rooftopde(){
    $data = array(
      'title' => $this->input->post('rooftopde')
    );
    $this->db->where('param','rooftopde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_offersen(){
    $data = array(
      'title' => $this->input->post('offersen')
    );
    $this->db->where('param','offersen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_offersde(){
    $data = array(
      'title' => $this->input->post('offersde')
    );
    $this->db->where('param','offersde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_galleryen(){
    $data = array(
      'title' => $this->input->post('galleryen')
    );
    $this->db->where('param','galleryen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_galleryde(){
    $data = array(
      'title' => $this->input->post('galleryde')
    );
    $this->db->where('param','galleryde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_infoen(){
    $data = array(
      'title' => $this->input->post('infoen')
    );
    $this->db->where('param','infoen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_infode(){
    $data = array(
      'title' => $this->input->post('infode')
    );
    $this->db->where('param','infode');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_availabilityen(){
    $data = array(
      'title' => $this->input->post('availabilityen')
    );
    $this->db->where('param','availabilityen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_availabilityde(){
    $data = array(
      'title' => $this->input->post('availabilityde')
    );
    $this->db->where('param','availabilityde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_guestbooken(){
    $data = array(
      'title' => $this->input->post('guestbooken')
    );
    $this->db->where('param','guestbooken');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_guestbookde(){
    $data = array(
      'title' => $this->input->post('guestbookde')
    );
    $this->db->where('param','guestbookde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_pricingen(){
    $data = array(
      'title' => $this->input->post('pricingen')
    );
    $this->db->where('param','pricingen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_pricingde(){
    $data = array(
      'title' => $this->input->post('pricingde')
    );
    $this->db->where('param','pricingde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_reservationen(){
    $data = array(
      'title' => $this->input->post('reservationen')
    );
    $this->db->where('param','reservationen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_reservationde(){
    $data = array(
      'title' => $this->input->post('reservationde')
    );
    $this->db->where('param','reservationde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_contacten(){
    $data = array(
      'title' => $this->input->post('contacten')
    );
    $this->db->where('param','contacten');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_contactde(){
    $data = array(
      'title' => $this->input->post('contactde')
    );
    $this->db->where('param','contactde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_gettingen(){
    $data = array(
      'title' => $this->input->post('gettingen')
    );
    $this->db->where('param','gettingen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_gettingde(){
    $data = array(
      'title' => $this->input->post('gettingde')
    );
    $this->db->where('param','gettingde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_dreamen(){
    $data = array(
      'title' => $this->input->post('dreamen')
    );
    $this->db->where('param','dreamen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_dreamde(){
    $data = array(
      'title' => $this->input->post('dreamde')
    );
    $this->db->where('param','dreamde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_privacyen(){
    $data = array(
      'title' => $this->input->post('privacyen')
    );
    $this->db->where('param','privacyen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_privacyde(){
    $data = array(
      'title' => $this->input->post('privacyde')
    );
    $this->db->where('param','privacyde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_groundplanen(){
    $data = array(
      'title' => $this->input->post('groundplanen')
    );
    $this->db->where('param','groundplanen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_groundplande(){
    $data = array(
      'title' => $this->input->post('groundplande')
    );
    $this->db->where('param','groundplande');
    return $this->db->update('tb_kamus',$data);
  }
  



}