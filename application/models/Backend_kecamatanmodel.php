<?php

class Backend_kecamatanmodel extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }


    public function district_list()
    {
        return $this->db->order_by('district_id', 'ASC')->get('tb_district')->result_array();
    }

    public function district_insert($data)
    {
        $this->db->insert('tb_district', $data);
    }

    public function district_row($where) {
        return $this->db->where($where)->get('tb_district')->row_array();
    }

    public function district_update($where, $data) {
        $this->db->where($where)->update('tb_district', $data);
    }

    public function district_delete($where) {
        $this->db->where($where)->delete('tb_district');
    }


}