<?php
class Backend_headerfootermodel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_headerfooter($slug){
    $query = $this->db->get_where('tb_general_data',array('general_sub_name' => $slug));
    return $query->row_array();
  }

  public function getrow_headerfooterfront($title,$slug){
    $query = $this->db->get_where('tb_general_data', array('general_name' => $title,'general_lang' => $slug));
    return $query->row_array();
  }

  public function get_sosiallink($slug =  FALSE){
    if($slug === FALSE){
      $query = $this->db->get_where('tb_social_link', array('published'=>'checked'));
      return $query->result_array();
    }
    $query = $this->db->get_where('tb_social_link', array('social_name' => $slug));
    return $query->row_array();
  }

  public function update_address(){
    $data = array(
      'general_data' => $this->input->post('address')
    );
    $this->db->where('general_sub_name','address');
    return $this->db->update('tb_general_data',$data);
  }

  public function update_phone1(){
    $data = array(
      'general_data' => $this->input->post('phone1')
    );
    $this->db->where('general_sub_name','phone1');
    return $this->db->update('tb_general_data',$data);
  }

  public function update_phone2(){
    $data = array(
      'general_data' => $this->input->post('phone2')
    );
    $this->db->where('general_sub_name','phone2');
    return $this->db->update('tb_general_data',$data);
  }

  public function update_email(){
    $data = array(
      'general_data' => $this->input->post('email')
    );
    $this->db->where('general_sub_name','email');
    return $this->db->update('tb_general_data',$data);
  }

  public function update_workingdays(){
    $data = array(
      'general_desc' => $this->input->post('working')
    );
    $this->db->where('general_sub_name','working_days');
    return $this->db->update('tb_general_data',$data);
  }

  public function update_quotede(){
    $data = array(
      'general_desc' => $this->input->post('quotede')
    );
    $this->db->where('general_sub_name','quotede');
    return $this->db->update('tb_general_data',$data);
  }

  public function update_buttonen(){
    $data = array(
      'general_data' => $this->input->post('buttonen')
    );
    $this->db->where('general_sub_name','buttonen');
    return $this->db->update('tb_general_data',$data);
  }

  // public function update_logoheader($additional_data){
  //   $data  = array(
  //      'main_image'      => $additional_data['file_name']
  //   );
   
  //   $this->db->where('general_sub_name','logo_header');
  //   return $this->db->update('tb_general_data',$data);
  // }

  public function update_logoheader($where, $data) {
      $this->db->where('general_sub_name',$where)->update('tb_general_data', $data);
  }

  public function update_logofavicon($where, $data) {
      $this->db->where('general_sub_name',$where)->update('tb_general_data', $data);
  }

  // public function update_logofavicon($additional_data,$additional_datafavicon){
  //   $data  = array(
  //      'main_image'      => $additional_data['file_name'],
  //      'secondary_image' => $additional_datafavicon['file_name']
  //   );
   
  //   $this->db->where('general_sub_name','logo_footer');
  //   return $this->db->update('tb_general_data',$data);
  // }

  public function update_facebook(){
    $data = array(
      'social_link' => $this->input->post('facebook'),
      'published' => $this->input->post('publishedfacebook')
    );

    $this->db->where('social_name','Facebook');
    return $this->db->update('tb_social_link',$data);
  }

  public function update_instagram(){
    $data = array(
      'social_link' => $this->input->post('instagram'),
      'published' => $this->input->post('publishedinstagram')
    );

    $this->db->where('social_name','Instagram');
    return $this->db->update('tb_social_link',$data);
  }

  public function update_twitter(){
    $data = array(
      'social_link' => $this->input->post('twitter'),
      'published' => $this->input->post('publishedtwitter')
    );

    $this->db->where('social_name','Twitter');
    return $this->db->update('tb_social_link',$data);
  }

  public function update_youtube(){
    $data = array(
      'social_link' => $this->input->post('youtube'),
      'published' => $this->input->post('publishedyoutube')
    );

    $this->db->where('social_name','Youtube');
    return $this->db->update('tb_social_link',$data);
  }



}