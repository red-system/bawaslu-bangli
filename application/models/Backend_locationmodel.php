<?php
class Backend_locationmodel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_locationall($slug){
    $query = $this->db->get_where('tb_general_data', array('general_name' => $slug));
    return $query->result_array();
  }

  public function getrow_locationaall($slug){
    $query = $this->db->get_where('tb_general_data', array('general_sub_name' => $slug));
    return $query->row_array();
  }

  public function getrow_locationfront($title,$slug){
    $query = $this->db->get_where('tb_general_data', array('general_name' => $title,'general_lang' => $slug));
    return $query->row_array();
  }


  public function get_image($slug){
    $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
    return $query->result_array();
  }

  public function getrow_image($slug){
    $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
    return $query->row_array();
  }

  public function getrow_map(){
    $query = $this->db->get_where('tb_location', array('id_location' => '1'));
    return $query->row_array();
  }

  public function get_location_by_id($id){
      $query = $this->db->get_where('tb_general_data', array('general_id' => $id));
      return $query->row_array();
  }

  public function get_locationimage_by_id($id){
      $query = $this->db->get_where('tb_picture', array('general_id' => $id));
      return $query->row_array();
  }

  public function get_locationimage_by_refid($slug){
      $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
      return $query->row_array();
  }

  public function update_location(){
    $data = array(
      'general_data' => $this->input->post('name'),
      'general_desc' => $this->input->post('description')
    );
    $this->db->where('general_id', $this->input->post('general_id'));
    return $this->db->update('tb_general_data',$data);
  }

  public function update_map(){
    $data = array(
      'latitude' => $this->input->post('lat'),
      'longitude' => $this->input->post('long')
    );
    $this->db->where('id_location', '1');
    return $this->db->update('tb_location',$data);
  }

  public function update_headerlocation($additional_data){
    $data  = array(
       'picture_name'      => $additional_data['file_name']
    );
   
    $this->db->where('general_ref_id','header_location');
    return $this->db->update('tb_picture',$data);
  }

  public function update_maplocation($additional_data){
    $data  = array(
       'picture_name'      => $additional_data['file_name']
    );
   
    $this->db->where('general_ref_id','map_location');
    return $this->db->update('tb_picture',$data);
  }

  public function image_add($data){
    $this->db->insert('tb_picture', $data);
    return;
  }

  public function image_delete($id){
    $this->db->where('general_id', $id);
    $this->db->delete('tb_picture');
    return true;
  }

  
}