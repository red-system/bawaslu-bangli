<?php
class Backend_availabilitymodel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_availabilityall($slug){
    $query = $this->db->get_where('tb_general_data', array('general_name' => $slug));
    return $query->result_array();
  }

  public function getrow_availabilityall($slug){
    $query = $this->db->get_where('tb_general_data', array('general_sub_name' => $slug));
    return $query->row_array();
  }

  public function getrow_availabilityfront($title,$slug){
    $query = $this->db->get_where('tb_general_data', array('general_name' => $title,'general_lang' => $slug));
    return $query->row_array();
  }

  public function get_image($slug){
    $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
    return $query->result_array();
  }

  public function getrow_image($slug){
    $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
    return $query->row_array();
  }

  public function getrow_map(){
    $query = $this->db->get_where('tb_availability', array('id_availability' => '1'));
    return $query->row_array();
  }

  public function get_availability_by_id($id){
      $query = $this->db->get_where('tb_general_data', array('general_id' => $id));
      return $query->row_array();
  }

  public function get_availabilityimage_by_id($id){
      $query = $this->db->get_where('tb_picture', array('general_id' => $id));
      return $query->row_array();
  }

  public function get_availabilityimage_by_refid($slug){
      $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
      return $query->row_array();
  }

  public function get_reservation(){
    $this->db->order_by('updated_at', 'DESC');
    $query = $this->db->get_where('tb_reservasi');
    return $query->result_array();
  }

  public function get_reservasi_by_id($id){
      $query = $this->db->get_where('tb_reservasi', array('id_reservasi' => $id));
      return $query->row_array();
  }

  public function update_availability(){
    $data = array(
      'general_data' => $this->input->post('name'),
      'general_desc' => $this->input->post('description')
    );
    $this->db->where('general_id', $this->input->post('general_id'));
    return $this->db->update('tb_general_data',$data);
  }

  public function update_pricelist($additional_data = FALSE){
    if(isset($additional_data['file_name'])){
      $data = array(
      'general_data' => $this->input->post('titleen'),
      'general_sub_data' => $this->input->post('titlede'),
      'main_image'   => $additional_data['file_name']
      );
    } else {
      $data = array(
      'general_data' => $this->input->post('titleen'),
      'general_sub_data' => $this->input->post('titlede')     
      );
    }
    $this->db->where('general_sub_name','availabilitylist');
    return $this->db->update('tb_general_data',$data);
  }

  public function update_headeravailability($additional_data){
    $data  = array(
       'picture_name'      => $additional_data['file_name']
    );
   
    $this->db->where('general_ref_id','header_availability');
    return $this->db->update('tb_picture',$data);
  }

  public function image_add($data){
    $this->db->insert('tb_picture', $data);
    return;
  }

  public function image_delete($id){
    $this->db->where('general_id', $id);
    $this->db->delete('tb_picture');
    return true;
  }

  public function reservation_add(){
    $year =  substr ($this->input->post('arrival'),6,4);
    $date = substr ($this->input->post('arrival'),3,2);
    $month =  substr ($this->input->post('arrival'),0,2);
    $arrival = $year."-".$month."-".$date;

    $year1 =  substr ($this->input->post('departure'),6,4);
    $date1 = substr ($this->input->post('departure'),3,2);
    $month1 =  substr ($this->input->post('departure'),0,2);
    $departure = $year1."-".$month1."-".$date1;

    $data = array(
        'name' => $this->input->post('name'),
        'email' => $this->input->post('email'),
        'phone' => $this->input->post('phone'),
        'start' => $arrival,
        'end' => $departure,
        'guest' => $this->input->post('guest'),
        'message' => $this->input->post('message'),
        'status' => $this->input->post('status')
    );
    return $this->db->insert('tb_reservasi',$data);
  }

  public function update_reservation(){
    $year =  substr ($this->input->post('arrival'),6,4);
    $date = substr ($this->input->post('arrival'),3,2);
    $month =  substr ($this->input->post('arrival'),0,2);
    $arrival = $year."-".$month."-".$date;

    $year1 =  substr ($this->input->post('departure'),6,4);
    $date1 = substr ($this->input->post('departure'),3,2);
    $month1 =  substr ($this->input->post('departure'),0,2);
    $departure = $year1."-".$month1."-".$date1;

    $data = array(
        'name' => $this->input->post('name'),
        'email' => $this->input->post('email'),
        'phone' => $this->input->post('phone'),
        'start' => $arrival,
        'end' => $departure,
        'guest' => $this->input->post('guest'),
        'message' => $this->input->post('message'),
        'status' => $this->input->post('status')
    );
    $this->db->where('id_reservasi', $this->input->post('id_reservation'));
    return $this->db->update('tb_reservasi',$data);
  }

  public function daystart_add($id,$arrival){
    $data = array(
        'ref_id'  => $id,
        'day'   => $arrival,
        'info'  => 'checkIn',
        'status' => $this->input->post('status')
    );
    return $this->db->insert('tb_day',$data);
  }

  public function dayend_add($id,$departure){
    $data = array(
        'ref_id'  => $id,
        'day'   => $departure,
        'info'  => 'checkOut',
        'status' => $this->input->post('status')
    );
    return $this->db->insert('tb_day',$data);
  }

  public function day_add($data){
    $this->db->insert('tb_day',$data);
    return;
  }

  public function day_delete($arrival){
    $this->db->where('day', $arrival);
    $this->db->delete('tb_day');
    return true;
  }

  public function allday_delete($ref_id){
    $this->db->where('ref_id', $ref_id);
    $this->db->delete('tb_day');
    return true;
  }

  public function reservation_delete($id){
    $this->db->where('id_reservasi', $id);
    $this->db->delete('tb_reservasi');
    return true;
  }

  public function get_exist_day($arrival){
    $this->db->from('tb_day');
    $this->db->where('day', $arrival);
    $query = $this->db->count_all_results();
    if($query>0) {
      $exist = 1;
    } else {
      $exist = 0;
    }
    return $exist;
  }

  public function get_duplikat(){
    $query = $this->db->query("SELECT day, COUNT(*) duplikat FROM tb_day GROUP BY day HAVING duplikat > 1");  
    return $query->row_array();
  }

  public function get_forntday(){
    $query = $this->db->get_where('tb_day');
    return $query->result_array();
  }

  public function get_day_by_distinct(){
      $this->db->select('day');
      $this->db->distinct();
      $query = $this->db->get('tb_day');
      return $query->result_array();
  }

  public function get_count_day($slug){
    $query = $this->db->get_where('tb_day', array('day' => $slug));
    return $query->num_rows();
  }

  public function reservation_form_edit($id,$data){
    $this->db->where('general_id', $id);
    $this->db->update('tb_general_data',$data);
    return;
  }

  
}