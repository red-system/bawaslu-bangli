<?php

class Backend_blogmodel extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }


    public function blog_list($where)
    {
        return $this->db->where($where)->order_by('blog_id', 'ASC')->get('tb_blog')->result_array();
    }

    public function blog_insert($data)
    {
        $this->db->insert('tb_blog', $data);
    }

    public function blog_row($where) {
        return $this->db->where($where)->get('tb_blog')->row_array();
    }

    public function blog_update($where, $data) {
        $this->db->where($where)->update('tb_blog', $data);
    }

    public function blog_delete($where) {
        $this->db->where($where)->delete('tb_blog');
    }


}