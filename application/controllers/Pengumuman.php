<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengumuman extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'html','language'));
	}
    
	public function index()
	{
        $this->load->view('templates/header');
        $this->load->view('pengumuman');
        $this->load->view('templates/footer');
	}

	public function detail() {
        $this->load->view('templates/header');
        $this->load->view('pengumuman_detail');
        $this->load->view('templates/footer');
    }

	

}
