<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'html','language'));
	}
    
	public function index()
	{
        $this->load->view('templates/header');
        $this->load->view('tentang');
        $this->load->view('templates/footer');
	}

	

}
