<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class The_villa extends CI_Controller {

	// public function __construct()
 //    {
 //        parent::__construct();
 //        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
 //        {
	// 		redirect('user_auth');
 //        }
 //    }
    function __construct(){
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'html','language'));
	}
	
	public function index()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'the_villa');

		//def
		$data['lang'] = $this->lang->lang();
		$data['meta'] = $this->backend_homemodel->get_metarow('metathevilla');
		$data['address'] = $this->backend_headerfootermodel->get_headerfooter('address');
		$data['phone1'] = $this->backend_headerfootermodel->get_headerfooter('phone1');
		$data['phone2'] = $this->backend_headerfootermodel->get_headerfooter('phone2');
		$data['email'] = $this->backend_headerfootermodel->get_headerfooter('email');
		$data['logoheader'] = $this->backend_headerfootermodel->get_headerfooter('logo_header');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['social_medias'] = $this->backend_headerfootermodel->get_sosiallink();
		$data['quote'] = $this->backend_headerfootermodel->getrow_headerfooterfront('headerfooterquote',$data['lang']);
		$data['button'] = $this->backend_headerfootermodel->getrow_headerfooterfront('headerfooterbutton',$data['lang']);
		$data['home'] = $this->backend_menutitlemodel->getrow_menutitlefront('home',$data['lang']);
		$data['paradise'] = $this->backend_menutitlemodel->getrow_menutitlefront('paradise',$data['lang']);
		$data['service'] = $this->backend_menutitlemodel->getrow_menutitlefront('service',$data['lang']);
		$data['about'] = $this->backend_menutitlemodel->getrow_menutitlefront('about',$data['lang']);
		$data['thevilla'] = $this->backend_menutitlemodel->getrow_menutitlefront('thevilla',$data['lang']);
		$data['location'] = $this->backend_menutitlemodel->getrow_menutitlefront('location',$data['lang']);
		$data['generous'] = $this->backend_menutitlemodel->getrow_menutitlefront('generous',$data['lang']);
		$data['luxurious'] = $this->backend_menutitlemodel->getrow_menutitlefront('luxurious',$data['lang']);
		$data['bathrooms'] = $this->backend_menutitlemodel->getrow_menutitlefront('bathrooms',$data['lang']);
		$data['kitchen'] = $this->backend_menutitlemodel->getrow_menutitlefront('kitchen',$data['lang']);
		$data['bedrooms'] = $this->backend_menutitlemodel->getrow_menutitlefront('bedrooms',$data['lang']);
		$data['rooftop'] = $this->backend_menutitlemodel->getrow_menutitlefront('rooftop',$data['lang']);
		$data['offers'] = $this->backend_menutitlemodel->getrow_menutitlefront('offers',$data['lang']);
		$data['gallery'] = $this->backend_menutitlemodel->getrow_menutitlefront('gallery',$data['lang']);
		$data['info'] = $this->backend_menutitlemodel->getrow_menutitlefront('info',$data['lang']);
		$data['reservasi'] = $this->backend_menutitlemodel->getrow_menutitlefront('reservation',$data['lang']);
		$data['availability'] = $this->backend_menutitlemodel->getrow_menutitlefront('availability',$data['lang']);
		$data['guestbook'] = $this->backend_menutitlemodel->getrow_menutitlefront('guestbook',$data['lang']);
		$data['pricing'] = $this->backend_menutitlemodel->getrow_menutitlefront('pricing',$data['lang']);
		$data['contact'] = $this->backend_menutitlemodel->getrow_menutitlefront('contact',$data['lang']);
		$data['getting'] = $this->backend_menutitlemodel->getrow_menutitlefront('getting',$data['lang']);
		$data['dream'] = $this->backend_menutitlemodel->getrow_menutitlefront('dream',$data['lang']);
		$data['privacy'] = $this->backend_menutitlemodel->getrow_menutitlefront('privacy',$data['lang']);
		//end of def

		//content
		$data['header'] = $this->backend_thevillamodel->getrow_thevillafront('thevillaheader',$data['lang']);
		$data['imageheader'] = $this->backend_thevillamodel->getrow_image('header_villa');
		$data['about'] = $this->backend_thevillamodel->getrow_thevillafront('thevillaabout',$data['lang']);
		$data['imageabout'] = $this->backend_thevillamodel->get_image('about_villa');
		$data['area'] = $this->backend_thevillamodel->getrow_thevillafront('thevillaarea',$data['lang']);
		$data['imagearea'] = $this->backend_thevillamodel->get_image('the_area');
		$data['building'] = $this->backend_thevillamodel->getrow_thevillafront('thevillabuilding',$data['lang']);
		$data['imagebuilding'] = $this->backend_thevillamodel->get_image('the_building');
		$data['comfort'] = $this->backend_thevillamodel->getrow_thevillafront('thevillacomfort',$data['lang']);
		$data['imagecomfort'] = $this->backend_thevillamodel->get_image('comfort');
		$data['teammember'] = $this->backend_menutitlemodel->getrow_menutitlefront('teammember',$data['lang']);
		$data['groundplan'] = $this->backend_menutitlemodel->getrow_menutitlefront('groundplan',$data['lang']);
		$data['teams'] = $this->backend_thevillamodel->get_thevillaall('team_member');
		//end of content

		$this->load->view('templates/header',$data);
		$this->load->view('the_villa');
		$this->load->view('templates/footer');
	}

	

}
