<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guest_book extends CI_Controller {

	// public function __construct()
 //    {
 //        parent::__construct();
 //        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
 //        {
	// 		redirect('user_auth');
 //        }
 //    }
    function __construct(){
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'html','language'));
	}

	public function index()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'guest_book');

		//def
		$data['lang'] = $this->lang->lang();
		$data['meta'] = $this->backend_homemodel->get_metarow('metaguestbook');
		$data['address'] = $this->backend_headerfootermodel->get_headerfooter('address');
		$data['phone1'] = $this->backend_headerfootermodel->get_headerfooter('phone1');
		$data['phone2'] = $this->backend_headerfootermodel->get_headerfooter('phone2');
		$data['email'] = $this->backend_headerfootermodel->get_headerfooter('email');
		$data['logoheader'] = $this->backend_headerfootermodel->get_headerfooter('logo_header');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['social_medias'] = $this->backend_headerfootermodel->get_sosiallink();
		$data['quote'] = $this->backend_headerfootermodel->getrow_headerfooterfront('headerfooterquote',$data['lang']);
		$data['button'] = $this->backend_headerfootermodel->getrow_headerfooterfront('headerfooterbutton',$data['lang']);
		$data['home'] = $this->backend_menutitlemodel->getrow_menutitlefront('home',$data['lang']);
		$data['paradise'] = $this->backend_menutitlemodel->getrow_menutitlefront('paradise',$data['lang']);
		$data['service'] = $this->backend_menutitlemodel->getrow_menutitlefront('service',$data['lang']);
		$data['about'] = $this->backend_menutitlemodel->getrow_menutitlefront('about',$data['lang']);
		$data['thevilla'] = $this->backend_menutitlemodel->getrow_menutitlefront('thevilla',$data['lang']);
		$data['location'] = $this->backend_menutitlemodel->getrow_menutitlefront('location',$data['lang']);
		$data['generous'] = $this->backend_menutitlemodel->getrow_menutitlefront('generous',$data['lang']);
		$data['luxurious'] = $this->backend_menutitlemodel->getrow_menutitlefront('luxurious',$data['lang']);
		$data['bathrooms'] = $this->backend_menutitlemodel->getrow_menutitlefront('bathrooms',$data['lang']);
		$data['kitchen'] = $this->backend_menutitlemodel->getrow_menutitlefront('kitchen',$data['lang']);
		$data['bedrooms'] = $this->backend_menutitlemodel->getrow_menutitlefront('bedrooms',$data['lang']);
		$data['rooftop'] = $this->backend_menutitlemodel->getrow_menutitlefront('rooftop',$data['lang']);
		$data['offers'] = $this->backend_menutitlemodel->getrow_menutitlefront('offers',$data['lang']);
		$data['gallery'] = $this->backend_menutitlemodel->getrow_menutitlefront('gallery',$data['lang']);
		$data['info'] = $this->backend_menutitlemodel->getrow_menutitlefront('info',$data['lang']);
		$data['reservasi'] = $this->backend_menutitlemodel->getrow_menutitlefront('reservation',$data['lang']);
		$data['availability'] = $this->backend_menutitlemodel->getrow_menutitlefront('availability',$data['lang']);
		$data['guestbook'] = $this->backend_menutitlemodel->getrow_menutitlefront('guestbook',$data['lang']);
		$data['pricing'] = $this->backend_menutitlemodel->getrow_menutitlefront('pricing',$data['lang']);
		$data['contact'] = $this->backend_menutitlemodel->getrow_menutitlefront('contact',$data['lang']);
		$data['getting'] = $this->backend_menutitlemodel->getrow_menutitlefront('getting',$data['lang']);
		$data['dream'] = $this->backend_menutitlemodel->getrow_menutitlefront('dream',$data['lang']);
		$data['privacy'] = $this->backend_menutitlemodel->getrow_menutitlefront('privacy',$data['lang']);
		//end of def

		//content
		$data['header'] = $this->backend_guestbookmodel->getrow_guestbookfront('guestbookheader',$data['lang']);
		$data['aboutgb'] = $this->backend_guestbookmodel->getrow_guestbookfront('guestbookabout',$data['lang']);
		$data['modal'] = $this->backend_guestbookmodel->getrow_guestbookfront('guestbookform',$data['lang']);
		$data['imageheader'] = $this->backend_guestbookmodel->getrow_image('header_guestbook');

		//konfigurasi pagination
        $config['base_url'] = site_url($data['lang'].'/guest-book/index'); //site url
        $config['total_rows'] = $this->backend_guestbookmodel->record_count();//total row
        $config['per_page'] = 12;  //show record per halaman
        $config["num_links"] = 5;
 		
      	$config['full_tag_open']   = '<ul class="page-navigation text-center">';       
      	$config['full_tag_close']  = '</ul>';                
      	$config['first_link']      = '<b>First</b>';         
      	$config['first_tag_open']  = '<li>';       
      	$config['first_tag_close'] = '</li>';                
      	$config['last_link']       = '<b>Last</b>';         
      	$config['last_tag_open']   = '<li>';        
      	$config['last_tag_close']  = '</li>';                
      	$config['next_link']       = ' <i class="fa fa-arrow-right"></i> ';         
      	$config['next_tag_open']   = '<li>';        
      	$config['next_tag_close']  = '</li>';                
      	$config['prev_link']       = ' <i class="fa fa-arrow-left"></i> ';         
      	$config['prev_tag_open']   = '<li>';        
      	$config['prev_tag_close']  = '</li>';                
      	$config['cur_tag_open']    = '<li class="current-page"><a href="#">';        
      	$config['cur_tag_close']   = '</a></li>';                 
      	$config['num_tag_open']    = '<li>';        
      	$config['num_tag_close']   = '</li>';

		$this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
 		$data['data'] = $this->backend_guestbookmodel->get_guestbook_page($config["per_page"], $data['page']);   
        $data['pagination'] = $this->pagination->create_links();
        $data['captcha'] = $this->captcha();
 
		//end of content

		$this->load->view('templates/header',$data);
		$this->load->view('guest_book');
		$this->load->view('templates/footer');
	}

	function captcha() {
		$this->load->helper('captcha');
		$vals = array(
			'img_path'	 => './assets/captcha_mwz/',
			'img_url'	 => base_url().'assets/captcha_mwz/',
			'font_path'	 => './assets/fonts/mvboli.ttf',
			'font_size'     => 20,
			'img_width'=>'250', 
    	    'img_height'=>'70',
			'border' => 0, 
			'expiration' => 7200,
			'pool' => '0123456789',
			'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 40, 40)
        	),
   	      	'word_length'   => '5' );
		$cap = create_captcha($vals);
		$captcha = $cap['image'];
		$this->session->set_userdata('captcha_mwz', $cap['word']);
		return $captcha;
	}

	function captcha_check($str) {
		if($str == $this->session->userdata('captcha_mwz')) return TRUE;	
		else {
			$this->form_validation->set_message('captcha_check', 'Security Code was wrong');
			return FALSE;	
		}
	}

	
	public function guestbook_addprocess()
	{
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$title = $this->input->post('title');
		$testimonial = $this->input->post('testimonial');
		$location = $this->input->post('location');

		$dataemail = $this->backend_headerfootermodel->get_headerfooter('email');
		$penerima = $dataemail['general_data'];

		$config['upload_path']          = './assets/images/guestbook/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		$this->form_validation->set_rules('name', 'Full Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
	    $this->form_validation->set_rules('location', 'Location', 'trim|required');
	    $this->form_validation->set_rules('title', 'Testiminial Title', 'trim|required');
	    $this->form_validation->set_rules('testimonial', 'Testiminial', 'trim|required');
	    $this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_captcha_check');
	    $this->form_validation->set_error_delimiters('<span style="color:red">', '</span>');

	    if ($this->form_validation->run() == FALSE) {
		    $alert = 'Fill the Form correctly';
			$str   = array('name','email','location','title','testimonial','captcha');
			$error = array();
			foreach($str as $row) $error[$row] = form_error($row);
			$json  = array_merge(array('status'=>'error', 'alert'=>$alert), $error);
			echo json_encode($json);
		} else {
				if(!$this->upload->do_upload('picture')){
				$data['file_name']= "noimage.png";
				$res=$this->backend_guestbookmodel->guestbook_add($data);
				if($res==true)
				{
				  	/* panggil library PHPMailerAutoload.php */ 
		         	require_once(APPPATH.'libraries/PHPMailerAutoload.php');
		         
		         	$mail_admin = new PHPMailer;
		         	$mail_pelanggan = new PHPMailer; 
		         
		         	/* setting SMTP */ 
		         	$mail_admin->isSMTP(); 
		        	$mail_admin->SMTPSecure = 'ssl';
		         	$mail_admin->Host = "mail.samarihillvillas.com"; //sesuaikan dengan host email anda 
		         	$mail_admin->SMTPDebug = 0;
		         	$mail_admin->Port = "465"; //sesuaikan port 
		         	$mail_admin->SMTPAuth = true;
		         	$mail_admin->Username = "info@samarihillvillas.com"; //sesuai dengan username yang digunakan 
		         	$mail_admin->Password = "info1samari12villa123";
		         	$mail_admin->WordWrap = 50; 
		         	
		         	
		         	$mail_admin->setFrom("info@samarihillvillas.com", "Info Samari Hill Villas"); //setting pengirim email 
		         	$mail_admin->addAddress($penerima); //alamat email yang dituju 
		         	
		         	$mail_admin->isHTML(true);

		         	$mail_admin->Subject = "Information or testimonial from guests"; //subject
		         	$mail_admin->Body = "Dear Administrator,<br /><br />
			        	You have new infromation or testimonial from:<br /><br />
						--------------- TESTIMONIAL DATA ---------------<br />
		         		<table width='100%'>
		         			<tr>
		         				<td width='80'>Name</td>
		         				<td width='920'>: <strong>".$name. "</strong></td>
		         			</tr>
		         			<tr>
		         				<td width='80'>Email</td>
		         				<td width='920'>: <strong>".$email. "</strong></td>
		         			</tr>
		         			<tr>
		         				<td width='80'>Location</td>
		         				<td width='920'>: <strong>".$location. "</strong></td>
		         			</tr>
		         			<tr>
		         				<td width='80'>Testimonial Title</td>
		         				<td width='920'>: <strong>".$title. "</strong></td>
		         			</tr>
		         			<tr>
		         				<td width='80'>Testimonial</td>
		         				<td width='920'>: <strong>".$testimonial. "</strong></td>
		         			</tr>
		         		</table>
						--------------- TESTIMONIAL DATA ---------------<br /><br>
						Thank you for your prompt attention to the above.<br><br>
						Kind Regards,<br>
						". $this->input->post('name') ."
						"; //isi pesan
					$mail_admin->send(); 
			         	$alert = 'Your message has been send, thank you for the testimony given to us :)';
					
							echo json_encode(array(
								'status'=>'success',
								'alert'=>$alert
							)); 
					
				
				} else {
					$gbr = $this->upload->data();
					//Compress Image
		            $config1['image_library']='gd2';
		            $config1['source_image']='./assets/images/guestbook/'.$gbr['file_name'];
		            $config1['create_thumb']= FALSE;
		            $config1['maintain_ratio']= FALSE;
		            $config1['width']= 115;
		            $config1['height']= 115;
		            $config1['new_image']= './assets/images/guestbook/'.$gbr['file_name'];
		            $this->image_lib->initialize($config1);
				 	$this->image_lib->resize();
				 	$this->image_lib->clear();

		           	$data['file_name']= $gbr['file_name'];
		           	$res=$this->backend_guestbookmodel->guestbook_add($data);
					if($res==true)
					{
					  /* panggil library PHPMailerAutoload.php */ 
			         	require_once(APPPATH.'libraries/PHPMailerAutoload.php');
			         
			         	$mail_admin = new PHPMailer;
			         	$mail_pelanggan = new PHPMailer; 
			         
			         	/* setting SMTP */ 
			         	$mail_admin->isSMTP(); 
			        	$mail_admin->SMTPSecure = 'ssl';
			         	$mail_admin->Host = "mail.samarihillvillas.com"; //sesuaikan dengan host email anda 
			         	$mail_admin->SMTPDebug = 0;
			         	$mail_admin->Port = "465"; //sesuaikan port 
			         	$mail_admin->SMTPAuth = true;
			         	$mail_admin->Username = "info@samarihillvillas.com"; //sesuai dengan username yang digunakan 
			         	$mail_admin->Password = "info1samari12villa123";
			         	$mail_admin->WordWrap = 50; 
			         	
			         	
			         	$mail_admin->setFrom("info@samarihillvillas.com", "Info Samari Hill Villas"); //setting pengirim email 
			         	$mail_admin->addAddress($penerima); //alamat email yang dituju 
			         	
			         	$mail_admin->isHTML(true);

			         	$mail_admin->Subject = "Information or testimonial from guests"; //subject
			         	$mail_admin->Body = "Dear Administrator,<br /><br />
				        	You have new infromation or testimonial from:<br /><br />
							--------------- TESTIMONIAL DATA ---------------<br />
			         		<table width='100%'>
			         			<tr>
			         				<td width='80'>Name</td>
			         				<td width='920'>: <strong>".$name. "</strong></td>
			         			</tr>
			         			<tr>
			         				<td width='80'>Email</td>
			         				<td width='920'>: <strong>".$email. "</strong></td>
			         			</tr>
			         			<tr>
			         				<td width='80'>Location</td>
			         				<td width='920'>: <strong>".$location. "</strong></td>
			         			</tr>
			         			<tr>
			         				<td width='80'>Testimonial Title</td>
			         				<td width='920'>: <strong>".$title. "</strong></td>
			         			</tr>
			         			<tr>
			         				<td width='80'>Testimonial</td>
			         				<td width='920'>: <strong>".$testimonial. "</strong></td>
			         			</tr>
			         		</table>
							--------------- TESTIMONIAL DATA ---------------<br /><br>
							Thank you for your prompt attention to the above.<br><br>
							Kind Regards,<br>
							". $this->input->post('name') ."
							"; //isi pesan
						$mail_admin->send(); 
				         	$alert = 'Your message has been send, thank you for the testimony given to us :)';
						
								echo json_encode(array(
									'status'=>'success',
									'alert'=>$alert
								)); 
					
					}
				}
			}

		
		}
		
	}

	

}
