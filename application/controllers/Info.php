<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends CI_Controller {

	// public function __construct()
 //    {
 //        parent::__construct();
 //        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
 //        {
	// 		redirect('user_auth');
 //        }
 //    }
	function __construct(){
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'html','language'));
	}
    
	public function index()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'info');

		//def
		$data['lang'] = $this->lang->lang();
		$data['meta'] = $this->backend_homemodel->get_metarow('metaavailability');
		$data['address'] = $this->backend_headerfootermodel->get_headerfooter('address');
		$data['phone1'] = $this->backend_headerfootermodel->get_headerfooter('phone1');
		$data['phone2'] = $this->backend_headerfootermodel->get_headerfooter('phone2');
		$data['email'] = $this->backend_headerfootermodel->get_headerfooter('email');
		$data['logoheader'] = $this->backend_headerfootermodel->get_headerfooter('logo_header');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['social_medias'] = $this->backend_headerfootermodel->get_sosiallink();
		$data['quote'] = $this->backend_headerfootermodel->getrow_headerfooterfront('headerfooterquote',$data['lang']);
		$data['button'] = $this->backend_headerfootermodel->getrow_headerfooterfront('headerfooterbutton',$data['lang']);
		$data['home'] = $this->backend_menutitlemodel->getrow_menutitlefront('home',$data['lang']);
		$data['paradise'] = $this->backend_menutitlemodel->getrow_menutitlefront('paradise',$data['lang']);
		$data['service'] = $this->backend_menutitlemodel->getrow_menutitlefront('service',$data['lang']);
		$data['about'] = $this->backend_menutitlemodel->getrow_menutitlefront('about',$data['lang']);
		$data['thevilla'] = $this->backend_menutitlemodel->getrow_menutitlefront('thevilla',$data['lang']);
		$data['location'] = $this->backend_menutitlemodel->getrow_menutitlefront('location',$data['lang']);
		$data['generous'] = $this->backend_menutitlemodel->getrow_menutitlefront('generous',$data['lang']);
		$data['luxurious'] = $this->backend_menutitlemodel->getrow_menutitlefront('luxurious',$data['lang']);
		$data['bathrooms'] = $this->backend_menutitlemodel->getrow_menutitlefront('bathrooms',$data['lang']);
		$data['kitchen'] = $this->backend_menutitlemodel->getrow_menutitlefront('kitchen',$data['lang']);
		$data['bedrooms'] = $this->backend_menutitlemodel->getrow_menutitlefront('bedrooms',$data['lang']);
		$data['rooftop'] = $this->backend_menutitlemodel->getrow_menutitlefront('rooftop',$data['lang']);
		$data['offers'] = $this->backend_menutitlemodel->getrow_menutitlefront('offers',$data['lang']);
		$data['gallery'] = $this->backend_menutitlemodel->getrow_menutitlefront('gallery',$data['lang']);
		$data['info'] = $this->backend_menutitlemodel->getrow_menutitlefront('info',$data['lang']);
		$data['reservasi'] = $this->backend_menutitlemodel->getrow_menutitlefront('reservation',$data['lang']);
		$data['availability'] = $this->backend_menutitlemodel->getrow_menutitlefront('availability',$data['lang']);
		$data['guestbook'] = $this->backend_menutitlemodel->getrow_menutitlefront('guestbook',$data['lang']);
		$data['pricing'] = $this->backend_menutitlemodel->getrow_menutitlefront('pricing',$data['lang']);
		$data['contact'] = $this->backend_menutitlemodel->getrow_menutitlefront('contact',$data['lang']);
		$data['getting'] = $this->backend_menutitlemodel->getrow_menutitlefront('getting',$data['lang']);
		$data['dream'] = $this->backend_menutitlemodel->getrow_menutitlefront('dream',$data['lang']);
		$data['privacy'] = $this->backend_menutitlemodel->getrow_menutitlefront('privacy',$data['lang']);
		//end of def

		//content
		$data['header'] = $this->backend_availabilitymodel->getrow_availabilityfront('availabilityheader',$data['lang']);
		$data['imageheader'] = $this->backend_availabilitymodel->getrow_image('header_availability');
		$data['days'] = $this->backend_availabilitymodel->get_day_by_distinct();
		$data['pricinglist'] = $this->backend_pricingmodel->getrow_pricingall('pricinglist');
		$data['menulist'] = $this->backend_pricingmodel->getrow_pricingall('menulist');
		$data['reservation'] = $this->backend_contactmodel->getrow_contactfront('reservation',$data['lang']);
		$data['captcha'] = $this->captcha();
		
		//end of content

		$this->load->view('templates/header',$data);
		$this->load->view('availability');
		$this->load->view('templates/footer');
	}

	function captcha() {
		$this->load->helper('captcha');
		$vals = array(
			'img_path'	 => './assets/captcha_mwz/',
			'img_url'	 => base_url().'assets/captcha_mwz/',
			'font_path'	 => './assets/fonts/mvboli.ttf',
			'font_size'     => 20,
			'img_width'=>'282', 
    	    'img_height'=>'70',
			'border' => 0, 
			'expiration' => 7200,
			'pool' => '0123456789',
			'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 40, 40)
        	),
   	      	'word_length'   => '5' );
		$cap = create_captcha($vals);
		$captcha = $cap['image'];
		$this->session->set_userdata('captcha_mwz', $cap['word']);
		return $captcha;
	}

	function captcha_check($str) {
		if($str == $this->session->userdata('captcha_mwz')) return TRUE;	
		else {
			$this->form_validation->set_message('captcha_check', 'Security Code was wrong');
			return FALSE;	
		}
	}

	function send_reservation() 
  	{
        $dataemail = $this->backend_headerfootermodel->get_headerfooter('email');
		$penerima = $dataemail['general_data'];

        $this->form_validation->set_rules('name', 'Full Name', 'trim|required');
	    $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
	    $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
	    $this->form_validation->set_rules('arrival', 'Arrival', 'trim|required');
	    $this->form_validation->set_rules('departure', 'Departure', 'trim|required');
	    $this->form_validation->set_rules('message', 'Message', 'trim|required');
	    $this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_captcha_check');
	    $this->form_validation->set_error_delimiters('<span style="color:red">', '</span>');

        if ($this->form_validation->run() == FALSE) {
		    $alert = 'Fill the Form correctly';
			$str   = array('name','phone','email','arrival','departure','message','captcha');
			$error = array();
			foreach($str as $row) $error[$row] = form_error($row);
			$json  = array_merge(array('status'=>'error', 'alert'=>$alert), $error);
			echo json_encode($json);
		} else {
          	/* panggil library PHPMailerAutoload.php */ 
         	require_once(APPPATH.'libraries/PHPMailerAutoload.php');
         
         	$mail_admin = new PHPMailer;
         	$mail_pelanggan = new PHPMailer; 
         
         	/* setting SMTP */ 
         	$mail_admin->isSMTP(); 
        	$mail_admin->SMTPSecure = 'ssl';
         	$mail_admin->Host = "mail.samarihillvillas.com"; //sesuaikan dengan host email anda 
         	$mail_admin->SMTPDebug = 0;
         	$mail_admin->Port = "465"; //sesuaikan port 
         	$mail_admin->SMTPAuth = true;
         	$mail_admin->Username = "info@samarihillvillas.com"; //sesuai dengan username yang digunakan 
         	$mail_admin->Password = "info1samari12villa123";
         	$mail_admin->WordWrap = 50; 
         	
         	
         	$mail_admin->setFrom("info@samarihillvillas.com", "Info Samari Hill Villas"); //setting pengirim email 
         	$mail_admin->addAddress($penerima); //alamat email yang dituju 
         	
         	$mail_admin->isHTML(true);

         	$mail_admin->Subject = "Reservation from info@samarihillvillas.com"; //subject 

	        $mail_admin->Body = "Dear Reservation,<br /><br />
	        	I have browsed your website and <br />
				I would like to book your villa and<br />
				my reservation detail is as follows:<br /><br />
				--------------- RESERVATION DATA ---------------<br />
         		<table width='100%'>
         			<tr>
         				<td width='100'>Name</td>
         				<td width='900'>: <strong>".$this->input->post('name'). "</strong></td>
         			</tr>
         			<tr>
         				<td width='100'>Email</td>
         				<td width='900'>: <strong>".$this->input->post('email'). "</strong></td>
         			</tr>
         			<tr>
         				<td width='100'>Phone</td>
         				<td width='900'>: <strong>".$this->input->post('phone'). "</strong></td>
         			</tr>
         			<tr>
         				<td width='100'>Arrival</td>
         				<td width='900'>: <strong>".$this->input->post('arrival'). "</strong></td>
         			</tr>
         			<tr>
         				<td width='100'>Departure</td>
         				<td width='900'>: <strong>".$this->input->post('departure'). "</strong></td>
         			</tr>
         			<tr>
         				<td width='100'>Guest</td>
         				<td width='900'>: <strong>".$this->input->post('guest'). "</strong></td>
         			</tr>
         			<tr>
         				<td width='100'>Message</td>
         				<td width='900'>: <strong>".$this->input->post('message'). "</strong></td>
         			</tr>
         		</table>
				--------------- RESERVATION DATA ---------------<br /><br>
				Thank you for your kind attention. <br>
				I look forward to your quick response regarding my reservation.<br><br>
				Kind Regards,<br>
				". $this->input->post('name') ."<br>
				(". $this->input->post('email') . ")
				"; //isi pesan 
	 
	        if ($mail_admin->send())
         	{
         		$mail_pelanggan->isSMTP(); 
	        	$mail_pelanggan->SMTPSecure = 'ssl';
	         	$mail_pelanggan->Host = "mail.samarihillvillas.com"; //sesuaikan dengan host email anda 
	         	$mail_pelanggan->SMTPDebug = 0;
	         	$mail_pelanggan->Port = "465"; //sesuaikan port 
	         	$mail_pelanggan->SMTPAuth = true;
	         	$mail_pelanggan->Username = "info@samarihillvillas.com"; //sesuai dengan username yang digunakan 
	         	$mail_pelanggan->Password = "info1samari12villa123";
	         	$mail_pelanggan->WordWrap = 50; 
	         	
	         	$mail_pelanggan->setFrom("info@samarihillvillas.com", "Info Samari Hill Villas"); //setting pengirim email 
	         	$mail_pelanggan->addAddress($this->input->post('email')); //alamat email yang dituju 
	         	$mail_pelanggan->isHTML(true);

	         	$mail_pelanggan->Subject = "Auto-Replay from info@samarihillvillas.com"; //subject
	         	$mail_pelanggan->Body = 
	         		'Dear, ' . $this->input->post('name') . ' <br/><br/>
		        	You have just submitted your request to Samari Hill Villas <br />
		        	Our team will reply on your request soon. <br /><br />
		        	Regards,
		        	<br />
		        	Samari Hill Villas Team'
		        	; //isi pesan 
				$mail_pelanggan->send();
				$alert = 'Your reservation has been sent, wait a while for reply from us :)';
			
					echo json_encode(array(
						'status'=>'success',
						'alert'=>$alert
					));  
	 		}
        }
    }
}