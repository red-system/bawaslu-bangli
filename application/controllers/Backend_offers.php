<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_offers extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }

	public function header_offers()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheaderoffers');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['offerss'] = $this->backend_offersmodel->get_offersall('offersheader');
		$data['title'] = 'Header Offers Page';
		$data['link'] = 'header_offers_edit';
		$data['general_name'] = 'header_offers';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/offersheader');
	    $this->load->view('backend/templates/footer');
	}

	public function header_offers_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheaderoffers');

		$data['offers'] = $this->backend_offersmodel->get_offers_by_id($id);
		$data['imageoffers'] = $this->backend_offersmodel->getrow_image('header_offers');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Header Offers Page';
		$data['general_name'] = 'header_offers';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/offersheader_edit');
		$this->load->view('backend/templates/footer');
	}

	public function offers_first()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers1');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['offerss'] = $this->backend_offersmodel->get_offersall('offersfirst');
		$data['imageoffers'] = $this->backend_offersmodel->get_image('offers_first');
		$data['title'] = 'Offers First';
		$data['link'] = 'offers_first_edit';
		$data['general_name'] = 'offers_first';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/offers');
	    $this->load->view('backend/templates/footer');
	}

	public function offers_first_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers1');

		$data['offers'] = $this->backend_offersmodel->get_offers_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Offers First';
		$data['general_name'] = 'offers_first';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/offers_edit');
		$this->load->view('backend/templates/footer');
	}

	public function offers_first_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers1');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Offers First';
		$data['general_name'] = 'offers_first';

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/offersimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function offers_second()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers2');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['offerss'] = $this->backend_offersmodel->get_offersall('offerssecond');
		$data['imageoffers'] = $this->backend_offersmodel->get_image('offers_second');
		$data['title'] = 'Offers Second';
		$data['link'] = 'offers_second_edit';
		$data['general_name'] = 'offers_second';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/offers');
	    $this->load->view('backend/templates/footer');
	}

	public function offers_second_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers2');

		$data['offers'] = $this->backend_offersmodel->get_offers_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Offers second';
		$data['general_name'] = 'offers_second';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/offers_edit');
		$this->load->view('backend/templates/footer');
	}

	public function offers_second_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers2');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Offers second';
		$data['general_name'] = 'offers_second';

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/offersimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function offers_third()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers3');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['offerss'] = $this->backend_offersmodel->get_offersall('offersthird');
		$data['imageoffers'] = $this->backend_offersmodel->get_image('offers_third');
		$data['title'] = 'Offers Third';
		$data['link'] = 'offers_third_edit';
		$data['general_name'] = 'offers_third';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/offers');
	    $this->load->view('backend/templates/footer');
	}

	public function offers_third_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers3');

		$data['offers'] = $this->backend_offersmodel->get_offers_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Offers Third';
		$data['general_name'] = 'offers_third';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/offers_edit');
		$this->load->view('backend/templates/footer');
	}

	public function offers_third_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers3');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Offers Third';
		$data['general_name'] = 'offers_third';

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/offersimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function offers_fourth()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers4');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['offerss'] = $this->backend_offersmodel->get_offersall('offersfourth');
		$data['imageoffers'] = $this->backend_offersmodel->get_image('offers_fourth');
		$data['title'] = 'Offers Fourth';
		$data['link'] = 'offers_fourth_edit';
		$data['general_name'] = 'offers_fourth';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/offers');
	    $this->load->view('backend/templates/footer');
	}

	public function offers_fourth_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers4');

		$data['offers'] = $this->backend_offersmodel->get_offers_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Offers Fourth';
		$data['general_name'] = 'offers_fourth';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/offers_edit');
		$this->load->view('backend/templates/footer');
	}

	public function offers_fourth_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers4');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Offers Fourth';
		$data['general_name'] = 'offers_fourth';

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/offersimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function offers_fifth()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers5');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['offerss'] = $this->backend_offersmodel->get_offersall('offersfifth');
		$data['imageoffers'] = $this->backend_offersmodel->get_image('offers_fifth');
		$data['title'] = 'Offers Fifth';
		$data['link'] = 'offers_fifth_edit';
		$data['general_name'] = 'offers_fifth';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/offers');
	    $this->load->view('backend/templates/footer');
	}

	public function offers_fifth_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers5');

		$data['offers'] = $this->backend_offersmodel->get_offers_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Offers Fifth';
		$data['general_name'] = 'offers_fifth';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/offers_edit');
		$this->load->view('backend/templates/footer');
	}

	public function offers_fifth_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers5');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Offers Fifth';
		$data['general_name'] = 'offers_fifth';

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/offersimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function offers_sixth()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers6');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['offerss'] = $this->backend_offersmodel->get_offersall('offerssixth');
		$data['imageoffers'] = $this->backend_offersmodel->get_image('offers_sixth');
		$data['title'] = 'Offers Sixth';
		$data['link'] = 'offers_sixth_edit';
		$data['general_name'] = 'offers_sixth';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/offers');
	    $this->load->view('backend/templates/footer');
	}

	public function offers_sixth_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers6');

		$data['offers'] = $this->backend_offersmodel->get_offers_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Offers Sixth';
		$data['general_name'] = 'offers_sixth';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/offers_edit');
		$this->load->view('backend/templates/footer');
	}

	public function offers_sixth_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers6');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Offers Sixth';
		$data['general_name'] = 'offers_sixth';

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/offersimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function offers_seventh()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers7');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['offerss'] = $this->backend_offersmodel->get_offersall('offersseventh');
		$data['imageoffers'] = $this->backend_offersmodel->get_image('offers_seventh');
		$data['title'] = 'Offers Seventh';
		$data['link'] = 'offers_seventh_edit';
		$data['general_name'] = 'offers_seventh';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/offers');
	    $this->load->view('backend/templates/footer');
	}

	public function offers_seventh_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers7');

		$data['offers'] = $this->backend_offersmodel->get_offers_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Offers Seventh';
		$data['general_name'] = 'offers_seventh';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/offers_edit');
		$this->load->view('backend/templates/footer');
	}

	public function offers_seventh_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers7');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Offers Seventh';
		$data['general_name'] = 'offers_seventh';

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/offersimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function offers_eighth()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers7');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['offerss'] = $this->backend_offersmodel->get_offersall('offerseighth');
		$data['imageoffers'] = $this->backend_offersmodel->get_image('offers_eighth');
		$data['title'] = 'Offers Eighth';
		$data['link'] = 'offers_eighth_edit';
		$data['general_name'] = 'offers_eighth';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/offers');
	    $this->load->view('backend/templates/footer');
	}

	public function offers_eighth_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers7');

		$data['offers'] = $this->backend_offersmodel->get_offers_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Offers Eighth';
		$data['general_name'] = 'offers_eighth';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/offers_edit');
		$this->load->view('backend/templates/footer');
	}

	public function offers_eighth_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offers7');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Offers Eighth';
		$data['general_name'] = 'offers_eighth';

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/offersimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function update_offers()
	{
		$res=$this->backend_offersmodel->update_offers();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function update_pageheaderoffers()
	{
		$this->backend_offersmodel->update_offers();

		$config['upload_path']          = './assets/images/banner/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('image')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/banner/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= TRUE;
            $config1['width']= 1920;
            $config1['new_image']= './assets/images/banner/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $pic= $this->backend_offersmodel->get_offersimage_by_refid('header_offers');
            $datapic = $pic['picture_name'];
            unlink('./assets/images/banner/'.$datapic);
			$this->backend_offersmodel->update_headeroffers($data);
		}

		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect($this->agent->referrer());

	}

	public function image_addprocess()
	{
		$general_name = $this->input->post('general_name');

		$post = $this->input->post();
	
		$config['upload_path']          = './assets/images/offers';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;

		$this->upload->initialize($config);
		if($this->upload->do_upload('file'))
		{
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/offers/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
           	$config1['width']= 585;
            $config1['height']= 439;
            $config1['new_image']= './assets/images/offers/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

           	$data = array(	'picture_name'		=> $gbr['file_name'],
							'general_ref_id'	=> $general_name,
						);
			$this->backend_offersmodel->image_add($data);
		}
	}

	public function image_delete($id)
	{
		$image= $this->backend_offersmodel->get_offersimage_by_id($id);
        $dataimage1 = $image['picture_name'];
       	unlink('./assets/images/offers/'.$dataimage1);
        $this->backend_offersmodel->image_delete($id);
		$this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Delete image done!!!
				 </p>');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	//meta
	public function metapage()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_offersmeta');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['meta'] = $this->backend_homemodel->get_metarow('metaoffers');
		$data['func'] = 'offers';
		$data['title'] = 'Offers';

		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/metapage');
	    $this->load->view('backend/templates/footer');
	}

	public function update_metapage()
	{
		$res=$this->backend_homemodel->update_metapage();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	
	

}