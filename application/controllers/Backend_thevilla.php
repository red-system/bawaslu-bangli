<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_thevilla extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }

	public function header_villa()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheadervilla');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['thevillas'] = $this->backend_thevillamodel->get_thevillaall('thevillaheader');
		$data['title'] = 'Header The Villa Page';
		$data['link'] = 'header_villa_edit';
		$data['general_name'] = 'header_villa';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevillaheader');
	    $this->load->view('backend/templates/footer');
	}

	public function header_villa_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheadervilla');

		$data['thevilla'] = $this->backend_thevillamodel->get_thevilla_by_id($id);
		$data['imagevilla'] = $this->backend_thevillamodel->getrow_image('header_villa');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Header The Villa Page';
		$data['general_name'] = 'header_villa';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/thevillaheader_edit');
		$this->load->view('backend/templates/footer');
	}

	public function about_villa()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_aboutvilla');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['thevillas'] = $this->backend_thevillamodel->get_thevillaall('thevillaabout');
		$data['imagevilla'] = $this->backend_thevillamodel->get_image('about_villa');
		$data['title'] = 'About Villa';
		$data['link'] = 'about_villa_edit';
		$data['general_name'] = 'about_villa';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevilla');
	    $this->load->view('backend/templates/footer');
	}

	public function about_villa_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_aboutvilla');

		$data['thevilla'] = $this->backend_thevillamodel->get_thevilla_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'About Villa';
		$data['general_name'] = 'about_villa';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/thevilla_edit');
		$this->load->view('backend/templates/footer');
	}

	public function about_villa_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_aboutvilla');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'About Villa';
		$data['general_name'] = 'about_villa';

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevillaimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function location()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_location');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['thevillas'] = $this->backend_thevillamodel->get_thevillaall('thevillalocation');
		$data['imagevilla'] = $this->backend_thevillamodel->get_image('location');
		$data['title'] = 'Location';
		$data['link'] = 'location_edit';
		$data['general_name'] = 'location';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevilla');
	    $this->load->view('backend/templates/footer');
	}

	public function location_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_location');

		$data['thevilla'] = $this->backend_thevillamodel->get_thevilla_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Location';
		$data['general_name'] = 'location';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/thevilla_edit');
		$this->load->view('backend/templates/footer');
	}

	public function location_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_location');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Location';
		$data['general_name'] = 'location';
		
		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevillaimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function generous_surrounding()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_generous_surrounding');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['thevillas'] = $this->backend_thevillamodel->get_thevillaall('generous_surrounding');
		$data['imagevilla'] = $this->backend_thevillamodel->get_image('generous_surrounding');
		$data['title'] = 'Generous Surrounding';
		$data['link'] = 'generous_surrounding_edit';
		$data['general_name'] = 'generous_surrounding';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevilla');
	    $this->load->view('backend/templates/footer');
	}

	public function generous_surrounding_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_generous_surrounding');

		$data['thevilla'] = $this->backend_thevillamodel->get_thevilla_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Generous Surrounding';
		$data['general_name'] = 'generous_surrounding';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/thevilla_edit');
		$this->load->view('backend/templates/footer');
	}

	public function generous_surrounding_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_generous_surrounding');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Generous Surrounding';
		$data['general_name'] = 'generous_surrounding';
		
		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevillaimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function luxurious_furnishing()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_luxurious_furnishing');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['thevillas'] = $this->backend_thevillamodel->get_thevillaall('luxurious_furnishing');
		$data['imagevilla'] = $this->backend_thevillamodel->get_image('luxurious_furnishing');
		$data['title'] = 'Luxurious Furnishing';
		$data['link'] = 'luxurious_furnishing_edit';
		$data['general_name'] = 'luxurious_furnishing';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevilla');
	    $this->load->view('backend/templates/footer');
	}

	public function luxurious_furnishing_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_luxurious_furnishing');

		$data['thevilla'] = $this->backend_thevillamodel->get_thevilla_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Luxurious Furnishing';
		$data['general_name'] = 'luxurious_furnishing';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/thevilla_edit');
		$this->load->view('backend/templates/footer');
	}

	public function luxurious_furnishing_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_luxurious_furnishing');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Luxurious Furnishing';
		$data['general_name'] = 'luxurious_furnishing';
		
		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevillaimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function bathrooms()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_bathrooms');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['thevillas'] = $this->backend_thevillamodel->get_thevillaall('bathrooms');
		$data['imagevilla'] = $this->backend_thevillamodel->get_image('bathrooms');
		$data['title'] = 'Bathrooms';
		$data['link'] = 'bathrooms_edit';
		$data['general_name'] = 'bathrooms';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevilla');
	    $this->load->view('backend/templates/footer');
	}

	public function bathrooms_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_bathrooms');

		$data['thevilla'] = $this->backend_thevillamodel->get_thevilla_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Bathrooms';
		$data['general_name'] = 'bathrooms';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/thevilla_edit');
		$this->load->view('backend/templates/footer');
	}

	public function bathrooms_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_bathrooms');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Bathrooms';
		$data['general_name'] = 'bathrooms';
		
		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevillaimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function perfect_equipped_kitchen()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_perfect_equipped_kitchen');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['thevillas'] = $this->backend_thevillamodel->get_thevillaall('perfect_equipped_kitchen');
		$data['imagevilla'] = $this->backend_thevillamodel->get_image('perfect_equipped_kitchen');
		$data['title'] = 'Perfect Equipped Kitchen';
		$data['link'] = 'perfect_equipped_kitchen_edit';
		$data['general_name'] = 'perfect_equipped_kitchen';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevilla');
	    $this->load->view('backend/templates/footer');
	}

	public function perfect_equipped_kitchen_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_perfect_equipped_kitchen');

		$data['thevilla'] = $this->backend_thevillamodel->get_thevilla_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Perfect Equipped Kitchen';
		$data['general_name'] = 'perfect_equipped_kitchen';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/thevilla_edit');
		$this->load->view('backend/templates/footer');
	}

	public function perfect_equipped_kitchen_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_perfect_equipped_kitchen');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Perfect Equipped Kitchen';
		$data['general_name'] = 'perfect_equipped_kitchen';
		
		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevillaimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function heavenly_bedrooms()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_heavenly_bedrooms');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['thevillas'] = $this->backend_thevillamodel->get_thevillaall('heavenly_bedrooms');
		$data['imagevilla'] = $this->backend_thevillamodel->get_image('heavenly_bedrooms');
		$data['title'] = 'Heavenly Bedrooms';
		$data['link'] = 'heavenly_bedrooms_edit';
		$data['general_name'] = 'heavenly_bedrooms';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevilla');
	    $this->load->view('backend/templates/footer');
	}

	public function heavenly_bedrooms_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_heavenly_bedrooms');

		$data['thevilla'] = $this->backend_thevillamodel->get_thevilla_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Heavenly Bedrooms';
		$data['general_name'] = 'heavenly_bedrooms';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/thevilla_edit');
		$this->load->view('backend/templates/footer');
	}

	public function heavenly_bedrooms_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_heavenly_bedrooms');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Heavenly Bedrooms';
		$data['general_name'] = 'heavenly_bedrooms';
		
		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevillaimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function gallery()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_gallery');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['thevillas'] = $this->backend_thevillamodel->get_thevillaall('gallery_home');
		$data['imagevilla'] = $this->backend_thevillamodel->get_image('gallery');
		$data['title'] = 'Gallery';
		$data['link'] = 'gallery_edit';
		$data['general_name'] = 'gallery';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevilla');
	    $this->load->view('backend/templates/footer');
	}

	public function gallery_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_gallery');

		$data['thevilla'] = $this->backend_thevillamodel->get_thevilla_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Gallery';
		$data['general_name'] = 'gallery';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/thevilla_edit');
		$this->load->view('backend/templates/footer');
	}

	public function gallery_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_gallery');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Gallery';
		$data['general_name'] = 'gallery';
		
		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevillaimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function rooftop()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_rooftop');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['thevillas'] = $this->backend_thevillamodel->get_thevillaall('rooftop');
		$data['imagevilla'] = $this->backend_thevillamodel->get_image('rooftop');
		$data['title'] = 'Rooftop';
		$data['link'] = 'rooftop_edit';
		$data['general_name'] = 'rooftop';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevilla');
	    $this->load->view('backend/templates/footer');
	}

	public function rooftop_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_rooftop');

		$data['thevilla'] = $this->backend_thevillamodel->get_thevilla_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Rooftop';
		$data['general_name'] = 'rooftop';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/thevilla_edit');
		$this->load->view('backend/templates/footer');
	}

	public function rooftop_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_rooftop');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Rooftop';
		$data['general_name'] = 'rooftop';
		
		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevillaimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function villa_by_night()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_villa_by_night');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['thevillas'] = $this->backend_thevillamodel->get_thevillaall('villa_by_night');
		$data['imagevilla'] = $this->backend_thevillamodel->get_image('villa_by_night');
		$data['title'] = 'Villa by Night';
		$data['link'] = 'villa_by_night_edit';
		$data['general_name'] = 'villa_by_night';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevilla');
	    $this->load->view('backend/templates/footer');
	}

	public function villa_by_night_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_villa_by_night');

		$data['thevilla'] = $this->backend_thevillamodel->get_thevilla_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Villa by Night';
		$data['general_name'] = 'villa_by_night';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/thevilla_edit');
		$this->load->view('backend/templates/footer');
	}

	public function villa_by_night_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_villa_by_night');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Villa by Night';
		$data['general_name'] = 'villa_by_night';
		
		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevillaimage_add');
	    $this->load->view('backend/templates/footer');
	}		

	public function update_thevilla()
	{
		$res=$this->backend_thevillamodel->update_thevilla();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function update_pageheaderthevilla()
	{
		$this->backend_thevillamodel->update_thevilla();

		$config['upload_path']          = './assets/images/banner/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('image')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/banner/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= TRUE;
            $config1['width']= 1920;
           	$config1['new_image']= './assets/images/banner/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $pic= $this->backend_thevillamodel->get_thevillaimage_by_refid('header_villa');
            $datapic = $pic['picture_name'];
            unlink('./assets/images/banner/'.$datapic);
			$this->backend_thevillamodel->update_headerthevilla($data);
		}

		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect($this->agent->referrer());

	}

	public function image_addprocess()
	{
		$general_name = $this->input->post('general_name');

		$post = $this->input->post();
	
		$config['upload_path']          = './assets/images/about';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;

		$this->upload->initialize($config);
		if($this->upload->do_upload('file'))
		{
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/about/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
            $config1['width']= 585;
            $config1['height']= 439;
            $config1['new_image']= './assets/images/about/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

           	$data = array(	'picture_name'		=> $gbr['file_name'],
							'general_ref_id'	=> $general_name,
						);
			$this->backend_thevillamodel->image_add($data);
		}
	}

	public function image_delete($id)
	{
		$image= $this->backend_thevillamodel->get_thevillaimage_by_id($id);
        $dataimage1 = $image['picture_name'];
       	unlink('./assets/images/about/'.$dataimage1);
        $this->backend_thevillamodel->image_delete($id);
		$this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Delete image done!!!
				 </p>');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	
	

}