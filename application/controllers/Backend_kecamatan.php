<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_kecamatan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
            redirect('user_auth');
        }
    }


    //meta
    public function metapage()
    {
        $this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'backend_kecamatan_meta');

        $data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
        $data['profile'] = $this->backend_profile_model->get_profile('admin');
        $data['meta'] = $this->backend_homemodel->get_metarow('metakecamatan');
        $data['func'] = 'kecamatan';
        $data['title'] = 'Kecamatan';

        $this->load->view('backend/templates/header', $data);
        $this->load->view('backend/templates/menu');
        $this->load->view('backend/metapage');
        $this->load->view('backend/templates/footer');
    }

    public function update_metapage()
    {
        $res = $this->backend_homemodel->update_metapage();
        if ($res == true) {
            $this->session->set_flashdata('true',
                '<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
        } else {
            $this->session->set_flashdata('err', "Update failed!!!");
        }

        $this->load->library('user_agent');
        redirect($this->agent->referrer());
    }

    public function daftar()
    {
        $this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'backend_kecamatan_daftar');

        $data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
        $data['profile'] = $this->backend_profile_model->get_profile('admin');
        $data['daftar'] = $this->backend_kecamatanmodel->district_list();
        $data['func'] = 'kecamatan';
        $data['title'] = 'Kecamatan';

        $this->load->view('backend/templates/header', $data);
        $this->load->view('backend/templates/menu');
        $this->load->view('backend/kecamatan_list');
        $this->load->view('backend/templates/footer');
    }

    public function add()
    {
        $this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'backend_kecamatan_daftar');

        $data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
        $data['profile'] = $this->backend_profile_model->get_profile('admin');
        $data['func'] = 'kecamatan';
        $data['title'] = 'Kecamatan';

        $this->load->view('backend/templates/header', $data);
        $this->load->view('backend/templates/menu');
        $this->load->view('backend/kecamatan_add');
        $this->load->view('backend/templates/footer');
    }

    public function insert()
    {
        $data = $this->input->post(NULL, TRUE);
        $this->backend_kecamatanmodel->district_insert($data);

        redirect('backend_kecamatan/daftar');
    }

    public function edit($district_id)
    {
        $this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'backend_kecamatan_daftar');

        $data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
        $data['profile'] = $this->backend_profile_model->get_profile('admin');
        $data['func'] = 'kecamatan';
        $data['title'] = 'Kecamatan';
        $data['district'] = $this->backend_kecamatanmodel->district_row(array('district_id' => $district_id));

        $this->load->view('backend/templates/header', $data);
        $this->load->view('backend/templates/menu');
        $this->load->view('backend/kecamatan_edit');
        $this->load->view('backend/templates/footer');
    }

    public function update($district_id)
    {
        $where = array(
            'district_id'=>$district_id
        );
        $data = $this->input->post(NULL, TRUE);
        $this->backend_kecamatanmodel->district_update($where, $data);

        redirect('backend_kecamatan/daftar');
    }

    public function delete($district_id) {
        $where = array('district_id'=>$district_id);
        $this->backend_kecamatanmodel->district_delete($where);

        redirect('backend_kecamatan/daftar');
    }


}