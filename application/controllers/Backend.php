<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }

	public function index()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend');

		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		
		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/templates/body');
	    $this->load->view('backend/templates/footer');
	}

	public function profile_updateprocess()
	{
		$pass = $this->backend_profile_model->get_profile('admin');
		$passworddata = $pass['password'];
		$currentpass = md5($this->input->post('currentpass'));

		if($passworddata != $currentpass){
			$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Current Password Wrong!
			 </h4> 
			 ');
		} else {
			$config['upload_path']          = './assets/images';
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$config['max_size']             = 10000;
			$config['max_width']            = 20000;
			$config['max_height']           = 20000;
			$this->upload->initialize($config);
			if($this->upload->do_upload('thumb_image')){
				$data['image_name']= $this->upload->data('file_name');
				$res=$this->backend_profile_model->update_profile($data);
				if($res==true)
				{
				  $this->session->set_flashdata('true', 
				  	'<button data-dismiss="alert" class="close close-sm" type="button">
	                 	<i class="icon-remove"></i>
	                 </button>
	                 <h4>
						<i class="icon-ok-sign"></i>
						Success!
					 </h4> 
					 <p>Update done!!!
					 </p>');
				}
				else
				{
				  $this->session->set_flashdata('err', "Update failed!!!");
				}
			} else {
				$res=$this->backend_profile_model->update_profile();
				if($res==true)
				{
				  $this->session->set_flashdata('true', 
				  	'<button data-dismiss="alert" class="close close-sm" type="button">
	                 	<i class="icon-remove"></i>
	                 </button>
	                 <h4>
						<i class="icon-ok-sign"></i>
						Success!
					 </h4> 
					 <p>Update done!!!
					 </p>');
				}
				else
				{
				  $this->session->set_flashdata('err', "Update failed!!!");
				}
			}
		}
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function profilepass_updateprocess()
	{
		$pass = $this->backend_profile_model->get_profile('admin');
		$passworddata = $pass['password'];
		$currentpass = md5($this->input->post('currentpass'));
		$password = md5($this->input->post('password'));
		$repass = md5($this->input->post('repass'));
		
		if($passworddata != $currentpass){
			$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Current Password Wrong!
			 </h4> 
			 ');
		} else {
			if($password!=$repass){
				$this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Confirm Password Wrong!
				 </h4> 
				 ');
			} else {
				$res=$this->backend_profile_model->update_profilepass();
				if($res==true)
				{
				  $this->session->set_flashdata('true', 
				  	'<button data-dismiss="alert" class="close close-sm" type="button">
		             	<i class="icon-remove"></i>
		             </button>
		             <h4>
						<i class="icon-ok-sign"></i>
						Success!
					 </h4> 
					 <p>Update done!!!
					 </p>');
				}
				else
				{
				  $this->session->set_flashdata('err', "Update failed!!!");
				}
			}
		}
		$this->load->library('user_agent');
		redirect($this->agent->referrer()); 
	}

}