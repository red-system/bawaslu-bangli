<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_blog extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
            redirect('user_auth');
        }

        $this->load->library('general');
        $this->load->helper("file");
    }

    public function daftar($type, $value_id)
    {
        $this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'backend_'.$type);
        $where = array(
            'type'=>$type,
            'value_id'=>$value_id
        );

        $data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
        $data['profile'] = $this->backend_profile_model->get_profile('admin');
        $data['daftar'] = $this->backend_blogmodel->blog_list($where);
        $data['func'] = 'type';
        $data['title'] = 'Blog '.$type;
        $data['type'] = $type;
        $data['value_id'] = $value_id;

        $this->load->view('backend/templates/header', $data);
        $this->load->view('backend/templates/menu');
        $this->load->view('backend/blog_list');
        $this->load->view('backend/templates/footer');
    }

    public function add($type, $value_id)
    {
        $this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'backend_'.$type);

        $data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
        $data['profile'] = $this->backend_profile_model->get_profile('admin');
        $data['func'] = 'type';
        $data['title'] = 'Blog '.$type;
        $data['type'] = $type;
        $data['value_id'] = $value_id;

        $this->load->view('backend/templates/header', $data);
        $this->load->view('backend/templates/menu');
        $this->load->view('backend/blog_add');
        $this->load->view('backend/templates/footer');
    }

    public function insert($type, $value_id)
    {
        $this->load->library('upload');
        $config['upload_path']          = './assets/images/blog/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size'] = '10240000';
        $config['max_width']  = '10240000';
        $config['max_height']  = '7680000';
        $config['file_name'] = $this->general->slug($this->input->post('blog_title'));

        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload('userfile'))
        {
            $error = array('error' => $this->upload->display_errors());
            echo json_encode($error);
            redirect('backend_blog/daftar/'.$type.'/'.$value_id);
        }
        else
        {
            $filename = $this->upload->file_name;
            $data = $this->input->post(NULL, TRUE);
            $data['blog_thumbnail'] = $filename;
            $data['type'] = $type;
            $data['value_id'] = $value_id;

            $this->backend_blogmodel->blog_insert($data);

            $path_from = './assets/images/blog/'.$filename;
            $path_to = './assets/images/blog/'.$filename;
            $this->general->resize_image($path_from, $path_to, 800);

            redirect('backend_blog/daftar/'.$type.'/'.$value_id);
        }
    }

    public function edit($type, $value_id, $blog_id)
    {
        $this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'backend_'.$type);

        $data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
        $data['profile'] = $this->backend_profile_model->get_profile('admin');
        $data['title'] = 'Blog '.$type;
        $data['type'] = $type;
        $data['value_id'] = $value_id;
        $data['blog'] = $this->backend_blogmodel->blog_row(array('blog_id' => $blog_id));

        $this->load->view('backend/templates/header', $data);
        $this->load->view('backend/templates/menu');
        $this->load->view('backend/blog_edit');
        $this->load->view('backend/templates/footer');
    }

    public function update($type, $value_id, $blog_id)
    {
        $this->load->library('upload');
        $config['upload_path']          = './assets/images/blog/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size'] = '10240000';
        $config['max_width']  = '10240000';
        $config['max_height']  = '7680000';
        $config['file_name'] = $this->general->slug($this->input->post('blog_title'));

        $this->upload->initialize($config);


        $where = array('blog_id'=>$blog_id);
        $data = $this->input->post(NULL, TRUE);

        if(!empty($_FILES['userfile']['name'])) {
            if ( ! $this->upload->do_upload('userfile'))
            {
                $error = array('error' => $this->upload->display_errors());
                echo json_encode($error);
                redirect('backend_blog/daftar/'.$type.'/'.$value_id);
            }
            else
            {


                $filename = $this->upload->file_name;
                $data['blog_thumbnail'] = $filename;

                $path_from = './assets/images/blog/'.$filename;
                $path_to = './assets/images/blog/'.$filename;
                $this->general->resize_image($path_from, $path_to, 800);
            }
        }


        $this->backend_blogmodel->blog_update($where, $data);
        //echo json_encode($data);


        redirect('backend_blog/daftar/'.$type.'/'.$value_id);
    }

    public function delete($type, $value_id, $blog_id) {

        $where = array('blog_id'=>$blog_id);
        $this->backend_blogmodel->blog_delete($where);

        redirect('backend_blog/daftar/'.$type.'/'.$value_id);
    }


}