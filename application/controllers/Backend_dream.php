<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_dream extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }

	public function header_dream()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheaderdream');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['dreams'] = $this->backend_dreammodel->get_dreamall('dreamheader');
		$data['title'] = 'Header A Dream Comes True Page';
		$data['link'] = 'header_dream_edit';
		$data['general_name'] = 'header_dream';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/dreamheader');
	    $this->load->view('backend/templates/footer');
	}

	public function header_dream_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheaderdream');

		$data['dream'] = $this->backend_dreammodel->get_dream_by_id($id);
		$data['imagedream'] = $this->backend_dreammodel->getrow_image('header_dream');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Header A Dream Comes True Page';
		$data['general_name'] = 'header_dream';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/dreamheader_edit');
		$this->load->view('backend/templates/footer');
	}

	
	public function update_pageheaderdream()
	{
		$this->backend_dreammodel->update_dream();

		$config['upload_path']          = './assets/images/banner/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('image')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/banner/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= TRUE;
            $config1['width']= 1920;
            $config1['new_image']= './assets/images/banner/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $pic= $this->backend_dreammodel->get_dreamimage_by_refid('header_dream');
            $datapic = $pic['picture_name'];
            unlink('./assets/images/banner/'.$datapic);
			$this->backend_dreammodel->update_headerdream($data);
		}

		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect($this->agent->referrer());

	}

	//meta
	public function metapage()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_dreammeta');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['meta'] = $this->backend_homemodel->get_metarow('metadream');
		$data['func'] = 'dream';
		$data['title'] = 'A Dream Comes True';

		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/metapage');
	    $this->load->view('backend/templates/footer');
	}

	public function update_metapage()
	{
		$res=$this->backend_homemodel->update_metapage();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	
	public function dream()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_dream');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['dreams'] = $this->backend_dreammodel->get_dreamall('dream');
		$data['title'] = 'A Dream Comes True';
		$data['link'] = 'dream_edit';
		$data['general_name'] = 'dream';

		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/dream');
	    $this->load->view('backend/templates/footer');
	}

	public function dream_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_dream');

		$data['dream'] = $this->backend_dreammodel->get_dream_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'A Dream Comes True';
		$data['general_name'] = 'dream';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/dream_edit');
		$this->load->view('backend/templates/footer');
	}

	public function update_dream()
	{
		$res=$this->backend_dreammodel->update_dream();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function dream_gallery()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_dream_gallery');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['imagegallery'] = $this->backend_dreammodel->get_image('dream_gallery');
		$data['title'] = 'A Dream Comes True Gallery';
		$data['link'] = 'dream_gallery_add';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/dream_gallery');
	    $this->load->view('backend/templates/footer');
	}

	public function dream_gallery_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_dream_gallery');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'A Dream Comes True';
		$data['general_name'] = 'dream_gallery';
		$data['link'] = 'dream_gallery';

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/dream_galleryimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function image_addprocess()
	{
		$general_name = $this->input->post('general_name');

		$post = $this->input->post();
	
		$config['upload_path']          = './assets/images/dream';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;

		$this->upload->initialize($config);
		if($this->upload->do_upload('file'))
		{
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/dream/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= TRUE;
            $config1['width']= 1920;
            $config1['new_image']= './assets/images/dream/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $config2['image_library']='gd2';
            $config2['source_image']='./assets/images/dream/'.$gbr['file_name'];
            $config2['create_thumb']= TRUE;
            $config2['maintain_ratio']= TRUE;
            $config2['width']= 280;
            $config2['new_image']= './assets/images/dream/thumb/'.$gbr['file_name'];
            $this->image_lib->initialize($config2);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

		 	$data = array(	'picture_name'		=> $gbr['file_name'],
							'second_picture'	=> $gbr['raw_name'].'_thumb'.$gbr['file_ext'],
							'general_ref_id'	=> $general_name,
						);
			$this->backend_dreammodel->image_add($data);
		}
	}

	public function image_delete($id)
	{
		$image= $this->backend_dreammodel->get_dreamimage_by_id($id);
        $dataimage1 = $image['picture_name'];
        $dataimage2 = $image['second_picture'];
        unlink('./assets/images/dream/'.$dataimage1);
        unlink('./assets/images/dream/thumb/'.$dataimage2);
		$this->backend_dreammodel->image_delete($id);
		$this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Delete picture done!!!
				 </p>');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	

}