<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_pricing extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }

	public function pricelist()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pricelist');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['pricing'] = $this->backend_pricingmodel->getrow_pricingall('pricinglist');
		$data['title'] = 'Pricelist';
		$data['func'] = 'pricelist';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/pricelist');
	    $this->load->view('backend/templates/footer');
	}

	public function menulist()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_menulist');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['pricing'] = $this->backend_pricingmodel->getrow_pricingall('menulist');
		$data['title'] = 'Menu list';
		$data['func'] = 'menulist';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/pricelist');
	    $this->load->view('backend/templates/footer');
	}

	public function update_pricelist()
	{
		//start
		$config['upload_path']          = './assets';
		$config['allowed_types']        = '*';
		$config['max_size']             = 10000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('file')){
			$res=$this->backend_pricingmodel->update_pricelist();
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$file= $this->backend_pricingmodel->getrow_pricingall('pricinglist');
            $datafile = $file['main_image'];
            unlink('./assets/'.$datafile);
			$res=$this->backend_pricingmodel->update_pricelist($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		}
		//end

		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function update_menulist()
	{
		//start
		$config['upload_path']          = './assets';
		$config['allowed_types']        = '*';
		$config['max_size']             = 10000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('file')){
			$res=$this->backend_pricingmodel->update_menulist();
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$file= $this->backend_pricingmodel->getrow_pricingall('menulist');
            $datafile = $file['main_image'];
            unlink('./assets/'.$datafile);
			$res=$this->backend_pricingmodel->update_menulist($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		}
		//end

		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	
	

}