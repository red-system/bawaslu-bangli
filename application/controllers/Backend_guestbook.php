<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_guestbook extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }

	public function header_guestbook()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheaderguestbook');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['guestbooks'] = $this->backend_guestbookmodel->get_guestbookall('guestbookheader');
		$data['title'] = 'Header Guestbook Page';
		$data['link'] = 'header_guestbook_edit';
		$data['general_name'] = 'header_guestbook';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/guestbookheader');
	    $this->load->view('backend/templates/footer');
	}

	public function header_guestbook_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheaderguestbook');

		$data['guestbook'] = $this->backend_guestbookmodel->get_guestbook_by_id($id);
		$data['imageguestbook'] = $this->backend_guestbookmodel->getrow_image('header_guestbook');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Header Guestbook Page';
		$data['general_name'] = 'header_guestbook';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/guestbookheader_edit');
		$this->load->view('backend/templates/footer');
	}

	public function about_guestbook()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_guestbookabout');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['guestbooks'] = $this->backend_guestbookmodel->get_guestbookall('guestbookabout');
		$data['title'] = 'About Guestbook';
		$data['link'] = 'about_guestbook_edit';
		$data['general_name'] = 'about_guestbook';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/guestbookheader');
	    $this->load->view('backend/templates/footer');
	}

	public function about_guestbook_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_guestbookabout');

		$data['guestbook'] = $this->backend_guestbookmodel->get_guestbook_by_id($id);
		$data['imageguestbook'] = $this->backend_guestbookmodel->getrow_image('guestbook_about');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'About Guestbook';
		$data['general_name'] = 'about_guestbook';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/guestbookabout_edit');
		$this->load->view('backend/templates/footer');
	}

	public function guestbook_form()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_guestbookform');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['guestbooks'] = $this->backend_guestbookmodel->get_guestbookall('guestbookform');
		$data['title'] = 'Guestbook Form';
		$data['link'] = 'guestbook_form_edit';
		
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/guestbookheader');
	    $this->load->view('backend/templates/footer');
	}

	public function guestbook_form_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_guestbookform');

		$data['guestbook'] = $this->backend_guestbookmodel->get_guestbook_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Guestbook Form';
		$data['general_name'] = 'guestbook_form';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/guestbookform_edit');
		$this->load->view('backend/templates/footer');
	}

	public function guestbook_list()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_guestbooklist');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['guestbooks'] = $this->backend_guestbookmodel->get_guestbookall('guestbooklist');
		$data['title'] = 'Guestbook List';
		$data['link'] = 'guestbook_list_edit';
		$data['link1'] = 'guestbook_list_delete';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/guestbook');
	    $this->load->view('backend/templates/footer');
	}

	public function  guestbook_list_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_guestbooklist');

		$data['guestbook'] = $this->backend_guestbookmodel->get_guestbook_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Guestbook List';
		$data['general_name'] = 'guestbook_list';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/guestbook_edit');
		$this->load->view('backend/templates/footer');
	}

	public function update_guestbook()
	{
		$res=$this->backend_guestbookmodel->update_guestbook();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function update_formguestbook()
	{
		$res=$this->backend_guestbookmodel->update_formguestbook();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function update_guestbooklist()
	{
		$res=$this->backend_guestbookmodel->update_guestbooklist();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function update_pageheaderguestbook()
	{
		$this->backend_guestbookmodel->update_guestbook();

		$config['upload_path']          = './assets/images/banner/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('image')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/banner/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= TRUE;
            $config1['width']= 1920;
            $config1['new_image']= './assets/images/banner/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $data['ref_id']= 'header_guestbook';
            $pic= $this->backend_guestbookmodel->get_guestbookimage_by_refid('header_guestbook');
            $datapic = $pic['picture_name'];
            unlink('./assets/images/banner/'.$datapic);
			$this->backend_guestbookmodel->update_headerguestbook($data);
		}

		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect($this->agent->referrer());

	}

	public function update_aboutguestbook()
	{
		$this->backend_guestbookmodel->update_guestbook();

		$config['upload_path']          = './assets/images/guestbook/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('image')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/guestbook/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= TRUE;
            $config1['width']= 770;
            $config1['new_image']= './assets/images/guestbook/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $data['ref_id']= 'guestbook_about';
            $pic= $this->backend_guestbookmodel->get_guestbookimage_by_refid('guestbook_about');
            $datapic = $pic['picture_name'];
            unlink('./assets/images/guestbook/'.$datapic);
			$this->backend_guestbookmodel->update_headerguestbook($data);
		}

		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect($this->agent->referrer());

	}

	public function guestbook_list_delete($id)
	{
		$pic= $this->backend_guestbookmodel->get_guestbook_by_id($id);
        $datapic = $pic['main_image'];
        
        if ($datapic=='noimage.png'){
        	$this->backend_guestbookmodel->delete_guestbook_list($id);
			$this->session->set_flashdata('true', 
				  	'<button data-dismiss="alert" class="close close-sm" type="button">
	                 	<i class="icon-remove"></i>
	                 </button>
	                 <h4>
						<i class="icon-ok-sign"></i>
						Success!
					 </h4> 
					 <p>Delete done!!!
					 </p>');
        } else {
	        unlink('./assets/images/guestbook/'.$datapic);
	        $this->backend_guestbookmodel->delete_guestbook_list($id);
			$this->session->set_flashdata('true', 
				  	'<button data-dismiss="alert" class="close close-sm" type="button">
	                 	<i class="icon-remove"></i>
	                 </button>
	                 <h4>
						<i class="icon-ok-sign"></i>
						Success!
					 </h4> 
					 <p>Delete done!!!
					 </p>');
		}
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	//meta
	public function metapage()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_guestbookmeta');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['meta'] = $this->backend_homemodel->get_metarow('metaguestbook');
		$data['func'] = 'guestbook';
		$data['title'] = 'Guestbook';

		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/metapage');
	    $this->load->view('backend/templates/footer');
	}

	public function update_metapage()
	{
		$res=$this->backend_homemodel->update_metapage();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	
}