<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_gallery extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }

	public function header_gallery()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheadergallery');

		$data['logofavicon'] = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['gallerys'] = $this->backend_gallerymodel->get_galleryall('galleryheader');
		$data['title'] = 'Header Gallery Page';
		$data['link'] = 'header_gallery_edit';
		$data['general_name'] = 'header_gallery';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/galleryheader');
	    $this->load->view('backend/templates/footer');
	}

	public function header_gallery_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheadergallery');

		$data['gallery'] = $this->backend_gallerymodel->get_gallery_by_id($id);
		$data['imagegallery'] = $this->backend_gallerymodel->getrow_image('header_gallery');
		$data['logofavicon'] = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Header Gallery Page';
		$data['general_name'] = 'header_gallery';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/galleryheader_edit');
		$this->load->view('backend/templates/footer');
	}

	public function first_view()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_gallery1');

		$data['logofavicon'] = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['gallerys'] = $this->backend_gallerymodel->get_galleryall('galleryfirst');
		$data['title'] = 'First View';
		$data['link'] = 'first_view_edit';
		$data['general_name'] = 'first_view';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/gallery');
	    $this->load->view('backend/templates/footer');
	}

	public function first_view_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_gallery1');

		$data['gallery'] = $this->backend_gallerymodel->get_gallery_by_id($id);
		$data['logofavicon'] = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'First View';
		$data['general_name'] = 'first_view';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/gallery_edit');
		$this->load->view('backend/templates/footer');
	}

	public function second_view()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_gallery2');

		$data['logofavicon'] = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['gallerys'] = $this->backend_gallerymodel->get_galleryall('gallerysecond');
		$data['title'] = 'Second View';
		$data['link'] = 'second_view_edit';
		$data['general_name'] = 'second_view';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/gallery');
	    $this->load->view('backend/templates/footer');
	}

	public function second_view_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_gallery2');

		$data['gallery'] = $this->backend_gallerymodel->get_gallery_by_id($id);
		$data['logofavicon'] = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Second View';
		$data['general_name'] = 'second_view';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/gallery_edit');
		$this->load->view('backend/templates/footer');
	}

	public function the_villa()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_gallerythevilla');

		$data['logofavicon'] = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['gallery'] = $this->backend_gallerymodel->getrow_galleryall('gallerythevilla');
		$data['imagegallery'] = $this->backend_gallerymodel->get_image('gallery_thevilla');
		$data['title'] = 'The Villa Gallery';
		$data['link'] = 'the_villa_add';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/galleryimage');
	    $this->load->view('backend/templates/footer');
	}

	public function the_villa_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_gallerythevilla');

		$data['logofavicon'] = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'The Villa Gallery';
		$data['general_name'] = 'gallery_thevilla';
		$data['link'] = 'the_villa';

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/galleryimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function facets()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_galleryfacets');

		$data['logofavicon'] = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['gallery'] = $this->backend_gallerymodel->getrow_galleryall('galleryfacets');
		$data['imagegallery'] = $this->backend_gallerymodel->get_image('gallery_facets');
		$data['title'] = 'Facets Gallery';
		$data['link'] = 'facets_add';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/galleryimage');
	    $this->load->view('backend/templates/footer');
	}

	public function facets_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_galleryfacets');

		$data['logofavicon'] = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Facets Gallery';
		$data['general_name'] = 'gallery_facets';
		$data['link'] = 'facets';

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/galleryimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function additional()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_galleryadditional');

		$data['logofavicon'] = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['gallery'] = $this->backend_gallerymodel->getrow_galleryall('galleryadditional');
		$data['imagegallery'] = $this->backend_gallerymodel->get_image('gallery_additional');
		$data['title'] = 'Additional Gallery';
		$data['link'] = 'additional_add';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/galleryimage');
	    $this->load->view('backend/templates/footer');
	}

	public function additional_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_galleryadditional');

		$data['logofavicon'] = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Additional Gallery';
		$data['general_name'] = 'gallery_additional';
		$data['link'] = 'additional';

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/galleryimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function update_gallery()
	{
		$res=$this->backend_gallerymodel->update_gallery();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function gallery_editprocess()
	{
		$res=$this->backend_gallerymodel->gallery_editprocess();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function update_pageheadergallery()
	{
		$this->backend_gallerymodel->update_gallery();

		$config['upload_path']          = './assets/images/banner/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('image')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/banner/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= TRUE;
            $config1['width']= 1920;
            $config1['new_image']= './assets/images/banner/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $pic= $this->backend_gallerymodel->get_galleryimage_by_refid('header_gallery');
            $datapic = $pic['picture_name'];
            unlink('./assets/images/banner/'.$datapic);
			$this->backend_gallerymodel->update_headergallery($data);
		}

		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect($this->agent->referrer());

	}

	public function image_addprocess()
	{
		$general_name = $this->input->post('general_name');

		$post = $this->input->post();
	
		$config['upload_path']          = './assets/images/gallery';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;

		$this->upload->initialize($config);
		if($this->upload->do_upload('file'))
		{
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/gallery/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= TRUE;
            $config1['width']= 1920;
            $config1['new_image']= './assets/images/gallery/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $config2['image_library']='gd2';
            $config2['source_image']='./assets/images/gallery/'.$gbr['file_name'];
            $config2['create_thumb']= TRUE;
            $config2['maintain_ratio']= TRUE;
            $config2['width']= 280;
            $config2['new_image']= './assets/images/gallery/thumb/'.$gbr['file_name'];
            $this->image_lib->initialize($config2);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

		 	$data = array(	'picture_name'		=> $gbr['file_name'],
							'second_picture'	=> $gbr['raw_name'].'_thumb'.$gbr['file_ext'],
							'general_ref_id'	=> $general_name,
						);
			$this->backend_gallerymodel->image_add($data);
		}
	}

	public function image_delete($id)
	{
		$image= $this->backend_gallerymodel->get_galleryimage_by_id($id);
        $dataimage1 = $image['picture_name'];
        $dataimage2 = $image['second_picture'];
        unlink('./assets/images/gallery/'.$dataimage1);
        unlink('./assets/images/gallery/thumb/'.$dataimage2);
		$this->backend_gallerymodel->image_delete($id);
		$this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Delete picture done!!!
				 </p>');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	//meta
	public function metapage()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_gallerymeta');

		$data['logofavicon'] = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['meta'] = $this->backend_homemodel->get_metarow('metagallery');
		$data['func'] = 'gallery';
		$data['title'] = 'Gallery';

		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/metapage');
	    $this->load->view('backend/templates/footer');
	}

	public function update_metapage()
	{
		$res=$this->backend_homemodel->update_metapage();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	
	

}