<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_headerfooter extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
        $this->load->library('general');
        $this->load->helper("file");
    }

	public function index()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_headerfooter');

		$data['address'] = $this->backend_headerfootermodel->get_headerfooter('address');
		$data['phone1'] = $this->backend_headerfootermodel->get_headerfooter('phone1');
		$data['email'] = $this->backend_headerfootermodel->get_headerfooter('email');
		$data['logoheader'] = $this->backend_headerfootermodel->get_headerfooter('logo_header');
		$data['logofavicon'] = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
		$data['working'] = $this->backend_headerfootermodel->get_headerfooter('working_days');
		$data['facebook'] = $this->backend_headerfootermodel->get_sosiallink('Facebook');
		$data['instagram'] = $this->backend_headerfootermodel->get_sosiallink('Instagram');
		$data['twitter'] = $this->backend_headerfootermodel->get_sosiallink('Twitter');
		$data['youtube'] = $this->backend_headerfootermodel->get_sosiallink('Youtube');

		$data['profile'] = $this->backend_profile_model->get_profile('admin');

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/headerfooter');
	    $this->load->view('backend/templates/footer');
	}

	public function update_headerfooter(){
		$this->backend_headerfootermodel->update_address();
		$this->backend_headerfootermodel->update_phone1();
		$this->backend_headerfootermodel->update_email();
		$this->backend_headerfootermodel->update_workingdays();
		$this->backend_headerfootermodel->update_facebook();
		$this->backend_headerfootermodel->update_instagram();
		$this->backend_headerfootermodel->update_twitter();
		$this->backend_headerfootermodel->update_youtube();

		$this->load->library('upload');
        $config['upload_path']          = './assets/images/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size'] = '10240000';
        $config['max_width']  = '10240000';
        $config['max_height']  = '7680000';
        $config['file_name'] = 'logo-bawaslu-bangli';

        $this->upload->initialize($config);

        $where = 'logo_header';
        
        if(!empty($_FILES['logoheader']['name'])) {
            if ( ! $this->upload->do_upload('logoheader'))
            {
                $error = array('error' => $this->upload->display_errors());
                redirect('backend_headerfooter');
            }
            else
            {
            	$logoheader = $this->backend_headerfootermodel->get_headerfooter('logo_header');
                unlink('./assets/images/'.$logoheader['main_image']);


                $filename = $this->upload->file_name;
                $data['main_image'] = $filename;

                $path_from = './assets/images/'.$filename;
                $path_to = './assets/images/'.$filename;
                $this->general->resize_image($path_from, $path_to, 341);
            }
            $this->backend_headerfootermodel->update_logoheader($where, $data);
        }
		
        $config['upload_path']          = './assets/images/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size'] = '10240000';
        $config['max_width']  = '10240000';
        $config['max_height']  = '7680000';
        $config['file_name'] = 'logo-favicon-bawaslu-bangli';

        $this->upload->initialize($config);

        $where = 'logo_favicon';
		if(!empty($_FILES['logofavicon']['name'])) {
            if ( ! $this->upload->do_upload('logofavicon'))
            {
                $error = array('error' => $this->upload->display_errors());
                redirect('backend_headerfooter');
            }
            else
            {
            	$logofavicon = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
                unlink('./assets/images/'.$logofavicon['main_image']);


                $filename = $this->upload->file_name;
                $data['main_image'] = $filename;

                $path_from = './assets/images/'.$filename;
                $path_to = './assets/images/'.$filename;
                $this->general->resize_image($path_from, $path_to, 34);
            }
            $this->backend_headerfootermodel->update_logofavicon($where, $data);
        }
		
			
		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
}