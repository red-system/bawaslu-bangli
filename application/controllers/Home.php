<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'html','language'));
	}
    
	public function index()
	{
		//start default
		$data['address'] = $this->backend_headerfootermodel->get_headerfooter('address');
		$data['phone1'] = $this->backend_headerfootermodel->get_headerfooter('phone1');
		$data['email'] = $this->backend_headerfootermodel->get_headerfooter('email');
		$data['logoheader'] = $this->backend_headerfootermodel->get_headerfooter('logo_header');
		$data['logofavicon'] = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
		$data['working'] = $this->backend_headerfootermodel->get_headerfooter('working_days');
		$data['social_medias'] = $this->backend_headerfootermodel->get_sosiallink();
		//end default
        
        $data['pictures'] = $this->backend_homemodel->get_homearray('slider');


        $this->load->view('templates/header', $data);
        $this->load->view('slider');
        $this->load->view('home');
        $this->load->view('templates/footer');
	}

	

}
