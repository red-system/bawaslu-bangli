<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_struktur extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
            redirect('user_auth');
        }
    }

    //meta
    public function metapage()
    {
        $this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'backend_struktur_meta');

        $data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
        $data['profile'] = $this->backend_profile_model->get_profile('admin');
        $data['meta'] = $this->backend_homemodel->get_metarow('metastruktur');
        $data['func'] = 'struktur';
        $data['title'] = 'struktur';

        $this->load->view('backend/templates/header', $data);
        $this->load->view('backend/templates/menu');
        $this->load->view('backend/metapage');
        $this->load->view('backend/templates/footer');
    }

    public function update_metapage()
    {
        $res = $this->backend_homemodel->update_metapage();
        if ($res == true) {
            $this->session->set_flashdata('true',
                '<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
        } else {
            $this->session->set_flashdata('err', "Update failed!!!");
        }

        $this->load->library('user_agent');
        redirect($this->agent->referrer());
    }

    public function deskripsi()
    {
        $this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'backend_struktur_deskripsi');

        $data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
        $data['profile'] = $this->backend_profile_model->get_profile('admin');
        $data['row'] = $this->db->where('general_name', 'profil_struktur')->get('tb_general_data')->row_array();
        $data['title'] = 'struktur';
        $data['general_name'] = 'struktur';

        $this->load->view('backend/templates/header', $data);
        $this->load->view('backend/templates/menu');
        $this->load->view('backend/struktur_deskripsi');
        $this->load->view('backend/templates/footer');
    }

    public function deskripsi_update($general_id)
    {
        $description = $this->input->post('description');
        $where = array(
            'general_id'=>$general_id
        );
        $data = array(
            'general_desc'=>$description
        );

        $this->db->where($where)->update('tb_general_data', $data);

        redirect('backend_struktur/deskripsi');
    }


}