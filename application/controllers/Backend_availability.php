<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_availability extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }

	public function header_availability()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheaderavailability');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['availabilitys'] = $this->backend_availabilitymodel->get_availabilityall('availabilityheader');
		$data['title'] = 'Header Availability Page';
		$data['link'] = 'header_availability_edit';
		$data['general_name'] = 'header_availability';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/availabilityheader');
	    $this->load->view('backend/templates/footer');
	}

	public function header_availability_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheaderavailability');

		$data['availability'] = $this->backend_availabilitymodel->get_availability_by_id($id);
		$data['imageavailability'] = $this->backend_availabilitymodel->getrow_image('header_availability');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Header Availability Page';
		$data['general_name'] = 'header_availability';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/availabilityheader_edit');
		$this->load->view('backend/templates/footer');
	}

	public function update_availability()
	{
		$res=$this->backend_availabilitymodel->update_availability();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function update_pageheaderavailability()
	{
		$this->backend_availabilitymodel->update_availability();

		$config['upload_path']          = './assets/images/banner/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('image')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/banner/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= TRUE;
            $config1['width']= 1920;
            $config1['new_image']= './assets/images/banner/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $pic= $this->backend_availabilitymodel->get_availabilityimage_by_refid('header_availability');
            $datapic = $pic['picture_name'];
            unlink('./assets/images/banner/'.$datapic);
			$this->backend_availabilitymodel->update_headeravailability($data);
		}

		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect($this->agent->referrer());

	}

	//meta
	public function metapage()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_availabilitymeta');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['meta'] = $this->backend_homemodel->get_metarow('metaavailability');
		$data['func'] = 'availability';
		$data['title'] = 'Availability';

		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/metapage');
	    $this->load->view('backend/templates/footer');
	}

	public function update_metapage()
	{
		$res=$this->backend_homemodel->update_metapage();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}


	public function reservation()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_reservation');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['reservations'] = $this->backend_availabilitymodel->get_reservation();
		$data['title'] = 'Reservation';
		$data['link'] = 'reservation_edit';
		$data['link1'] = 'reservation_delete';
		$data['general_name'] = 'reservation';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/reservation');
	    $this->load->view('backend/templates/footer');
	}

	public function add_reservation()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_reservation');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Reservation';
		$data['general_name'] = 'reservation';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/reservation_add');
	    $this->load->view('backend/templates/footer');
	}

	public function reservation_addprocess()
	{
		$status = $this->input->post('status');

		$year =  substr ($this->input->post('arrival'),6,4);
	    $date = substr ($this->input->post('arrival'),3,2);
	    $month =  substr ($this->input->post('arrival'),0,2);
	    $arrival = $year."-".$month."-".$date;

	   	$year1 =  substr ($this->input->post('departure'),6,4);
	    $date1 = substr ($this->input->post('departure'),3,2);
	    $month1 =  substr ($this->input->post('departure'),0,2);
	    $departure = $year1."-".$month1."-".$date1;

	    $this->backend_availabilitymodel->reservation_add();
			
		$query = $this->db->query('SELECT id_reservasi FROM tb_reservasi ORDER BY id_reservasi DESC LIMIT 1');  
	    $result = $query->result_array();  
	    $id = $result[0]['id_reservasi'];

	    $this->backend_availabilitymodel->daystart_add($id,$arrival);

	    $period = new DatePeriod(
		     new DateTime($arrival),
		     new DateInterval('P1D'),
		     new DateTime($departure)
		);

	    foreach($period as $date) { 
	        $array[] = $date->format('Y-m-d'); 
	    }
	   
	   	$countday = count($array);

	    for($i=1; $i<$countday; $i++)
	    {
	    	$data = array(
      					  'ref_id' 	=> $id,
      					  'day' 	=> $array[$i],
      					  'info' 	=> 'booked',
      					  'status'	=> $status
                       	 );
	    	$this->backend_availabilitymodel->day_add($data);
	    }

	    $this->backend_availabilitymodel->dayend_add($id,$departure);

	    $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Add done!!!
			 </p>');
      	$this->load->library('user_agent');
		redirect('backend_availability/reservation');
	}

	public function reservation_edit($id)
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_reservation');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['reservation'] = $this->backend_availabilitymodel->get_reservasi_by_id($id);
		$data['title'] = 'Reservation';
		$data['general_name'] = 'reservation';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/reservation_edit');
	    $this->load->view('backend/templates/footer');
	}

	public function reservation_editprocess()
	{
		$ref_id = $this->input->post('id_reservation');
		$status = $this->input->post('status');

		$year =  substr ($this->input->post('arrival'),6,4);
	    $date = substr ($this->input->post('arrival'),3,2);
	    $month =  substr ($this->input->post('arrival'),0,2);
	    $arrival = $year."-".$month."-".$date;

	   	$year1 =  substr ($this->input->post('departure'),6,4);
	    $date1 = substr ($this->input->post('departure'),3,2);
	    $month1 =  substr ($this->input->post('departure'),0,2);
	    $departure = $year1."-".$month1."-".$date1;

	    $this->backend_availabilitymodel->allday_delete($ref_id);

	    $this->backend_availabilitymodel->update_reservation();
	   	
	   	$this->backend_availabilitymodel->daystart_add($ref_id,$arrival);
		
		$period = new DatePeriod(
		     new DateTime($arrival),
		     new DateInterval('P1D'),
		     new DateTime($departure)
		);

	    foreach($period as $date) { 
	        $array[] = $date->format('Y-m-d'); 
	    }
	   
	   	$countday = count($array);

	    for($i=1; $i<$countday; $i++)
	    {
	    	$data = array(
      					  'ref_id' 	=> $ref_id,
      					  'day' 	=> $array[$i],
      					  'info' 	=> 'booked',
      					  'status'	=> $status
                       	 );
	    	$this->backend_availabilitymodel->day_add($data);
	    }

	    $this->backend_availabilitymodel->dayend_add($ref_id,$departure);

	    $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
      	$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function reservation_delete($id)
	{
		$this->backend_availabilitymodel->allday_delete($id);

		$this->backend_availabilitymodel->reservation_delete($id);

		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Delete done!!!
			 </p>');
       
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function reservation_form()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_formreservation');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['reservationen'] = $this->backend_availabilitymodel->getrow_availabilityall('reservationen');
		$data['reservationde'] = $this->backend_availabilitymodel->getrow_availabilityall('reservationde');
		$data['title'] = 'Reservation';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/reservation_form');
	    $this->load->view('backend/templates/footer');
	}
	
	public function reservation_form_edit()
	{
		$data = array(	
				'general_data' => $this->input->post('general_data_form_en'),
			    'general_sub_data' => $this->input->post('general_sub_data_form_en'),
			    'general_desc' => $this->input->post('general_desc_form_en'),
			    'general_sub_desc' => $this->input->post('general_sub_desc_form_en'),
			    'general_link' => $this->input->post('general_link_form_en'),
			    'main_image' => $this->input->post('main_image_form_en'),
			    'secondary_image' => $this->input->post('secondary_image_form_en'),
			    'general_password' => $this->input->post('general_password_form_en'),
			    'facebook_link' => $this->input->post('facebook_link_form_en'),
			    'general_additional_info' => $this->input->post('general_additional_info_form_en')
		);
		$id_form_en = $this->input->post('id_form_en');
		$this->backend_availabilitymodel->reservation_form_edit($id_form_en,$data);

		$data = array(	
				'general_data' => $this->input->post('general_data_form_de'),
			    'general_sub_data' => $this->input->post('general_sub_data_form_de'),
			    'general_desc' => $this->input->post('general_desc_form_de'),
			    'general_sub_desc' => $this->input->post('general_sub_desc_form_de'),
			    'general_link' => $this->input->post('general_link_form_de'),
			    'main_image' => $this->input->post('main_image_form_de'),
			    'secondary_image' => $this->input->post('secondary_image_form_de'),
			    'general_password' => $this->input->post('general_password_form_de'),
			    'facebook_link' => $this->input->post('facebook_link_form_de'),
			    'general_additional_info' => $this->input->post('general_additional_info_form_de')
		);
		$id_form_de = $this->input->post('id_form_de');
		$this->backend_availabilitymodel->reservation_form_edit($id_form_de,$data);

		$this->session->set_flashdata('true', 
	  	'<button data-dismiss="alert" class="close close-sm" type="button">
         	<i class="icon-remove"></i>
         </button>
         <h4>
			<i class="icon-ok-sign"></i>
			Success!
		 </h4> 
		 <p>Update done!!!
		 </p>');
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	

}