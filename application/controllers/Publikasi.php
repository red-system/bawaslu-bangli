<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publikasi extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'html','language'));
	}
    
	public function index()
	{
        $this->load->view('templates/header');
        $this->load->view('publikasi');
        $this->load->view('templates/footer');
	}

	public function detail() {
        $this->load->view('templates/header');
        $this->load->view('publikasi_detail');
        $this->load->view('templates/footer');
    }

	

}
