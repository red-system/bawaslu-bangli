<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_privacy_policy extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }

	public function header_privacy_policy()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheaderprivacy_policy');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['privacy_policys'] = $this->backend_privacy_policymodel->get_privacy_policyall('privacy_policyheader');
		$data['title'] = 'Header Privacy Policy Page';
		$data['link'] = 'header_privacy_policy_edit';
		$data['general_name'] = 'header_privacy_policy';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/privacy_policyheader');
	    $this->load->view('backend/templates/footer');
	}

	public function header_privacy_policy_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheaderprivacy_policy');

		$data['privacy_policy'] = $this->backend_privacy_policymodel->get_privacy_policy_by_id($id);
		$data['imageprivacy_policy'] = $this->backend_privacy_policymodel->getrow_image('header_privacy_policy');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Header Privacy Policy Page';
		$data['general_name'] = 'header_privacy_policy';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/privacy_policyheader_edit');
		$this->load->view('backend/templates/footer');
	}

	
	public function update_pageheaderprivacy_policy()
	{
		$this->backend_privacy_policymodel->update_privacy_policy();

		$config['upload_path']          = './assets/images/banner/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('image')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/banner/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= TRUE;
            $config1['width']= 1920;
            $config1['new_image']= './assets/images/banner/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $pic= $this->backend_privacy_policymodel->get_privacy_policyimage_by_refid('header_privacy_policy');
            $datapic = $pic['picture_name'];
            unlink('./assets/images/banner/'.$datapic);
			$this->backend_privacy_policymodel->update_headerprivacy_policy($data);
		}

		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect($this->agent->referrer());

	}

	//meta
	public function metapage()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_privacy_policymeta');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['meta'] = $this->backend_homemodel->get_metarow('metaprivacy_policy');
		$data['func'] = 'privacy_policy';
		$data['title'] = 'Privacy Policy';

		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/metapage');
	    $this->load->view('backend/templates/footer');
	}

	public function update_metapage()
	{
		$res=$this->backend_homemodel->update_metapage();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	
	public function privacy_policy()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_privacy_policy');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['gettings'] = $this->backend_privacy_policymodel->get_privacy_policyall('privacy_policy');
		$data['title'] = 'Privacy Policy Page';
		$data['link'] = 'privacy_policy_edit';
		$data['general_name'] = 'privacy_policy';

		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/privacy_policy');
	    $this->load->view('backend/templates/footer');
	}

	public function privacy_policy_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_privacy_policy');

		$data['privacy_policy'] = $this->backend_privacy_policymodel->get_privacy_policy_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Privacy Policy Page';
		$data['general_name'] = 'privacy_policy';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/privacy_policy_edit');
		$this->load->view('backend/templates/footer');
	}

	public function update_privacy_policy()
	{
		$res=$this->backend_privacy_policymodel->update_privacy_policy();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	

}