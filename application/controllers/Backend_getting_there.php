<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_getting_there extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }

	public function header_getting_there()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheadergetting_there');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['getting_theres'] = $this->backend_getting_theremodel->get_getting_thereall('getting_thereheader');
		$data['title'] = 'Header Getting There Page';
		$data['link'] = 'header_getting_there_edit';
		$data['general_name'] = 'header_getting_there';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/getting_thereheader');
	    $this->load->view('backend/templates/footer');
	}

	public function header_getting_there_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheadergetting_there');

		$data['getting_there'] = $this->backend_getting_theremodel->get_getting_there_by_id($id);
		$data['imagegetting_there'] = $this->backend_getting_theremodel->getrow_image('header_getting_there');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Header Getting There Page';
		$data['general_name'] = 'header_getting_there';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/getting_thereheader_edit');
		$this->load->view('backend/templates/footer');
	}

	
	public function update_pageheadergetting_there()
	{
		$this->backend_getting_theremodel->update_getting_there();

		$config['upload_path']          = './assets/images/banner/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('image')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/banner/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= TRUE;
            $config1['width']= 1920;
            $config1['new_image']= './assets/images/banner/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $pic= $this->backend_getting_theremodel->get_getting_thereimage_by_refid('header_getting_there');
            $datapic = $pic['picture_name'];
            unlink('./assets/images/banner/'.$datapic);
			$this->backend_getting_theremodel->update_headergetting_there($data);
		}

		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect($this->agent->referrer());

	}

	//meta
	public function metapage()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_getting_theremeta');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['meta'] = $this->backend_homemodel->get_metarow('metagetting_there');
		$data['func'] = 'getting_there';
		$data['title'] = 'Getting There';

		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/metapage');
	    $this->load->view('backend/templates/footer');
	}

	public function update_metapage()
	{
		$res=$this->backend_homemodel->update_metapage();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	
	public function getting_there()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_getting_there');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['gettings'] = $this->backend_getting_theremodel->get_getting_thereall('getting_there');
		$data['title'] = 'Getting There Page';
		$data['link'] = 'getting_there_edit';
		$data['general_name'] = 'getting_there';

		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/getting_there');
	    $this->load->view('backend/templates/footer');
	}

	public function getting_there_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_getting_there');

		$data['getting_there'] = $this->backend_getting_theremodel->get_getting_there_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Getting There Page';
		$data['general_name'] = 'getting_there';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/getting_there_edit');
		$this->load->view('backend/templates/footer');
	}

	public function update_getting_there()
	{
		$res=$this->backend_getting_theremodel->update_getting_there();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	

}