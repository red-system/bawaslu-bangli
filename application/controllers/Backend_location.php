<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_location extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }

	public function header_location()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheaderlocation');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['locations'] = $this->backend_locationmodel->get_locationall('locationheader');
		$data['title'] = 'Header Location Page';
		$data['link'] = 'header_location_edit';
		$data['general_name'] = 'header_location';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/locationheader');
	    $this->load->view('backend/templates/footer');
	}

	public function header_location_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheaderlocation');

		$data['location'] = $this->backend_locationmodel->get_location_by_id($id);
		$data['imagelocation'] = $this->backend_locationmodel->getrow_image('header_location');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Header  Location Page';
		$data['general_name'] = 'header_location';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/locationheader_edit');
		$this->load->view('backend/templates/footer');
	}

	

	public function about_location()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_aboutlocation');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['locations'] = $this->backend_locationmodel->get_locationall('locationabout');
		$data['imagelocation'] = $this->backend_locationmodel->get_image('about_location');
		$data['title'] = 'About Location';
		$data['link'] = 'about_location_edit';
		$data['general_name'] = 'about_location';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/location');
	    $this->load->view('backend/templates/footer');
	}

	public function about_location_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_aboutlocation');

		$data['location'] = $this->backend_locationmodel->get_location_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'About location';
		$data['general_name'] = 'about_location';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/location_edit');
		$this->load->view('backend/templates/footer');
	}

	public function about_location_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_aboutlocation');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'About location';
		$data['general_name'] = 'about_location';

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/locationimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function condition()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_condition');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['locations'] = $this->backend_locationmodel->get_locationall('locationcondition');
		$data['imagelocation'] = $this->backend_locationmodel->get_image('condition');
		$data['title'] = 'Condition';
		$data['link'] = 'condition_edit';
		$data['general_name'] = 'condition';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/location');
	    $this->load->view('backend/templates/footer');
	}

	public function condition_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_condition');

		$data['location'] = $this->backend_locationmodel->get_location_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Condition';
		$data['general_name'] = 'condition';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/location_edit');
		$this->load->view('backend/templates/footer');
	}

	public function condition_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_condition');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Condition';
		$data['general_name'] = 'condition';
		
		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/locationimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function map()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_map');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['locations'] = $this->backend_locationmodel->get_locationall('locationmap');
		$data['title'] = 'Map';
		$data['link'] = 'map_edit';
		$data['general_name'] = 'map';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/map');
	    $this->load->view('backend/templates/footer');
	}

	public function map_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_map');

		$data['location'] = $this->backend_locationmodel->get_location_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['image'] = $this->backend_locationmodel->getrow_image('map_location');
		$data['title'] = 'Map';
		$data['general_name'] = 'map';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/map_edit');
		$this->load->view('backend/templates/footer');
	}

	public function update_location()
	{
		$res=$this->backend_locationmodel->update_location();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function update_map()
	{
		$this->backend_locationmodel->update_location();
		
		$config['upload_path']          = './assets/images/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('image')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/banner/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= TRUE;
            $config1['width']= 585;
            $config1['new_image']= './assets/images/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $pic= $this->backend_locationmodel->getrow_image('map_location');
            $datapic = $pic['picture_name'];
            unlink('./assets/images/'.$datapic);
			$this->backend_locationmodel->update_maplocation($data);
		}

		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect($this->agent->referrer());

	}

	public function update_pageheaderlocation()
	{
		$this->backend_locationmodel->update_location();

		$config['upload_path']          = './assets/images/banner/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('image')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/banner/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= TRUE;
            $config1['width']= 1920;
            $config1['new_image']= './assets/images/banner/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $pic= $this->backend_locationmodel->get_locationimage_by_refid('header_location');
            $datapic = $pic['picture_name'];
            unlink('./assets/images/banner/'.$datapic);
			$this->backend_locationmodel->update_headerlocation($data);
		}

		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect($this->agent->referrer());

	}

	public function image_addprocess()
	{
		$general_name = $this->input->post('general_name');

		$post = $this->input->post();
	
		$config['upload_path']          = './assets/images/location';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;

		$this->upload->initialize($config);
		if($this->upload->do_upload('file'))
		{
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/location/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
            $config1['width']= 585;
            $config1['height']= 439;
            $config1['new_image']= './assets/images/location/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

           	$data = array(	'picture_name'		=> $gbr['file_name'],
							'general_ref_id'	=> $general_name,
						);
			$this->backend_locationmodel->image_add($data);
		}
	}

	public function image_delete($id)
	{
		$image= $this->backend_locationmodel->get_locationimage_by_id($id);
        $dataimage1 = $image['picture_name'];
       	unlink('./assets/images/location/'.$dataimage1);
        $this->backend_locationmodel->image_delete($id);
		$this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Delete image done!!!
				 </p>');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	//meta
	public function metapage()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_locationmeta');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['meta'] = $this->backend_homemodel->get_metarow('metalocation');
		$data['func'] = 'location';
		$data['title'] = 'Location';

		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/metapage');
	    $this->load->view('backend/templates/footer');
	}

	public function update_metapage()
	{
		$res=$this->backend_homemodel->update_metapage();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	
	

}