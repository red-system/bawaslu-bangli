<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_home extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
        $this->load->library('general');
        $this->load->helper("file");
    }

	public function homepicture()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_homepicture');

		$data['logofavicon'] = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['sliders'] = $this->backend_homemodel->get_homearray('slider');
		$data['title'] = 'Slider';

		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/homepicture');
	    $this->load->view('backend/templates/footer');
	}

	public function homepicture_add()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_homepicture');

		$data['logofavicon'] = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Slider';
		$data['link'] = 'homepicture';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/homepicture_add');
	    $this->load->view('backend/templates/footer');
	}


	public function homepicture_insert(){

		$this->load->library('upload');
        $config['upload_path']          = './assets/images/slider/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size'] = '10240000';
        $config['max_width']  = '10240000';
        $config['max_height']  = '7680000';
        $config['file_name'] = 'image-bawaslu-bangli';

        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload('picture'))
        {
            $error = array('error' => $this->upload->display_errors());
            redirect('backend_home/homepicture_add');
        }
        else
        {
            $filename = $this->upload->file_name;
            $data = array( 'general_name' => 'slider',
            			   'main_image' => $filename,
        				   'general_data' => $this->input->post('line1'),
        				   'general_sub_data' => $this->input->post('line2')
        				);
            $this->backend_homemodel->homepicture_insert($data);

            $path_from = './assets/images/slider/'.$filename;
            $path_to = './assets/images/slider/'.$filename;
            $this->general->resize_image($path_from, $path_to, 1920);

        }
			
		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Add done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect('backend_home/homepicture');
	}

	public function homepicture_edit($slider_id)
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_homepicture');

		$data['logofavicon'] = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['slider'] = $this->backend_homemodel->get_home_by_id($slider_id);
		$data['title'] = 'Slider';
		$data['link'] = 'homepicture';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/homepicture_edit');
	    $this->load->view('backend/templates/footer');
	}

	public function homepicture_update($slider_id){
		$this->load->library('upload');
        $config['upload_path']          = './assets/images/slider/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size'] = '10240000';
        $config['max_width']  = '10240000';
        $config['max_height']  = '7680000';
        $config['file_name'] = 'image-bawaslu-bangli';

        $this->upload->initialize($config);

        $where = array('general_id'=>$slider_id);

        if(!$this->upload->do_upload('picture')){
        	$data = array( 'general_name' => 'slider',
            			   'general_data' => $this->input->post('line1'),
        				   'general_sub_data' => $this->input->post('line2')
        				);
			$this->backend_homemodel->homepicture_update($where,$data);
			
		} else {
			$slider = $this->backend_homemodel->get_home_by_id($slider_id);
            unlink('./assets/images/slider/'.$slider['main_image']);

			$filename = $this->upload->file_name;
            $data = array( 'general_name' => 'slider',
            			   'main_image' => $filename,
        				   'general_data' => $this->input->post('line1'),
        				   'general_sub_data' => $this->input->post('line2')
        				);
            $this->backend_homemodel->homepicture_update($where,$data);

            $path_from = './assets/images/slider/'.$filename;
            $path_to = './assets/images/slider/'.$filename;
            $this->general->resize_image($path_from, $path_to, 1920);
			
		}
		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');

		$this->load->library('user_agent');
		redirect('backend_home/homepicture_edit/'.$slider_id);

	}

	public function homepicture_delete($slider_id)
	{
		$slider = $this->backend_homemodel->get_home_by_id($slider_id);
        unlink('./assets/images/slider/'.$slider['main_image']);
        
		$this->backend_homemodel->homepicture_delete($slider_id);
		$this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Delete picture done!!!
				 </p>');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	
	//meta
	public function metapage()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_homemeta');

		$data['logofavicon'] = $this->backend_headerfootermodel->get_headerfooter('logo_favicon');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['meta'] = $this->backend_homemodel->get_metarow('metahome');
		$data['func'] = 'home';
		$data['title'] = 'Home';

		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/metapage');
	    $this->load->view('backend/templates/footer');
	}

	public function update_metapage()
	{
		$res=$this->backend_homemodel->update_metapage();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}


	//meta	

}