<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_menu_title extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }

	public function index()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_menutitle');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['viewdetailen'] = $this->backend_menutitlemodel->get_menutitle('viewdetailen');
		$data['viewdetailde'] = $this->backend_menutitlemodel->get_menutitle('viewdetailde');
		$data['homeen'] = $this->backend_menutitlemodel->get_menutitle('homeen');
		$data['homede'] = $this->backend_menutitlemodel->get_menutitle('homede');
		$data['thevillaen'] = $this->backend_menutitlemodel->get_menutitle('thevillaen');
		$data['thevillade'] = $this->backend_menutitlemodel->get_menutitle('thevillade');
		$data['locationen'] = $this->backend_menutitlemodel->get_menutitle('locationen');
		$data['locationde'] = $this->backend_menutitlemodel->get_menutitle('locationde');
		$data['offersen'] = $this->backend_menutitlemodel->get_menutitle('offersen');
		$data['offersde'] = $this->backend_menutitlemodel->get_menutitle('offersde');
		$data['galleryen'] = $this->backend_menutitlemodel->get_menutitle('galleryen');
		$data['galleryde'] = $this->backend_menutitlemodel->get_menutitle('galleryde');
		$data['availabilityen'] = $this->backend_menutitlemodel->get_menutitle('availabilityen');
		$data['availabilityde'] = $this->backend_menutitlemodel->get_menutitle('availabilityde');
		$data['guestbooken'] = $this->backend_menutitlemodel->get_menutitle('guestbooken');
		$data['guestbookde'] = $this->backend_menutitlemodel->get_menutitle('guestbookde');
		$data['pricingen'] = $this->backend_menutitlemodel->get_menutitle('pricingen');
		$data['pricingde'] = $this->backend_menutitlemodel->get_menutitle('pricingde');
		$data['reservationen'] = $this->backend_menutitlemodel->get_menutitle('reservationen');
		$data['reservationde'] = $this->backend_menutitlemodel->get_menutitle('reservationde');
		$data['contacten'] = $this->backend_menutitlemodel->get_menutitle('contacten');
		$data['contactde'] = $this->backend_menutitlemodel->get_menutitle('contactde');
		$data['groundplanen'] = $this->backend_menutitlemodel->get_menutitle('groundplanen');
		$data['groundplande'] = $this->backend_menutitlemodel->get_menutitle('groundplande');
		$data['paradiseen'] = $this->backend_menutitlemodel->get_menutitle('paradiseen');
		$data['paradisede'] = $this->backend_menutitlemodel->get_menutitle('paradisede');
		$data['serviceen'] = $this->backend_menutitlemodel->get_menutitle('serviceen');
		$data['servicede'] = $this->backend_menutitlemodel->get_menutitle('servicede');
		$data['abouten'] = $this->backend_menutitlemodel->get_menutitle('abouten');
		$data['aboutde'] = $this->backend_menutitlemodel->get_menutitle('aboutde');
		$data['generousen'] = $this->backend_menutitlemodel->get_menutitle('generousen');
		$data['generousde'] = $this->backend_menutitlemodel->get_menutitle('generousde');
		$data['luxuriousen'] = $this->backend_menutitlemodel->get_menutitle('luxuriousen');
		$data['luxuriousde'] = $this->backend_menutitlemodel->get_menutitle('luxuriousde');
		$data['bathroomsen'] = $this->backend_menutitlemodel->get_menutitle('bathroomsen');
		$data['bathroomsde'] = $this->backend_menutitlemodel->get_menutitle('bathroomsde');
		$data['kitchenen'] = $this->backend_menutitlemodel->get_menutitle('kitchenen');
		$data['kitchende'] = $this->backend_menutitlemodel->get_menutitle('kitchende');
		$data['bedroomsen'] = $this->backend_menutitlemodel->get_menutitle('bedroomsen');
		$data['bedroomsde'] = $this->backend_menutitlemodel->get_menutitle('bedroomsde');
		$data['rooftopen'] = $this->backend_menutitlemodel->get_menutitle('rooftopen');
		$data['rooftopde'] = $this->backend_menutitlemodel->get_menutitle('rooftopde');
		$data['gettingen'] = $this->backend_menutitlemodel->get_menutitle('gettingen');
		$data['gettingde'] = $this->backend_menutitlemodel->get_menutitle('gettingde');
		$data['dreamen'] = $this->backend_menutitlemodel->get_menutitle('dreamen');
		$data['dreamde'] = $this->backend_menutitlemodel->get_menutitle('dreamde');
		$data['infoen'] = $this->backend_menutitlemodel->get_menutitle('infoen');
		$data['infode'] = $this->backend_menutitlemodel->get_menutitle('infode');
		$data['privacyen'] = $this->backend_menutitlemodel->get_menutitle('privacyen');
		$data['privacyde'] = $this->backend_menutitlemodel->get_menutitle('privacyde');

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/menutitle');
	    $this->load->view('backend/templates/footer');
	}

	public function update_menutitle(){
		$this->backend_menutitlemodel->update_groundplanen();
		$this->backend_menutitlemodel->update_groundplande();
		$this->backend_menutitlemodel->update_homeen();
		$this->backend_menutitlemodel->update_homede();
		$this->backend_menutitlemodel->update_paradiseen();
		$this->backend_menutitlemodel->update_paradisede();
		$this->backend_menutitlemodel->update_serviceen();
		$this->backend_menutitlemodel->update_servicede();
		$this->backend_menutitlemodel->update_abouten();
		$this->backend_menutitlemodel->update_aboutde();
		$this->backend_menutitlemodel->update_thevillaen();
		$this->backend_menutitlemodel->update_thevillade();
		$this->backend_menutitlemodel->update_locationen();
		$this->backend_menutitlemodel->update_locationde();
		$this->backend_menutitlemodel->update_generousen();
		$this->backend_menutitlemodel->update_generousde();
		$this->backend_menutitlemodel->update_luxuriousen();
		$this->backend_menutitlemodel->update_luxuriousde();
		$this->backend_menutitlemodel->update_bathroomsen();
		$this->backend_menutitlemodel->update_bathroomsde();
		$this->backend_menutitlemodel->update_kitchenen();
		$this->backend_menutitlemodel->update_kitchende();
		$this->backend_menutitlemodel->update_bedroomsen();
		$this->backend_menutitlemodel->update_bedroomsde();
		$this->backend_menutitlemodel->update_rooftopen();
		$this->backend_menutitlemodel->update_rooftopde();
		$this->backend_menutitlemodel->update_offersen();
		$this->backend_menutitlemodel->update_offersde();
		$this->backend_menutitlemodel->update_galleryen();
		$this->backend_menutitlemodel->update_galleryde();
		$this->backend_menutitlemodel->update_infoen();
		$this->backend_menutitlemodel->update_infode();
		$this->backend_menutitlemodel->update_availabilityen();
		$this->backend_menutitlemodel->update_availabilityde();
		$this->backend_menutitlemodel->update_guestbooken();
		$this->backend_menutitlemodel->update_guestbookde();
		$this->backend_menutitlemodel->update_pricingen();
		$this->backend_menutitlemodel->update_pricingde();
		$this->backend_menutitlemodel->update_reservationen();
		$this->backend_menutitlemodel->update_reservationde();
		$this->backend_menutitlemodel->update_contacten();
		$this->backend_menutitlemodel->update_contactde();
		$this->backend_menutitlemodel->update_gettingen();
		$this->backend_menutitlemodel->update_gettingde();
		$this->backend_menutitlemodel->update_dreamen();
		$this->backend_menutitlemodel->update_dreamde();
		$this->backend_menutitlemodel->update_privacyen();
		$this->backend_menutitlemodel->update_privacyde();
				
		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
}