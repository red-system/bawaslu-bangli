module.exports = function(grunt) {

  var base_js = [
      'assets/js/lib/jquery-1.11.0.min.js',
      'assets/js/lib/jquery-ui.min.js',
      'assets/js/lib/bootstrap.min.js',
      'assets/js/lib/bootstrap-select.js',
      'assets/js/lib/isotope.pkgd.min.js',
      'assets/js/lib/jquery.themepunch.revolution.min.js',
      'assets/js/lib/jquery.themepunch.tools.min.js',
      'assets/js/lib/owl.carousel.js',
      'assets/js/lib/jquery.appear.min.js',
      'assets/js/lib/jquery.countTo.js',
      'assets/js/lib/jquery.countdown.min.js',
      'assets/js/lib/jquery.parallax-1.1.3.js',
      'assets/js/lib/jquery.magnific-popup.min.js',
      'assets/js/lib/SmoothScroll.js',
      'assets/js/scripts.js'
  ];

  var base_css = [
      'assets/css/general.css',
      'assets/css/lib/font-awesome.min.css',
      'assets/css/lib/font-lotusicon.css',
      'assets/css/lib/bootstrap.min.css',
      'assets/css/lib/owl.carousel.css',
      'assets/css/lib/jquery-ui.min.css',
      'assets/css/lib/magnific-popup.css',
      'assets/css/lib/settings.css',
      'assets/css/lib/bootstrap-select.min.css',
      'assets/css/helper.css',
      'assets/css/modal.css',
      'assets/css/style.css'
  ];

  // Begin thevilla

  var thevilla_js = [
      'assets/js/map-marker.js',
      'assets/js/map-marker2.js',
      'assets/js/slide-groudplan.js',
      'assets/js/iframe-js.js'
  ];
  thevilla_js = base_js.concat(thevilla_js);
  var thevilla_css = [
      'assets/css/map-marker.css',
      'assets/css/map-marker2.css',
      'assets/css/slide-groundplan.css',
  ];
  thevilla_css = base_css.concat(thevilla_css);
  var thevilla_file = thevilla_js.concat(thevilla_css);

  // End thevilla
  // Begin Guestbook

  var guestbook_js = [
      'assets/js/mymodal.js',
      'assets/plugin/sweet-alert/sweet-alert.min.js',
      'assets/js/guestbook.js'
  ];
  guestbook_js = base_js.concat(guestbook_js);
  var guestbook_css = [
      'assets/plugin/sweet-alert/sweet-alert.css'
  ];
  guestbook_css = base_css.concat(guestbook_css);
  var guestbook_file = guestbook_js.concat(guestbook_css);

  // End Guestbook
  // Begin Contact

  var contact_js = [
      'assets/plugin/sweet-alert/sweet-alert.min.js',
      'assets/js/reservation.js'
  ];
  contact_js = base_js.concat(contact_js);
  var contact_css = [
      'assets/plugin/sweet-alert/sweet-alert.css'
  ];
  contact_css = base_css.concat(contact_css);
  var contact_file = contact_js.concat(contact_css);

  // End Contact

  var all_file = guestbook_file.concat(contact_file);

  grunt.initConfig({
    jsDistDir: 'js/',
    cssDistDir: 'css/template/',
    pkg: grunt.file.readJSON('package.json'),
    concat: {
            base_js: {
                options: {
                    separator: ';'
                },
                src: base_js,
                dest: '<%=jsDistDir%>base.js'
            },
            base_css: {
                src: base_css,
                dest: '<%=cssDistDir%>base.css'
            },
            thevilla_js: {
                options: {
                    separator: ';'
                },
                src: thevilla_js,
                dest: '<%=jsDistDir%>thevilla.js'
            },
            thevilla_css: {
                src: thevilla_css,
                dest: '<%=cssDistDir%>thevilla.css'
            },
            guestbook_js: {
                options: {
                    separator: ';'
                },
                src: guestbook_js,
                dest: '<%=jsDistDir%>guestbook.js'
            },
            guestbook_css: {
                src: guestbook_css,
                dest: '<%=cssDistDir%>guestbook.css'
            },
            contact_js: {
                options: {
                    separator: ';'
                },
                src: contact_js,
                dest: '<%=jsDistDir%>contact.js'
            },
            contact_css: {
                src: contact_css,
                dest: '<%=cssDistDir%>contact.css'
            },
        },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          '<%=jsDistDir%>base.min.js': ['<%= concat.base_js.dest %>'],
          '<%=jsDistDir%>thevilla.min.js': ['<%= concat.thevilla_js.dest %>'],
          '<%=jsDistDir%>guestbook.min.js': ['<%= concat.guestbook_js.dest %>'],
          '<%=jsDistDir%>contact.min.js': ['<%= concat.contact_js.dest %>']
        }
      }
    },
    cssmin: {
      add_banner: {
        options: {
          banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
        },
        files: {
          '<%=cssDistDir%>base.min.css': ['<%= concat.base_css.dest %>'],
          '<%=cssDistDir%>thevilla.min.css': ['<%= concat.thevilla_css.dest %>'],
          '<%=cssDistDir%>guestbook.min.css': ['<%= concat.guestbook_css.dest %>'],
          '<%=cssDistDir%>contact.min.css': ['<%= concat.contact_css.dest %>']
        }
      }
    },
    watch: {
      files: all_file,
      tasks: ['concat', 'uglify', 'cssmin']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', [
      'concat',
      'uglify',
      'cssmin',
      'watch'
  ]);

};
  