/*
Navicat MySQL Data Transfer

Source Server         : Local C
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : gtn_bawaslu

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-08-05 10:24:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_blog
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog`;
CREATE TABLE `tb_blog` (
  `blog_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `value_id` int(11) DEFAULT NULL,
  `blog_title` varchar(255) DEFAULT '',
  `blog_content` text DEFAULT NULL,
  `blog_thumbnail` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`blog_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_blog
-- ----------------------------
INSERT INTO `tb_blog` VALUES ('2', null, null, 'Kecamatan Bangli', '<p>Desc Kecamatan Bangli</p>\r\n', null, 'Kecamatan Bangli', 'Kecamatan Bangli', 'Kecamatan Bangli');
INSERT INTO `tb_blog` VALUES ('3', null, null, 'Kecamatan Kintamani', '<p>Kecamatan Bangli</p>\r\n', null, 'tesKecamatan Bangli', 'Kecamatan Bangli', 'Kecamatan Bangli');
INSERT INTO `tb_blog` VALUES ('4', null, null, 'Kecamatan Susut', '<p>Kecamatan Susut</p>\r\n', null, 'Kecamatan Susut', 'Kecamatan Susut', 'Kecamatan Susut');
INSERT INTO `tb_blog` VALUES ('5', null, null, 'Kecamatan Tembuku', '<p>Kecamatan Tembuku</p>\r\n', null, 'Kecamatan Tembuku', 'Kecamatan Tembuku', 'Kecamatan Tembuku');
INSERT INTO `tb_blog` VALUES ('7', '0', '2', '1', '<p>2</p>\r\n', 'wallpaper3.jpg', '3', '4', '5');
INSERT INTO `tb_blog` VALUES ('8', '0', '3', '1', '<p>2</p>\r\n', 'express-checkout-hero-sg.png', '3', '4', '5');

-- ----------------------------
-- Table structure for tb_day
-- ----------------------------
DROP TABLE IF EXISTS `tb_day`;
CREATE TABLE `tb_day` (
  `id_day` int(9) NOT NULL AUTO_INCREMENT,
  `ref_id` int(9) NOT NULL,
  `day` date NOT NULL,
  `info` varchar(250) NOT NULL,
  `status` varchar(200) NOT NULL,
  PRIMARY KEY (`id_day`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tb_day
-- ----------------------------
INSERT INTO `tb_day` VALUES ('74', '8', '2019-07-09', 'checkIn', 'booked');
INSERT INTO `tb_day` VALUES ('75', '8', '2019-07-10', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('76', '8', '2019-07-11', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('77', '8', '2019-07-12', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('78', '8', '2019-07-13', 'checkOut', 'booked');
INSERT INTO `tb_day` VALUES ('79', '9', '2019-07-27', 'checkIn', 'booked');
INSERT INTO `tb_day` VALUES ('80', '9', '2019-07-28', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('81', '9', '2019-07-29', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('82', '9', '2019-07-30', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('83', '9', '2019-07-31', 'checkOut', 'booked');
INSERT INTO `tb_day` VALUES ('84', '10', '2019-08-09', 'checkIn', 'booked');
INSERT INTO `tb_day` VALUES ('85', '10', '2019-08-10', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('86', '10', '2019-08-11', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('87', '10', '2019-08-12', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('88', '10', '2019-08-13', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('89', '10', '2019-08-14', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('90', '10', '2019-08-15', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('91', '10', '2019-08-16', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('92', '10', '2019-08-17', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('93', '10', '2019-08-18', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('94', '10', '2019-08-19', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('95', '10', '2019-08-20', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('96', '10', '2019-08-21', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('97', '10', '2019-08-22', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('98', '10', '2019-08-23', 'checkOut', 'booked');
INSERT INTO `tb_day` VALUES ('103', '12', '2019-12-26', 'checkIn', 'booked');
INSERT INTO `tb_day` VALUES ('104', '12', '2019-12-27', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('105', '12', '2019-12-28', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('106', '12', '2019-12-29', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('107', '12', '2019-12-30', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('108', '12', '2019-12-31', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('109', '12', '2020-01-01', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('110', '12', '2020-01-02', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('111', '12', '2020-01-03', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('112', '12', '2020-01-04', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('113', '12', '2020-01-05', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('114', '12', '2020-01-06', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('115', '12', '2020-01-07', 'checkOut', 'booked');
INSERT INTO `tb_day` VALUES ('128', '11', '2019-09-05', 'checkIn', 'booked');
INSERT INTO `tb_day` VALUES ('129', '11', '2019-09-06', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('130', '11', '2019-09-07', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('131', '11', '2019-09-08', 'checkOut', 'booked');
INSERT INTO `tb_day` VALUES ('134', '14', '2019-10-16', 'checkIn', 'booked');
INSERT INTO `tb_day` VALUES ('135', '14', '2019-10-17', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('136', '14', '2019-10-18', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('137', '14', '2019-10-19', 'checkOut', 'booked');
INSERT INTO `tb_day` VALUES ('138', '13', '2019-09-15', 'checkIn', 'booked');
INSERT INTO `tb_day` VALUES ('139', '13', '2019-09-16', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('140', '13', '2019-09-17', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('141', '13', '2019-09-18', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('142', '13', '2019-09-19', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('143', '13', '2019-09-20', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('144', '13', '2019-09-21', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('145', '13', '2019-09-22', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('146', '13', '2019-09-23', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('147', '13', '2019-09-24', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('148', '13', '2019-09-25', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('149', '13', '2019-09-26', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('150', '13', '2019-09-27', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('151', '13', '2019-09-28', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('152', '13', '2019-09-29', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('153', '13', '2019-09-30', 'booked', 'booked');
INSERT INTO `tb_day` VALUES ('154', '13', '2019-10-01', 'checkOut', 'booked');

-- ----------------------------
-- Table structure for tb_district
-- ----------------------------
DROP TABLE IF EXISTS `tb_district`;
CREATE TABLE `tb_district` (
  `district_id` int(11) NOT NULL AUTO_INCREMENT,
  `district_name` varchar(255) DEFAULT NULL,
  `district_description` text DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`district_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_district
-- ----------------------------
INSERT INTO `tb_district` VALUES ('2', 'Kecamatan Bangli', '<p>Desc Kecamatan Bangli</p>\r\n', 'Kecamatan Bangli', 'Kecamatan Bangli', 'Kecamatan Bangli');
INSERT INTO `tb_district` VALUES ('3', 'Kecamatan Kintamani', '<p>Kecamatan Bangli</p>\r\n', 'tesKecamatan Bangli', 'Kecamatan Bangli', 'Kecamatan Bangli');
INSERT INTO `tb_district` VALUES ('4', 'Kecamatan Susut', '<p>Kecamatan Susut</p>\r\n', 'Kecamatan Susut', 'Kecamatan Susut', 'Kecamatan Susut');
INSERT INTO `tb_district` VALUES ('5', 'Kecamatan Tembuku', '<p>Kecamatan Tembuku</p>\r\n', 'Kecamatan Tembuku', 'Kecamatan Tembuku', 'Kecamatan Tembuku');

-- ----------------------------
-- Table structure for tb_general_data
-- ----------------------------
DROP TABLE IF EXISTS `tb_general_data`;
CREATE TABLE `tb_general_data` (
  `general_id` int(9) NOT NULL AUTO_INCREMENT,
  `general_ref_id` int(9) NOT NULL,
  `oreder` int(9) NOT NULL,
  `general_name` varchar(200) NOT NULL,
  `general_sub_name` varchar(500) NOT NULL,
  `general_data` varchar(500) NOT NULL,
  `general_sub_data` varchar(500) NOT NULL,
  `general_desc` text NOT NULL,
  `general_sub_desc` text NOT NULL,
  `general_link` varchar(500) NOT NULL,
  `main_image` varchar(200) NOT NULL,
  `secondary_image` varchar(200) NOT NULL,
  `general_password` varchar(500) NOT NULL,
  `general_lang` varchar(100) NOT NULL,
  `general_created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `general_updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `general_additional_info` text NOT NULL,
  `facebook_link` text NOT NULL,
  `instagram_link` text NOT NULL,
  `twitter_link` text NOT NULL,
  `email_link` text NOT NULL,
  PRIMARY KEY (`general_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tb_general_data
-- ----------------------------
INSERT INTO `tb_general_data` VALUES ('1', '0', '0', 'headerfooter', 'address', 'Br.Dinas Antasari, Ds.Pacung, Kec.Tejakula, Buleleng, Bali', '', '', '', '', '', '', '', '', '2019-03-13 17:54:36', '2019-04-23 13:26:35', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('2', '0', '0', 'headerfooter', 'phone1', '+62 857 3920 4351 (Bali)', '', '', '', '', '', '', '', '', '2019-03-13 17:56:07', '2019-07-09 13:15:18', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('3', '0', '0', 'headerfooter', 'email', 'info@samarihillvillas.com', '', '', '', '', '', '', '', '', '2019-03-13 17:56:51', '2019-06-24 13:17:20', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('4', '0', '0', 'headerfooter', 'logo_header', '', '', '', '', '', 'logo-Samari-Hill-Villas.png', '', '', '', '2019-03-13 17:58:02', '2019-07-29 12:17:13', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('5', '0', '0', 'headerfooter', 'logo_footer', '', '', '', '', '', 'logo-footer-Samari-Hill-Villas.png', 'logo-footer-Samari-Hill-Villas_thumb.png', '', '', '2019-03-13 17:58:54', '2019-05-28 17:11:01', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('6', '0', '0', 'headerfooterquote', 'quoteen', '', '', 'Great Stay & Convenient Location', '', '', '', '', '', 'en', '2019-03-14 15:24:20', '2019-04-10 12:45:14', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('7', '0', '0', 'headerfooterquote', 'quotede', '', '', 'Toller Aufenthalt und günstige Lage', '', '', '', '', '', 'de', '2019-03-14 15:24:40', '2019-04-10 12:45:18', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('8', '0', '0', 'headerfooterbutton', 'buttonen', 'BOOK NOW', '', '', '', '', '', '', '', 'en', '2019-03-14 16:13:35', '2019-04-10 12:51:27', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('9', '0', '0', 'headerfooterbutton', 'buttonde', 'JETZT BUCHEN', '', '', '', '', '', '', '', 'de', '2019-03-14 16:15:28', '2019-04-10 12:51:31', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('10', '0', '0', 'home', 'slide', '', '', '', '', '', 'wallpaper.jpg', '', '', '', '2019-03-15 17:50:55', '2019-08-04 22:37:15', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('11', '0', '0', 'home', 'icon', '', '', '', '', '', 'logo-samari-hill-villas1.png', '', '', '', '2019-03-15 17:51:50', '2019-06-26 17:43:43', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('12', '0', '0', 'homepicture', 'homepicture', 'WELCOME TO', 'SAMARI HILL VILLAS', '', '', '', '', '', '', 'en', '2019-03-15 18:05:33', '2019-04-10 13:01:41', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('13', '0', '0', 'homepicture', 'homepicture', 'WILLKOMMEN IN', 'SAMARI HILL VILLAS', '', '', '', '', '', '', 'de', '2019-03-15 18:06:27', '2019-07-11 22:37:27', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('20', '0', '0', 'homeparadise_on_earth', 'homeparadise_on_earth', 'Paradise On Earth', '', '<p style=\"margin-left:0in; margin-right:0in; text-align:justify\"><span style=\"font-size:12pt\"><span style=\"background-color:white\"><span style=\"font-size:10.0pt\"><span style=\"font-family:Roboto\"><span style=\"color:#333333\">Together with our Bali family, we have been able to fulfill our long-standing dream in the north of the beautiful, Indonesian island in the Indian Ocean and realize the project &#39;SAMARI HILL VILLAS&#39;.</span></span></span></span></span></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\"><span style=\"font-size:12pt\"><span style=\"background-color:white\"><span style=\"font-size:10.0pt\"><span style=\"font-family:Roboto\"><span style=\"color:#333333\">The 8-cornered dream villa with rooftop and 12 x 5 meter freshwater infinity pool stands on a 5&#39;500 m2 large property, on a hillside with 180 degree sea view. The whole property with the villa is only reserved for you. In a totally quiet and secluded location Bali can be enjoyed from its most beautiful side. You can admire the most wonderful sunrises and sunsets directly from the pool or the terrace - pure nature in a luxurious ambience!</span></span></span></span></span></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:start\"><span style=\"font-size:12pt\"><span style=\"background-color:white\"><span style=\"font-size:10.0pt\"><span style=\"font-family:Roboto\"><span style=\"color:#333333\">f.l.t.r: Made Santiara, Rini Astuti (doctor), Marlise, Roger,</span></span></span></span></span></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:start\"><span style=\"font-size:12pt\"><span style=\"background-color:white\"><span style=\"font-size:10.0pt\"><span style=\"font-family:Roboto\"><span style=\"color:#333333\">front: Sathya, Rio</span></span></span></span></span></p>\r\n', '', '', 'aboutus.jpg', '', '', 'en', '2019-03-27 14:20:16', '2019-07-11 22:48:34', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('21', '0', '0', 'homeparadise_on_earth', 'homeparadise_on_earth', 'Paradies Auf Erden', '', '<p style=\"margin-left:0in; margin-right:0in; text-align:justify\"><span style=\"font-size:14px\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-family:Roboto\">Mit unserer Bali-Familie zusammen haben wir im Norden der wundersch&ouml;nen, zu Indonesien geh&ouml;renden Insel im indischen Ozean, unseren langj&auml;hrigen Traum erf&uuml;llen und das Projekt &sbquo;SAMARI HILL VILLAS&lsquo; realisieren k&ouml;nnen.</span></span></span></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\"><span style=\"font-size:14px\"><span style=\"background-color:white\"><span style=\"font-family:Roboto\"><span style=\"color:#333333\"><span style=\"font-family:Roboto\">Die 8-eckige Traumvilla mit Hochsitz und 12 x 5 Meter S&uuml;sswasser-Infinity-Pool steht auf einem 5&lsquo;500 m2 grossen Grundst&uuml;ck, in Hanglage mit 180 Grad Meersicht. <strong>Das ganze Grundst&uuml;ck mit der Villa ist nur f&uuml;r Dich allein reserviert. </strong>In<strong> </strong>total<strong> </strong>ruhiger<strong> </strong>und<strong> </strong>abgeschiedener Lage kann<strong> </strong>Bali<strong> </strong>von<strong> </strong>seiner<strong> </strong>sch&ouml;nsten<strong> </strong>Seite<strong> </strong>genossen<strong> </strong>werden. Die wundervollsten Sonnenauf und -unterg&auml;nge kannst Du hier direkt vom Pool oder der Terrasse aus bewundern, Natur pur in luxeri&ouml;sem Ambiente!</span></span></span></span></span></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\"><span style=\"font-size:14px\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-family:Roboto\">v.l.n.r.: Made Santiara, Rini Astuti (&Auml;rztin), Marlise, Roger, </span></span></span></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\"><span style=\"font-size:14px\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-family:Roboto\">vorne: Sathya, Rio </span></span></span></p>\r\n', '', '', 'aboutus1.jpg', '', '', 'de', '2019-03-27 14:20:38', '2019-07-10 12:44:39', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('22', '0', '0', 'homeservice', 'homeservice', 'Service', '', '<p style=\"margin-left:0in; margin-right:0in; text-align:justify\"><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:10.0pt\"><span style=\"font-family:Roboto\">Made and his team Putri and Putu (Housekeeping and cook / gardener) welcome and pamper you with Balinese kindness.&nbsp;</span></span></span></span><span style=\"font-size:10.0pt\"><span style=\"font-family:Roboto\">Plan a tour with the new air-conditioned luxury van? or request special dishes? just meet Made! He will fulfill all your wishes with pleasure</span></span></p>\r\n', '', '', 'ourbest.jpg', '', '', 'en', '2019-03-27 19:21:26', '2019-07-05 18:43:43', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('23', '0', '0', 'homeservice', 'homeservice', 'Balinesische Herzlichkeit', '', '<p style=\"text-align:justify\">Made und sein Team Putri und Putu (Houseservice und K&ouml;chin /G&auml;rtner) begr&uuml;ssen und verw&ouml;hnen Dich mit balinesischer Herzlichkeit.&nbsp;Seien es Ausfl&uuml;ge mit dem neuen klimatisierten, luxeri&ouml;sen Van oder spezielle Gerichte, wende Dich an Made, er wird Dir s&auml;mtliche W&uuml;nsche mit Freude versuchen zu erf&uuml;llen.</p>\r\n', '', '', 'ourbest(1).jpg', '', '', 'de', '2019-03-27 19:22:01', '2019-07-05 18:43:36', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('36', '0', '0', 'thevillaabout', 'aboutvillaen', 'The Villa', '', '<p style=\"text-align:justify\"><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:10.0pt\"><span style=\"font-family:Roboto\">The villa has 2 air-conditioned bedrooms with 2 open bathrooms, a large living room with open kitchen and a bar, a large gallery with additional double bed, spacious, covered terrace and a &#39;rooftop&#39; equipped with cozy corner and loungers over the Master Bathroom, including a wonderful view over the sea and over the property.</span></span></span></span></p>\r\n', '', '', '', '', '', 'en', '2019-03-29 13:05:33', '2019-07-05 18:23:37', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('37', '0', '0', 'thevillaabout', 'aboutvillade', 'Die Villa', '', '<p style=\"text-align:justify\">Die Villa hat 2 klimatisierte Schlafzimmer mit 2&nbsp;offenen&nbsp;Badezimmern, einen grossen Living Room&nbsp;mit&nbsp;offener K&uuml;che und eine Bar, eine grosse Galerie&nbsp;mit&nbsp;zus&auml;tzlichem&nbsp;&nbsp;Doppelbett, grossz&uuml;gige, gedeckte Terrasse sowie einen &sbquo;Hochsitz&lsquo; ausgestattet mit Kuschelecke und Liegen &uuml;ber dem Masterbathroom, inklusive einem wunderbaren Blick &uuml;ber&lsquo;s Meer und &uuml;ber das Grundst&uuml;ck.</p>\r\n', '', '', '', '', '', 'de', '2019-03-29 13:06:34', '2019-07-05 18:23:14', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('38', '0', '0', 'thevillalocation', 'areavillaen', 'Location', '', '<p style=\"margin-left:0in; margin-right:0in; text-align:justify\"><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:10.0pt\"><span style=\"font-family:Roboto\">On the beautiful north coast of Bali, in the village of Pacung on a hill with 180 degree sea view is our property.&nbsp;</span></span></span></span><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:10.0pt\"><span style=\"font-family:Roboto\">Let yourself be enchanted by the uniqueness of the North Balinese coastal region, which has not yet been destroyed by tourism, and enjoy your stay with all your senses.</span></span></span></span></p>\r\n', '', '', '', '', '', 'en', '2019-03-29 15:25:28', '2019-07-05 18:27:08', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('39', '0', '0', 'thevillalocation', 'areavillade', 'Lage', '', '<p style=\"text-align:justify\">An der wundersch&ouml;nen Nordk&uuml;ste von Bali, im Dorf Pacung auf einem H&uuml;gel gelegen mit 180 Grad Meersicht befindet sich unser Anwesen. Lass Dich von der Einzigartigkeit der vom Tourismus noch nicht zerst&ouml;rten nordbalinesischen K&uuml;stenregion verzaubern und genie&szlig;Deinen&nbsp;Aufenthalt mit allen Sinnen.</p>\r\n', '', '', '', '', '', 'de', '2019-03-29 15:26:01', '2019-07-05 18:26:40', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('40', '0', '0', 'generous_surrounding', 'buildingvillaen', 'Generous Surrounding', '', '<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:10.0pt\"><span style=\"font-family:Roboto\">... a sensational and beautifully maintained surrounding awaits you on your little walks ...</span></span></span></span></p>\r\n', '', '', '', '', '', 'en', '2019-03-29 17:39:22', '2019-07-05 18:27:31', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('41', '0', '0', 'generous_surrounding', 'buildingvillade', 'Grosszügiger Umschwung', '', '<p style=\"text-align:justify\">... ein sensationeller und wunderbar gepflegter Umschwung erwartet Dich auf Deinen kleinen Spazierg&auml;ngen...</p>\r\n', '', '', '', '', '', 'de', '2019-03-29 17:39:58', '2019-07-05 18:27:48', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('43', '0', '0', 'luxurious_furnishing', 'comfortvillaen', 'Luxurious Furnishing', '', '<p style=\"text-align:justify\"><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:10.0pt\"><span style=\"font-family:Roboto\">... the luxurious interior in a wonderful environment and unique ambience leaves nothing to be desired: &quot;just enjoy and feel good!&quot;</span></span></span></span></p>\r\n', '', '', '', '', '', 'en', '2019-03-29 17:46:02', '2019-07-05 18:30:11', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('44', '0', '0', 'luxurious_furnishing', 'comfortvillade', 'Luxeriöse Einrichtung', '', '<p style=\"text-align:justify\">... die luxuri&ouml;se Einrichtung in wunderbarster Umgebung und einmaligem Ambiente l&auml;sst keine W&uuml;nsche offen: &quot;just enjoy and feel good!&ldquo;</p>\r\n', '', '', '', '', '', 'de', '2019-03-29 17:46:30', '2019-07-05 18:30:51', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('45', '0', '0', 'thevillaheader', 'headervillaen', 'The Samari Hill Villas', '', '', '', '', '', '', '', 'en', '2019-03-30 13:43:03', '2019-04-09 18:46:08', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('46', '0', '0', 'thevillaheader', 'headervillade', 'The Samari Hill Villas de', '', '', '', '', '', '', '', 'de', '2019-03-30 13:43:32', '2019-04-09 18:47:59', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('47', '0', '0', 'bathrooms', 'comfortvillaen', 'Bathrooms', '', '<p style=\"margin-left:0in; margin-right:0in\"><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-family:Roboto\"><span style=\"color:#222222\">... pure comfort and absolute hygiene await you in the bathrooms ...</span></span></span></span></p>\r\n', '', '', '', '', '', 'en', '2019-04-01 14:50:57', '2019-07-22 15:11:40', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('48', '0', '0', 'bathrooms', 'comfortvillade', 'Badezimmer', '', '<p style=\"text-align:justify\">...purer Komfort und absolute Hygiene erwarten Dich in den Badezimmern...</p>\r\n', '', '', '', '', '', 'de', '2019-04-01 15:10:52', '2019-07-17 15:50:55', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('52', '0', '0', 'locationheader', 'locationheaderen', 'Location Of The Villa', '', '', '', '', '', '', '', 'en', '2019-04-01 18:07:22', '2019-04-09 18:46:13', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('53', '0', '0', 'locationheader', 'locationheaderde', 'Lage der Villa', '', '', '', '', '', '', '', 'de', '2019-04-01 18:07:44', '2019-04-09 18:47:53', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('54', '0', '0', 'locationabout', 'aboutlocationen', 'Location of the villa', '', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text.</p>\r\n', '', '', '', '', '', 'en', '2019-04-01 18:52:59', '2019-04-09 18:46:16', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('55', '0', '0', 'locationabout', 'aboutlocationde', 'Lage der Villa', '', '<p>it is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text.</p>\r\n', '', '', '', '', '', 'de', '2019-04-01 18:53:01', '2019-04-09 18:47:51', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('56', '0', '0', 'locationcondition', 'conditionen', 'Directly at the sea', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '', '', '', '', '', 'en', '2019-04-01 20:10:27', '2019-04-09 18:46:18', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('57', '0', '0', 'locationcondition', 'conditionde', 'Direkt am Meer', '', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '', '', '', '', '', 'de', '2019-04-01 20:11:02', '2019-04-09 18:47:49', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('58', '0', '0', 'locationmap', 'mapen', 'Far from tourism', '', '<p>Let yourself be enchanted by the uniqueness of the North Balinese coastal region, that is not yet ruined by tourism, and enjoy your stay with all your senses.</p>\r\n', '', '', '', '', '', 'en', '2019-04-02 12:49:01', '2019-04-09 18:46:20', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('59', '0', '0', 'locationmap', 'mapde', 'Weit weg vom Tourismus', '', '<p>Let yourself be enchanted by the uniqueness of the North Balinese coastal region, that is not yet ruined by tourism, and enjoy your stay with all your senses.</p>\r\n', '', '', '', '', '', 'de', '2019-04-02 12:49:19', '2019-04-09 18:47:47', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('60', '0', '0', 'offersheader', 'offersheaderen', 'Our offers', '', '', '', '', '', '', '', 'en', '2019-04-02 17:51:12', '2019-04-09 18:46:22', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('61', '0', '0', 'offersheader', 'offersheaderde', 'Unsere Angebote', '', '', '', '', '', '', '', 'de', '2019-04-02 17:51:44', '2019-04-09 18:47:45', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('62', '0', '0', 'offersfirst', 'offersfirsten', 'Relax and discover', '', '<p style=\"text-align:justify\">Do you just want to enjoy the tranquility on a lounger at the infinity pool, on the rooftop or in the garden, relax from everyday life and unwind, explore the surrounding area yourself with the provided scooter or take a day trip with Made to explore north and central Bali?</p>\r\n', '', '', '', '', '', 'en', '2019-04-04 13:55:10', '2019-07-05 18:46:24', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('63', '0', '0', 'offersfirst', 'offersfirstde', 'Entspannen und entdecken', '', '<p style=\"text-align:justify\">M&ouml;chtestDu einfach auf einer Liege am Infinity-Pool, auf dem Hochsitz oder im Garten die Ruhe geniessen, vom Alltag abschalten und die Seele baumeln lassen, die n&auml;here Umgebung mit dem zur Verf&uuml;gung gestellten Scooter selbst erkunden oder einen Tagesausflug&nbsp;mit Made zu den Sehensw&uuml;rdigkeiten Nord- und Zentralbalis unternehmen?</p>\r\n', '', '', '', '', '', 'de', '2019-04-04 13:56:03', '2019-07-05 18:46:12', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('64', '0', '0', 'offerssecond', 'offersseconden', 'The sky is the limit', '', '<p style=\"text-align:justify\">Do you want a massage? Or prepare Balinese food yourself? Feel-good guarantee for our guests is our passion. Our &#39;Dream Team&#39;, Made Santiara (Manager), Putri (Kitchen / Housekeeping) and Putu (Gardener) will make your stay a memorable one. Your wishes will be read from your lips, if not speak!</p>\r\n', '', '', '', '', '', 'en', '2019-04-04 14:13:04', '2019-07-05 18:46:53', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('65', '0', '0', 'offerssecond', 'offerssecondde', 'Der Himmel ist die Grenze', '', '<p style=\"text-align:justify\">Lust auf eine Massage? Oder balinesische Kost selber mal zubereiten? Wohlf&uuml;hlgarantie f&uuml;r unser G&auml;ste ist unsere Passion. Unser &#39;Dream-Team&lsquo;, Made Santiara (Manager), Putri (K&uuml;che/Houseservice) und Putu (G&auml;rtner) werden Deinen Aufenthalt zu einem unvergesslichen Erlebnis wahr werden lassen. Deine W&uuml;nsche werden Dir von den Lippen abgelesen, wenn nicht sprich!</p>\r\n', '', '', '', '', '', 'de', '2019-04-04 14:13:28', '2019-07-05 18:46:45', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('66', '0', '0', 'offersthird', 'offersthirden', 'Food and beverage', '', '<p style=\"text-align:justify\">Cleanliness and freshness are main concern in our kitchen. The dishes from our menu are always freshly prepared. You say when and what and Putri conjures you a delicious meal. The healthy breakfast with homemade bread, homemade jam, fresh tropical fruits, freshly squeezed fruit juice, egg choices and coffee from the espresso machine are served on our sea view terrace every morning with a smile.</p>\r\n', '', '', '', '', '', 'en', '2019-04-04 14:27:05', '2019-07-05 18:47:33', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('67', '0', '0', 'offersthird', 'offersthirdde', 'Essen und Trinken', '', '<p style=\"text-align:justify\">Sauberkeit und Frische sind in unserer K&uuml;che eine Selbstverst&auml;ndlichkeit. Die Speisen aus unserer Speisekarte werden stets frisch on time zubereitet. Du sagst wann und was und Putri zaubert Dir ein k&ouml;stliches Mahl. Das reichhaltige Fr&uuml;hst&uuml;ck mit selbstgemachtem Brot, selbstgemachter Marmelade, frischen Fr&uuml;chten, frischgepresstem Fruchtsaft, Eierspeisen und Kaffee aus der Espressomaschine wird Dir auf unserer Meerblick-Terrasse jeden Morgen mit einem L&auml;cheln serviert.</p>\r\n', '', '', '', '', '', 'de', '2019-04-04 14:27:15', '2019-07-05 18:47:21', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('68', '0', '0', 'offersfourth', 'offersfourthen', 'Refreshment', '', '<p style=\"text-align:justify\">Fresh drinking water from the water galleons is free. Chilled drinks are always available in our mini bar, just write down what you get out of the mini bar, and then will be charged beforeyour departure.</p>\r\n', '', '', '', '', '', 'en', '2019-04-04 14:43:10', '2019-07-05 18:47:48', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('69', '0', '0', 'offersfourth', 'offersfourthde', 'Erfrischung', '', '<p style=\"text-align:justify\">Frisches Trinkwasser aus der Wassergallone sind gratis. Gek&uuml;hlte Getr&auml;nke stehen Dir jederzeit in unserem K&uuml;hlschrank zur Verf&uuml;gung, schreib einfach auf, was Du aus dem K&uuml;hlschrank holst, abgerechnet wird bei Abreise.</p>\r\n', '', '', '', '', '', 'de', '2019-04-04 14:45:14', '2019-07-05 18:47:42', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('70', '0', '0', 'offersfifth', 'offersfifthen', 'Comfort on the way', '', '<p style=\"text-align:justify\">The rides are carried out with an air-conditioned, comfortable van. The extensive excursion program includes not only classic, mostly cultural orientated sightseeing tours, but also fun- and sport-oriented such as: Trekking or rafting. All excursions and tours are led by our lovely, perfect English and German speaking manager Made Santiara. He can explain you, more than any other, in a very competent way much of the culture and nature of Bali. You will definitely be excited.</p>\r\n', '', '', '', '', '', 'en', '2019-04-04 14:56:49', '2019-07-05 18:48:04', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('71', '0', '0', 'offersfifth', 'offersfifthde', 'Komfort auch unterwegs', '', '<p style=\"text-align:justify\">Die Fahrten werden mit einem klimatisierten, komfortablen Van&nbsp;durchgef&uuml;hrt. Das umfangreiche Ausflugsprogramm beinhaltet neben klassischen, meist kulturell orientierten&nbsp;Sightseeing-Touren, auch spa&szlig;- und sportorientierte Schwerpunkte, wie z.B.&nbsp;Trekking oder Rafting. Alle Ausfl&uuml;ge und Touren werden von unserem liebenswerten, gut deutsch sprechenden Manager Made Santiara geleitet. Er kann Dir,wie kein Anderer, auf sehr&nbsp;kompetente Weise&nbsp;viel von der Kultur und Natur Balis n&auml;herbringen. Du wirstdefinitivbegeistert sein.</p>\r\n', '', '', '', '', '', 'de', '2019-04-04 14:57:15', '2019-07-05 18:48:18', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('72', '0', '0', 'offerssixth', 'offerssixthen', 'Snorkeling and diving', '', '<p style=\"text-align:justify\">North Bali is famous for its beautiful black sandy beach with its colourful coral reefs, sponges, and fishes. Our manager Made will bring and show you to the places where you can enjoy the beuty of under water. There are many snorkeling and diving spots near the villa. Diving will be conducted by a profesional dive guide / dive master from some good dive centers near the villa.</p>\r\n', '', '', '', '', '', 'en', '2019-04-04 15:05:31', '2019-07-05 18:48:36', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('73', '0', '0', 'offerssixth', 'offerssixthde', 'Schnorcheln und Tauchen', '', '<p style=\"text-align:justify\">Nord-Bali ist ber&uuml;hmt f&uuml;r seinen wundersch&ouml;nen schwarzen Sandstrand mit seinen bunten Korallenriffen, Schw&auml;mmen und Fischen. Unser Manager Made bringt Dich zu den Orten, an denen Du die Sch&ouml;nheit der balinesischen Unterwasserwelt genie&szlig;en kannst. Es gibt viele Schnorchel- und Tauchpl&auml;tze in der N&auml;he der Villa. Das Tauchen wird von einem professionellen Tauchguide / Tauchlehrer aus einigen guten Tauchbasen in der N&auml;he der Villa durchgef&uuml;hrt.</p>\r\n', '', '', '', '', '', 'de', '2019-04-04 15:05:37', '2019-07-05 18:48:27', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('74', '0', '0', 'offersseventh', 'offersseventhen', 'Massage', '', '<p style=\"text-align:justify\">Complete your holiday with a relaxing massage by the pool. A professional massage therapist will come to the villa on your request. You can enjoy Head, Shoulder and Back Massage, Relaxing Foot Reflexology, Balinese Traditional Massage, or Indonesian Urut Massage. All massagechoicesarefor 1 hour.</p>\r\n', '', '', '', '', '', 'en', '2019-04-04 15:11:25', '2019-07-05 18:48:45', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('75', '0', '0', 'offersseventh', 'offersseventhde', 'Massage', '', '<p style=\"text-align:justify\">Vervollst&auml;ndige Deinen Urlaub mit einer entspannenden Massage im Bale am Pool. Eine professionelle Massagetherapeutin kommt in der Villa vorbei.Dukannst Kopf-, Schulter- und R&uuml;ckenmassage, entspannende Fu&szlig;reflexzonenmassage, traditionelle balinesische Massage oder indonesische Urut-Massage genie&szlig;en. Alle Massagen dauern 1 Stunde.</p>\r\n', '', '', '', '', '', 'de', '2019-04-04 15:11:29', '2019-07-05 18:48:51', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('76', '0', '0', 'offerseighth', 'offerseighthen', 'Massage', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '', '', '', '', '', 'en', '2019-04-04 15:20:33', '2019-04-09 18:46:39', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('77', '0', '0', 'offerseighth', 'offerseighthde', 'Massage', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '', '', '', '', '', 'de', '2019-04-04 15:20:37', '2019-04-09 18:47:24', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('78', '0', '0', 'pricingheader', 'pricingheaderen', 'Pricing', '', '', '', '', '', '', '', 'en', '2019-04-04 17:33:01', '2019-04-09 18:46:41', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('79', '0', '0', 'pricingheader', 'pricingheaderde', 'Die Preise', '', '', '', '', '', '', '', 'de', '2019-04-04 17:33:04', '2019-04-11 14:36:38', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('80', '0', '0', 'pricinglist', 'pricinglist', 'Pricelist (download)', 'Preislist (herunterladen)', '', '', '', 'Price_List_2019_Samari_Hill_Villas.pdf', '', '', '', '2019-04-04 18:25:51', '2019-07-09 15:01:44', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('85', '0', '0', 'galleryheader', 'galleryheaderen', 'Photo impressions', '', '', '', '', '', '', '', 'en', '2019-04-04 20:04:36', '2019-04-09 18:46:48', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('86', '0', '0', 'galleryheader', 'galleryheaderde', 'Fotoimpressionen', '', '', '', '', '', '', '', 'de', '2019-04-04 20:05:01', '2019-04-09 18:47:16', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('87', '0', '0', 'galleryfirst', 'galleryfirsten', 'Surrounded by palm trees', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing\r\npackages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy.', '', '', '', '', '', 'en', '2019-04-05 12:00:44', '2019-04-09 18:46:50', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('88', '0', '0', 'galleryfirst', 'galleryfirstde', 'Umgeben von Palmen', '', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy.</p>\r\n', '', '', '', '', '', 'de', '2019-04-05 12:00:49', '2019-04-09 18:47:13', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('89', '0', '0', 'gallerysecond', 'galleryseconden', 'Beautiful places', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '', '', '', '', '', 'en', '2019-04-05 12:02:17', '2019-04-09 18:46:53', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('90', '0', '0', 'gallerysecond', 'gallerysecondde', 'Schöne Orte', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '', '', '', '', '', 'de', '2019-04-05 12:02:50', '2019-04-09 18:47:12', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('91', '0', '0', 'gallerythevilla', 'gallerythevilla', 'The Villa', 'die Villa', '', '', '', '', '', '', '', '2019-04-05 13:00:19', '2019-04-05 13:06:46', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('92', '0', '0', 'galleryfacets', 'galleryfacets', 'Facets of Bali', 'Facetten von Bali', '', '', '', '', '', '', '', '2019-04-05 13:00:34', '2019-04-05 13:50:52', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('93', '0', '0', 'galleryadditional', 'galleryadditional', 'Additional gallery', 'Zusätzliche Galerie', '', '', '', '', '', '', '', '2019-04-05 13:02:38', '2019-04-05 13:02:38', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('94', '0', '0', 'guestbookheader', 'guestbookheaderen', 'Our guestbook', '', '', '', '', '', '', '', 'en', '2019-04-05 17:10:12', '2019-04-09 18:46:56', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('95', '0', '0', 'guestbookheader', 'guestbookheaderde', 'Unser Gästebuch', '', '', '', '', '', '', '', 'de', '2019-04-05 17:10:17', '2019-04-09 18:47:10', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('96', '0', '0', 'guestbookabout', 'guestbookabouten', 'READ OUR GUEST BOOK FORM CUSTOMER', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal .', 'WRITE IN GUEST BOOK', '', '', '', '', 'en', '2019-04-05 18:14:32', '2019-04-09 18:46:59', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('97', '0', '0', 'guestbookabout', 'guestbookaboutde', 'LESEN SIE UNSEREN GÄSTEBUCH-FORMULAR KUNDEN', '', 'Es ist seit langem bekannt, dass ein Leser beim Blick auf das Layout vom lesbaren Inhalt einer Seite abgelenkt wird. Der Punkt bei der Verwendung von Lorem Ipsum ist, dass es mehr oder weniger normal ist.', 'SCHREIBEN SIE IN GÄSTEBUCH', '', '', '', '', 'de', '2019-04-05 18:14:40', '2019-04-10 11:06:44', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('104', '0', '0', 'guestbooklist', '', 'David Gari', 'david@gmail.com', 'Love', 'Both the outstanding staff and the beautiful room made our first visit to Samari Hill Villas such a success! We enjoyed the appetizers during \"wine time\", the turndown service, the fresh flowers in our room and the breakfast delivered to our room in a wicker basket.. An attendant set it out for us in a charming fashion. We would not consider another property when we return to Samari Hill Villas!', 'Jakarta', 'noimage.png', '', '', '', '2019-04-08 13:54:20', '2019-04-10 23:52:59', 'checked', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('112', '0', '0', 'guestbookform', 'guestbookformen', 'GIVE ME YOU THING WITH US', 'Your Name', 'Your feedback means the world to us, it\'s how we improve our level of service. Feel free to share your experience if you\'ve stayed with us before.', 'Locations (Town / Country)', 'Testimonial Title', 'Your Testimonial', 'Profil Picture', 'WRITE IN GUEST BOOK', 'en', '2019-04-10 16:24:43', '2019-04-10 16:43:16', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('113', '0', '0', 'guestbookform', 'guestbookformde', 'Gib mir was mit uns', 'Dein Name', 'Ihr Feedback bedeutet für uns die Welt, wie wir unser Serviceniveau verbessern. Teilen Sie uns Ihre Erfahrungen mit, wenn Sie zuvor bei uns geblieben sind.', 'Standorte (Stadt / Land)', 'Testimonial Titel', 'Ihr Zeugnis', 'Profilbild', 'SCHREIBEN SIE IN GÄSTEBUCH', 'de', '2019-04-10 16:24:49', '2019-04-10 16:29:24', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('114', '0', '0', 'contactheader', 'contactheaderen', 'Contact Us', '', '', '', '', '', '', '', 'en', '2019-04-10 17:01:06', '2019-04-10 17:12:01', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('115', '0', '0', 'contactheader', 'contactheaderde', 'Kontaktiere uns', '', '', '', '', '', '', '', 'de', '2019-04-10 17:01:16', '2019-04-10 17:02:03', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('116', '0', '0', 'reservation', 'reservationen', 'YOUR RESERVATION', 'Name', 'Email', 'Phone number', 'Arrival', 'Departure', 'Guests', 'Message', 'en', '2019-04-10 18:47:39', '2019-07-05 13:05:20', 'Submit request', 'Security Code', '', '', '');
INSERT INTO `tb_general_data` VALUES ('117', '0', '0', 'reservation', 'reservationde', 'DEINE RESERVIERUNG', 'Name', 'E-mail', 'Telefonnummer', 'Anreise', 'Abreise', 'Anzahl Gäste', 'Nachricht', 'de', '2019-04-10 18:47:51', '2019-06-21 16:55:31', 'Anfrage senden', 'Sicherheitscode', '', '', '');
INSERT INTO `tb_general_data` VALUES ('118', '0', '0', 'contactcontent', 'contactcontenten', 'Do you still have questions? We are happy to help!', 'Location in Bali', '<p>Our team on site and also in our home country of Switzerland will help you with all your questions and the planning of your dream holiday in the Samari Hill Villa. Call or send an email.</p>\r\n', '<p style=\"text-align:justify\">On site you will be looked after by our very good German and English speaking manager Made Santiara. He lives with his wife Rini, a general practitioner, and his two sons, Sathya and Rio, just a few minutes from the villa. Made is delighted to be able to fulfill your wishes together with his lovely team. If you wish, he will show you much of what makes up the mystique and beauty of Bali. Let yourself be enchanted by the touristy unspoiled uniqueness of the North Balinese coastal region and enjoy with all your senses.</p>\r\n', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.050733080409!2d115.25277221534964!3d-8.09630909417121!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd19326c7c0b5f9%3A0x4816d5cbea4505e1!2sSamari+Hill+Villas!5e0!3m2!1sen!2sid!4v1562299857663!5m2!1sen!2sid\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', '', '', '', 'en', '2019-04-10 19:26:19', '2019-07-05 18:55:24', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('119', '0', '0', 'contactcontent', 'contactcontentde', 'Hast Du noch Fragen? Wir helfen gerne!', 'Lage auf Bali', '<p>Unser Team vor Ort und auch in unserer Heimat der Schweiz hilft Dir bei all Deinen Fragen und der Planung Deines Traumurlaubes in der Samari Hill Villa. Ruf an oder send ein eMail.</p>\r\n', '<p style=\"text-align:justify\">Vor Ort werden Sie durch unseren&nbsp;<strong>sehr gut deutsch und englisch sprechenden Manager Made Santiara</strong>&nbsp;betreut. Er wohnt zusammen mit seiner Frau Rini, einer Klinik&auml;rztin, und seinen zwei S&ouml;hnen Sathya und Rio nur wenige Minuten von der Villa entfernt. Made freut sich riesig, zusammen mit seinem liebenswerten Team&nbsp;<strong>Ihre W&uuml;nsche erf&uuml;llen</strong>&nbsp;zu d&uuml;rfen. Wenn Sie m&ouml;chten, wird er Ihnen viel von dem zeigen, was die<strong>&nbsp;Mystik und Sch&ouml;nheit<br />\r\nBalis</strong>&nbsp;ausmacht. Lassen Sie sich verzaubern von der touristisch unverdorbenen Einmaligkeit der nordbalinesischen K&uuml;stenregion und&nbsp;<strong>genie&szlig;en Sie mit allen Sinnen.</strong></p>\r\n', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.050733080409!2d115.25277221534964!3d-8.09630909417121!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd19326c7c0b5f9%3A0x4816d5cbea4505e1!2sSamari+Hill+Villas!5e0!3m2!1sen!2sid!4v1562299857663!5m2!1sen!2sid\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', '', '', '', 'de', '2019-04-10 19:26:31', '2019-07-05 18:55:12', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('120', '0', '0', 'availabilityheader', 'availabilityheaderen', 'Samari Hill Villas Availability', '', '', '', '', '', '', '', 'en', '2019-04-11 18:06:12', '2019-04-11 18:19:52', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('121', '0', '0', 'availabilityheader', 'availabilityheaderde', 'Belegungsplan Samari Hill Villas', '', '', '', '', '', '', '', 'de', '2019-04-11 18:06:22', '2019-04-11 18:19:17', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('122', '0', '0', 'headerfooter', 'phone2', '+41 79 350 47 33 (Switzerland)', '', '', '', '', '', '', '', '', '2019-04-23 13:11:22', '2019-07-05 23:54:22', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('126', '0', '0', 'metahome', 'Samari Hill Villas', 'Private Bali Villas for rent in Buleleng-Bali. Exceptional villas in Bali to suit any budget. ', 'Private Bali Villen zu vermieten in Buleleng-Bali. Außergewöhnliche Villen auf Bali für jedes Budget.', 'Bali villas, Bali Indonesia villas, Bali accommodation, Bali luxury villas, Bali pool villas, Bali villa rentals, Bali Indonesia villa rentals, Bali Indonesia rentals, Bali holiday, Bali Indonesia holiday', 'Bali Villen, Bali Indonesien Villen, Bali Unterkunft, Bali Luxus Villen, Bali Pool Villen, Bali Villen, Bali Indonesien Villen, Bali Indonesien Villen, Bali Ferien, Bali Indonesien Ferien', 'Samari Hill Villas', '', '', '', '', '2019-05-21 16:42:14', '2019-05-22 12:31:31', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('127', '0', '0', 'metathevilla', 'Information Samari Hill Villas', 'Information about Samari Hill Villas such as area, building, groundplan, and team.', 'Informationen zu Villen auf Samari-Hügeln wie Fläche, Gebäude, Grundriss und Team.', 'About Samari Hill Villas, Area Samari Hill Villas, Building Samari Hill Villas, Comfort Samari Hill Villas', 'Über Samari Hill Villas, Bereich Samari Hill Villas, Gebäude Samari Hill Villas, Komfort Samari Hill Villas', 'Informationen Samari Hill Villas', '', '', '', '', '2019-05-21 18:09:55', '2019-05-22 12:33:03', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('128', '0', '0', 'metalocation', 'Location Samari Hill Villas', 'Information about the location at Samari Hill Villas', 'Informationen zur Lage der Samari Hill Villas', 'Location, Map, Far from tourism', 'Lage, Karte, Weit weg vom Tourismus', 'Lage Samari Hill Villas', '', '', '', '', '2019-05-21 18:18:14', '2019-05-22 12:33:35', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('129', '0', '0', 'metaoffers', 'Offers Samari Hill Villas', 'Interesting offer owned by Samari Hill Villas.', 'Interessantes angebot von Samari Hill Villas', 'Offer, Relaxation, Massage, Tours', 'Angebot, Entspannung, Massage, Touren', 'Angebote Samari Hill Villas', '', '', '', '', '2019-05-21 19:07:35', '2019-05-22 12:34:00', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('130', '0', '0', 'metagallery', 'Gallery Samari Hill Villas', 'Image gallery at Samari Hill Villas.', 'Bildergalerie bei Samari Hill Villas.', 'gallery', 'galerie', 'Galerie Samari Hill Villas', '', '', '', '', '2019-05-21 19:17:19', '2019-05-22 12:34:38', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('131', '0', '0', 'metaavailability', 'Availability Samari Hill Villas', 'Information on the availability of villas in the Samari Hill Villas.', 'Informationen zur Verfügbarkeit von Villen in den Samari Hill Villas.', 'booking, reservation', 'Buchungs, Reservierung', 'Verfügbarkeit Samari Hill Villas', '', '', '', '', '2019-05-21 19:21:08', '2019-05-22 12:35:33', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('132', '0', '0', 'metaguestbook', 'Guestbook Samari Hill Villas', 'Guest reviews about Samari Hill Villas.', 'Gästebewertungen über Samari Hill Villas.', 'reviews, comments, good villa, beautiful place', 'Bewertungen, Kommentare, gute Villa, schöner Ort', 'Gästebuch Samari Hill Villas', '', '', '', '', '2019-05-22 12:03:49', '2019-05-22 12:36:00', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('133', '0', '0', 'metapricing', 'Pricing Samari Hill Villas', 'Rental price for Samari Hill Villas.', 'Mietpreis für Samari Hill Villen.', 'Cheap villas, Affordable villas, Nice villas', 'billige Villen, erschwingliche Villen, schöne Villen', 'Preise Samari Hill Villas', '', '', '', '', '2019-05-22 12:09:15', '2019-05-22 12:36:27', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('134', '0', '0', 'metacontact', 'Contact Samari Hill Villas', 'Booking and information on Samari Hill Villas.', 'Buchung und Informationen über Samari Hill Villas', 'booking villa, information villa', 'Buchung Villa, Informationen Villa', 'Kontaktieren Sie Samari Hill Villas', '', '', '', '', '2019-05-22 12:14:51', '2019-05-22 12:36:50', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('137', '0', '0', 'perfect_equipped_kitchen', 'comfortvillaen', 'Perfect equipped kitchen', '', '<p style=\"text-align:justify\"><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:10.0pt\"><span style=\"font-family:Roboto\">... let us spoil you with culinary delights, Putri will cook you the most exquisite dishes from our menu.....</span></span></span></span></p>\r\n', '', '', '', '', '', 'en', '2019-06-06 18:29:30', '2019-07-05 18:34:54', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('138', '0', '0', 'perfect_equipped_kitchen', 'comfortvillade', 'Top eingerichtete Küche', '', '<p style=\"text-align:justify\">...lass Dich kulinarisch verw&ouml;hnen, Putri&nbsp;kocht Dir die vorz&uuml;glichsten Gerichte aus&nbsp;unserer Speisekarte .....</p>\r\n', '', '', '', '', '', 'de', '2019-06-06 18:29:30', '2019-07-05 18:34:36', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('139', '0', '0', 'heavenly_bedrooms', 'comfortvillaen', 'Heavenly Bedrooms', '', '<p style=\"text-align:justify\"><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:10.0pt\"><span style=\"font-family:Roboto\">... the two air-conditioned bedrooms guarantee you a wonderful, restful sleep ...</span></span></span></span></p>\r\n', '', '', '', '', '', 'en', '2019-06-06 18:35:03', '2019-07-05 18:37:18', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('140', '0', '0', 'heavenly_bedrooms', 'comfortvillaen', 'Himmliche Schlafzimmer', '', '<p>... die beiden klimatisierten Schlafzimmer garantieren Dir einen wunderbaren, erholsamen Tiefschlaf...</p>\r\n', '', '', '', '', '', 'de', '2019-06-06 18:36:03', '2019-07-05 18:37:43', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('141', '0', '0', 'rooftop', 'comfortvillaen', 'Rooftop', '', '<p style=\"text-align:justify\"><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:10.0pt\"><span style=\"font-family:Roboto\">... a dreamlike world with wonderful foresight is ready to explore ...</span></span></span></span></p>\r\n', '', '', '', '', '', 'en', '2019-06-06 18:41:22', '2019-07-05 18:39:45', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('142', '0', '0', 'rooftop', 'comfortvillaen', 'Der Hochsitz', '', '<p style=\"text-align:justify\">... eine traumhafte Welt mit herrlicher Weitsicht liegt Dir zu F&uuml;ssen...</p>\r\n', '', '', '', '', '', 'de', '2019-06-06 18:41:26', '2019-07-05 18:38:26', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('143', '0', '0', 'villa_by_night', 'comfortvillaen', 'Samari Hill Villas by night', '', '<p style=\"text-align:justify\"><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:10.0pt\"><span style=\"font-family:Roboto\">... an indescribable experience ...</span></span></span></span></p>\r\n', '', '', '', '', '', 'en', '2019-06-06 18:46:52', '2019-07-05 18:41:24', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('144', '0', '0', 'villa_by_night', 'comfortvillaen', 'Samari Hill Villas bei Nacht', '', '<p style=\"text-align:justify\">... ein unbeschreibliches Erlebnis...</p>\r\n', '', '', '', '', '', 'de', '2019-06-06 18:47:00', '2019-07-05 18:41:07', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('145', '0', '0', 'menulist', 'menulist', 'Menulist (download)', 'Menüliste (herunterladen)', '', '', '', 'Menu_Samari_Hill_Villas_2019.pdf', '', '', '', '2019-06-21 16:17:59', '2019-07-09 15:03:26', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('146', '0', '0', 'getting_thereheader', 'getting_thereheaderen', 'Getting There', '', '', '', '', '', '', '', 'en', '2019-06-21 18:11:34', '2019-06-21 18:18:06', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('147', '0', '0', 'getting_thereheader', 'getting_thereheaderde', 'Anreise', '', '', '', '', '', '', '', 'de', '2019-06-21 18:12:20', '2019-06-21 18:18:38', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('148', '0', '0', 'metagetting_there', 'Getting There Samari Hill Villas', 'Getting There Samari Hill Villas', 'Anreise Samari Hill villas', 'getting there', 'anreise', 'Anreise Samari Hill villas', '', '', '', '', '2019-06-21 18:19:43', '2019-06-21 18:26:31', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('149', '0', '0', 'getting_there', '', '', '', '<p style=\"text-align:justify\"><span style=\"font-size:16px\"><strong>Arrival / How can I get there?</strong></span></p>\r\n\r\n<p style=\"text-align:justify\">Easy going. Book a flight to Denpasar (DPS) Ngurah Rai International Airport. There is no direct flight. From Switzerland we usually book via Qatarairways.com with a stopover in Doha or www.singaporeair.com / www.swiss.com with stopover in Singapore.</p>\r\n\r\n<p style=\"text-align:justify\">Arriving at the airport, you go to the arrivals hall. If you do not want to stay in Bali for more than 30 days, you will go directly to the immigration counter with a passport and boarding pass, and you will receive a 30-day free visa (this cannot be renewed, and you have to leave before the end of these 30 days). However, if you want to stay longer than 30 days and up to 60 days in the country, you have to turn to the counter &#39;Visa on Arrival&#39;, here you will receive for a fee (currently USD 35) a visa for 30 days, which can be extended for another 30 days (so totally 60 days, cannot be extended thereafter and you have to leave). Then, you pick up your luggage, go through customs and arrive in the outer area of the airport.</p>\r\n\r\n<p style=\"text-align:justify\">Made will wait for you there and a private sightseeing transfer from the airport in the tourist area of South Bali takes you through rice fields in Ubud, the surroundings of the volcano Batur and other beautiful landscapes and temples to the quiet, yet pristine north coast of Bali. After 3.5 hours driving you will reach the village of Pacung, where our villa is located. It is located on a beautiful hill with 180 degree sea views. Welcome!</p>\r\n', '', '', '', '', '', 'en', '2019-06-21 18:29:45', '2019-07-11 22:51:30', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('150', '0', '0', 'getting_there', '', '', '', '<p style=\"text-align:justify\"><span style=\"font-size:16px\"><strong>Anreise / Wie komme ich da hin</strong></span></p>\r\n\r\n<p style=\"text-align:justify\">Easy going. Buche einen Flug nach Denpasar (DPS) Ngurah Rai International Airport.&nbsp; Es gibt keinen Direktflug. Von der Schweiz aus buchen wir normalerweise via Qatarairways.com mit Zwischenhalt in Doha oder www.singaporeair.com / www.swiss.com mit Zwischenhalt in Singapore.</p>\r\n\r\n<p style=\"text-align:justify\">Am Airport angekommen, begibst Du Dich in die Ankunftshalle. Falls Du nicht l&auml;nger als 30 Tage in Bali bleiben m&ouml;chtest, begibst Du Dich direkt mit Pass und Boarding Pass zum Immigration Schalter, da erh&auml;ltst Du ein Visa f&uuml;r 30 Tage kostenlos (dieses kann nicht mehr verl&auml;ngert werden, und Du musst vor Ende dieser 30 Tage ausreisen). Wenn Du jedoch l&auml;nger als 30 Tage und bis zu 60 Tage im Land bleiben m&ouml;chtest, musst Du Dich an den Schalter &sbquo;Visa on Arrival&lsquo; wenden, hier erh&auml;ltst Du gegen Geb&uuml;hr (im Moment USD 35) ein Visa f&uuml;r 30 Tage, welches nochmals f&uuml;r 30 Tage verl&auml;ngert werden kann (also total 60 Tage, kann danach nicht mehr verl&auml;ngert werden und Du musst ausreisen). Dann holst Du Dein Gep&auml;ck, gehst durch den Zoll und kommst im Aussenbereich des Flughafens an.</p>\r\n\r\n<p style=\"text-align:justify\">Made wird Dich dort erwarten und ein privater Sightseeing-Transfer vom Flughafen im Touristengebiet S&uuml;d-Bali bringt Dich durch Reisfelder in Ubud, die Umgebung vom Vulkan Batur und andere wundersch&ouml;ne Landschaften und Tempel an die ruhige, noch urspr&uuml;ngliche Nordk&uuml;ste Balis. Nach 3,5 Stunden Fahrt erreichst Du das Dorf Pacung, wo sich unsere Villa befindet. Sie liegt auf einem wundersch&ouml;nen H&uuml;gel mit 180 Grad Meerblick. Welcome!</p>\r\n', '', '', '', '', '', 'de', '2019-06-21 18:30:03', '2019-07-05 18:56:51', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('151', '0', '0', 'dreamheader', 'dreamheaderen', 'A Dream Comes True', '', '', '', '', '', '', '', 'en', '2019-06-23 16:09:50', '2019-06-23 16:10:47', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('152', '0', '0', 'dreamheader', 'dreamheaderde', 'Ein Traum wird wahr', '', '', '', '', '', '', '', 'de', '2019-06-23 16:10:00', '2019-06-23 16:10:50', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('153', '0', '0', 'metadream', 'A Dream Comes True', 'A Dream Comes True Samarai Hill Villas', 'Ein Traum wird wahr Samari Hill Villas', 'dream', 'traum', 'Ein Traum wird wahr', '', '', '', '', '2019-06-23 16:16:59', '2019-06-23 16:18:32', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('154', '0', '0', 'dream', '', '', '', '<p style=\"text-align:center\"><span style=\"font-size:22px\"><strong>A dream comes true</strong></span></p>\r\n\r\n<p style=\"text-align:justify\"><br />\r\nThe desire to have a domicile in Asia has been dormant on us for a long time. The fact that it was finally realized in Bali was probably due to the fact that &#39;in the right place at the right time&#39;. After many trips to Asia (Philippines, Thailand, Myanmar, Maldives), we decided to visit the remote island of the gods Bali. It may have been a long trip for our tight 14 days of vacation, but it was definitely the right decision. Our criteria were and are still today: rest, relaxation, not too touristy and well-being guarantee.</p>\r\n\r\n<p style=\"text-align:justify\">Without further ado we booked in September 2010 a private villa in the north of Bali with pool and staff, the price was okay, not more expensive than a good hotel or bungalow resorts. Of course we were very excited about what to expect, because we did not know anything like that until then. Wow! It was overwhelming. Simply fantastic, the lovely, helpful and competent nature of the manager Made Santiara made us feel well looked after and like at home. So began our friendship with Made. From then on we were infected by the &#39;Bali virus&#39; and visited the island at least once or twice a year to get to know the country and the people better.</p>\r\n\r\n<p style=\"text-align:justify\">It was time, Made had the appropriate property, and everything went great. The Balinese architect was found, who could bring our ideas from the countless trips to Asia on paper and thus the realization nothing more stood in the way. Samari (Sathya, Made, Rini and now also Rio) is a reality. But then a health blow from Roger became a drama. Let&#39;s wait. Time passed, but in the fall of 2017 we said, now more than ever, we realize our dream.</p>\r\n\r\n<p style=\"text-align:justify\">Made has always left us the time we needed. Meanwhile, he has his own house in Les, not far from the villa, is married to Rini and has two sons. That Rini is a super great woman and also an excellent doctor with own practice, simplifies everything tremendously. During the construction phase, Made took over the construction, which he conscientiously and skillfully pulled through. We are very grateful to him for everything and very happy to always count on him. We have grown together to a Balinese / Swiss family. Home sweet home.</p>\r\n\r\n<p style=\"text-align:justify\">The villa has been completed since December 2018 and has become really beautiful. We are happy when we can share our second home with other people, allowing them to experience the warmth of the Balinese as we enjoy and appreciate it each time.</p>\r\n', '', '', '', '', '', 'en', '2019-06-23 16:22:12', '2019-07-29 11:39:40', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('155', '0', '0', 'dream', '', '', '', '<p style=\"text-align:center\"><span style=\"font-size:22px\"><strong>Ein Traum wird wahr</strong></span></p>\r\n\r\n<p style=\"text-align:justify\">Der Wunsch, ein Domizil in Asien zu haben, schlummerte schon lange in uns. Dass es schlussendlich in Bali realisiert wurde, war dann wohl auf die Tatsache zur&uuml;ckzuf&uuml;hren: &sbquo;zur rechten Zeit am richtigen Ort&lsquo;.</p>\r\n\r\n<p style=\"text-align:justify\">Nach vielen Asienreisen (Philippinen, Thailand, Myanmar, Malediven) fassten wir den Entschluss, mal die ferne G&ouml;tterinsel Bali zu besuchen. Ist zwar ein langer Trip f&uuml;r unsere knappen 14 Tage Urlaub, aber es war definitiv die richtige Entscheidung. Unsere Kriterien waren und sind es noch heute: Ruhe, Erholung, nicht zu touristisch und Wohlf&uuml;hlgarantie.</p>\r\n\r\n<p style=\"text-align:justify\">Kurzerhand buchten wir im September 2010 eine Privatvilla im Norden von Bali mit Pool und Personal, vom Preis her in Ordnung, nicht teurer als ein gutes Hotel oder Bungalowanlagen.Nat&uuml;rlich waren wir sehr gespannt was uns erwartet, denn sowas kannten wir bis dahin noch nicht. Wow! Es war &uuml;berw&auml;ltigend. Einfach traumhaft. Durch die liebenswerte, hilfsbereite und kompetente Art des Managers Made Santiara f&uuml;hlten wir uns bestens umsorgt und wie zuhause. So begann unsere Freundschaft mit Made. Von da an waren wir vom &sbquo;Bali Virus&lsquo; infiziert und haben die Insel jedes Jahr mindestens 1 bis 2 mal besucht um das Land und die Menschen besser kennenzulernen.</p>\r\n\r\n<p style=\"text-align:justify\">Es war es soweit, Made hatte das entsprechende Grundst&uuml;ck, alles lief super. Der balinesische Architekt war gefunden, welcher unsere Ideen aus den unz&auml;hligen Asienreisen auf Papier bringen konnte und somit stand der Realisierung nichts mehr im Weg.Samari (Sathya, Made, Rini und jetzt auch Rio) wird Realit&auml;t. Doch dann wurde ein gesundheitlicher Schicksalsschlag von Roger zum Drama. Mal abwarten. Die Zeit verging, aber im Herbst 2017 sagten wir uns, jetzt erst recht, wir realisieren unseren Traum.</p>\r\n\r\n<p style=\"text-align:justify\">Made hat uns die Zeit immer gelassen, die wir brauchten. Mittlerweile hat er sein eigenes Haus in Les, unweit der Villa, ist mit Rini verheiratet und hat zwei S&ouml;hne. Dass Rini eine supertolle Frau und dazu noch eine ausgezeichnete Aerztin mit eigener Praxis ist, vereinfacht alles ungemein.</p>\r\n\r\n<p style=\"text-align:justify\">W&auml;hrend der Bauphase hat Made die Bauf&uuml;hrung &uuml;bernommen, dies hat er gewissenhaft und gekonnt durchgezogen. Wir sind ihm f&uuml;r alles sehr dankbar und sehr gl&uuml;cklich, immer auf ihn z&auml;hlen zu k&ouml;nnen. Wir sind zu einer balinesischen/schweizerischen Familie zusammengewachsen. Home sweethome.</p>\r\n\r\n<p style=\"text-align:justify\">Die Villa ist nun seit Dezember 2018 fertiggestellt und ist wirklich wundersch&ouml;n geworden. Wir freuen uns, wenn wir unser zweites Heim mit anderen Leuten teilen k&ouml;nnen und sie dadurch die Herzlichkeit der Balinesen so erfahren d&uuml;rfen, wie wir es jedes Mal wieder aufs Neue geniessen und sch&auml;tzen.</p>\r\n', '', '', '', '', '', 'de', '2019-06-23 16:22:22', '2019-06-23 18:01:24', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('156', '0', '0', 'youtube', 'youtube', '', '', '<iframe width=\"100%\" height=\"450\" data-src=\"https://www.youtube.com/embed/5jcnJ5DL5HE\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '', '', '', '', '', '', '2019-06-23 18:18:57', '2019-07-05 12:56:26', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('158', '0', '0', 'privacy_policyheader', 'privacy_policyheaderen', 'Privacy Policy', '', '', '', '', '', '', '', 'en', '2019-07-05 15:48:58', '2019-07-05 15:50:35', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('159', '0', '0', 'privacy_policyheader', 'privacy_policyheaderde', 'Datenschutz-Bestimmungen', '', '', '', '', '', '', '', 'de', '2019-07-05 15:51:21', '2019-07-05 15:54:31', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('160', '0', '0', 'metaprivacy_policy', 'Privacy Policy12', 'Desciption about website12', 'Beschreibung der Website', 'privacy policy12', 'Datenschutz-Bestimmungen', 'Datenschutz-Bestimmungen', '', '', '', '', '2019-07-05 15:55:18', '2019-08-04 23:28:29', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('161', '0', '0', 'privacy_policy', '', '', '', '<h1 style=\"text-align:center\">Privacy Policy for Samari Hill Villas</h1>\r\n\r\n<p style=\"text-align:justify\">If you require any more information or have any questions about our privacy policy, please feel free to contact us by email at info@samarihillvillas.com .</p>\r\n\r\n<p style=\"text-align:justify\">At https://samarihillvillas we consider the privacy of our visitors to be extremely important. This privacy policy document describes in detail the types of personal information is collected and recorded by https://samarihillvillas and how we use it.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Log Files</strong><br />\r\nLike many other Web sites, https://samarihillvillas makes use of log files. These files merely logs visitors to the site - usually a standard procedure for hosting companies and a part of hosting services&#39;s analytics. The information inside the log files includes internet protocol (IP) addresses, browser type, Internet Service Provider (ISP), date/time stamp, referring/exit pages, and possibly the number of clicks. This information is used to analyze trends, administer the site, track user&#39;s movement around the site, and gather demographic information. IP addresses, and other such information are not linked to any information that is personally identifiable.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Cookies and Web Beacons</strong><br />\r\nhttps://samarihillvillas uses cookies to store information about visitors&#39; preferences, to record user-specific information on which pages the site visitor accesses or visits, and to personalize or customize our web page content based upon visitors&#39; browser type or other information that the visitor sends via their browser.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>DoubleClick DART Cookie</strong></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">&rarr; Google, as a third party vendor, uses cookies to serve ads on https://samarihillvillas.<br />\r\n&rarr; Google&#39;s use of the DART cookie enables it to serve ads to our site&#39;s visitors based upon their visit to https://samarihillvillas and other sites on the Internet.<br />\r\n&rarr; Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy at the following URL - <a href=\"http://www.google.com/privacy_ads.html\" title=\"Opt out of the Dart Cookie\">http://www.google.com/privacy_ads.html</a></p>\r\n\r\n<p style=\"text-align:justify\"><strong>Our Advertising Partners</strong></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">Some of our advertising partners may use cookies and web beacons on our site. Our advertising partners include .......</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<ul>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">These third-party ad servers or ad networks use technology in their respective advertisements and links that appear on https://samarihillvillas and which are sent directly to your browser. They automatically receive your IP address when this occurs. Other technologies (such as cookies, JavaScript, or Web Beacons) may also be used by our site&#39;s third-party ad networks to measure the effectiveness of their advertising campaigns and/or to personalize the advertising content that you see on the site.</p>\r\n\r\n<p style=\"text-align:justify\">https://samarihillvillas has no access to or control over these cookies that are used by third-party advertisers.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Third Party Privacy Policies</strong></p>\r\n\r\n<p style=\"text-align:justify\">If you wish to disable cookies, you may do so through your individual browser options. More detailed information about cookie management with specific web browsers can be found at the browsers&#39; respective websites. <a href=\"http://www.privacypolicyonline.com/what-are-cookies\">What Are Cookies?</a></p>\r\n\r\n<p style=\"text-align:justify\"><strong>Children&#39;s Information</strong><br />\r\nWe believe it is important to provide added protection for children online. We encourage parents and guardians to spend time online with their children to observe, participate in and/or monitor and guide their online activity. https://samarihillvillas does not knowingly collect any personally identifiable information from children under the age of 13. If a parent or guardian believes that https://samarihillvillas has in its database the personally-identifiable information of a child under the age of 13, please contact us immediately (using the contact in the first paragraph) and we will use our best efforts to promptly remove such information from our records.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Online Privacy Policy Only</strong><br />\r\nThis privacy policy applies only to our online activities and is valid for visitors to our website and regarding information shared and/or collected there. This policy does not apply to any information collected offline or via channels other than this website.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Consent</strong><br />\r\nBy using our website, you hereby consent to our privacy policy and agree to its terms.</p>\r\n', '', '', '', '', '', 'en', '2019-07-05 16:00:28', '2019-07-05 19:29:22', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('162', '0', '0', 'privacy_policy', '', '', '', '<h1>Privacy Policy for Samari Hill Villas</h1>\r\n\r\n<p>If you require any more information or have any questions about our privacy policy, please feel free to contact us by email at info@samarihillvillas.com .</p>\r\n\r\n<p>At https://samarihillvillas we consider the privacy of our visitors to be extremely important. This privacy policy document describes in detail the types of personal information is collected and recorded by https://samarihillvillas and how we use it.</p>\r\n\r\n<p><strong>Log Files</strong><br />\r\nLike many other Web sites, https://samarihillvillas makes use of log files. These files merely logs visitors to the site - usually a standard procedure for hosting companies and a part of hosting services&#39;s analytics. The information inside the log files includes internet protocol (IP) addresses, browser type, Internet Service Provider (ISP), date/time stamp, referring/exit pages, and possibly the number of clicks. This information is used to analyze trends, administer the site, track user&#39;s movement around the site, and gather demographic information. IP addresses, and other such information are not linked to any information that is personally identifiable.</p>\r\n\r\n<p><strong>Cookies and Web Beacons</strong><br />\r\nhttps://samarihillvillas uses cookies to store information about visitors&#39; preferences, to record user-specific information on which pages the site visitor accesses or visits, and to personalize or customize our web page content based upon visitors&#39; browser type or other information that the visitor sends via their browser.</p>\r\n\r\n<p><strong>DoubleClick DART Cookie</strong></p>\r\n\r\n<p>&rarr; Google, as a third party vendor, uses cookies to serve ads on https://samarihillvillas.<br />\r\n&rarr; Google&#39;s use of the DART cookie enables it to serve ads to our site&#39;s visitors based upon their visit to https://samarihillvillas and other sites on the Internet.<br />\r\n&rarr; Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy at the following URL - <a href=\"http://www.google.com/privacy_ads.html\" title=\"Opt out of the Dart Cookie\">http://www.google.com/privacy_ads.html</a></p>\r\n\r\n<p><strong>Our Advertising Partners</strong></p>\r\n\r\n<p>Some of our advertising partners may use cookies and web beacons on our site. Our advertising partners include .......</p>\r\n\r\n<ul>\r\n</ul>\r\n\r\n<p>These third-party ad servers or ad networks use technology in their respective advertisements and links that appear on https://samarihillvillas and which are sent directly to your browser. They automatically receive your IP address when this occurs. Other technologies (such as cookies, JavaScript, or Web Beacons) may also be used by our site&#39;s third-party ad networks to measure the effectiveness of their advertising campaigns and/or to personalize the advertising content that you see on the site.</p>\r\n\r\n<p>https://samarihillvillas has no access to or control over these cookies that are used by third-party advertisers.</p>\r\n\r\n<p><strong>Third Party Privacy Policies</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>If you wish to disable cookies, you may do so through your individual browser options. More detailed information about cookie management with specific web browsers can be found at the browsers&#39; respective websites. <a href=\"http://www.privacypolicyonline.com/what-are-cookies\">What Are Cookies?</a></p>\r\n\r\n<p><strong>Children&#39;s Information</strong><br />\r\nWe believe it is important to provide added protection for children online. We encourage parents and guardians to spend time online with their children to observe, participate in and/or monitor and guide their online activity. https://samarihillvillas does not knowingly collect any personally identifiable information from children under the age of 13. If a parent or guardian believes that https://samarihillvillas has in its database the personally-identifiable information of a child under the age of 13, please contact us immediately (using the contact in the first paragraph) and we will use our best efforts to promptly remove such information from our records.</p>\r\n\r\n<p><strong>Online Privacy Policy Only</strong><br />\r\nThis privacy policy applies only to our online activities and is valid for visitors to our website and regarding information shared and/or collected there. This policy does not apply to any information collected offline or via channels other than this website.</p>\r\n\r\n<p><strong>Consent</strong><br />\r\nBy using our website, you hereby consent to our privacy policy and agree to its terms.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '', '', '', '', '', 'de', '2019-07-05 16:00:37', '2019-07-05 16:31:23', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('163', '0', '0', 'gallery_home', '', 'Gallery', '', '<p>.. create description</p>\r\n', '', '', '', '', '', 'en', '2019-07-26 15:33:25', '2019-07-26 15:39:37', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('164', '0', '0', 'gallery_home', '', 'Galerie', '', '<p>.. create description</p>\r\n', '', '', '', '', '', 'de', '2019-07-26 15:33:25', '2019-07-26 15:39:59', '', '', '', '', '');
INSERT INTO `tb_general_data` VALUES ('165', '0', '0', 'metakecamatan', 'Bawaslu Kecamatan', 'Bawaslu Kecamatan Desc', '', 'kecamatan, ', '', '', '', '', '', '', '2019-08-04 23:30:54', '2019-08-04 23:32:20', '', '', '', '', '');

-- ----------------------------
-- Table structure for tb_kamus
-- ----------------------------
DROP TABLE IF EXISTS `tb_kamus`;
CREATE TABLE `tb_kamus` (
  `id_kamus` int(9) NOT NULL AUTO_INCREMENT,
  `id_lang` varchar(200) NOT NULL,
  `kategori` varchar(100) NOT NULL,
  `title` varchar(250) NOT NULL,
  `param` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_kamus`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tb_kamus
-- ----------------------------
INSERT INTO `tb_kamus` VALUES ('1', 'en', 'viewdetail', '', 'viewdetailen', '2019-03-25 17:29:19', '2019-06-23 18:15:16');
INSERT INTO `tb_kamus` VALUES ('2', 'de', 'viewdetail', '', 'viewdetailde', '2019-04-11 11:18:03', '2019-06-23 18:15:16');
INSERT INTO `tb_kamus` VALUES ('3', 'en', 'home', 'Welcome', 'homeen', '2019-04-11 11:35:59', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('4', 'de', 'home', 'Welcome', 'homede', '2019-04-11 11:36:32', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('5', 'en', 'thevilla', 'The Villa', 'thevillaen', '2019-04-11 11:40:12', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('6', 'de', 'thevilla', 'Die Villa', 'thevillade', '2019-04-11 11:40:58', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('7', 'en', 'location', 'Location', 'locationen', '2019-04-11 11:43:25', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('8', 'de', 'location', 'Lage', 'locationde', '2019-04-11 11:43:40', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('9', 'en', 'offers', 'Offers', 'offersen', '2019-04-11 11:46:28', '2019-06-23 19:30:18');
INSERT INTO `tb_kamus` VALUES ('10', 'de', 'offers', 'Angebote', 'offersde', '2019-04-11 11:48:28', '2019-06-23 19:30:18');
INSERT INTO `tb_kamus` VALUES ('11', 'en', 'gallery', 'Gallery', 'galleryen', '2019-04-11 11:49:38', '2019-06-23 19:30:18');
INSERT INTO `tb_kamus` VALUES ('12', 'de', 'gallery', 'Galerie', 'galleryde', '2019-04-11 11:50:26', '2019-06-23 19:30:18');
INSERT INTO `tb_kamus` VALUES ('13', 'en', 'availability', 'Availability', 'availabilityen', '2019-04-11 11:52:40', '2019-06-23 19:30:18');
INSERT INTO `tb_kamus` VALUES ('14', 'de', 'availability', 'Belegung', 'availabilityde', '2019-04-11 11:53:06', '2019-06-23 19:30:18');
INSERT INTO `tb_kamus` VALUES ('15', 'en', 'guestbook', 'Guestbook', 'guestbooken', '2019-04-11 11:54:49', '2019-06-23 19:30:18');
INSERT INTO `tb_kamus` VALUES ('16', 'de', 'guestbook', 'Gästebuch', 'guestbookde', '2019-04-11 11:55:06', '2019-06-23 19:30:18');
INSERT INTO `tb_kamus` VALUES ('17', 'en', 'pricing', 'Pricing', 'pricingen', '2019-04-11 11:57:49', '2019-06-23 19:30:18');
INSERT INTO `tb_kamus` VALUES ('18', 'de', 'pricing', 'Preise', 'pricingde', '2019-04-11 11:58:08', '2019-06-23 19:30:18');
INSERT INTO `tb_kamus` VALUES ('19', 'en', 'contact', 'Contact', 'contacten', '2019-04-11 11:59:42', '2019-06-23 19:30:18');
INSERT INTO `tb_kamus` VALUES ('20', 'de', 'contact', 'Kontakt', 'contactde', '2019-04-11 12:00:07', '2019-06-23 19:30:18');
INSERT INTO `tb_kamus` VALUES ('21', 'en', 'teammember', '', 'teammemberen', '2019-04-11 13:52:02', '2019-06-23 18:15:17');
INSERT INTO `tb_kamus` VALUES ('22', 'de', 'teammember', '', 'teammemberde', '2019-04-11 13:52:28', '2019-06-23 18:15:17');
INSERT INTO `tb_kamus` VALUES ('23', 'en', 'groundplan', 'Groundplan', 'groundplanen', '2019-04-22 12:22:13', '2019-06-23 19:30:16');
INSERT INTO `tb_kamus` VALUES ('24', 'de', 'groundplan', 'Grundriss', 'groundplande', '2019-04-22 12:23:23', '2019-06-23 19:30:16');
INSERT INTO `tb_kamus` VALUES ('25', 'en', 'paradise', 'Paradise on Earth', 'paradiseen', '2019-06-23 18:54:34', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('26', 'de', 'paradise', 'Paradies Auf Erden', 'paradisede', '2019-06-23 18:54:58', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('27', 'en', 'service', 'Service', 'serviceen', '2019-06-23 18:55:32', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('28', 'de', 'service', 'Service', 'servicede', '2019-06-23 18:55:51', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('29', 'en', 'about', 'About', 'abouten', '2019-06-23 18:56:17', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('30', 'de', 'about', 'Über', 'aboutde', '2019-06-23 18:56:49', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('31', 'en', 'generous', 'Generous Surrounding', 'generousen', '2019-06-23 18:57:31', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('32', 'de', 'generous', 'Grosszügiger Umschwung', 'generousde', '2019-06-23 18:59:18', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('33', 'en', 'luxurious', 'Luxurious Furnishing', 'luxuriousen', '2019-06-23 19:00:28', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('34', 'de', 'luxurious', 'Luxeriöse Einrichtung', 'luxuriousde', '2019-06-23 19:01:10', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('35', 'en', 'bathrooms', 'Bathrooms', 'bathroomsen', '2019-06-23 19:01:17', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('36', 'de', 'bathrooms', 'Badezimmer', 'bathroomsde', '2019-06-23 19:01:20', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('37', 'en', 'kitchen', 'Perfect equipped kitchen', 'kitchenen', '2019-06-23 19:02:56', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('38', 'de', 'kitchen', 'Top eingerichtete Küche', 'kitchende', '2019-06-23 19:03:01', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('39', 'en', 'bedrooms', 'Heavenly Bedrooms', 'bedroomsen', '2019-06-23 19:04:11', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('40', 'de', 'bedrooms', 'Himmliche Schlafzimmer', 'bedroomsde', '2019-06-23 19:04:21', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('41', 'en', 'rooftop', 'Rooftop', 'rooftopen', '2019-06-23 19:05:26', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('42', 'de', 'rooftop', 'Hochsitz', 'rooftopde', '2019-06-23 19:05:29', '2019-06-23 19:30:17');
INSERT INTO `tb_kamus` VALUES ('43', 'en', 'getting', 'Getting There', 'gettingen', '2019-06-23 19:06:38', '2019-06-23 19:30:18');
INSERT INTO `tb_kamus` VALUES ('44', 'de', 'getting', 'Anreise', 'gettingde', '2019-06-23 19:06:46', '2019-06-23 19:30:18');
INSERT INTO `tb_kamus` VALUES ('45', 'en', 'dream', 'A Dream Comes True', 'dreamen', '2019-06-23 19:07:38', '2019-06-23 19:30:18');
INSERT INTO `tb_kamus` VALUES ('46', 'de', 'dream', 'Ein Traum Wird War', 'dreamde', '2019-06-23 19:07:45', '2019-06-23 19:30:18');
INSERT INTO `tb_kamus` VALUES ('47', 'en', 'info', 'Info', 'infoen', '2019-06-23 19:15:33', '2019-06-23 19:30:18');
INSERT INTO `tb_kamus` VALUES ('48', 'de', 'info', 'Info', 'infode', '2019-06-23 19:15:37', '2019-06-23 19:30:18');
INSERT INTO `tb_kamus` VALUES ('49', 'en', 'reservation', 'Reservation', 'reservationen', '2019-06-23 20:07:58', '2019-06-23 20:38:32');
INSERT INTO `tb_kamus` VALUES ('50', 'de', 'reservation', 'Reservierung', 'reservationde', '2019-06-23 20:08:27', '2019-06-23 20:38:32');
INSERT INTO `tb_kamus` VALUES ('51', 'en', 'privacy', 'Privacy Policy', 'privacyen', '2019-07-05 16:41:30', '2019-07-05 16:44:32');
INSERT INTO `tb_kamus` VALUES ('52', 'de', 'privacy', 'Datenschutz', 'privacyde', '2019-07-05 16:42:17', '2019-07-05 16:44:32');

-- ----------------------------
-- Table structure for tb_language
-- ----------------------------
DROP TABLE IF EXISTS `tb_language`;
CREATE TABLE `tb_language` (
  `id_lang` int(9) NOT NULL AUTO_INCREMENT,
  `code_lang` varchar(50) NOT NULL,
  `language` varchar(50) NOT NULL,
  `lang_word` varchar(50) NOT NULL,
  PRIMARY KEY (`id_lang`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tb_language
-- ----------------------------
INSERT INTO `tb_language` VALUES ('1', 'de', 'Deutsch (German)', 'deutsch(german)');
INSERT INTO `tb_language` VALUES ('2', 'en', 'English (Englisch)', 'english(englisch)');

-- ----------------------------
-- Table structure for tb_location
-- ----------------------------
DROP TABLE IF EXISTS `tb_location`;
CREATE TABLE `tb_location` (
  `id_location` int(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  PRIMARY KEY (`id_location`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tb_location
-- ----------------------------
INSERT INTO `tb_location` VALUES ('1', 'Samari Hill Villas', '-8.096309060950142', '115.25496058166027');

-- ----------------------------
-- Table structure for tb_picture
-- ----------------------------
DROP TABLE IF EXISTS `tb_picture`;
CREATE TABLE `tb_picture` (
  `general_id` int(9) NOT NULL AUTO_INCREMENT,
  `general_ref_id` varchar(200) NOT NULL,
  `picture_name` varchar(200) NOT NULL,
  `second_picture` varchar(200) NOT NULL,
  PRIMARY KEY (`general_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=387 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tb_picture
-- ----------------------------
INSERT INTO `tb_picture` VALUES ('13', 'header_villa', 'page-header-the-villa-Samari-Hill-Villas1.jpg', '');
INSERT INTO `tb_picture` VALUES ('14', 'home_menulink1', 'Discover-Bali.jpg', '');
INSERT INTO `tb_picture` VALUES ('15', 'header_location', 'page-header-location-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('24', 'header_offers', 'page-header-offers-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('37', 'header_pricing', 'page-header-pricing-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('40', 'header_gallery', 'page-header-gallery-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('50', 'header_guestbook', 'page-header-guestbook-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('51', 'guestbook_about', 'form-guestbook.jpg', '');
INSERT INTO `tb_picture` VALUES ('52', 'homemenulink1', 'discover-Bali.jpg', '');
INSERT INTO `tb_picture` VALUES ('53', 'homemenulink2', 'experience-culture.jpg', '');
INSERT INTO `tb_picture` VALUES ('54', 'homemenulink3', 'sense-relaxation.jpg', '');
INSERT INTO `tb_picture` VALUES ('55', 'header_contact', 'page-header-contact-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('56', 'header_contact', 'page-header-contact-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('73', 'header_availability', 'page-header-availability-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('76', 'guestbook_gallery', 'guestbook-gallery-samari-hill-villa.jpg', '');
INSERT INTO `tb_picture` VALUES ('77', 'guestbook_gallery', 'guestbook-gallery.jpg', '');
INSERT INTO `tb_picture` VALUES ('110', 'about_location', 'location-villa-samari.jpg', '');
INSERT INTO `tb_picture` VALUES ('111', 'about_location', 'location-samari-hill-villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('112', 'condition', 'condition-Samari-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('113', 'condition', 'condition-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('114', 'map_location', 'Bali-map-airport-to-Samari-Hill-Villas.png', '');
INSERT INTO `tb_picture` VALUES ('115', 'offers_first', 'relax-Samari-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('116', 'offers_first', 'relax-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('117', 'offers_second', 'sky-limit-Samari-hill-villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('118', 'offers_third', 'food-and-drink-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('119', 'offers_fourth', 'refreshment-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('123', 'offers_eighth', 'massage-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('124', 'gallery_thevilla', 'Samari-Hill-Villas.jpg', 'Samari-Hill-Villas_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('125', 'gallery_thevilla', 'bed-Samari-Hill-Villas.jpg', 'bed-Samari-Hill-Villas_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('126', 'gallery_facets', 'barong-dance.jpg', 'barong-dance_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('127', 'gallery_facets', 'GWK.jpg', 'GWK_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('128', 'gallery_facets', 'pura-agung.jpg', 'pura-agung_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('129', 'gallery_additional', 'food-drink-Samari-Hill-Villas.jpg', 'food-drink-Samari-Hill-Villas_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('130', 'gallery_additional', 'ulundanu-beratan-Bali.jpg', 'ulundanu-beratan-Bali_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('134', 'first_feeling', 'feel-comfort-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('135', 'second_feeling', 'comfort-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('139', 'location', 'Bali-map-airport-to-Samari-Hill-Villas.png', '');
INSERT INTO `tb_picture` VALUES ('144', 'villa_by_night', 'the-area-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('145', 'header_getting_there', 'page-header-getting-there-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('146', 'header_dream', 'page-header-a-dream-comes-true-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('169', 'generous_surrounding', 'generous-surrounding-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('174', 'perfect_equipped_kitchen', 'kitchen-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('176', 'heavenly_bedrooms', 'bedrooms-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('177', 'rooftop', 'rooftop-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('182', 'offers_seventh', 'massage-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('183', 'offers_sixth', 'snorkeling-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('184', 'offers_fifth', 'comfort-even-on-the-way-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('186', 'header_privacy_policy', 'page-header-privacy-policy-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('187', 'homeparadise_on_earth', 'the-Samari-Hill-Villas-Paradise-on-Earth.jpg', '');
INSERT INTO `tb_picture` VALUES ('189', 'homeservice', 'the-Samari-Hill-Villas-Team1.jpg', '');
INSERT INTO `tb_picture` VALUES ('190', 'generous_surrounding', 'garden-Samari-Hill-Villa-2.jpg', '');
INSERT INTO `tb_picture` VALUES ('191', 'generous_surrounding', 'garden-Samari-Hill-Villa-3.jpg', '');
INSERT INTO `tb_picture` VALUES ('192', 'generous_surrounding', 'garden-Samari-Hill-Villa-4.jpg', '');
INSERT INTO `tb_picture` VALUES ('193', 'generous_surrounding', 'garden-Samari-Hill-Villa-5.jpg', '');
INSERT INTO `tb_picture` VALUES ('194', 'generous_surrounding', 'garden-Samari-Hill-Villa-6.jpg', '');
INSERT INTO `tb_picture` VALUES ('195', 'luxurious_furnishing', 'furniture-Samari-Hill-Villa-1.jpg', '');
INSERT INTO `tb_picture` VALUES ('197', 'luxurious_furnishing', 'living-room-Samari-Hill-Villa-1.jpg', '');
INSERT INTO `tb_picture` VALUES ('198', 'luxurious_furnishing', 'living-room-Samari-Hill-Villa-2.jpg', '');
INSERT INTO `tb_picture` VALUES ('199', 'rooftop', 'roof-top-Samari-Hill-Villa-1.jpg', '');
INSERT INTO `tb_picture` VALUES ('201', 'rooftop', 'roof-top-Samari-Hill-Villa-3.jpg', '');
INSERT INTO `tb_picture` VALUES ('202', 'rooftop', 'roof-top-Samari-Hill-Villa-4.jpg', '');
INSERT INTO `tb_picture` VALUES ('203', 'gallery_thevilla', 'roof-top-Samari-Hill-Villa-2.jpg', 'roof-top-Samari-Hill-Villa-2_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('204', 'gallery_thevilla', 'the-Samari-Hill-Villas.jpg', 'the-Samari-Hill-Villas_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('213', 'about_villa', 'home-page-Samari-Hill-Villa.jpg', '');
INSERT INTO `tb_picture` VALUES ('214', 'villa_by_night', 'Samari-Hill-Villas-at-night-1.jpg', '');
INSERT INTO `tb_picture` VALUES ('215', 'villa_by_night', 'Samari-Hill-Villas-at-night-3.jpg', '');
INSERT INTO `tb_picture` VALUES ('219', 'about_villa', 'the-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('228', 'perfect_equipped_kitchen', 'kitchen-Samari-Hill-Villa-1.JPG', '');
INSERT INTO `tb_picture` VALUES ('229', 'perfect_equipped_kitchen', 'kitchen-Samari-Hill-Villa-4.JPG', '');
INSERT INTO `tb_picture` VALUES ('230', 'perfect_equipped_kitchen', 'kitchen-Samari-Hill-Villa-3.JPG', '');
INSERT INTO `tb_picture` VALUES ('231', 'heavenly_bedrooms', 'bedroom-Samari-Hill-Villa-1.JPG', '');
INSERT INTO `tb_picture` VALUES ('232', 'heavenly_bedrooms', 'bedroom-Samari-Hill-Villa-2.JPG', '');
INSERT INTO `tb_picture` VALUES ('233', 'heavenly_bedrooms', 'bedroom-Samari-Hill-Villa-3.JPG', '');
INSERT INTO `tb_picture` VALUES ('234', 'heavenly_bedrooms', 'bedroom-Samari-Hill-Villa-4.JPG', '');
INSERT INTO `tb_picture` VALUES ('235', 'heavenly_bedrooms', 'bedroom-Samari-Hill-Villa-5.JPG', '');
INSERT INTO `tb_picture` VALUES ('236', 'bathrooms', 'bathroom-Samari-Hill-Villa-12.JPG', '');
INSERT INTO `tb_picture` VALUES ('237', 'bathrooms', 'bathroom-Samari-Hill-Villa-1.JPG', '');
INSERT INTO `tb_picture` VALUES ('238', 'bathrooms', 'bathroom-Samari-Hill-Villa-2.JPG', '');
INSERT INTO `tb_picture` VALUES ('239', 'bathrooms', 'bathroom-Samari-Hill-Villa-3.jpg', '');
INSERT INTO `tb_picture` VALUES ('240', 'bathrooms', 'bathroom-Samari-Hill-Villa-4.jpg', '');
INSERT INTO `tb_picture` VALUES ('241', 'bathrooms', 'bathroom-Samari-Hill-Villa-6.JPG', '');
INSERT INTO `tb_picture` VALUES ('242', 'bathrooms', 'bathroom-Samari-Hill-Villa-5.JPG', '');
INSERT INTO `tb_picture` VALUES ('243', 'bathrooms', 'bathroom-Samari-Hill-Villa-7.JPG', '');
INSERT INTO `tb_picture` VALUES ('246', 'perfect_equipped_kitchen', 'kitchen-Samari-Hill-Villa-5.JPG', '');
INSERT INTO `tb_picture` VALUES ('247', 'gallery_thevilla', 'Samari-Hill-Villas-by-sunset-2.JPG', 'Samari-Hill-Villas-by-sunset-2_thumb.JPG');
INSERT INTO `tb_picture` VALUES ('248', 'gallery_thevilla', 'Samari-Hill-Villas-by-sunset-1.JPG', 'Samari-Hill-Villas-by-sunset-1_thumb.JPG');
INSERT INTO `tb_picture` VALUES ('249', 'gallery', 'about-us-Samari-Hill-Villas.jpg', '');
INSERT INTO `tb_picture` VALUES ('250', 'heavenly_bedrooms', 'bedroom-Samari-Hill-Villa-3.jpg', '');
INSERT INTO `tb_picture` VALUES ('251', 'heavenly_bedrooms', 'bedroom-Samari-Hill-Villa-4.jpg', '');
INSERT INTO `tb_picture` VALUES ('304', 'dream_gallery', 'BilderBau_1.jpg', 'BilderBau_1_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('305', 'dream_gallery', 'BilderBau_2.jpg', 'BilderBau_2_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('306', 'dream_gallery', 'BilderBau_3.jpg', 'BilderBau_3_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('307', 'dream_gallery', 'BilderBau_4.jpg', 'BilderBau_4_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('308', 'dream_gallery', 'BilderBau_5.jpg', 'BilderBau_5_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('309', 'dream_gallery', 'BilderBau_6.jpg', 'BilderBau_6_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('310', 'dream_gallery', 'BilderBau_7.jpg', 'BilderBau_7_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('311', 'dream_gallery', 'BilderBau_8.jpg', 'BilderBau_8_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('313', 'dream_gallery', 'BilderBau_10.jpg', 'BilderBau_10_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('343', 'dream_gallery', 'BilderBau_9.jpg', 'BilderBau_9_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('344', 'dream_gallery', 'BilderBau_12.jpg', 'BilderBau_12_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('345', 'dream_gallery', 'BilderBau_11.jpg', 'BilderBau_11_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('346', 'dream_gallery', 'BilderBau_13.jpg', 'BilderBau_13_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('347', 'dream_gallery', 'BilderBau_17.jpg', 'BilderBau_17_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('348', 'dream_gallery', 'BilderBau_16.jpg', 'BilderBau_16_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('349', 'dream_gallery', 'BilderBau_18.jpg', 'BilderBau_18_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('350', 'dream_gallery', 'BilderBau_19.jpg', 'BilderBau_19_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('351', 'dream_gallery', 'BilderBau_20.jpg', 'BilderBau_20_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('352', 'dream_gallery', 'BilderBau_14.jpg', 'BilderBau_14_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('353', 'dream_gallery', 'BilderBau_15.jpg', 'BilderBau_15_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('354', 'dream_gallery', 'BilderBau_21.jpg', 'BilderBau_21_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('355', 'dream_gallery', 'BilderBau_22.jpg', 'BilderBau_22_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('356', 'dream_gallery', 'BilderBau_23.jpg', 'BilderBau_23_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('357', 'dream_gallery', 'BilderBau_24.jpg', 'BilderBau_24_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('358', 'dream_gallery', 'BilderBau_25.jpg', 'BilderBau_25_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('359', 'dream_gallery', 'BilderBau_26.jpg', 'BilderBau_26_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('360', 'dream_gallery', 'BilderBau_28.jpg', 'BilderBau_28_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('361', 'dream_gallery', 'BilderBau_27.jpg', 'BilderBau_27_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('362', 'dream_gallery', 'BilderBau_29.jpg', 'BilderBau_29_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('363', 'dream_gallery', 'BilderBau_32.jpg', 'BilderBau_32_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('364', 'dream_gallery', 'BilderBau_30.jpg', 'BilderBau_30_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('365', 'dream_gallery', 'BilderBau_31.jpg', 'BilderBau_31_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('366', 'dream_gallery', 'BilderBau_33.jpg', 'BilderBau_33_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('367', 'dream_gallery', 'BilderBau_34.jpg', 'BilderBau_34_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('368', 'dream_gallery', 'BilderBau_35.jpg', 'BilderBau_35_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('369', 'dream_gallery', 'BilderBau_37.jpg', 'BilderBau_37_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('370', 'dream_gallery', 'BilderBau_36.jpg', 'BilderBau_36_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('371', 'dream_gallery', 'BilderBau_38.jpg', 'BilderBau_38_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('372', 'dream_gallery', 'BilderBau_40.jpg', 'BilderBau_40_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('373', 'dream_gallery', 'BilderBau_39.jpg', 'BilderBau_39_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('374', 'dream_gallery', 'BilderBau_41.jpg', 'BilderBau_41_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('375', 'dream_gallery', 'BilderBau_42.jpg', 'BilderBau_42_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('376', 'dream_gallery', 'BilderBau_43.jpg', 'BilderBau_43_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('377', 'dream_gallery', 'BilderBau_44.jpg', 'BilderBau_44_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('378', 'dream_gallery', 'BilderBau_45.jpg', 'BilderBau_45_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('379', 'dream_gallery', 'BilderBau_46.jpg', 'BilderBau_46_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('380', 'dream_gallery', 'BilderBau_47.jpg', 'BilderBau_47_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('381', 'dream_gallery', 'BilderBau_48.jpg', 'BilderBau_48_thumb.jpg');
INSERT INTO `tb_picture` VALUES ('382', 'gallery_thevilla', 'gallery-Samari-Hill-Villas-3.JPG', 'gallery-Samari-Hill-Villas-3_thumb.JPG');
INSERT INTO `tb_picture` VALUES ('383', 'gallery_thevilla', 'gallery-Samari-Hill-Villas-4.JPG', 'gallery-Samari-Hill-Villas-4_thumb.JPG');
INSERT INTO `tb_picture` VALUES ('384', 'gallery_thevilla', 'gallery-Samari-Hill-Villas-1.JPG', 'gallery-Samari-Hill-Villas-1_thumb.JPG');
INSERT INTO `tb_picture` VALUES ('385', 'gallery_thevilla', 'gallery-Samari-Hill-Villas-2.JPG', 'gallery-Samari-Hill-Villas-2_thumb.JPG');
INSERT INTO `tb_picture` VALUES ('386', 'gallery_thevilla', 'gallery-Samari-Hill-Villas-5.JPG', 'gallery-Samari-Hill-Villas-5_thumb.JPG');

-- ----------------------------
-- Table structure for tb_reservasi
-- ----------------------------
DROP TABLE IF EXISTS `tb_reservasi`;
CREATE TABLE `tb_reservasi` (
  `id_reservasi` int(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `guest` varchar(10) NOT NULL,
  `message` text NOT NULL,
  `status` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_reservasi`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tb_reservasi
-- ----------------------------
INSERT INTO `tb_reservasi` VALUES ('8', 'Marcel Bogusch', 'mbogus.17321@guest.booking.com', '+4917621211649', '2019-07-09', '2019-07-13', '1-2', 'Booking.com', 'booked', '2019-07-09 14:24:58', '2019-07-09 14:24:58');
INSERT INTO `tb_reservasi` VALUES ('9', 'Henry Choner', 'hchone.324289@guest.booking.com', '', '2019-07-27', '2019-07-31', '1-2', 'Booking.com', 'booked', '2019-07-09 14:29:06', '2019-07-09 14:29:06');
INSERT INTO `tb_reservasi` VALUES ('10', 'Roy Grundel', 'made.santiara@gmail.com', ' +61 430 201 029', '2019-08-09', '2019-08-23', '1-2', 'Airbnb', 'booked', '2019-07-13 13:53:10', '2019-07-13 13:53:10');
INSERT INTO `tb_reservasi` VALUES ('11', 'Marlis Aberegg', 'lisaberegg@hotmail.com', '', '2019-09-05', '2019-09-08', '1-2', 'Homepage', 'booked', '2019-07-13 13:57:10', '2019-07-13 13:59:55');
INSERT INTO `tb_reservasi` VALUES ('12', 'Roland Müri', 'rmueri@tech-ag.ch', '+41 79 343 51 41', '2019-12-26', '2020-01-07', '1-2', 'Homepage', 'booked', '2019-07-13 13:59:34', '2019-07-13 13:59:34');
INSERT INTO `tb_reservasi` VALUES ('13', 'Roger Rüfenacht', 'roger.ruefenacht@ggs.ch', '+41793504733', '2019-09-15', '2019-10-01', '3-4', 'Privat', 'booked', '2019-07-13 14:02:47', '2019-07-13 14:11:59');
INSERT INTO `tb_reservasi` VALUES ('14', 'René Vetterli', 'Rene.Vetterli@burkhalter.ch', '', '2019-10-16', '2019-10-19', '1-2', 'Homepage', 'booked', '2019-07-13 14:08:10', '2019-07-13 14:08:10');

-- ----------------------------
-- Table structure for tb_social_link
-- ----------------------------
DROP TABLE IF EXISTS `tb_social_link`;
CREATE TABLE `tb_social_link` (
  `id_social_link` int(9) NOT NULL AUTO_INCREMENT,
  `social_name` varchar(300) NOT NULL,
  `social_link` varchar(300) NOT NULL,
  `social_user_id` int(9) NOT NULL,
  `social_logo` varchar(50) NOT NULL,
  `social_desc` text NOT NULL,
  `published` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_social_link`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tb_social_link
-- ----------------------------
INSERT INTO `tb_social_link` VALUES ('1', 'Facebook', 'https://web.facebook.com/samarihillvillas/?modal=admin_todo_tour', '0', 'fa fa-facebook', '', 'checked', '2019-03-13 17:50:26', '2019-07-09 14:35:55');
INSERT INTO `tb_social_link` VALUES ('2', 'Instagram', 'https://www.instagram.com/', '0', 'fa fa-instagram', '', 'checked', '2019-03-13 17:50:26', '2019-03-13 19:27:38');
INSERT INTO `tb_social_link` VALUES ('3', 'Twitter', 'https://twitter.com/', '0', 'fa fa-twitter', '', 'checked', '2019-03-13 17:50:26', '2019-05-28 17:04:08');
INSERT INTO `tb_social_link` VALUES ('4', 'Youtube', 'https://youtu.be/5jcnJ5DL5HE', '0', 'fa fa-youtube', '', 'checked', '2019-03-13 17:50:26', '2019-07-09 14:35:55');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) NOT NULL,
  `email` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `type` varchar(100) NOT NULL,
  `remember_token` varchar(255) NOT NULL,
  `created_token` date NOT NULL,
  `image` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user_desc` text NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', 'Administrator Bawaslu Bangli', 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'admin', '5fff1b292a0804a0c79d748d59b49a', '2019-04-12', 'logo_bawaslu_bali1.png', '2019-04-12 12:49:50', '2019-08-05 10:11:13', '');
