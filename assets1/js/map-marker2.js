jQuery(function($) {
  var pop = $('.map2-popup');
  pop.click(function(e) {
    e.stopPropagation();
  });
  
  $('a.marker2').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    $(this).next('.map2-popup').toggleClass('open');
    $(this).parent().siblings().children('.map2-popup').removeClass('open');
  });
  
  $(document).click(function() {
    pop.removeClass('open');
  });
  
  pop.each(function() {
    var w = $(window).outerWidth(),
        edge = Math.round( ($(this).offset().left) + ($(this).outerWidth()) );
    if( w < edge ) {
      $(this).addClass('edge');
    }
  });
});

$(document).ready(function(){
      $('a.marker2').click(function(){
        $('a.marker2').removeClass("marker2-active");
        $(this).addClass("marker2-active");
    });
});