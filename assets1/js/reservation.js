$(document).ready(function(e) {
  $(".formreservation").submit(function(e) {
  		//var base_url = $('#base-value').data('base-url');
  		e.preventDefault();
        vardata = $(this).serialize();
        $('#gif').show();
		$.ajax({
			url: $(this).attr("action"),
			type: 'POST',
			data: vardata,
			beforeSend: function(){
				
			},
			success: function(data){
				$('#gif').hide();
				var json = JSON.parse(data);
				var ke   = "ke";
				if(json.status == 'error'){
					swal("Failed", json.alert, "warning");
					
					$("span.form-error:eq(0)").html(json.name).fadeIn("normal");
					$("span.form-error:eq(1)").html(json.email).fadeIn("normal");
					$("span.form-error:eq(2)").html(json.phone).fadeIn("normal");
					$("span.form-error:eq(3)").html(json.arrival).fadeIn("normal");
					$("span.form-error:eq(4)").html(json.departure).fadeIn("normal");
					$("span.form-error:eq(5)").html(json.message).fadeIn("normal");
					$("span.form-error:eq(6)").html(json.captcha).fadeIn("normal");
					
					setTimeout(function(){
						$("input, textarea").removeClass("error-border");
						
					}, 5000);
				}else if(json.status == 'success'){
					
					swal({
						title: "Success",
						text: json.alert,
						type: "success",
					}, function() {
						window.location.reload();
					});
				}
			}
		});
		return false;
  });
});

